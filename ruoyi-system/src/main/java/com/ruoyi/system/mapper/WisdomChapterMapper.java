package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WisdomChapter;

/**
 * 智慧元典章节Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface WisdomChapterMapper 
{
    /**
     * 查询智慧元典章节
     * 
     * @param id 智慧元典章节主键
     * @return 智慧元典章节
     */
    public WisdomChapter selectWisdomChapterById(Long id);

    /**
     * 查询智慧元典章节列表
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 智慧元典章节集合
     */
    public List<WisdomChapter> selectWisdomChapterList(WisdomChapter wisdomChapter);

    /**
     * 新增智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    public int insertWisdomChapter(WisdomChapter wisdomChapter);

    /**
     * 修改智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    public int updateWisdomChapter(WisdomChapter wisdomChapter);

    /**
     * 删除智慧元典章节
     * 
     * @param id 智慧元典章节主键
     * @return 结果
     */
    public int deleteWisdomChapterById(Long id);

    /**
     * 批量删除智慧元典章节
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWisdomChapterByIds(Long[] ids);
}
