package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PoetrySense;
import com.ruoyi.system.domain.PoetrySenseResp;
import org.apache.ibatis.annotations.Param;

/**
 * 诗词常识Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface PoetrySenseMapper 
{
    /**
     * 查询诗词常识
     * 
     * @param id 诗词常识主键
     * @return 诗词常识
     */
    public PoetrySense selectPoetrySenseById(Long id);

    /**
     * 查询诗词常识列表
     * 
     * @param poetrySense 诗词常识
     * @return 诗词常识集合
     */
    public List<PoetrySense> selectPoetrySenseList(PoetrySense poetrySense);

    /**
     * 新增诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    public int insertPoetrySense(PoetrySense poetrySense);

    /**
     * 修改诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    public int updatePoetrySense(PoetrySense poetrySense);

    /**
     * 删除诗词常识
     * 
     * @param id 诗词常识主键
     * @return 结果
     */
    public int deletePoetrySenseById(Long id);

    /**
     * 批量删除诗词常识
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePoetrySenseByIds(Long[] ids);

    List<PoetrySenseResp> selectPoetrySenseListAll(@Param("gradeId") Long gradeId,
                                                   @Param("module") Long module,
                                                   @Param("childModule") Long childModule);

    List<PoetrySenseResp> selectPoetrySenseListIsDynamic(@Param("gradeId") Long gradeId,
                                                         @Param("module") Long module,
                                                         @Param("childModule") Long childModule);
}
