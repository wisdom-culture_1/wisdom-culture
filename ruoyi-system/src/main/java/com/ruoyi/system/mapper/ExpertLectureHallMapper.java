package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ExpertLectureHall;

/**
 * 专家讲堂视频Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface ExpertLectureHallMapper 
{
    /**
     * 查询专家讲堂视频
     * 
     * @param id 专家讲堂视频主键
     * @return 专家讲堂视频
     */
    public ExpertLectureHall selectExpertLectureHallById(Long id);

    /**
     * 查询专家讲堂视频列表
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 专家讲堂视频集合
     */
    public List<ExpertLectureHall> selectExpertLectureHallList(ExpertLectureHall expertLectureHall);

    /**
     * 新增专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    public int insertExpertLectureHall(ExpertLectureHall expertLectureHall);

    /**
     * 修改专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    public int updateExpertLectureHall(ExpertLectureHall expertLectureHall);

    /**
     * 删除专家讲堂视频
     * 
     * @param id 专家讲堂视频主键
     * @return 结果
     */
    public int deleteExpertLectureHallById(Long id);

    /**
     * 批量删除专家讲堂视频
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExpertLectureHallByIds(Long[] ids);
}
