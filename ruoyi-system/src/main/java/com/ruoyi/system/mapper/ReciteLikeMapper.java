package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ReciteLike;

/**
 * 背诵打卡点赞Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface ReciteLikeMapper 
{
    /**
     * 查询背诵打卡点赞
     * 
     * @param id 背诵打卡点赞主键
     * @return 背诵打卡点赞
     */
    public ReciteLike selectReciteLikeById(Long id);

    /**
     * 查询背诵打卡点赞列表
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 背诵打卡点赞集合
     */
    public List<ReciteLike> selectReciteLikeList(ReciteLike reciteLike);

    /**
     * 新增背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    public int insertReciteLike(ReciteLike reciteLike);

    /**
     * 修改背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    public int updateReciteLike(ReciteLike reciteLike);

    /**
     * 删除背诵打卡点赞
     * 
     * @param id 背诵打卡点赞主键
     * @return 结果
     */
    public int deleteReciteLikeById(Long id);

    /**
     * 批量删除背诵打卡点赞
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReciteLikeByIds(Long[] ids);
}
