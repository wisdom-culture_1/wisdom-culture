package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MasterpiecesChaptersProcess;

/**
 * 章节试听进度Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface MasterpiecesChaptersProcessMapper 
{
    /**
     * 查询章节试听进度
     * 
     * @param id 章节试听进度主键
     * @return 章节试听进度
     */
    public MasterpiecesChaptersProcess selectMasterpiecesChaptersProcessById(Long id);

    /**
     * 查询章节试听进度列表
     * 
     * @param masterpiecesChaptersProcess 章节试听进度
     * @return 章节试听进度集合
     */
    public List<MasterpiecesChaptersProcess> selectMasterpiecesChaptersProcessList(MasterpiecesChaptersProcess masterpiecesChaptersProcess);

    /**
     * 新增章节试听进度
     * 
     * @param masterpiecesChaptersProcess 章节试听进度
     * @return 结果
     */
    public int insertMasterpiecesChaptersProcess(MasterpiecesChaptersProcess masterpiecesChaptersProcess);

    /**
     * 修改章节试听进度
     * 
     * @param masterpiecesChaptersProcess 章节试听进度
     * @return 结果
     */
    public int updateMasterpiecesChaptersProcess(MasterpiecesChaptersProcess masterpiecesChaptersProcess);

    /**
     * 删除章节试听进度
     * 
     * @param id 章节试听进度主键
     * @return 结果
     */
    public int deleteMasterpiecesChaptersProcessById(Long id);

    /**
     * 批量删除章节试听进度
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMasterpiecesChaptersProcessByIds(Long[] ids);
}
