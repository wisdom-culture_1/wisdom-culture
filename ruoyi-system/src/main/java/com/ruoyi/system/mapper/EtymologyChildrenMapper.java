package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.EtymologyChildren;

/**
 * 字源示例Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface EtymologyChildrenMapper 
{
    /**
     * 查询字源示例
     * 
     * @param id 字源示例主键
     * @return 字源示例
     */
    public EtymologyChildren selectEtymologyChildrenById(Long id);

    /**
     * 查询字源示例列表
     * 
     * @param etymologyChildren 字源示例
     * @return 字源示例集合
     */
    public List<EtymologyChildren> selectEtymologyChildrenList(EtymologyChildren etymologyChildren);

    /**
     * 新增字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    public int insertEtymologyChildren(EtymologyChildren etymologyChildren);

    /**
     * 修改字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    public int updateEtymologyChildren(EtymologyChildren etymologyChildren);

    /**
     * 删除字源示例
     * 
     * @param id 字源示例主键
     * @return 结果
     */
    public int deleteEtymologyChildrenById(Long id);

    /**
     * 批量删除字源示例
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEtymologyChildrenByIds(Long[] ids);
}
