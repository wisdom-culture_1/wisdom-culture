package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.IdiomStory;
import com.ruoyi.system.domain.IdiomStoryResp;
import org.apache.ibatis.annotations.Param;

/**
 * 成语典故Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface IdiomStoryMapper 
{
    /**
     * 查询成语典故
     * 
     * @param id 成语典故主键
     * @return 成语典故
     */
    public IdiomStory selectIdiomStoryById(Long id);

    /**
     * 查询成语典故列表
     * 
     * @param idiomStory 成语典故
     * @return 成语典故集合
     */
    public List<IdiomStory> selectIdiomStoryList(IdiomStory idiomStory);

    /**
     * 新增成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    public int insertIdiomStory(IdiomStory idiomStory);

    /**
     * 修改成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    public int updateIdiomStory(IdiomStory idiomStory);

    /**
     * 删除成语典故
     * 
     * @param id 成语典故主键
     * @return 结果
     */
    public int deleteIdiomStoryById(Long id);

    /**
     * 批量删除成语典故
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIdiomStoryByIds(Long[] ids);

    public List<IdiomStory> selectIdiomStoryListIsDynamic(@Param("gradeId") Long gradeId,
                                                          @Param("module") Long module,
                                                          @Param("childModule") Long childModule,
                                                          @Param("detailName")String detailName);

    List<IdiomStoryResp> selectIdiomStoryListAll(@Param("gradeId") Long gradeId,
                                                 @Param("module") Long module,
                                                 @Param("childModule") Long childModule,
                                                 @Param("detailName")String detailName);
}
