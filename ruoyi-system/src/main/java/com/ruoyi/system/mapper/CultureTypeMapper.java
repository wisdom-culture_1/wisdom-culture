package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.CultureType;

/**
 * 中华文化分类Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface CultureTypeMapper 
{
    /**
     * 查询中华文化分类
     * 
     * @param id 中华文化分类主键
     * @return 中华文化分类
     */
    public CultureType selectCultureTypeById(Long id);

    /**
     * 查询中华文化分类列表
     * 
     * @param cultureType 中华文化分类
     * @return 中华文化分类集合
     */
    public List<CultureType> selectCultureTypeList(CultureType cultureType);

    /**
     * 新增中华文化分类
     * 
     * @param cultureType 中华文化分类
     * @return 结果
     */
    public int insertCultureType(CultureType cultureType);

    /**
     * 修改中华文化分类
     * 
     * @param cultureType 中华文化分类
     * @return 结果
     */
    public int updateCultureType(CultureType cultureType);

    /**
     * 删除中华文化分类
     * 
     * @param id 中华文化分类主键
     * @return 结果
     */
    public int deleteCultureTypeById(Long id);

    /**
     * 批量删除中华文化分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCultureTypeByIds(Long[] ids);
}
