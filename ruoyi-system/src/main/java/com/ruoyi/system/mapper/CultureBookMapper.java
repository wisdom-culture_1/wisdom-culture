package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.CultureBook;
import com.ruoyi.system.domain.CultureBookResp;
import org.apache.ibatis.annotations.Param;

/**
 * 中华文化书籍Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface CultureBookMapper 
{
    /**
     * 查询中华文化书籍
     * 
     * @param id 中华文化书籍主键
     * @return 中华文化书籍
     */
    public CultureBook selectCultureBookById(Long id);

    /**
     * 查询中华文化书籍列表
     * 
     * @param cultureBook 中华文化书籍
     * @return 中华文化书籍集合
     */
    public List<CultureBook> selectCultureBookList(CultureBook cultureBook);

    /**
     * 新增中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    public int insertCultureBook(CultureBook cultureBook);

    /**
     * 修改中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    public int updateCultureBook(CultureBook cultureBook);

    /**
     * 删除中华文化书籍
     * 
     * @param id 中华文化书籍主键
     * @return 结果
     */
    public int deleteCultureBookById(Long id);

    /**
     * 批量删除中华文化书籍
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCultureBookByIds(Long[] ids);

    List<CultureBookResp> selectcultureBookListAll(@Param("gradeId") Long gradeId,
                                                   @Param("module") Long module,
                                                   @Param("childModule") Long childModule);

    List<CultureBookResp> selectcultureBookListIsDynamic(@Param("gradeId") Long gradeId,
                                                   @Param("module") Long module,
                                                   @Param("childModule") Long childModule);
}
