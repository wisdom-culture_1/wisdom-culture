package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ExcerptType;

/**
 * 金句摘抄分类Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface ExcerptTypeMapper 
{
    /**
     * 查询金句摘抄分类
     * 
     * @param id 金句摘抄分类主键
     * @return 金句摘抄分类
     */
    public ExcerptType selectExcerptTypeById(Long id);

    /**
     * 查询金句摘抄分类列表
     * 
     * @param excerptType 金句摘抄分类
     * @return 金句摘抄分类集合
     */
    public List<ExcerptType> selectExcerptTypeList(ExcerptType excerptType);

    /**
     * 新增金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    public int insertExcerptType(ExcerptType excerptType);

    /**
     * 修改金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    public int updateExcerptType(ExcerptType excerptType);

    /**
     * 删除金句摘抄分类
     * 
     * @param id 金句摘抄分类主键
     * @return 结果
     */
    public int deleteExcerptTypeById(Long id);

    /**
     * 批量删除金句摘抄分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExcerptTypeByIds(Long[] ids);
}
