package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Excerpt;
import com.ruoyi.system.domain.ExcerptResp;
import org.apache.ibatis.annotations.Param;

/**
 * 金句摘抄Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface ExcerptMapper 
{
    /**
     * 查询金句摘抄
     * 
     * @param id 金句摘抄主键
     * @return 金句摘抄
     */
    public Excerpt selectExcerptById(Long id);

    /**
     * 查询金句摘抄列表
     * 
     * @param excerpt 金句摘抄
     * @return 金句摘抄集合
     */
    public List<Excerpt> selectExcerptList(Excerpt excerpt);

    /**
     * 新增金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    public int insertExcerpt(Excerpt excerpt);

    /**
     * 修改金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    public int updateExcerpt(Excerpt excerpt);

    /**
     * 删除金句摘抄
     * 
     * @param id 金句摘抄主键
     * @return 结果
     */
    public int deleteExcerptById(Long id);

    /**
     * 批量删除金句摘抄
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExcerptByIds(Long[] ids);

    List<ExcerptResp> selectExcerptListAll(@Param("gradeId") Long gradeId,
                                           @Param("module") Long module,
                                           @Param("childModule") Long childModule);

    List<ExcerptResp> selectExcerptListIsDynamic(@Param("gradeId") Long gradeId,
                                           @Param("module") Long module,
                                           @Param("childModule") Long childModule);
}
