package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DictionaryChildren;

/**
 * 子字典表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-25
 */
public interface DictionaryChildrenMapper 
{
    /**
     * 查询子字典表
     * 
     * @param id 子字典表主键
     * @return 子字典表
     */
    public DictionaryChildren selectDictionaryChildrenById(Integer id);

    /**
     * 查询子字典表列表
     * 
     * @param dictionaryChildren 子字典表
     * @return 子字典表集合
     */
    public List<DictionaryChildren> selectDictionaryChildrenList(DictionaryChildren dictionaryChildren);

    /**
     * 新增子字典表
     * 
     * @param dictionaryChildren 子字典表
     * @return 结果
     */
    public int insertDictionaryChildren(DictionaryChildren dictionaryChildren);

    /**
     * 修改子字典表
     * 
     * @param dictionaryChildren 子字典表
     * @return 结果
     */
    public int updateDictionaryChildren(DictionaryChildren dictionaryChildren);

    /**
     * 删除子字典表
     * 
     * @param id 子字典表主键
     * @return 结果
     */
    public int deleteDictionaryChildrenById(Integer id);

    /**
     * 批量删除子字典表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDictionaryChildrenByIds(Integer[] ids);
}
