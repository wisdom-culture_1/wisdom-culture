package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Etymology;
import com.ruoyi.system.domain.EtymologyResp;
import org.apache.ibatis.annotations.Param;

/**
 * 字源Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface EtymologyMapper 
{
    /**
     * 查询字源
     * 
     * @param id 字源主键
     * @return 字源
     */
    public Etymology selectEtymologyById(Long id);

    /**
     * 查询字源列表
     * 
     * @param etymology 字源
     * @return 字源集合
     */
    public List<Etymology> selectEtymologyList(Etymology etymology);

    /**
     * 新增字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    public int insertEtymology(Etymology etymology);

    /**
     * 修改字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    public int updateEtymology(Etymology etymology);

    /**
     * 删除字源
     * 
     * @param id 字源主键
     * @return 结果
     */
    public int deleteEtymologyById(Long id);

    /**
     * 批量删除字源
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEtymologyByIds(Long[] ids);

    List<Etymology> selectEtymologyListIsDynamic(@Param("gradeId") Long gradeId,
                                                 @Param("module") Long module,
                                                 @Param("childModule") Long childModule,
                                                 @Param("detailName")String detailName);

    List<EtymologyResp> selectEtymologyListAll(@Param("gradeId") Long gradeId,
                                                     @Param("module") Long module,
                                                     @Param("childModule") Long childModule,
                                                     @Param("detailName")String detailName);
}
