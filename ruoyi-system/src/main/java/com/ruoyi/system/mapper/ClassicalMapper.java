package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Classical;
import com.ruoyi.system.domain.ClassicalResp;
import org.apache.ibatis.annotations.Param;

/**
 * 文言文Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface ClassicalMapper 
{
    /**
     * 查询文言文
     * 
     * @param id 文言文主键
     * @return 文言文
     */
    public Classical selectClassicalById(Long id);

    /**
     * 查询文言文列表
     * 
     * @param classical 文言文
     * @return 文言文集合
     */
    public List<Classical> selectClassicalList(Classical classical);

    /**
     * 新增文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    public int insertClassical(Classical classical);

    /**
     * 修改文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    public int updateClassical(Classical classical);

    /**
     * 删除文言文
     * 
     * @param id 文言文主键
     * @return 结果
     */
    public int deleteClassicalById(Long id);

    /**
     * 批量删除文言文
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteClassicalByIds(Long[] ids);

    List<ClassicalResp> selectClassicaListAll(@Param("gradeId") Long gradeId,
                                                    @Param("module") Long module,
                                                    @Param("childModule") Long childModule,
                                                    @Param("type") Long type,
                                                    @Param("detailName")String detailName);
    List<ClassicalResp> selectClassicaListIsDynamic(@Param("gradeId") Long gradeId,
                                                    @Param("module") Long module,
                                                    @Param("childModule") Long childModule,
                                                    @Param("type") Long type,
                                                    @Param("detailName")String detailName);
}
