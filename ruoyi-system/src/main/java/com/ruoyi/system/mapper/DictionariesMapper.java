package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Dictionaries;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-25
 */
public interface DictionariesMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Dictionaries selectDictionariesById(Integer id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dictionaries 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Dictionaries> selectDictionariesList(Dictionaries dictionaries);

    /**
     * 新增【请填写功能名称】
     * 
     * @param dictionaries 【请填写功能名称】
     * @return 结果
     */
    public int insertDictionaries(Dictionaries dictionaries);

    /**
     * 修改【请填写功能名称】
     * 
     * @param dictionaries 【请填写功能名称】
     * @return 结果
     */
    public int updateDictionaries(Dictionaries dictionaries);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDictionariesById(Integer id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDictionariesByIds(Integer[] ids);
}
