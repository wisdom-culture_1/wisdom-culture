package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.IdiomStoryResp;
import com.ruoyi.system.domain.OriginOfChineseCharacters;
import com.ruoyi.system.domain.OriginOfChineseCharactersResp;
import org.apache.ibatis.annotations.Param;

/**
 * 汉字起源Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface OriginOfChineseCharactersMapper 
{
    /**
     * 查询汉字起源
     * 
     * @param id 汉字起源主键
     * @return 汉字起源
     */
    public OriginOfChineseCharacters selectOriginOfChineseCharactersById(Long id);

    /**
     * 查询汉字起源列表
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 汉字起源集合
     */
    public List<OriginOfChineseCharacters> selectOriginOfChineseCharactersList(OriginOfChineseCharacters originOfChineseCharacters);

    /**
     * 新增汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    public int insertOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters);

    /**
     * 修改汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    public int updateOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters);

    /**
     * 删除汉字起源
     * 
     * @param id 汉字起源主键
     * @return 结果
     */
    public int deleteOriginOfChineseCharactersById(Long id);

    /**
     * 批量删除汉字起源
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOriginOfChineseCharactersByIds(Long[] ids);

    List<OriginOfChineseCharactersResp> selectOriginOfChineseCharactersListAll(@Param("gradeId") Long gradeId,
                                                                               @Param("module") Long module,
                                                                               @Param("childModule") Long childModule);

    List<OriginOfChineseCharacters> selectOriginOfChineseCharactersListIsDynamic(@Param("gradeId") Long gradeId,
                                                                                 @Param("module") Long module,
                                                                                 @Param("childModule") Long childModule);
}
