package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PoetryOde;
import com.ruoyi.system.domain.PoetryOdeResp;
import org.apache.ibatis.annotations.Param;

/**
 * 诗词歌赋Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface PoetryOdeMapper 
{
    /**
     * 查询诗词歌赋
     * 
     * @param id 诗词歌赋主键
     * @return 诗词歌赋
     */
    public PoetryOde selectPoetryOdeById(Long id);

    /**
     * 查询诗词歌赋列表
     * 
     * @param poetryOde 诗词歌赋
     * @return 诗词歌赋集合
     */
    public List<PoetryOde> selectPoetryOdeList(PoetryOde poetryOde);

    /**
     * 新增诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    public int insertPoetryOde(PoetryOde poetryOde);

    /**
     * 修改诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    public int updatePoetryOde(PoetryOde poetryOde);

    /**
     * 删除诗词歌赋
     * 
     * @param id 诗词歌赋主键
     * @return 结果
     */
    public int deletePoetryOdeById(Long id);

    /**
     * 批量删除诗词歌赋
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePoetryOdeByIds(Long[] ids);

    List<PoetryOdeResp> selectPoetryOdeListAll(@Param("gradeId") Long gradeId,
                                               @Param("module") Long module,
                                               @Param("childModule") Long childModule,
                                               @Param("detailName")String detailName);

    List<PoetryOdeResp> selectPoetryOdeListIsDynamic(@Param("gradeId") Long gradeId,
                                                     @Param("module") Long module,
                                                     @Param("childModule") Long childModule,
                                                     @Param("detailName")String detailName);
}
