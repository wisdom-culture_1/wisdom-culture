package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PoetryOdeExtend;

/**
 * 诗词歌赋扩展Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface PoetryOdeExtendMapper 
{
    /**
     * 查询诗词歌赋扩展
     * 
     * @param id 诗词歌赋扩展主键
     * @return 诗词歌赋扩展
     */
    public PoetryOdeExtend selectPoetryOdeExtendById(Long id);

    /**
     * 查询诗词歌赋扩展列表
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 诗词歌赋扩展集合
     */
    public List<PoetryOdeExtend> selectPoetryOdeExtendList(PoetryOdeExtend poetryOdeExtend);

    /**
     * 新增诗词歌赋扩展
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 结果
     */
    public int insertPoetryOdeExtend(PoetryOdeExtend poetryOdeExtend);

    /**
     * 修改诗词歌赋扩展
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 结果
     */
    public int updatePoetryOdeExtend(PoetryOdeExtend poetryOdeExtend);

    /**
     * 删除诗词歌赋扩展
     * 
     * @param id 诗词歌赋扩展主键
     * @return 结果
     */
    public int deletePoetryOdeExtendById(Long id);

    /**
     * 批量删除诗词歌赋扩展
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePoetryOdeExtendByIds(Long[] ids);
}
