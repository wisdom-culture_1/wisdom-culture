package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Recite;

/**
 * 背诵打卡Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface ReciteMapper 
{
    /**
     * 查询背诵打卡
     * 
     * @param id 背诵打卡主键
     * @return 背诵打卡
     */
    public Recite selectReciteById(Long id);

    /**
     * 查询背诵打卡列表
     * 
     * @param recite 背诵打卡
     * @return 背诵打卡集合
     */
    public List<Recite> selectReciteList(Recite recite);

    /**
     * 新增背诵打卡
     * 
     * @param recite 背诵打卡
     * @return 结果
     */
    public int insertRecite(Recite recite);

    /**
     * 修改背诵打卡
     * 
     * @param recite 背诵打卡
     * @return 结果
     */
    public int updateRecite(Recite recite);

    /**
     * 删除背诵打卡
     * 
     * @param id 背诵打卡主键
     * @return 结果
     */
    public int deleteReciteById(Long id);

    /**
     * 批量删除背诵打卡
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReciteByIds(Long[] ids);
}
