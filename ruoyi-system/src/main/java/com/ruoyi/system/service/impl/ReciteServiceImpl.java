package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.Recite;
import com.ruoyi.system.mapper.ReciteMapper;
import com.ruoyi.system.service.IReciteService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ReciteMapper;

import com.ruoyi.system.service.IReciteService;

/**
 * 背诵打卡Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class ReciteServiceImpl implements IReciteService
{
    @Autowired
    private ReciteMapper reciteMapper;

    /**
     * 查询背诵打卡
     * 
     * @param id 背诵打卡主键
     * @return 背诵打卡
     */
    @Override
    public Recite selectReciteById(Long id)
    {
        return reciteMapper.selectReciteById(id);
    }

    /**
     * 查询背诵打卡列表
     * 
     * @param recite 背诵打卡
     * @return 背诵打卡
     */
    @Override
    public List<Recite> selectReciteList(Recite recite)
    {
//        recite.setUserId(SecurityUtils.getUserId());
        return reciteMapper.selectReciteList(recite);
    }

    /**
     * 新增背诵打卡
     * 
     * @param recite 背诵打卡
     * @return 结果
     */
    @Override
    public int insertRecite(Recite recite)
    {
        return reciteMapper.insertRecite(recite);
    }

    /**
     * 修改背诵打卡
     * 
     * @param recite 背诵打卡
     * @return 结果
     */
    @Override
    public int updateRecite(Recite recite)
    {
        return reciteMapper.updateRecite(recite);
    }

    /**
     * 批量删除背诵打卡
     * 
     * @param ids 需要删除的背诵打卡主键
     * @return 结果
     */
    @Override
    public int deleteReciteByIds(Long[] ids)
    {
        return reciteMapper.deleteReciteByIds(ids);
    }

    /**
     * 删除背诵打卡信息
     * 
     * @param id 背诵打卡主键
     * @return 结果
     */
    @Override
    public int deleteReciteById(Long id)
    {
        return reciteMapper.deleteReciteById(id);
    }
}
