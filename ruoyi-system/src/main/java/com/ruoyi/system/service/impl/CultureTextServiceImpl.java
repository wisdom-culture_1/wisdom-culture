package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CultureTextMapper;
import com.ruoyi.system.domain.CultureText;
import com.ruoyi.system.service.ICultureTextService;

/**
 * 中华文化内容Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class CultureTextServiceImpl implements ICultureTextService 
{
    @Autowired
    private CultureTextMapper cultureTextMapper;

    /**
     * 查询中华文化内容
     * 
     * @param id 中华文化内容主键
     * @return 中华文化内容
     */
    @Override
    public CultureText selectCultureTextById(Long id)
    {
        return cultureTextMapper.selectCultureTextById(id);
    }

    /**
     * 查询中华文化内容列表
     * 
     * @param cultureText 中华文化内容
     * @return 中华文化内容
     */
    @Override
    public List<CultureText> selectCultureTextList(CultureText cultureText)
    {
        return cultureTextMapper.selectCultureTextList(cultureText);
    }

    /**
     * 新增中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    @Override
    public int insertCultureText(CultureText cultureText)
    {
        return cultureTextMapper.insertCultureText(cultureText);
    }

    /**
     * 修改中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    @Override
    public int updateCultureText(CultureText cultureText)
    {
        return cultureTextMapper.updateCultureText(cultureText);
    }

    /**
     * 批量删除中华文化内容
     * 
     * @param ids 需要删除的中华文化内容主键
     * @return 结果
     */
    @Override
    public int deleteCultureTextByIds(Long[] ids)
    {
        return cultureTextMapper.deleteCultureTextByIds(ids);
    }

    /**
     * 删除中华文化内容信息
     * 
     * @param id 中华文化内容主键
     * @return 结果
     */
    @Override
    public int deleteCultureTextById(Long id)
    {
        return cultureTextMapper.deleteCultureTextById(id);
    }
}
