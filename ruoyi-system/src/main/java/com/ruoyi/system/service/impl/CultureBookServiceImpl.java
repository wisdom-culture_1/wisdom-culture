package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CultureBookMapper;
import com.ruoyi.system.domain.CultureBook;
import com.ruoyi.system.service.ICultureBookService;

/**
 * 中华文化书籍Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class CultureBookServiceImpl implements ICultureBookService 
{
    @Autowired
    private CultureBookMapper cultureBookMapper;

    /**
     * 查询中华文化书籍
     * 
     * @param id 中华文化书籍主键
     * @return 中华文化书籍
     */
    @Override
    public CultureBook selectCultureBookById(Long id)
    {
        return cultureBookMapper.selectCultureBookById(id);
    }

    /**
     * 查询中华文化书籍列表
     * 
     * @param cultureBook 中华文化书籍
     * @return 中华文化书籍
     */
    @Override
    public List<CultureBook> selectCultureBookList(CultureBook cultureBook)
    {
        return cultureBookMapper.selectCultureBookList(cultureBook);
    }

    /**
     * 新增中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    @Override
    public int insertCultureBook(CultureBook cultureBook)
    {
        return cultureBookMapper.insertCultureBook(cultureBook);
    }

    /**
     * 修改中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    @Override
    public int updateCultureBook(CultureBook cultureBook)
    {
        return cultureBookMapper.updateCultureBook(cultureBook);
    }

    /**
     * 批量删除中华文化书籍
     * 
     * @param ids 需要删除的中华文化书籍主键
     * @return 结果
     */
    @Override
    public int deleteCultureBookByIds(Long[] ids)
    {
        return cultureBookMapper.deleteCultureBookByIds(ids);
    }

    /**
     * 删除中华文化书籍信息
     * 
     * @param id 中华文化书籍主键
     * @return 结果
     */
    @Override
    public int deleteCultureBookById(Long id)
    {
        return cultureBookMapper.deleteCultureBookById(id);
    }
}
