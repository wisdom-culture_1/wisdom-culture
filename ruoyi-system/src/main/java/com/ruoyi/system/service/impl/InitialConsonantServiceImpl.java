package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.InitialConsonantMapper;
import com.ruoyi.system.domain.InitialConsonant;
import com.ruoyi.system.service.IInitialConsonantService;

/**
 * 声母 韵母Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class InitialConsonantServiceImpl implements IInitialConsonantService 
{
    @Autowired
    private InitialConsonantMapper initialConsonantMapper;

    /**
     * 查询声母 韵母
     * 
     * @param id 声母 韵母主键
     * @return 声母 韵母
     */
    @Override
    public InitialConsonant selectInitialConsonantById(Long id)
    {
        return initialConsonantMapper.selectInitialConsonantById(id);
    }

    /**
     * 查询声母 韵母列表
     * 
     * @param initialConsonant 声母 韵母
     * @return 声母 韵母
     */
    @Override
    public List<InitialConsonant> selectInitialConsonantList(InitialConsonant initialConsonant)
    {
        return initialConsonantMapper.selectInitialConsonantList(initialConsonant);
    }

    /**
     * 新增声母 韵母
     * 
     * @param initialConsonant 声母 韵母
     * @return 结果
     */
    @Override
    public int insertInitialConsonant(InitialConsonant initialConsonant)
    {
        return initialConsonantMapper.insertInitialConsonant(initialConsonant);
    }

    /**
     * 修改声母 韵母
     * 
     * @param initialConsonant 声母 韵母
     * @return 结果
     */
    @Override
    public int updateInitialConsonant(InitialConsonant initialConsonant)
    {
        return initialConsonantMapper.updateInitialConsonant(initialConsonant);
    }

    /**
     * 批量删除声母 韵母
     * 
     * @param ids 需要删除的声母 韵母主键
     * @return 结果
     */
    @Override
    public int deleteInitialConsonantByIds(Long[] ids)
    {
        return initialConsonantMapper.deleteInitialConsonantByIds(ids);
    }

    /**
     * 删除声母 韵母信息
     * 
     * @param id 声母 韵母主键
     * @return 结果
     */
    @Override
    public int deleteInitialConsonantById(Long id)
    {
        return initialConsonantMapper.deleteInitialConsonantById(id);
    }
}
