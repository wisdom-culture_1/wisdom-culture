package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExpertLectureHallChapterMapper;
import com.ruoyi.system.domain.ExpertLectureHallChapter;
import com.ruoyi.system.service.IExpertLectureHallChapterService;

/**
 * 专家讲堂章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ExpertLectureHallChapterServiceImpl implements IExpertLectureHallChapterService 
{
    @Autowired
    private ExpertLectureHallChapterMapper expertLectureHallChapterMapper;

    /**
     * 查询专家讲堂章节
     * 
     * @param id 专家讲堂章节主键
     * @return 专家讲堂章节
     */
    @Override
    public ExpertLectureHallChapter selectExpertLectureHallChapterById(Long id)
    {
        return expertLectureHallChapterMapper.selectExpertLectureHallChapterById(id);
    }

    /**
     * 查询专家讲堂章节列表
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 专家讲堂章节
     */
    @Override
    public List<ExpertLectureHallChapter> selectExpertLectureHallChapterList(ExpertLectureHallChapter expertLectureHallChapter)
    {
        return expertLectureHallChapterMapper.selectExpertLectureHallChapterList(expertLectureHallChapter);
    }

    /**
     * 新增专家讲堂章节
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 结果
     */
    @Override
    public int insertExpertLectureHallChapter(ExpertLectureHallChapter expertLectureHallChapter)
    {
        return expertLectureHallChapterMapper.insertExpertLectureHallChapter(expertLectureHallChapter);
    }

    /**
     * 修改专家讲堂章节
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 结果
     */
    @Override
    public int updateExpertLectureHallChapter(ExpertLectureHallChapter expertLectureHallChapter)
    {
        return expertLectureHallChapterMapper.updateExpertLectureHallChapter(expertLectureHallChapter);
    }

    /**
     * 批量删除专家讲堂章节
     * 
     * @param ids 需要删除的专家讲堂章节主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallChapterByIds(Long[] ids)
    {
        return expertLectureHallChapterMapper.deleteExpertLectureHallChapterByIds(ids);
    }

    /**
     * 删除专家讲堂章节信息
     * 
     * @param id 专家讲堂章节主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallChapterById(Long id)
    {
        return expertLectureHallChapterMapper.deleteExpertLectureHallChapterById(id);
    }
}
