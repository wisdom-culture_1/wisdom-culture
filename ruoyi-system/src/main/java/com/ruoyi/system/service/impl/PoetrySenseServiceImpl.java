package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PoetrySenseMapper;
import com.ruoyi.system.domain.PoetrySense;
import com.ruoyi.system.service.IPoetrySenseService;

/**
 * 诗词常识Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class PoetrySenseServiceImpl implements IPoetrySenseService 
{
    @Autowired
    private PoetrySenseMapper poetrySenseMapper;

    /**
     * 查询诗词常识
     * 
     * @param id 诗词常识主键
     * @return 诗词常识
     */
    @Override
    public PoetrySense selectPoetrySenseById(Long id)
    {
        return poetrySenseMapper.selectPoetrySenseById(id);
    }

    /**
     * 查询诗词常识列表
     * 
     * @param poetrySense 诗词常识
     * @return 诗词常识
     */
    @Override
    public List<PoetrySense> selectPoetrySenseList(PoetrySense poetrySense)
    {
        return poetrySenseMapper.selectPoetrySenseList(poetrySense);
    }

    /**
     * 新增诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    @Override
    public int insertPoetrySense(PoetrySense poetrySense)
    {
        return poetrySenseMapper.insertPoetrySense(poetrySense);
    }

    /**
     * 修改诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    @Override
    public int updatePoetrySense(PoetrySense poetrySense)
    {
        return poetrySenseMapper.updatePoetrySense(poetrySense);
    }

    /**
     * 批量删除诗词常识
     * 
     * @param ids 需要删除的诗词常识主键
     * @return 结果
     */
    @Override
    public int deletePoetrySenseByIds(Long[] ids)
    {
        return poetrySenseMapper.deletePoetrySenseByIds(ids);
    }

    /**
     * 删除诗词常识信息
     * 
     * @param id 诗词常识主键
     * @return 结果
     */
    @Override
    public int deletePoetrySenseById(Long id)
    {
        return poetrySenseMapper.deletePoetrySenseById(id);
    }
}
