package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ChallengeQuestions;

/**
 * 其他挑战（成语诗词....）Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IChallengeQuestionsService 
{
    /**
     * 查询其他挑战（成语诗词....）
     * 
     * @param id 其他挑战（成语诗词....）主键
     * @return 其他挑战（成语诗词....）
     */
    public ChallengeQuestions selectChallengeQuestionsById(Long id);

    /**
     * 查询其他挑战（成语诗词....）列表
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 其他挑战（成语诗词....）集合
     */
    public List<ChallengeQuestions> selectChallengeQuestionsList(ChallengeQuestions challengeQuestions);

    /**
     * 新增其他挑战（成语诗词....）
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 结果
     */
    public int insertChallengeQuestions(ChallengeQuestions challengeQuestions);

    /**
     * 修改其他挑战（成语诗词....）
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 结果
     */
    public int updateChallengeQuestions(ChallengeQuestions challengeQuestions);

    /**
     * 批量删除其他挑战（成语诗词....）
     * 
     * @param ids 需要删除的其他挑战（成语诗词....）主键集合
     * @return 结果
     */
    public int deleteChallengeQuestionsByIds(Long[] ids);

    /**
     * 删除其他挑战（成语诗词....）信息
     * 
     * @param id 其他挑战（成语诗词....）主键
     * @return 结果
     */
    public int deleteChallengeQuestionsById(Long id);
}
