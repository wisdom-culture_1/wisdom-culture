package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ClassicalMapper;
import com.ruoyi.system.domain.Classical;
import com.ruoyi.system.service.IClassicalService;

/**
 * 文言文Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ClassicalServiceImpl implements IClassicalService 
{
    @Autowired
    private ClassicalMapper classicalMapper;

    /**
     * 查询文言文
     * 
     * @param id 文言文主键
     * @return 文言文
     */
    @Override
    public Classical selectClassicalById(Long id)
    {
        return classicalMapper.selectClassicalById(id);
    }

    /**
     * 查询文言文列表
     * 
     * @param classical 文言文
     * @return 文言文
     */
    @Override
    public List<Classical> selectClassicalList(Classical classical)
    {
        return classicalMapper.selectClassicalList(classical);
    }

    /**
     * 新增文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    @Override
    public int insertClassical(Classical classical)
    {
        return classicalMapper.insertClassical(classical);
    }

    /**
     * 修改文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    @Override
    public int updateClassical(Classical classical)
    {
        return classicalMapper.updateClassical(classical);
    }

    /**
     * 批量删除文言文
     * 
     * @param ids 需要删除的文言文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalByIds(Long[] ids)
    {
        return classicalMapper.deleteClassicalByIds(ids);
    }

    /**
     * 删除文言文信息
     * 
     * @param id 文言文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalById(Long id)
    {
        return classicalMapper.deleteClassicalById(id);
    }
}
