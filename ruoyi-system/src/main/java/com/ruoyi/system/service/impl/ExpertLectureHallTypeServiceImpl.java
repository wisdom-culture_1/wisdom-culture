package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExpertLectureHallTypeMapper;
import com.ruoyi.system.domain.ExpertLectureHallType;
import com.ruoyi.system.service.IExpertLectureHallTypeService;

/**
 * 专家讲堂分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ExpertLectureHallTypeServiceImpl implements IExpertLectureHallTypeService 
{
    @Autowired
    private ExpertLectureHallTypeMapper expertLectureHallTypeMapper;

    /**
     * 查询专家讲堂分类
     * 
     * @param id 专家讲堂分类主键
     * @return 专家讲堂分类
     */
    @Override
    public ExpertLectureHallType selectExpertLectureHallTypeById(Long id)
    {
        return expertLectureHallTypeMapper.selectExpertLectureHallTypeById(id);
    }

    /**
     * 查询专家讲堂分类列表
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 专家讲堂分类
     */
    @Override
    public List<ExpertLectureHallType> selectExpertLectureHallTypeList(ExpertLectureHallType expertLectureHallType)
    {
        return expertLectureHallTypeMapper.selectExpertLectureHallTypeList(expertLectureHallType);
    }

    /**
     * 新增专家讲堂分类
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 结果
     */
    @Override
    public int insertExpertLectureHallType(ExpertLectureHallType expertLectureHallType)
    {
        return expertLectureHallTypeMapper.insertExpertLectureHallType(expertLectureHallType);
    }

    /**
     * 修改专家讲堂分类
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 结果
     */
    @Override
    public int updateExpertLectureHallType(ExpertLectureHallType expertLectureHallType)
    {
        return expertLectureHallTypeMapper.updateExpertLectureHallType(expertLectureHallType);
    }

    /**
     * 批量删除专家讲堂分类
     * 
     * @param ids 需要删除的专家讲堂分类主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallTypeByIds(Long[] ids)
    {
        return expertLectureHallTypeMapper.deleteExpertLectureHallTypeByIds(ids);
    }

    /**
     * 删除专家讲堂分类信息
     * 
     * @param id 专家讲堂分类主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallTypeById(Long id)
    {
        return expertLectureHallTypeMapper.deleteExpertLectureHallTypeById(id);
    }
}
