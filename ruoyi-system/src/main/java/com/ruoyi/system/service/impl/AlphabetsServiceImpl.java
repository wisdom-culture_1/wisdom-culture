package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AlphabetsMapper;
import com.ruoyi.system.domain.Alphabets;
import com.ruoyi.system.service.IAlphabetsService;

/**
 * 字母Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class AlphabetsServiceImpl implements IAlphabetsService 
{
    @Autowired
    private AlphabetsMapper alphabetsMapper;

    /**
     * 查询字母
     * 
     * @param id 字母主键
     * @return 字母
     */
    @Override
    public Alphabets selectAlphabetsById(Long id)
    {
        return alphabetsMapper.selectAlphabetsById(id);
    }

    /**
     * 查询字母列表
     * 
     * @param alphabets 字母
     * @return 字母
     */
    @Override
    public List<Alphabets> selectAlphabetsList(Alphabets alphabets)
    {
        return alphabetsMapper.selectAlphabetsList(alphabets);
    }

    /**
     * 新增字母
     * 
     * @param alphabets 字母
     * @return 结果
     */
    @Override
    public int insertAlphabets(Alphabets alphabets)
    {
        return alphabetsMapper.insertAlphabets(alphabets);
    }

    /**
     * 修改字母
     * 
     * @param alphabets 字母
     * @return 结果
     */
    @Override
    public int updateAlphabets(Alphabets alphabets)
    {
        return alphabetsMapper.updateAlphabets(alphabets);
    }

    /**
     * 批量删除字母
     * 
     * @param ids 需要删除的字母主键
     * @return 结果
     */
    @Override
    public int deleteAlphabetsByIds(Long[] ids)
    {
        return alphabetsMapper.deleteAlphabetsByIds(ids);
    }

    /**
     * 删除字母信息
     * 
     * @param id 字母主键
     * @return 结果
     */
    @Override
    public int deleteAlphabetsById(Long id)
    {
        return alphabetsMapper.deleteAlphabetsById(id);
    }
}
