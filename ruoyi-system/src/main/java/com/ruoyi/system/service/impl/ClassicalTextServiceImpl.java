package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ClassicalTextMapper;
import com.ruoyi.system.domain.ClassicalText;
import com.ruoyi.system.service.IClassicalTextService;

/**
 * 文言文正文Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ClassicalTextServiceImpl implements IClassicalTextService 
{
    @Autowired
    private ClassicalTextMapper classicalTextMapper;

    /**
     * 查询文言文正文
     * 
     * @param id 文言文正文主键
     * @return 文言文正文
     */
    @Override
    public ClassicalText selectClassicalTextById(Long id)
    {
        return classicalTextMapper.selectClassicalTextById(id);
    }

    /**
     * 查询文言文正文列表
     * 
     * @param classicalText 文言文正文
     * @return 文言文正文
     */
    @Override
    public List<ClassicalText> selectClassicalTextList(ClassicalText classicalText)
    {
        return classicalTextMapper.selectClassicalTextList(classicalText);
    }

    /**
     * 新增文言文正文
     * 
     * @param classicalText 文言文正文
     * @return 结果
     */
    @Override
    public int insertClassicalText(ClassicalText classicalText)
    {
        return classicalTextMapper.insertClassicalText(classicalText);
    }

    /**
     * 修改文言文正文
     * 
     * @param classicalText 文言文正文
     * @return 结果
     */
    @Override
    public int updateClassicalText(ClassicalText classicalText)
    {
        return classicalTextMapper.updateClassicalText(classicalText);
    }

    /**
     * 批量删除文言文正文
     * 
     * @param ids 需要删除的文言文正文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalTextByIds(Long[] ids)
    {
        return classicalTextMapper.deleteClassicalTextByIds(ids);
    }

    /**
     * 删除文言文正文信息
     * 
     * @param id 文言文正文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalTextById(Long id)
    {
        return classicalTextMapper.deleteClassicalTextById(id);
    }
}
