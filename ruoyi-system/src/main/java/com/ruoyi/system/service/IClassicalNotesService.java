package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ClassicalNotes;

/**
 * 文言文注释Service接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface IClassicalNotesService 
{
    /**
     * 查询文言文注释
     * 
     * @param id 文言文注释主键
     * @return 文言文注释
     */
    public ClassicalNotes selectClassicalNotesById(Long id);

    /**
     * 查询文言文注释列表
     * 
     * @param classicalNotes 文言文注释
     * @return 文言文注释集合
     */
    public List<ClassicalNotes> selectClassicalNotesList(ClassicalNotes classicalNotes);

    /**
     * 新增文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    public int insertClassicalNotes(ClassicalNotes classicalNotes);

    /**
     * 修改文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    public int updateClassicalNotes(ClassicalNotes classicalNotes);

    /**
     * 批量删除文言文注释
     * 
     * @param ids 需要删除的文言文注释主键集合
     * @return 结果
     */
    public int deleteClassicalNotesByIds(Long[] ids);

    /**
     * 删除文言文注释信息
     * 
     * @param id 文言文注释主键
     * @return 结果
     */
    public int deleteClassicalNotesById(Long id);
}
