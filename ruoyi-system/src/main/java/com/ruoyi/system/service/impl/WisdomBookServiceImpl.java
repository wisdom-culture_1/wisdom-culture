package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WisdomBookMapper;
import com.ruoyi.system.domain.WisdomBook;
import com.ruoyi.system.service.IWisdomBookService;

/**
 * 智慧元典书籍Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class WisdomBookServiceImpl implements IWisdomBookService 
{
    @Autowired
    private WisdomBookMapper wisdomBookMapper;

    /**
     * 查询智慧元典书籍
     * 
     * @param id 智慧元典书籍主键
     * @return 智慧元典书籍
     */
    @Override
    public WisdomBook selectWisdomBookById(Long id)
    {
        return wisdomBookMapper.selectWisdomBookById(id);
    }

    /**
     * 查询智慧元典书籍列表
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 智慧元典书籍
     */
    @Override
    public List<WisdomBook> selectWisdomBookList(WisdomBook wisdomBook)
    {
        return wisdomBookMapper.selectWisdomBookList(wisdomBook);
    }

    /**
     * 新增智慧元典书籍
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 结果
     */
    @Override
    public int insertWisdomBook(WisdomBook wisdomBook)
    {
        return wisdomBookMapper.insertWisdomBook(wisdomBook);
    }

    /**
     * 修改智慧元典书籍
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 结果
     */
    @Override
    public int updateWisdomBook(WisdomBook wisdomBook)
    {
        return wisdomBookMapper.updateWisdomBook(wisdomBook);
    }

    /**
     * 批量删除智慧元典书籍
     * 
     * @param ids 需要删除的智慧元典书籍主键
     * @return 结果
     */
    @Override
    public int deleteWisdomBookByIds(Long[] ids)
    {
        return wisdomBookMapper.deleteWisdomBookByIds(ids);
    }

    /**
     * 删除智慧元典书籍信息
     * 
     * @param id 智慧元典书籍主键
     * @return 结果
     */
    @Override
    public int deleteWisdomBookById(Long id)
    {
        return wisdomBookMapper.deleteWisdomBookById(id);
    }
}
