package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OriginOfChineseCharactersMapper;
import com.ruoyi.system.domain.OriginOfChineseCharacters;
import com.ruoyi.system.service.IOriginOfChineseCharactersService;

/**
 * 汉字起源Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class OriginOfChineseCharactersServiceImpl implements IOriginOfChineseCharactersService 
{
    @Autowired
    private OriginOfChineseCharactersMapper originOfChineseCharactersMapper;

    /**
     * 查询汉字起源
     * 
     * @param id 汉字起源主键
     * @return 汉字起源
     */
    @Override
    public OriginOfChineseCharacters selectOriginOfChineseCharactersById(Long id)
    {
        return originOfChineseCharactersMapper.selectOriginOfChineseCharactersById(id);
    }

    /**
     * 查询汉字起源列表
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 汉字起源
     */
    @Override
    public List<OriginOfChineseCharacters> selectOriginOfChineseCharactersList(OriginOfChineseCharacters originOfChineseCharacters)
    {
        return originOfChineseCharactersMapper.selectOriginOfChineseCharactersList(originOfChineseCharacters);
    }

    /**
     * 新增汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    @Override
    public int insertOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters)
    {
        return originOfChineseCharactersMapper.insertOriginOfChineseCharacters(originOfChineseCharacters);
    }

    /**
     * 修改汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    @Override
    public int updateOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters)
    {
        return originOfChineseCharactersMapper.updateOriginOfChineseCharacters(originOfChineseCharacters);
    }

    /**
     * 批量删除汉字起源
     * 
     * @param ids 需要删除的汉字起源主键
     * @return 结果
     */
    @Override
    public int deleteOriginOfChineseCharactersByIds(Long[] ids)
    {
        return originOfChineseCharactersMapper.deleteOriginOfChineseCharactersByIds(ids);
    }

    /**
     * 删除汉字起源信息
     * 
     * @param id 汉字起源主键
     * @return 结果
     */
    @Override
    public int deleteOriginOfChineseCharactersById(Long id)
    {
        return originOfChineseCharactersMapper.deleteOriginOfChineseCharactersById(id);
    }
}
