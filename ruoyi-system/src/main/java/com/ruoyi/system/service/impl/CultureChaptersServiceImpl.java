package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CultureChaptersMapper;
import com.ruoyi.system.domain.CultureChapters;
import com.ruoyi.system.service.ICultureChaptersService;

/**
 * 文化章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class CultureChaptersServiceImpl implements ICultureChaptersService 
{
    @Autowired
    private CultureChaptersMapper cultureChaptersMapper;

    /**
     * 查询文化章节
     * 
     * @param id 文化章节主键
     * @return 文化章节
     */
    @Override
    public CultureChapters selectCultureChaptersById(Long id)
    {
        return cultureChaptersMapper.selectCultureChaptersById(id);
    }

    /**
     * 查询文化章节列表
     * 
     * @param cultureChapters 文化章节
     * @return 文化章节
     */
    @Override
    public List<CultureChapters> selectCultureChaptersList(CultureChapters cultureChapters)
    {
        return cultureChaptersMapper.selectCultureChaptersList(cultureChapters);
    }

    /**
     * 新增文化章节
     * 
     * @param cultureChapters 文化章节
     * @return 结果
     */
    @Override
    public int insertCultureChapters(CultureChapters cultureChapters)
    {
        return cultureChaptersMapper.insertCultureChapters(cultureChapters);
    }

    /**
     * 修改文化章节
     * 
     * @param cultureChapters 文化章节
     * @return 结果
     */
    @Override
    public int updateCultureChapters(CultureChapters cultureChapters)
    {
        return cultureChaptersMapper.updateCultureChapters(cultureChapters);
    }

    /**
     * 批量删除文化章节
     * 
     * @param ids 需要删除的文化章节主键
     * @return 结果
     */
    @Override
    public int deleteCultureChaptersByIds(Long[] ids)
    {
        return cultureChaptersMapper.deleteCultureChaptersByIds(ids);
    }

    /**
     * 删除文化章节信息
     * 
     * @param id 文化章节主键
     * @return 结果
     */
    @Override
    public int deleteCultureChaptersById(Long id)
    {
        return cultureChaptersMapper.deleteCultureChaptersById(id);
    }
}
