package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PoetryOdeMapper;
import com.ruoyi.system.domain.PoetryOde;
import com.ruoyi.system.service.IPoetryOdeService;

/**
 * 诗词歌赋Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class PoetryOdeServiceImpl implements IPoetryOdeService 
{
    @Autowired
    private PoetryOdeMapper poetryOdeMapper;

    /**
     * 查询诗词歌赋
     * 
     * @param id 诗词歌赋主键
     * @return 诗词歌赋
     */
    @Override
    public PoetryOde selectPoetryOdeById(Long id)
    {
        return poetryOdeMapper.selectPoetryOdeById(id);
    }

    /**
     * 查询诗词歌赋列表
     * 
     * @param poetryOde 诗词歌赋
     * @return 诗词歌赋
     */
    @Override
    public List<PoetryOde> selectPoetryOdeList(PoetryOde poetryOde)
    {
        return poetryOdeMapper.selectPoetryOdeList(poetryOde);
    }

    /**
     * 新增诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    @Override
    public int insertPoetryOde(PoetryOde poetryOde)
    {
        return poetryOdeMapper.insertPoetryOde(poetryOde);
    }

    /**
     * 修改诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    @Override
    public int updatePoetryOde(PoetryOde poetryOde)
    {
        return poetryOdeMapper.updatePoetryOde(poetryOde);
    }

    /**
     * 批量删除诗词歌赋
     * 
     * @param ids 需要删除的诗词歌赋主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeByIds(Long[] ids)
    {
        return poetryOdeMapper.deletePoetryOdeByIds(ids);
    }

    /**
     * 删除诗词歌赋信息
     * 
     * @param id 诗词歌赋主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeById(Long id)
    {
        return poetryOdeMapper.deletePoetryOdeById(id);
    }
}
