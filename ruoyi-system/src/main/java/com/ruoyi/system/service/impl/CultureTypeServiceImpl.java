package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CultureTypeMapper;
import com.ruoyi.system.domain.CultureType;
import com.ruoyi.system.service.ICultureTypeService;

/**
 * 中华文化分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class CultureTypeServiceImpl implements ICultureTypeService 
{
    @Autowired
    private CultureTypeMapper cultureTypeMapper;

    /**
     * 查询中华文化分类
     * 
     * @param id 中华文化分类主键
     * @return 中华文化分类
     */
    @Override
    public CultureType selectCultureTypeById(Long id)
    {
        return cultureTypeMapper.selectCultureTypeById(id);
    }

    /**
     * 查询中华文化分类列表
     * 
     * @param cultureType 中华文化分类
     * @return 中华文化分类
     */
    @Override
    public List<CultureType> selectCultureTypeList(CultureType cultureType)
    {
        return cultureTypeMapper.selectCultureTypeList(cultureType);
    }

    /**
     * 新增中华文化分类
     * 
     * @param cultureType 中华文化分类
     * @return 结果
     */
    @Override
    public int insertCultureType(CultureType cultureType)
    {
        return cultureTypeMapper.insertCultureType(cultureType);
    }

    /**
     * 修改中华文化分类
     * 
     * @param cultureType 中华文化分类
     * @return 结果
     */
    @Override
    public int updateCultureType(CultureType cultureType)
    {
        return cultureTypeMapper.updateCultureType(cultureType);
    }

    /**
     * 批量删除中华文化分类
     * 
     * @param ids 需要删除的中华文化分类主键
     * @return 结果
     */
    @Override
    public int deleteCultureTypeByIds(Long[] ids)
    {
        return cultureTypeMapper.deleteCultureTypeByIds(ids);
    }

    /**
     * 删除中华文化分类信息
     * 
     * @param id 中华文化分类主键
     * @return 结果
     */
    @Override
    public int deleteCultureTypeById(Long id)
    {
        return cultureTypeMapper.deleteCultureTypeById(id);
    }
}
