package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ChallengeQuestionsMapper;
import com.ruoyi.system.domain.ChallengeQuestions;
import com.ruoyi.system.service.IChallengeQuestionsService;

/**
 * 其他挑战（成语诗词....）Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ChallengeQuestionsServiceImpl implements IChallengeQuestionsService 
{
    @Autowired
    private ChallengeQuestionsMapper challengeQuestionsMapper;

    /**
     * 查询其他挑战（成语诗词....）
     * 
     * @param id 其他挑战（成语诗词....）主键
     * @return 其他挑战（成语诗词....）
     */
    @Override
    public ChallengeQuestions selectChallengeQuestionsById(Long id)
    {
        return challengeQuestionsMapper.selectChallengeQuestionsById(id);
    }

    /**
     * 查询其他挑战（成语诗词....）列表
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 其他挑战（成语诗词....）
     */
    @Override
    public List<ChallengeQuestions> selectChallengeQuestionsList(ChallengeQuestions challengeQuestions)
    {
        return challengeQuestionsMapper.selectChallengeQuestionsList(challengeQuestions);
    }

    /**
     * 新增其他挑战（成语诗词....）
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 结果
     */
    @Override
    public int insertChallengeQuestions(ChallengeQuestions challengeQuestions)
    {
        return challengeQuestionsMapper.insertChallengeQuestions(challengeQuestions);
    }

    /**
     * 修改其他挑战（成语诗词....）
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 结果
     */
    @Override
    public int updateChallengeQuestions(ChallengeQuestions challengeQuestions)
    {
        return challengeQuestionsMapper.updateChallengeQuestions(challengeQuestions);
    }

    /**
     * 批量删除其他挑战（成语诗词....）
     * 
     * @param ids 需要删除的其他挑战（成语诗词....）主键
     * @return 结果
     */
    @Override
    public int deleteChallengeQuestionsByIds(Long[] ids)
    {
        return challengeQuestionsMapper.deleteChallengeQuestionsByIds(ids);
    }

    /**
     * 删除其他挑战（成语诗词....）信息
     * 
     * @param id 其他挑战（成语诗词....）主键
     * @return 结果
     */
    @Override
    public int deleteChallengeQuestionsById(Long id)
    {
        return challengeQuestionsMapper.deleteChallengeQuestionsById(id);
    }
}
