package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ListenBook;

/**
 * 诗词磨耳朵Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IListenBookService 
{
    /**
     * 查询诗词磨耳朵
     * 
     * @param id 诗词磨耳朵主键
     * @return 诗词磨耳朵
     */
    public ListenBook selectListenBookById(Long id);

    /**
     * 查询诗词磨耳朵列表
     * 
     * @param listenBook 诗词磨耳朵
     * @return 诗词磨耳朵集合
     */
    public List<ListenBook> selectListenBookList(ListenBook listenBook);

    /**
     * 新增诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    public int insertListenBook(ListenBook listenBook);

    /**
     * 修改诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    public int updateListenBook(ListenBook listenBook);

    /**
     * 批量删除诗词磨耳朵
     * 
     * @param ids 需要删除的诗词磨耳朵主键集合
     * @return 结果
     */
    public int deleteListenBookByIds(Long[] ids);

    /**
     * 删除诗词磨耳朵信息
     * 
     * @param id 诗词磨耳朵主键
     * @return 结果
     */
    public int deleteListenBookById(Long id);
}
