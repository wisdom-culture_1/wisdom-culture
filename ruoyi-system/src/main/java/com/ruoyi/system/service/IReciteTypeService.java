package com.ruoyi.system.service;

import com.ruoyi.system.domain.ReciteType;

import java.util.List;

/**
 * 背诵打卡类别Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IReciteTypeService 
{
    /**
     * 查询背诵打卡类别
     * 
     * @param id 背诵打卡类别主键
     * @return 背诵打卡类别
     */
    public ReciteType selectReciteTypeById(Long id);

    /**
     * 查询背诵打卡类别列表
     * 
     * @param reciteType 背诵打卡类别
     * @return 背诵打卡类别集合
     */
    public List<ReciteType> selectReciteTypeList(ReciteType reciteType);

    /**
     * 新增背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    public int insertReciteType(ReciteType reciteType);

    /**
     * 修改背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    public int updateReciteType(ReciteType reciteType);

    /**
     * 批量删除背诵打卡类别
     * 
     * @param ids 需要删除的背诵打卡类别主键集合
     * @return 结果
     */
    public int deleteReciteTypeByIds(Long[] ids);

    /**
     * 删除背诵打卡类别信息
     * 
     * @param id 背诵打卡类别主键
     * @return 结果
     */
    public int deleteReciteTypeById(Long id);
}
