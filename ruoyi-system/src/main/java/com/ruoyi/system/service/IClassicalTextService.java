package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ClassicalText;

/**
 * 文言文正文Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IClassicalTextService 
{
    /**
     * 查询文言文正文
     * 
     * @param id 文言文正文主键
     * @return 文言文正文
     */
    public ClassicalText selectClassicalTextById(Long id);

    /**
     * 查询文言文正文列表
     * 
     * @param classicalText 文言文正文
     * @return 文言文正文集合
     */
    public List<ClassicalText> selectClassicalTextList(ClassicalText classicalText);

    /**
     * 新增文言文正文
     * 
     * @param classicalText 文言文正文
     * @return 结果
     */
    public int insertClassicalText(ClassicalText classicalText);

    /**
     * 修改文言文正文
     * 
     * @param classicalText 文言文正文
     * @return 结果
     */
    public int updateClassicalText(ClassicalText classicalText);

    /**
     * 批量删除文言文正文
     * 
     * @param ids 需要删除的文言文正文主键集合
     * @return 结果
     */
    public int deleteClassicalTextByIds(Long[] ids);

    /**
     * 删除文言文正文信息
     * 
     * @param id 文言文正文主键
     * @return 结果
     */
    public int deleteClassicalTextById(Long id);
}
