package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DictionariesMapper;
import com.ruoyi.system.domain.Dictionaries;
import com.ruoyi.system.service.IDictionariesService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-25
 */
@Service
public class DictionariesServiceImpl implements IDictionariesService 
{
    @Autowired
    private DictionariesMapper dictionariesMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Dictionaries selectDictionariesById(Integer id)
    {
        return dictionariesMapper.selectDictionariesById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dictionaries 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Dictionaries> selectDictionariesList(Dictionaries dictionaries)
    {
        return dictionariesMapper.selectDictionariesList(dictionaries);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param dictionaries 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDictionaries(Dictionaries dictionaries)
    {
        return dictionariesMapper.insertDictionaries(dictionaries);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param dictionaries 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDictionaries(Dictionaries dictionaries)
    {
        return dictionariesMapper.updateDictionaries(dictionaries);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDictionariesByIds(Integer[] ids)
    {
        return dictionariesMapper.deleteDictionariesByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDictionariesById(Integer id)
    {
        return dictionariesMapper.deleteDictionariesById(id);
    }
}
