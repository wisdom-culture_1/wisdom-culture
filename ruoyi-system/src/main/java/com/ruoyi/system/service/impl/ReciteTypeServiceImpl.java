package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.ReciteType;
import com.ruoyi.system.domain.Recite;
import com.ruoyi.system.mapper.ReciteMapper;
import com.ruoyi.system.mapper.ReciteTypeMapper;
import com.ruoyi.system.service.IReciteTypeService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;

//import com.ruoyi.system.mapper.ReciteTypeMapper;
//import com.ruoyi.system.domain.ReciteType;
//import com.ruoyi.system.service.IReciteTypeService;
import javax.annotation.Resource;

/**
 * 背诵打卡类别Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class ReciteTypeServiceImpl implements IReciteTypeService
{
    @Resource
    private ReciteTypeMapper reciteTypeMapper;

    @Resource
    private ReciteMapper reciteMapper;
    /**
     * 查询背诵打卡类别
     * 
     * @param id 背诵打卡类别主键
     * @return 背诵打卡类别
     */
    @Override
    public ReciteType selectReciteTypeById(Long id)
    {
        return reciteTypeMapper.selectReciteTypeById(id);
    }

    /**
     * 查询背诵打卡类别列表
     * 
     * @param reciteType 背诵打卡类别
     * @return 背诵打卡类别
     */
    @Override
    public List<ReciteType> selectReciteTypeList(ReciteType reciteType)
    {
        List<ReciteType> reciteTypes = reciteTypeMapper.selectReciteTypeList(reciteType);
        reciteTypes.forEach(item ->{
            ReciteType reciteType1 = new ReciteType();
            reciteType1.setPid(item.getId());
            List<ReciteType> reciteTypes1 = reciteTypeMapper.selectReciteTypeList(reciteType1);
            reciteTypes1.forEach(item1 ->{
                Recite recite = new Recite();
                recite.setTypeId(item1.getId());
                recite.setUserId(SecurityUtils.getUserId());
                List<Recite> recites = reciteMapper.selectReciteList(recite);
                if(recites != null && recites.size() >0){
                    item1.setReciteFlag("1");
                }
            });
            item.setChild(reciteTypes1);
        });

        return reciteTypes;
    }

    /**
     * 新增背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    @Override
    public int insertReciteType(ReciteType reciteType)
    {
        return reciteTypeMapper.insertReciteType(reciteType);
    }

    /**
     * 修改背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    @Override
    public int updateReciteType(ReciteType reciteType)
    {
        return reciteTypeMapper.updateReciteType(reciteType);
    }

    /**
     * 批量删除背诵打卡类别
     * 
     * @param ids 需要删除的背诵打卡类别主键
     * @return 结果
     */
    @Override
    public int deleteReciteTypeByIds(Long[] ids)
    {
        return reciteTypeMapper.deleteReciteTypeByIds(ids);
    }

    /**
     * 删除背诵打卡类别信息
     * 
     * @param id 背诵打卡类别主键
     * @return 结果
     */
    @Override
    public int deleteReciteTypeById(Long id)
    {
        return reciteTypeMapper.deleteReciteTypeById(id);
    }
}
