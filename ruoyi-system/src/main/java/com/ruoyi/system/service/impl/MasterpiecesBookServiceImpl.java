package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MasterpiecesBookMapper;
import com.ruoyi.system.domain.MasterpiecesBook;
import com.ruoyi.system.service.IMasterpiecesBookService;

/**
 * 名著书籍Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class MasterpiecesBookServiceImpl implements IMasterpiecesBookService 
{
    @Autowired
    private MasterpiecesBookMapper masterpiecesBookMapper;

    /**
     * 查询名著书籍
     * 
     * @param id 名著书籍主键
     * @return 名著书籍
     */
    @Override
    public MasterpiecesBook selectMasterpiecesBookById(Long id)
    {
        return masterpiecesBookMapper.selectMasterpiecesBookById(id);
    }

    /**
     * 查询名著书籍列表
     * 
     * @param masterpiecesBook 名著书籍
     * @return 名著书籍
     */
    @Override
    public List<MasterpiecesBook> selectMasterpiecesBookList(MasterpiecesBook masterpiecesBook)
    {
        return masterpiecesBookMapper.selectMasterpiecesBookList(masterpiecesBook);
    }

    /**
     * 新增名著书籍
     * 
     * @param masterpiecesBook 名著书籍
     * @return 结果
     */
    @Override
    public int insertMasterpiecesBook(MasterpiecesBook masterpiecesBook)
    {
        return masterpiecesBookMapper.insertMasterpiecesBook(masterpiecesBook);
    }

    /**
     * 修改名著书籍
     * 
     * @param masterpiecesBook 名著书籍
     * @return 结果
     */
    @Override
    public int updateMasterpiecesBook(MasterpiecesBook masterpiecesBook)
    {
        return masterpiecesBookMapper.updateMasterpiecesBook(masterpiecesBook);
    }

    /**
     * 批量删除名著书籍
     * 
     * @param ids 需要删除的名著书籍主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesBookByIds(Long[] ids)
    {
        return masterpiecesBookMapper.deleteMasterpiecesBookByIds(ids);
    }

    /**
     * 删除名著书籍信息
     * 
     * @param id 名著书籍主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesBookById(Long id)
    {
        return masterpiecesBookMapper.deleteMasterpiecesBookById(id);
    }
}
