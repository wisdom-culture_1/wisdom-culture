package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MasterpiecesChapters;

/**
 * 名著章节Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IMasterpiecesChaptersService 
{
    /**
     * 查询名著章节
     * 
     * @param id 名著章节主键
     * @return 名著章节
     */
    public MasterpiecesChapters selectMasterpiecesChaptersById(Long id);

    /**
     * 查询名著章节列表
     * 
     * @param masterpiecesChapters 名著章节
     * @return 名著章节集合
     */
    public List<MasterpiecesChapters> selectMasterpiecesChaptersList(MasterpiecesChapters masterpiecesChapters);

    /**
     * 新增名著章节
     * 
     * @param masterpiecesChapters 名著章节
     * @return 结果
     */
    public int insertMasterpiecesChapters(MasterpiecesChapters masterpiecesChapters);

    /**
     * 修改名著章节
     * 
     * @param masterpiecesChapters 名著章节
     * @return 结果
     */
    public int updateMasterpiecesChapters(MasterpiecesChapters masterpiecesChapters);

    /**
     * 批量删除名著章节
     * 
     * @param ids 需要删除的名著章节主键集合
     * @return 结果
     */
    public int deleteMasterpiecesChaptersByIds(Long[] ids);

    /**
     * 删除名著章节信息
     * 
     * @param id 名著章节主键
     * @return 结果
     */
    public int deleteMasterpiecesChaptersById(Long id);
}
