package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MasterpiecesTextMapper;
import com.ruoyi.system.domain.MasterpiecesText;
import com.ruoyi.system.service.IMasterpiecesTextService;

/**
 * 名著章节内容Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class MasterpiecesTextServiceImpl implements IMasterpiecesTextService 
{
    @Autowired
    private MasterpiecesTextMapper masterpiecesTextMapper;

    /**
     * 查询名著章节内容
     * 
     * @param id 名著章节内容主键
     * @return 名著章节内容
     */
    @Override
    public MasterpiecesText selectMasterpiecesTextById(Long id)
    {
        return masterpiecesTextMapper.selectMasterpiecesTextById(id);
    }

    /**
     * 查询名著章节内容列表
     * 
     * @param masterpiecesText 名著章节内容
     * @return 名著章节内容
     */
    @Override
    public List<MasterpiecesText> selectMasterpiecesTextList(MasterpiecesText masterpiecesText)
    {
        return masterpiecesTextMapper.selectMasterpiecesTextList(masterpiecesText);
    }

    /**
     * 新增名著章节内容
     * 
     * @param masterpiecesText 名著章节内容
     * @return 结果
     */
    @Override
    public int insertMasterpiecesText(MasterpiecesText masterpiecesText)
    {
        return masterpiecesTextMapper.insertMasterpiecesText(masterpiecesText);
    }

    /**
     * 修改名著章节内容
     * 
     * @param masterpiecesText 名著章节内容
     * @return 结果
     */
    @Override
    public int updateMasterpiecesText(MasterpiecesText masterpiecesText)
    {
        return masterpiecesTextMapper.updateMasterpiecesText(masterpiecesText);
    }

    /**
     * 批量删除名著章节内容
     * 
     * @param ids 需要删除的名著章节内容主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTextByIds(Long[] ids)
    {
        return masterpiecesTextMapper.deleteMasterpiecesTextByIds(ids);
    }

    /**
     * 删除名著章节内容信息
     * 
     * @param id 名著章节内容主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTextById(Long id)
    {
        return masterpiecesTextMapper.deleteMasterpiecesTextById(id);
    }
}
