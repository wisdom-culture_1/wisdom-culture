package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EtymologyMapper;
import com.ruoyi.system.domain.Etymology;
import com.ruoyi.system.service.IEtymologyService;

/**
 * 字源Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class EtymologyServiceImpl implements IEtymologyService 
{
    @Autowired
    private EtymologyMapper etymologyMapper;

    /**
     * 查询字源
     * 
     * @param id 字源主键
     * @return 字源
     */
    @Override
    public Etymology selectEtymologyById(Long id)
    {
        return etymologyMapper.selectEtymologyById(id);
    }

    /**
     * 查询字源列表
     * 
     * @param etymology 字源
     * @return 字源
     */
    @Override
    public List<Etymology> selectEtymologyList(Etymology etymology)
    {
        return etymologyMapper.selectEtymologyList(etymology);
    }

    /**
     * 新增字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    @Override
    public int insertEtymology(Etymology etymology)
    {
        return etymologyMapper.insertEtymology(etymology);
    }

    /**
     * 修改字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    @Override
    public int updateEtymology(Etymology etymology)
    {
        return etymologyMapper.updateEtymology(etymology);
    }

    /**
     * 批量删除字源
     * 
     * @param ids 需要删除的字源主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyByIds(Long[] ids)
    {
        return etymologyMapper.deleteEtymologyByIds(ids);
    }

    /**
     * 删除字源信息
     * 
     * @param id 字源主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyById(Long id)
    {
        return etymologyMapper.deleteEtymologyById(id);
    }
}
