package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WisdomChapterMapper;
import com.ruoyi.system.domain.WisdomChapter;
import com.ruoyi.system.service.IWisdomChapterService;

/**
 * 智慧元典章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class WisdomChapterServiceImpl implements IWisdomChapterService 
{
    @Autowired
    private WisdomChapterMapper wisdomChapterMapper;

    /**
     * 查询智慧元典章节
     * 
     * @param id 智慧元典章节主键
     * @return 智慧元典章节
     */
    @Override
    public WisdomChapter selectWisdomChapterById(Long id)
    {
        return wisdomChapterMapper.selectWisdomChapterById(id);
    }

    /**
     * 查询智慧元典章节列表
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 智慧元典章节
     */
    @Override
    public List<WisdomChapter> selectWisdomChapterList(WisdomChapter wisdomChapter)
    {
        return wisdomChapterMapper.selectWisdomChapterList(wisdomChapter);
    }

    /**
     * 新增智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    @Override
    public int insertWisdomChapter(WisdomChapter wisdomChapter)
    {
        return wisdomChapterMapper.insertWisdomChapter(wisdomChapter);
    }

    /**
     * 修改智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    @Override
    public int updateWisdomChapter(WisdomChapter wisdomChapter)
    {
        return wisdomChapterMapper.updateWisdomChapter(wisdomChapter);
    }

    /**
     * 批量删除智慧元典章节
     * 
     * @param ids 需要删除的智慧元典章节主键
     * @return 结果
     */
    @Override
    public int deleteWisdomChapterByIds(Long[] ids)
    {
        return wisdomChapterMapper.deleteWisdomChapterByIds(ids);
    }

    /**
     * 删除智慧元典章节信息
     * 
     * @param id 智慧元典章节主键
     * @return 结果
     */
    @Override
    public int deleteWisdomChapterById(Long id)
    {
        return wisdomChapterMapper.deleteWisdomChapterById(id);
    }
}
