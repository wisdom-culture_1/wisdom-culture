package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EtymologyChildrenMapper;
import com.ruoyi.system.domain.EtymologyChildren;
import com.ruoyi.system.service.IEtymologyChildrenService;

/**
 * 字源示例Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class EtymologyChildrenServiceImpl implements IEtymologyChildrenService 
{
    @Autowired
    private EtymologyChildrenMapper etymologyChildrenMapper;

    /**
     * 查询字源示例
     * 
     * @param id 字源示例主键
     * @return 字源示例
     */
    @Override
    public EtymologyChildren selectEtymologyChildrenById(Long id)
    {
        return etymologyChildrenMapper.selectEtymologyChildrenById(id);
    }

    /**
     * 查询字源示例列表
     * 
     * @param etymologyChildren 字源示例
     * @return 字源示例
     */
    @Override
    public List<EtymologyChildren> selectEtymologyChildrenList(EtymologyChildren etymologyChildren)
    {
        return etymologyChildrenMapper.selectEtymologyChildrenList(etymologyChildren);
    }

    /**
     * 新增字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    @Override
    public int insertEtymologyChildren(EtymologyChildren etymologyChildren)
    {
        return etymologyChildrenMapper.insertEtymologyChildren(etymologyChildren);
    }

    /**
     * 修改字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    @Override
    public int updateEtymologyChildren(EtymologyChildren etymologyChildren)
    {
        return etymologyChildrenMapper.updateEtymologyChildren(etymologyChildren);
    }

    /**
     * 批量删除字源示例
     * 
     * @param ids 需要删除的字源示例主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyChildrenByIds(Long[] ids)
    {
        return etymologyChildrenMapper.deleteEtymologyChildrenByIds(ids);
    }

    /**
     * 删除字源示例信息
     * 
     * @param id 字源示例主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyChildrenById(Long id)
    {
        return etymologyChildrenMapper.deleteEtymologyChildrenById(id);
    }
}
