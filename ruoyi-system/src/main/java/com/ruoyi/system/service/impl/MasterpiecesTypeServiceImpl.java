package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MasterpiecesTypeMapper;
import com.ruoyi.system.domain.MasterpiecesType;
import com.ruoyi.system.service.IMasterpiecesTypeService;

/**
 * 名著阅读分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class MasterpiecesTypeServiceImpl implements IMasterpiecesTypeService 
{
    @Autowired
    private MasterpiecesTypeMapper masterpiecesTypeMapper;

    /**
     * 查询名著阅读分类
     * 
     * @param id 名著阅读分类主键
     * @return 名著阅读分类
     */
    @Override
    public MasterpiecesType selectMasterpiecesTypeById(Long id)
    {
        return masterpiecesTypeMapper.selectMasterpiecesTypeById(id);
    }

    /**
     * 查询名著阅读分类列表
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 名著阅读分类
     */
    @Override
    public List<MasterpiecesType> selectMasterpiecesTypeList(MasterpiecesType masterpiecesType)
    {
        return masterpiecesTypeMapper.selectMasterpiecesTypeList(masterpiecesType);
    }

    /**
     * 新增名著阅读分类
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 结果
     */
    @Override
    public int insertMasterpiecesType(MasterpiecesType masterpiecesType)
    {
        return masterpiecesTypeMapper.insertMasterpiecesType(masterpiecesType);
    }

    /**
     * 修改名著阅读分类
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 结果
     */
    @Override
    public int updateMasterpiecesType(MasterpiecesType masterpiecesType)
    {
        return masterpiecesTypeMapper.updateMasterpiecesType(masterpiecesType);
    }

    /**
     * 批量删除名著阅读分类
     * 
     * @param ids 需要删除的名著阅读分类主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTypeByIds(Long[] ids)
    {
        return masterpiecesTypeMapper.deleteMasterpiecesTypeByIds(ids);
    }

    /**
     * 删除名著阅读分类信息
     * 
     * @param id 名著阅读分类主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTypeById(Long id)
    {
        return masterpiecesTypeMapper.deleteMasterpiecesTypeById(id);
    }
}
