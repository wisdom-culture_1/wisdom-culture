package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MasterpiecesChaptersProcessMapper;
import com.ruoyi.system.domain.MasterpiecesChaptersProcess;
import com.ruoyi.system.service.IMasterpiecesChaptersProcessService;

/**
 * 章节试听进度Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class MasterpiecesChaptersProcessServiceImpl implements IMasterpiecesChaptersProcessService 
{
    @Autowired
    private MasterpiecesChaptersProcessMapper masterpiecesChaptersProcessMapper;

    /**
     * 查询章节试听进度
     * 
     * @param id 章节试听进度主键
     * @return 章节试听进度
     */
    @Override
    public MasterpiecesChaptersProcess selectMasterpiecesChaptersProcessById(Long id)
    {
        return masterpiecesChaptersProcessMapper.selectMasterpiecesChaptersProcessById(id);
    }

    /**
     * 查询章节试听进度列表
     * 
     * @param masterpiecesChaptersProcess 章节试听进度
     * @return 章节试听进度
     */
    @Override
    public List<MasterpiecesChaptersProcess> selectMasterpiecesChaptersProcessList(MasterpiecesChaptersProcess masterpiecesChaptersProcess)
    {
        return masterpiecesChaptersProcessMapper.selectMasterpiecesChaptersProcessList(masterpiecesChaptersProcess);
    }

    /**
     * 新增章节试听进度
     * 
     * @param masterpiecesChaptersProcess 章节试听进度
     * @return 结果
     */
    @Override
    public int insertMasterpiecesChaptersProcess(MasterpiecesChaptersProcess masterpiecesChaptersProcess)
    {
        return masterpiecesChaptersProcessMapper.insertMasterpiecesChaptersProcess(masterpiecesChaptersProcess);
    }

    /**
     * 修改章节试听进度
     * 
     * @param masterpiecesChaptersProcess 章节试听进度
     * @return 结果
     */
    @Override
    public int updateMasterpiecesChaptersProcess(MasterpiecesChaptersProcess masterpiecesChaptersProcess)
    {
        return masterpiecesChaptersProcessMapper.updateMasterpiecesChaptersProcess(masterpiecesChaptersProcess);
    }

    /**
     * 批量删除章节试听进度
     * 
     * @param ids 需要删除的章节试听进度主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesChaptersProcessByIds(Long[] ids)
    {
        return masterpiecesChaptersProcessMapper.deleteMasterpiecesChaptersProcessByIds(ids);
    }

    /**
     * 删除章节试听进度信息
     * 
     * @param id 章节试听进度主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesChaptersProcessById(Long id)
    {
        return masterpiecesChaptersProcessMapper.deleteMasterpiecesChaptersProcessById(id);
    }
}
