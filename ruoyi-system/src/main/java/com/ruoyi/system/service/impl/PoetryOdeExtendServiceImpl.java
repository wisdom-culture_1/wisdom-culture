package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PoetryOdeExtendMapper;
import com.ruoyi.system.domain.PoetryOdeExtend;
import com.ruoyi.system.service.IPoetryOdeExtendService;

/**
 * 诗词歌赋扩展Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class PoetryOdeExtendServiceImpl implements IPoetryOdeExtendService 
{
    @Autowired
    private PoetryOdeExtendMapper poetryOdeExtendMapper;

    /**
     * 查询诗词歌赋扩展
     * 
     * @param id 诗词歌赋扩展主键
     * @return 诗词歌赋扩展
     */
    @Override
    public PoetryOdeExtend selectPoetryOdeExtendById(Long id)
    {
        return poetryOdeExtendMapper.selectPoetryOdeExtendById(id);
    }

    /**
     * 查询诗词歌赋扩展列表
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 诗词歌赋扩展
     */
    @Override
    public List<PoetryOdeExtend> selectPoetryOdeExtendList(PoetryOdeExtend poetryOdeExtend)
    {
        return poetryOdeExtendMapper.selectPoetryOdeExtendList(poetryOdeExtend);
    }

    /**
     * 新增诗词歌赋扩展
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 结果
     */
    @Override
    public int insertPoetryOdeExtend(PoetryOdeExtend poetryOdeExtend)
    {
        return poetryOdeExtendMapper.insertPoetryOdeExtend(poetryOdeExtend);
    }

    /**
     * 修改诗词歌赋扩展
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 结果
     */
    @Override
    public int updatePoetryOdeExtend(PoetryOdeExtend poetryOdeExtend)
    {
        return poetryOdeExtendMapper.updatePoetryOdeExtend(poetryOdeExtend);
    }

    /**
     * 批量删除诗词歌赋扩展
     * 
     * @param ids 需要删除的诗词歌赋扩展主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeExtendByIds(Long[] ids)
    {
        return poetryOdeExtendMapper.deletePoetryOdeExtendByIds(ids);
    }

    /**
     * 删除诗词歌赋扩展信息
     * 
     * @param id 诗词歌赋扩展主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeExtendById(Long id)
    {
        return poetryOdeExtendMapper.deletePoetryOdeExtendById(id);
    }
}
