package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ChallengeCompleteMapper;
import com.ruoyi.system.domain.ChallengeComplete;
import com.ruoyi.system.service.IChallengeCompleteService;

/**
 * 挑战结果Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ChallengeCompleteServiceImpl implements IChallengeCompleteService 
{
    @Autowired
    private ChallengeCompleteMapper challengeCompleteMapper;

    /**
     * 查询挑战结果
     * 
     * @param id 挑战结果主键
     * @return 挑战结果
     */
    @Override
    public ChallengeComplete selectChallengeCompleteById(Long id)
    {
        return challengeCompleteMapper.selectChallengeCompleteById(id);
    }

    /**
     * 查询挑战结果列表
     * 
     * @param challengeComplete 挑战结果
     * @return 挑战结果
     */
    @Override
    public List<ChallengeComplete> selectChallengeCompleteList(ChallengeComplete challengeComplete)
    {
        return challengeCompleteMapper.selectChallengeCompleteList(challengeComplete);
    }

    /**
     * 新增挑战结果
     * 
     * @param challengeComplete 挑战结果
     * @return 结果
     */
    @Override
    public int insertChallengeComplete(ChallengeComplete challengeComplete)
    {
        return challengeCompleteMapper.insertChallengeComplete(challengeComplete);
    }

    /**
     * 修改挑战结果
     * 
     * @param challengeComplete 挑战结果
     * @return 结果
     */
    @Override
    public int updateChallengeComplete(ChallengeComplete challengeComplete)
    {
        return challengeCompleteMapper.updateChallengeComplete(challengeComplete);
    }

    /**
     * 批量删除挑战结果
     * 
     * @param ids 需要删除的挑战结果主键
     * @return 结果
     */
    @Override
    public int deleteChallengeCompleteByIds(Long[] ids)
    {
        return challengeCompleteMapper.deleteChallengeCompleteByIds(ids);
    }

    /**
     * 删除挑战结果信息
     * 
     * @param id 挑战结果主键
     * @return 结果
     */
    @Override
    public int deleteChallengeCompleteById(Long id)
    {
        return challengeCompleteMapper.deleteChallengeCompleteById(id);
    }
}
