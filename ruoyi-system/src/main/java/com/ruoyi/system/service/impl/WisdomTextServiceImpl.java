package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WisdomTextMapper;
import com.ruoyi.system.domain.WisdomText;
import com.ruoyi.system.service.IWisdomTextService;

/**
 * 智慧元典正文Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class WisdomTextServiceImpl implements IWisdomTextService 
{
    @Autowired
    private WisdomTextMapper wisdomTextMapper;

    /**
     * 查询智慧元典正文
     * 
     * @param id 智慧元典正文主键
     * @return 智慧元典正文
     */
    @Override
    public WisdomText selectWisdomTextById(Long id)
    {
        return wisdomTextMapper.selectWisdomTextById(id);
    }

    /**
     * 查询智慧元典正文列表
     * 
     * @param wisdomText 智慧元典正文
     * @return 智慧元典正文
     */
    @Override
    public List<WisdomText> selectWisdomTextList(WisdomText wisdomText)
    {
        return wisdomTextMapper.selectWisdomTextList(wisdomText);
    }

    /**
     * 新增智慧元典正文
     * 
     * @param wisdomText 智慧元典正文
     * @return 结果
     */
    @Override
    public int insertWisdomText(WisdomText wisdomText)
    {
        return wisdomTextMapper.insertWisdomText(wisdomText);
    }

    /**
     * 修改智慧元典正文
     * 
     * @param wisdomText 智慧元典正文
     * @return 结果
     */
    @Override
    public int updateWisdomText(WisdomText wisdomText)
    {
        return wisdomTextMapper.updateWisdomText(wisdomText);
    }

    /**
     * 批量删除智慧元典正文
     * 
     * @param ids 需要删除的智慧元典正文主键
     * @return 结果
     */
    @Override
    public int deleteWisdomTextByIds(Long[] ids)
    {
        return wisdomTextMapper.deleteWisdomTextByIds(ids);
    }

    /**
     * 删除智慧元典正文信息
     * 
     * @param id 智慧元典正文主键
     * @return 结果
     */
    @Override
    public int deleteWisdomTextById(Long id)
    {
        return wisdomTextMapper.deleteWisdomTextById(id);
    }
}
