package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExcerptMapper;
import com.ruoyi.system.domain.Excerpt;
import com.ruoyi.system.service.IExcerptService;

/**
 * 金句摘抄Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ExcerptServiceImpl implements IExcerptService 
{
    @Autowired
    private ExcerptMapper excerptMapper;

    /**
     * 查询金句摘抄
     * 
     * @param id 金句摘抄主键
     * @return 金句摘抄
     */
    @Override
    public Excerpt selectExcerptById(Long id)
    {
        return excerptMapper.selectExcerptById(id);
    }

    /**
     * 查询金句摘抄列表
     * 
     * @param excerpt 金句摘抄
     * @return 金句摘抄
     */
    @Override
    public List<Excerpt> selectExcerptList(Excerpt excerpt)
    {
        return excerptMapper.selectExcerptList(excerpt);
    }

    /**
     * 新增金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    @Override
    public int insertExcerpt(Excerpt excerpt)
    {
        return excerptMapper.insertExcerpt(excerpt);
    }

    /**
     * 修改金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    @Override
    public int updateExcerpt(Excerpt excerpt)
    {
        return excerptMapper.updateExcerpt(excerpt);
    }

    /**
     * 批量删除金句摘抄
     * 
     * @param ids 需要删除的金句摘抄主键
     * @return 结果
     */
    @Override
    public int deleteExcerptByIds(Long[] ids)
    {
        return excerptMapper.deleteExcerptByIds(ids);
    }

    /**
     * 删除金句摘抄信息
     * 
     * @param id 金句摘抄主键
     * @return 结果
     */
    @Override
    public int deleteExcerptById(Long id)
    {
        return excerptMapper.deleteExcerptById(id);
    }
}
