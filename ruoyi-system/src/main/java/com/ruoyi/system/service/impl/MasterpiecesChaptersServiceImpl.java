package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MasterpiecesChaptersMapper;
import com.ruoyi.system.domain.MasterpiecesChapters;
import com.ruoyi.system.service.IMasterpiecesChaptersService;

/**
 * 名著章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class MasterpiecesChaptersServiceImpl implements IMasterpiecesChaptersService 
{
    @Autowired
    private MasterpiecesChaptersMapper masterpiecesChaptersMapper;

    /**
     * 查询名著章节
     * 
     * @param id 名著章节主键
     * @return 名著章节
     */
    @Override
    public MasterpiecesChapters selectMasterpiecesChaptersById(Long id)
    {
        return masterpiecesChaptersMapper.selectMasterpiecesChaptersById(id);
    }

    /**
     * 查询名著章节列表
     * 
     * @param masterpiecesChapters 名著章节
     * @return 名著章节
     */
    @Override
    public List<MasterpiecesChapters> selectMasterpiecesChaptersList(MasterpiecesChapters masterpiecesChapters)
    {
        return masterpiecesChaptersMapper.selectMasterpiecesChaptersList(masterpiecesChapters);
    }

    /**
     * 新增名著章节
     * 
     * @param masterpiecesChapters 名著章节
     * @return 结果
     */
    @Override
    public int insertMasterpiecesChapters(MasterpiecesChapters masterpiecesChapters)
    {
        return masterpiecesChaptersMapper.insertMasterpiecesChapters(masterpiecesChapters);
    }

    /**
     * 修改名著章节
     * 
     * @param masterpiecesChapters 名著章节
     * @return 结果
     */
    @Override
    public int updateMasterpiecesChapters(MasterpiecesChapters masterpiecesChapters)
    {
        return masterpiecesChaptersMapper.updateMasterpiecesChapters(masterpiecesChapters);
    }

    /**
     * 批量删除名著章节
     * 
     * @param ids 需要删除的名著章节主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesChaptersByIds(Long[] ids)
    {
        return masterpiecesChaptersMapper.deleteMasterpiecesChaptersByIds(ids);
    }

    /**
     * 删除名著章节信息
     * 
     * @param id 名著章节主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesChaptersById(Long id)
    {
        return masterpiecesChaptersMapper.deleteMasterpiecesChaptersById(id);
    }
}
