package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ReciteLikeMapper;
import com.ruoyi.system.domain.ReciteLike;
import com.ruoyi.system.service.IReciteLikeService;

/**
 * 背诵打卡点赞Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ReciteLikeServiceImpl implements IReciteLikeService 
{
    @Autowired
    private ReciteLikeMapper reciteLikeMapper;

    /**
     * 查询背诵打卡点赞
     * 
     * @param id 背诵打卡点赞主键
     * @return 背诵打卡点赞
     */
    @Override
    public ReciteLike selectReciteLikeById(Long id)
    {
        return reciteLikeMapper.selectReciteLikeById(id);
    }

    /**
     * 查询背诵打卡点赞列表
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 背诵打卡点赞
     */
    @Override
    public List<ReciteLike> selectReciteLikeList(ReciteLike reciteLike)
    {
        return reciteLikeMapper.selectReciteLikeList(reciteLike);
    }

    /**
     * 新增背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    @Override
    public int insertReciteLike(ReciteLike reciteLike)
    {
        return reciteLikeMapper.insertReciteLike(reciteLike);
    }

    /**
     * 修改背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    @Override
    public int updateReciteLike(ReciteLike reciteLike)
    {
        return reciteLikeMapper.updateReciteLike(reciteLike);
    }

    /**
     * 批量删除背诵打卡点赞
     * 
     * @param ids 需要删除的背诵打卡点赞主键
     * @return 结果
     */
    @Override
    public int deleteReciteLikeByIds(Long[] ids)
    {
        return reciteLikeMapper.deleteReciteLikeByIds(ids);
    }

    /**
     * 删除背诵打卡点赞信息
     * 
     * @param id 背诵打卡点赞主键
     * @return 结果
     */
    @Override
    public int deleteReciteLikeById(Long id)
    {
        return reciteLikeMapper.deleteReciteLikeById(id);
    }
}
