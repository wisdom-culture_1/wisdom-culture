package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.CultureChapters;

/**
 * 文化章节Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface ICultureChaptersService 
{
    /**
     * 查询文化章节
     * 
     * @param id 文化章节主键
     * @return 文化章节
     */
    public CultureChapters selectCultureChaptersById(Long id);

    /**
     * 查询文化章节列表
     * 
     * @param cultureChapters 文化章节
     * @return 文化章节集合
     */
    public List<CultureChapters> selectCultureChaptersList(CultureChapters cultureChapters);

    /**
     * 新增文化章节
     * 
     * @param cultureChapters 文化章节
     * @return 结果
     */
    public int insertCultureChapters(CultureChapters cultureChapters);

    /**
     * 修改文化章节
     * 
     * @param cultureChapters 文化章节
     * @return 结果
     */
    public int updateCultureChapters(CultureChapters cultureChapters);

    /**
     * 批量删除文化章节
     * 
     * @param ids 需要删除的文化章节主键集合
     * @return 结果
     */
    public int deleteCultureChaptersByIds(Long[] ids);

    /**
     * 删除文化章节信息
     * 
     * @param id 文化章节主键
     * @return 结果
     */
    public int deleteCultureChaptersById(Long id);
}
