package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExpertLectureHallMapper;
import com.ruoyi.system.domain.ExpertLectureHall;
import com.ruoyi.system.service.IExpertLectureHallService;

/**
 * 专家讲堂视频Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ExpertLectureHallServiceImpl implements IExpertLectureHallService 
{
    @Autowired
    private ExpertLectureHallMapper expertLectureHallMapper;

    /**
     * 查询专家讲堂视频
     * 
     * @param id 专家讲堂视频主键
     * @return 专家讲堂视频
     */
    @Override
    public ExpertLectureHall selectExpertLectureHallById(Long id)
    {
        return expertLectureHallMapper.selectExpertLectureHallById(id);
    }

    /**
     * 查询专家讲堂视频列表
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 专家讲堂视频
     */
    @Override
    public List<ExpertLectureHall> selectExpertLectureHallList(ExpertLectureHall expertLectureHall)
    {
        return expertLectureHallMapper.selectExpertLectureHallList(expertLectureHall);
    }

    /**
     * 新增专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    @Override
    public int insertExpertLectureHall(ExpertLectureHall expertLectureHall)
    {
        return expertLectureHallMapper.insertExpertLectureHall(expertLectureHall);
    }

    /**
     * 修改专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    @Override
    public int updateExpertLectureHall(ExpertLectureHall expertLectureHall)
    {
        return expertLectureHallMapper.updateExpertLectureHall(expertLectureHall);
    }

    /**
     * 批量删除专家讲堂视频
     * 
     * @param ids 需要删除的专家讲堂视频主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallByIds(Long[] ids)
    {
        return expertLectureHallMapper.deleteExpertLectureHallByIds(ids);
    }

    /**
     * 删除专家讲堂视频信息
     * 
     * @param id 专家讲堂视频主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallById(Long id)
    {
        return expertLectureHallMapper.deleteExpertLectureHallById(id);
    }
}
