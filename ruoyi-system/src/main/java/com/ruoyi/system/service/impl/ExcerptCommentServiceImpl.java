package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExcerptCommentMapper;
import com.ruoyi.system.domain.ExcerptComment;
import com.ruoyi.system.service.IExcerptCommentService;

/**
 * 金句摘抄评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ExcerptCommentServiceImpl implements IExcerptCommentService 
{
    @Autowired
    private ExcerptCommentMapper excerptCommentMapper;

    /**
     * 查询金句摘抄评论
     * 
     * @param id 金句摘抄评论主键
     * @return 金句摘抄评论
     */
    @Override
    public ExcerptComment selectExcerptCommentById(Long id)
    {
        return excerptCommentMapper.selectExcerptCommentById(id);
    }

    /**
     * 查询金句摘抄评论列表
     * 
     * @param excerptComment 金句摘抄评论
     * @return 金句摘抄评论
     */
    @Override
    public List<ExcerptComment> selectExcerptCommentList(ExcerptComment excerptComment)
    {
        return excerptCommentMapper.selectExcerptCommentList(excerptComment);
    }

    /**
     * 新增金句摘抄评论
     * 
     * @param excerptComment 金句摘抄评论
     * @return 结果
     */
    @Override
    public int insertExcerptComment(ExcerptComment excerptComment)
    {
        return excerptCommentMapper.insertExcerptComment(excerptComment);
    }

    /**
     * 修改金句摘抄评论
     * 
     * @param excerptComment 金句摘抄评论
     * @return 结果
     */
    @Override
    public int updateExcerptComment(ExcerptComment excerptComment)
    {
        return excerptCommentMapper.updateExcerptComment(excerptComment);
    }

    /**
     * 批量删除金句摘抄评论
     * 
     * @param ids 需要删除的金句摘抄评论主键
     * @return 结果
     */
    @Override
    public int deleteExcerptCommentByIds(Long[] ids)
    {
        return excerptCommentMapper.deleteExcerptCommentByIds(ids);
    }

    /**
     * 删除金句摘抄评论信息
     * 
     * @param id 金句摘抄评论主键
     * @return 结果
     */
    @Override
    public int deleteExcerptCommentById(Long id)
    {
        return excerptCommentMapper.deleteExcerptCommentById(id);
    }
}
