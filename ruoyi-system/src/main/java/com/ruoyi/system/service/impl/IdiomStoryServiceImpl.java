package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.IdiomStoryMapper;
import com.ruoyi.system.domain.IdiomStory;
import com.ruoyi.system.service.IIdiomStoryService;

/**
 * 成语典故Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class IdiomStoryServiceImpl implements IIdiomStoryService 
{
    @Autowired
    private IdiomStoryMapper idiomStoryMapper;

    /**
     * 查询成语典故
     * 
     * @param id 成语典故主键
     * @return 成语典故
     */
    @Override
    public IdiomStory selectIdiomStoryById(Long id)
    {
        return idiomStoryMapper.selectIdiomStoryById(id);
    }

    /**
     * 查询成语典故列表
     * 
     * @param idiomStory 成语典故
     * @return 成语典故
     */
    @Override
    public List<IdiomStory> selectIdiomStoryList(IdiomStory idiomStory)
    {
        return idiomStoryMapper.selectIdiomStoryList(idiomStory);
    }

    /**
     * 新增成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    @Override
    public int insertIdiomStory(IdiomStory idiomStory)
    {
        return idiomStoryMapper.insertIdiomStory(idiomStory);
    }

    /**
     * 修改成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    @Override
    public int updateIdiomStory(IdiomStory idiomStory)
    {
        return idiomStoryMapper.updateIdiomStory(idiomStory);
    }

    /**
     * 批量删除成语典故
     * 
     * @param ids 需要删除的成语典故主键
     * @return 结果
     */
    @Override
    public int deleteIdiomStoryByIds(Long[] ids)
    {
        return idiomStoryMapper.deleteIdiomStoryByIds(ids);
    }

    /**
     * 删除成语典故信息
     * 
     * @param id 成语典故主键
     * @return 结果
     */
    @Override
    public int deleteIdiomStoryById(Long id)
    {
        return idiomStoryMapper.deleteIdiomStoryById(id);
    }
}
