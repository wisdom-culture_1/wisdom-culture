package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ClassicalExtendMapper;
import com.ruoyi.system.domain.ClassicalExtend;
import com.ruoyi.system.service.IClassicalExtendService;

/**
 * 文言名篇扩展Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ClassicalExtendServiceImpl implements IClassicalExtendService 
{
    @Autowired
    private ClassicalExtendMapper classicalExtendMapper;

    /**
     * 查询文言名篇扩展
     * 
     * @param id 文言名篇扩展主键
     * @return 文言名篇扩展
     */
    @Override
    public ClassicalExtend selectClassicalExtendById(Long id)
    {
        return classicalExtendMapper.selectClassicalExtendById(id);
    }

    /**
     * 查询文言名篇扩展列表
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 文言名篇扩展
     */
    @Override
    public List<ClassicalExtend> selectClassicalExtendList(ClassicalExtend classicalExtend)
    {
        return classicalExtendMapper.selectClassicalExtendList(classicalExtend);
    }

    /**
     * 新增文言名篇扩展
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 结果
     */
    @Override
    public int insertClassicalExtend(ClassicalExtend classicalExtend)
    {
        return classicalExtendMapper.insertClassicalExtend(classicalExtend);
    }

    /**
     * 修改文言名篇扩展
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 结果
     */
    @Override
    public int updateClassicalExtend(ClassicalExtend classicalExtend)
    {
        return classicalExtendMapper.updateClassicalExtend(classicalExtend);
    }

    /**
     * 批量删除文言名篇扩展
     * 
     * @param ids 需要删除的文言名篇扩展主键
     * @return 结果
     */
    @Override
    public int deleteClassicalExtendByIds(Long[] ids)
    {
        return classicalExtendMapper.deleteClassicalExtendByIds(ids);
    }

    /**
     * 删除文言名篇扩展信息
     * 
     * @param id 文言名篇扩展主键
     * @return 结果
     */
    @Override
    public int deleteClassicalExtendById(Long id)
    {
        return classicalExtendMapper.deleteClassicalExtendById(id);
    }
}
