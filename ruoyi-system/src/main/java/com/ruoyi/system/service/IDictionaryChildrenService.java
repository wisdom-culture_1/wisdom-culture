package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.DictionaryChildren;

/**
 * 子字典表Service接口
 * 
 * @author ruoyi
 * @date 2023-08-25
 */
public interface IDictionaryChildrenService 
{
    /**
     * 查询子字典表
     * 
     * @param id 子字典表主键
     * @return 子字典表
     */
    public DictionaryChildren selectDictionaryChildrenById(Integer id);

    /**
     * 查询子字典表列表
     * 
     * @param dictionaryChildren 子字典表
     * @return 子字典表集合
     */
    public List<DictionaryChildren> selectDictionaryChildrenList(DictionaryChildren dictionaryChildren);

    /**
     * 新增子字典表
     * 
     * @param dictionaryChildren 子字典表
     * @return 结果
     */
    public int insertDictionaryChildren(DictionaryChildren dictionaryChildren);

    /**
     * 修改子字典表
     * 
     * @param dictionaryChildren 子字典表
     * @return 结果
     */
    public int updateDictionaryChildren(DictionaryChildren dictionaryChildren);

    /**
     * 批量删除子字典表
     * 
     * @param ids 需要删除的子字典表主键集合
     * @return 结果
     */
    public int deleteDictionaryChildrenByIds(Integer[] ids);

    /**
     * 删除子字典表信息
     * 
     * @param id 子字典表主键
     * @return 结果
     */
    public int deleteDictionaryChildrenById(Integer id);
}
