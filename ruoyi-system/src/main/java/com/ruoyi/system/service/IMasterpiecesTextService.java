package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MasterpiecesText;

/**
 * 名著章节内容Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IMasterpiecesTextService 
{
    /**
     * 查询名著章节内容
     * 
     * @param id 名著章节内容主键
     * @return 名著章节内容
     */
    public MasterpiecesText selectMasterpiecesTextById(Long id);

    /**
     * 查询名著章节内容列表
     * 
     * @param masterpiecesText 名著章节内容
     * @return 名著章节内容集合
     */
    public List<MasterpiecesText> selectMasterpiecesTextList(MasterpiecesText masterpiecesText);

    /**
     * 新增名著章节内容
     * 
     * @param masterpiecesText 名著章节内容
     * @return 结果
     */
    public int insertMasterpiecesText(MasterpiecesText masterpiecesText);

    /**
     * 修改名著章节内容
     * 
     * @param masterpiecesText 名著章节内容
     * @return 结果
     */
    public int updateMasterpiecesText(MasterpiecesText masterpiecesText);

    /**
     * 批量删除名著章节内容
     * 
     * @param ids 需要删除的名著章节内容主键集合
     * @return 结果
     */
    public int deleteMasterpiecesTextByIds(Long[] ids);

    /**
     * 删除名著章节内容信息
     * 
     * @param id 名著章节内容主键
     * @return 结果
     */
    public int deleteMasterpiecesTextById(Long id);
}
