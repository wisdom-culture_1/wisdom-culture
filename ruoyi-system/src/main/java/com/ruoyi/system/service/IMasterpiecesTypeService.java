package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MasterpiecesType;

/**
 * 名著阅读分类Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IMasterpiecesTypeService 
{
    /**
     * 查询名著阅读分类
     * 
     * @param id 名著阅读分类主键
     * @return 名著阅读分类
     */
    public MasterpiecesType selectMasterpiecesTypeById(Long id);

    /**
     * 查询名著阅读分类列表
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 名著阅读分类集合
     */
    public List<MasterpiecesType> selectMasterpiecesTypeList(MasterpiecesType masterpiecesType);

    /**
     * 新增名著阅读分类
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 结果
     */
    public int insertMasterpiecesType(MasterpiecesType masterpiecesType);

    /**
     * 修改名著阅读分类
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 结果
     */
    public int updateMasterpiecesType(MasterpiecesType masterpiecesType);

    /**
     * 批量删除名著阅读分类
     * 
     * @param ids 需要删除的名著阅读分类主键集合
     * @return 结果
     */
    public int deleteMasterpiecesTypeByIds(Long[] ids);

    /**
     * 删除名著阅读分类信息
     * 
     * @param id 名著阅读分类主键
     * @return 结果
     */
    public int deleteMasterpiecesTypeById(Long id);
}
