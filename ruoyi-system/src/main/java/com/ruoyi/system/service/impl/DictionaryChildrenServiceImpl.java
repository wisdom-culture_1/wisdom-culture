package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DictionaryChildrenMapper;
import com.ruoyi.system.domain.DictionaryChildren;
import com.ruoyi.system.service.IDictionaryChildrenService;

/**
 * 子字典表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-25
 */
@Service
public class DictionaryChildrenServiceImpl implements IDictionaryChildrenService 
{
    @Autowired
    private DictionaryChildrenMapper dictionaryChildrenMapper;

    /**
     * 查询子字典表
     * 
     * @param id 子字典表主键
     * @return 子字典表
     */
    @Override
    public DictionaryChildren selectDictionaryChildrenById(Integer id)
    {
        return dictionaryChildrenMapper.selectDictionaryChildrenById(id);
    }

    /**
     * 查询子字典表列表
     * 
     * @param dictionaryChildren 子字典表
     * @return 子字典表
     */
    @Override
    public List<DictionaryChildren> selectDictionaryChildrenList(DictionaryChildren dictionaryChildren)
    {
        return dictionaryChildrenMapper.selectDictionaryChildrenList(dictionaryChildren);
    }

    /**
     * 新增子字典表
     * 
     * @param dictionaryChildren 子字典表
     * @return 结果
     */
    @Override
    public int insertDictionaryChildren(DictionaryChildren dictionaryChildren)
    {
        return dictionaryChildrenMapper.insertDictionaryChildren(dictionaryChildren);
    }

    /**
     * 修改子字典表
     * 
     * @param dictionaryChildren 子字典表
     * @return 结果
     */
    @Override
    public int updateDictionaryChildren(DictionaryChildren dictionaryChildren)
    {
        return dictionaryChildrenMapper.updateDictionaryChildren(dictionaryChildren);
    }

    /**
     * 批量删除子字典表
     * 
     * @param ids 需要删除的子字典表主键
     * @return 结果
     */
    @Override
    public int deleteDictionaryChildrenByIds(Integer[] ids)
    {
        return dictionaryChildrenMapper.deleteDictionaryChildrenByIds(ids);
    }

    /**
     * 删除子字典表信息
     * 
     * @param id 子字典表主键
     * @return 结果
     */
    @Override
    public int deleteDictionaryChildrenById(Integer id)
    {
        return dictionaryChildrenMapper.deleteDictionaryChildrenById(id);
    }
}
