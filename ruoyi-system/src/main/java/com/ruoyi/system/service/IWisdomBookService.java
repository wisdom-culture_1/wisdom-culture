package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.WisdomBook;

/**
 * 智慧元典书籍Service接口
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public interface IWisdomBookService 
{
    /**
     * 查询智慧元典书籍
     * 
     * @param id 智慧元典书籍主键
     * @return 智慧元典书籍
     */
    public WisdomBook selectWisdomBookById(Long id);

    /**
     * 查询智慧元典书籍列表
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 智慧元典书籍集合
     */
    public List<WisdomBook> selectWisdomBookList(WisdomBook wisdomBook);

    /**
     * 新增智慧元典书籍
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 结果
     */
    public int insertWisdomBook(WisdomBook wisdomBook);

    /**
     * 修改智慧元典书籍
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 结果
     */
    public int updateWisdomBook(WisdomBook wisdomBook);

    /**
     * 批量删除智慧元典书籍
     * 
     * @param ids 需要删除的智慧元典书籍主键集合
     * @return 结果
     */
    public int deleteWisdomBookByIds(Long[] ids);

    /**
     * 删除智慧元典书籍信息
     * 
     * @param id 智慧元典书籍主键
     * @return 结果
     */
    public int deleteWisdomBookById(Long id);
}
