package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExcerptTypeMapper;
import com.ruoyi.system.domain.ExcerptType;
import com.ruoyi.system.service.IExcerptTypeService;

/**
 * 金句摘抄分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ExcerptTypeServiceImpl implements IExcerptTypeService 
{
    @Autowired
    private ExcerptTypeMapper excerptTypeMapper;

    /**
     * 查询金句摘抄分类
     * 
     * @param id 金句摘抄分类主键
     * @return 金句摘抄分类
     */
    @Override
    public ExcerptType selectExcerptTypeById(Long id)
    {
        return excerptTypeMapper.selectExcerptTypeById(id);
    }

    /**
     * 查询金句摘抄分类列表
     * 
     * @param excerptType 金句摘抄分类
     * @return 金句摘抄分类
     */
    @Override
    public List<ExcerptType> selectExcerptTypeList(ExcerptType excerptType)
    {
        return excerptTypeMapper.selectExcerptTypeList(excerptType);
    }

    /**
     * 新增金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    @Override
    public int insertExcerptType(ExcerptType excerptType)
    {
        return excerptTypeMapper.insertExcerptType(excerptType);
    }

    /**
     * 修改金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    @Override
    public int updateExcerptType(ExcerptType excerptType)
    {
        return excerptTypeMapper.updateExcerptType(excerptType);
    }

    /**
     * 批量删除金句摘抄分类
     * 
     * @param ids 需要删除的金句摘抄分类主键
     * @return 结果
     */
    @Override
    public int deleteExcerptTypeByIds(Long[] ids)
    {
        return excerptTypeMapper.deleteExcerptTypeByIds(ids);
    }

    /**
     * 删除金句摘抄分类信息
     * 
     * @param id 金句摘抄分类主键
     * @return 结果
     */
    @Override
    public int deleteExcerptTypeById(Long id)
    {
        return excerptTypeMapper.deleteExcerptTypeById(id);
    }
}
