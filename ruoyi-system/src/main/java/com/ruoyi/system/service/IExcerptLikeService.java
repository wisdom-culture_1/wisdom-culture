package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ExcerptLike;

/**
 * 金句摘抄点赞Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IExcerptLikeService 
{
    /**
     * 查询金句摘抄点赞
     * 
     * @param id 金句摘抄点赞主键
     * @return 金句摘抄点赞
     */
    public ExcerptLike selectExcerptLikeById(Long id);

    /**
     * 查询金句摘抄点赞列表
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 金句摘抄点赞集合
     */
    public List<ExcerptLike> selectExcerptLikeList(ExcerptLike excerptLike);

    /**
     * 新增金句摘抄点赞
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 结果
     */
    public int insertExcerptLike(ExcerptLike excerptLike);

    /**
     * 修改金句摘抄点赞
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 结果
     */
    public int updateExcerptLike(ExcerptLike excerptLike);

    /**
     * 批量删除金句摘抄点赞
     * 
     * @param ids 需要删除的金句摘抄点赞主键集合
     * @return 结果
     */
    public int deleteExcerptLikeByIds(Long[] ids);

    /**
     * 删除金句摘抄点赞信息
     * 
     * @param id 金句摘抄点赞主键
     * @return 结果
     */
    public int deleteExcerptLikeById(Long id);
}
