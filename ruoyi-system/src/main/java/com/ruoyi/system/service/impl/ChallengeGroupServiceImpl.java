package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ChallengeGroupMapper;
import com.ruoyi.system.domain.ChallengeGroup;
import com.ruoyi.system.service.IChallengeGroupService;

/**
 * 挑战组别Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ChallengeGroupServiceImpl implements IChallengeGroupService 
{
    @Autowired
    private ChallengeGroupMapper challengeGroupMapper;

    /**
     * 查询挑战组别
     * 
     * @param id 挑战组别主键
     * @return 挑战组别
     */
    @Override
    public ChallengeGroup selectChallengeGroupById(Long id)
    {
        return challengeGroupMapper.selectChallengeGroupById(id);
    }

    /**
     * 查询挑战组别列表
     * 
     * @param challengeGroup 挑战组别
     * @return 挑战组别
     */
    @Override
    public List<ChallengeGroup> selectChallengeGroupList(ChallengeGroup challengeGroup)
    {
        return challengeGroupMapper.selectChallengeGroupList(challengeGroup);
    }

    /**
     * 新增挑战组别
     * 
     * @param challengeGroup 挑战组别
     * @return 结果
     */
    @Override
    public int insertChallengeGroup(ChallengeGroup challengeGroup)
    {
        return challengeGroupMapper.insertChallengeGroup(challengeGroup);
    }

    /**
     * 修改挑战组别
     * 
     * @param challengeGroup 挑战组别
     * @return 结果
     */
    @Override
    public int updateChallengeGroup(ChallengeGroup challengeGroup)
    {
        return challengeGroupMapper.updateChallengeGroup(challengeGroup);
    }

    /**
     * 批量删除挑战组别
     * 
     * @param ids 需要删除的挑战组别主键
     * @return 结果
     */
    @Override
    public int deleteChallengeGroupByIds(Long[] ids)
    {
        return challengeGroupMapper.deleteChallengeGroupByIds(ids);
    }

    /**
     * 删除挑战组别信息
     * 
     * @param id 挑战组别主键
     * @return 结果
     */
    @Override
    public int deleteChallengeGroupById(Long id)
    {
        return challengeGroupMapper.deleteChallengeGroupById(id);
    }
}
