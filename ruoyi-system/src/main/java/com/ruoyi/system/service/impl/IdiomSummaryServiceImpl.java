package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.IdiomSummaryMapper;
import com.ruoyi.system.domain.IdiomSummary;
import com.ruoyi.system.service.IIdiomSummaryService;

/**
 * 成语汇总Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class IdiomSummaryServiceImpl implements IIdiomSummaryService 
{
    @Autowired
    private IdiomSummaryMapper idiomSummaryMapper;

    /**
     * 查询成语汇总
     * 
     * @param id 成语汇总主键
     * @return 成语汇总
     */
    @Override
    public IdiomSummary selectIdiomSummaryById(Long id)
    {
        return idiomSummaryMapper.selectIdiomSummaryById(id);
    }

    /**
     * 查询成语汇总列表
     * 
     * @param idiomSummary 成语汇总
     * @return 成语汇总
     */
    @Override
    public List<IdiomSummary> selectIdiomSummaryList(IdiomSummary idiomSummary)
    {
        return idiomSummaryMapper.selectIdiomSummaryList(idiomSummary);
    }

    /**
     * 新增成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    @Override
    public int insertIdiomSummary(IdiomSummary idiomSummary)
    {
        return idiomSummaryMapper.insertIdiomSummary(idiomSummary);
    }

    /**
     * 修改成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    @Override
    public int updateIdiomSummary(IdiomSummary idiomSummary)
    {
        return idiomSummaryMapper.updateIdiomSummary(idiomSummary);
    }

    /**
     * 批量删除成语汇总
     * 
     * @param ids 需要删除的成语汇总主键
     * @return 结果
     */
    @Override
    public int deleteIdiomSummaryByIds(Long[] ids)
    {
        return idiomSummaryMapper.deleteIdiomSummaryByIds(ids);
    }

    /**
     * 删除成语汇总信息
     * 
     * @param id 成语汇总主键
     * @return 结果
     */
    @Override
    public int deleteIdiomSummaryById(Long id)
    {
        return idiomSummaryMapper.deleteIdiomSummaryById(id);
    }
}
