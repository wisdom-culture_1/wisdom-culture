package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ListenBookMapper;
import com.ruoyi.system.domain.ListenBook;
import com.ruoyi.system.service.IListenBookService;

/**
 * 诗词磨耳朵Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ListenBookServiceImpl implements IListenBookService 
{
    @Autowired
    private ListenBookMapper listenBookMapper;

    /**
     * 查询诗词磨耳朵
     * 
     * @param id 诗词磨耳朵主键
     * @return 诗词磨耳朵
     */
    @Override
    public ListenBook selectListenBookById(Long id)
    {
        return listenBookMapper.selectListenBookById(id);
    }

    /**
     * 查询诗词磨耳朵列表
     * 
     * @param listenBook 诗词磨耳朵
     * @return 诗词磨耳朵
     */
    @Override
    public List<ListenBook> selectListenBookList(ListenBook listenBook)
    {
        return listenBookMapper.selectListenBookList(listenBook);
    }

    /**
     * 新增诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    @Override
    public int insertListenBook(ListenBook listenBook)
    {
        return listenBookMapper.insertListenBook(listenBook);
    }

    /**
     * 修改诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    @Override
    public int updateListenBook(ListenBook listenBook)
    {
        return listenBookMapper.updateListenBook(listenBook);
    }

    /**
     * 批量删除诗词磨耳朵
     * 
     * @param ids 需要删除的诗词磨耳朵主键
     * @return 结果
     */
    @Override
    public int deleteListenBookByIds(Long[] ids)
    {
        return listenBookMapper.deleteListenBookByIds(ids);
    }

    /**
     * 删除诗词磨耳朵信息
     * 
     * @param id 诗词磨耳朵主键
     * @return 结果
     */
    @Override
    public int deleteListenBookById(Long id)
    {
        return listenBookMapper.deleteListenBookById(id);
    }
}
