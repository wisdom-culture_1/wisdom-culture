package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ChallengeGroup;

/**
 * 挑战组别Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IChallengeGroupService 
{
    /**
     * 查询挑战组别
     * 
     * @param id 挑战组别主键
     * @return 挑战组别
     */
    public ChallengeGroup selectChallengeGroupById(Long id);

    /**
     * 查询挑战组别列表
     * 
     * @param challengeGroup 挑战组别
     * @return 挑战组别集合
     */
    public List<ChallengeGroup> selectChallengeGroupList(ChallengeGroup challengeGroup);

    /**
     * 新增挑战组别
     * 
     * @param challengeGroup 挑战组别
     * @return 结果
     */
    public int insertChallengeGroup(ChallengeGroup challengeGroup);

    /**
     * 修改挑战组别
     * 
     * @param challengeGroup 挑战组别
     * @return 结果
     */
    public int updateChallengeGroup(ChallengeGroup challengeGroup);

    /**
     * 批量删除挑战组别
     * 
     * @param ids 需要删除的挑战组别主键集合
     * @return 结果
     */
    public int deleteChallengeGroupByIds(Long[] ids);

    /**
     * 删除挑战组别信息
     * 
     * @param id 挑战组别主键
     * @return 结果
     */
    public int deleteChallengeGroupById(Long id);
}
