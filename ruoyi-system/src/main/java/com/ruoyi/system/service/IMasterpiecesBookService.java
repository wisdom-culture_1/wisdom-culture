package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MasterpiecesBook;

/**
 * 名著书籍Service接口
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public interface IMasterpiecesBookService 
{
    /**
     * 查询名著书籍
     * 
     * @param id 名著书籍主键
     * @return 名著书籍
     */
    public MasterpiecesBook selectMasterpiecesBookById(Long id);

    /**
     * 查询名著书籍列表
     * 
     * @param masterpiecesBook 名著书籍
     * @return 名著书籍集合
     */
    public List<MasterpiecesBook> selectMasterpiecesBookList(MasterpiecesBook masterpiecesBook);

    /**
     * 新增名著书籍
     * 
     * @param masterpiecesBook 名著书籍
     * @return 结果
     */
    public int insertMasterpiecesBook(MasterpiecesBook masterpiecesBook);

    /**
     * 修改名著书籍
     * 
     * @param masterpiecesBook 名著书籍
     * @return 结果
     */
    public int updateMasterpiecesBook(MasterpiecesBook masterpiecesBook);

    /**
     * 批量删除名著书籍
     * 
     * @param ids 需要删除的名著书籍主键集合
     * @return 结果
     */
    public int deleteMasterpiecesBookByIds(Long[] ids);

    /**
     * 删除名著书籍信息
     * 
     * @param id 名著书籍主键
     * @return 结果
     */
    public int deleteMasterpiecesBookById(Long id);
}
