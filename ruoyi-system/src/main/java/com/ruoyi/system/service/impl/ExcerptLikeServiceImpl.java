package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExcerptLikeMapper;
import com.ruoyi.system.domain.ExcerptLike;
import com.ruoyi.system.service.IExcerptLikeService;

/**
 * 金句摘抄点赞Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@Service
public class ExcerptLikeServiceImpl implements IExcerptLikeService 
{
    @Autowired
    private ExcerptLikeMapper excerptLikeMapper;

    /**
     * 查询金句摘抄点赞
     * 
     * @param id 金句摘抄点赞主键
     * @return 金句摘抄点赞
     */
    @Override
    public ExcerptLike selectExcerptLikeById(Long id)
    {
        return excerptLikeMapper.selectExcerptLikeById(id);
    }

    /**
     * 查询金句摘抄点赞列表
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 金句摘抄点赞
     */
    @Override
    public List<ExcerptLike> selectExcerptLikeList(ExcerptLike excerptLike)
    {
        return excerptLikeMapper.selectExcerptLikeList(excerptLike);
    }

    /**
     * 新增金句摘抄点赞
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 结果
     */
    @Override
    public int insertExcerptLike(ExcerptLike excerptLike)
    {
        return excerptLikeMapper.insertExcerptLike(excerptLike);
    }

    /**
     * 修改金句摘抄点赞
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 结果
     */
    @Override
    public int updateExcerptLike(ExcerptLike excerptLike)
    {
        return excerptLikeMapper.updateExcerptLike(excerptLike);
    }

    /**
     * 批量删除金句摘抄点赞
     * 
     * @param ids 需要删除的金句摘抄点赞主键
     * @return 结果
     */
    @Override
    public int deleteExcerptLikeByIds(Long[] ids)
    {
        return excerptLikeMapper.deleteExcerptLikeByIds(ids);
    }

    /**
     * 删除金句摘抄点赞信息
     * 
     * @param id 金句摘抄点赞主键
     * @return 结果
     */
    @Override
    public int deleteExcerptLikeById(Long id)
    {
        return excerptLikeMapper.deleteExcerptLikeById(id);
    }
}
