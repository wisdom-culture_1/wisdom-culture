package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ClassicalNotesMapper;
import com.ruoyi.system.domain.ClassicalNotes;
import com.ruoyi.system.service.IClassicalNotesService;

/**
 * 文言文注释Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class ClassicalNotesServiceImpl implements IClassicalNotesService 
{
    @Autowired
    private ClassicalNotesMapper classicalNotesMapper;

    /**
     * 查询文言文注释
     * 
     * @param id 文言文注释主键
     * @return 文言文注释
     */
    @Override
    public ClassicalNotes selectClassicalNotesById(Long id)
    {
        return classicalNotesMapper.selectClassicalNotesById(id);
    }

    /**
     * 查询文言文注释列表
     * 
     * @param classicalNotes 文言文注释
     * @return 文言文注释
     */
    @Override
    public List<ClassicalNotes> selectClassicalNotesList(ClassicalNotes classicalNotes)
    {
        return classicalNotesMapper.selectClassicalNotesList(classicalNotes);
    }

    /**
     * 新增文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    @Override
    public int insertClassicalNotes(ClassicalNotes classicalNotes)
    {
        return classicalNotesMapper.insertClassicalNotes(classicalNotes);
    }

    /**
     * 修改文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    @Override
    public int updateClassicalNotes(ClassicalNotes classicalNotes)
    {
        return classicalNotesMapper.updateClassicalNotes(classicalNotes);
    }

    /**
     * 批量删除文言文注释
     * 
     * @param ids 需要删除的文言文注释主键
     * @return 结果
     */
    @Override
    public int deleteClassicalNotesByIds(Long[] ids)
    {
        return classicalNotesMapper.deleteClassicalNotesByIds(ids);
    }

    /**
     * 删除文言文注释信息
     * 
     * @param id 文言文注释主键
     * @return 结果
     */
    @Override
    public int deleteClassicalNotesById(Long id)
    {
        return classicalNotesMapper.deleteClassicalNotesById(id);
    }
}
