package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 其他挑战（成语诗词....）对象 challenge_questions
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public class ChallengeQuestions extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 1.字源挑战 2、汉字挑战3、成语挑战、4诗词挑战、5常识挑战、6名著挑战、7三国演义，8西游记，9水浒传，10红楼梦 */
    @Excel(name = "1.字源挑战 2、汉字挑战3、成语挑战、4诗词挑战、5常识挑战、6名著挑战、7三国演义，8西游记，9水浒传，10红楼梦")
    private Long typeId;

    /** 分组id */
    @Excel(name = "分组id")
    private Long groupId;

    /** 题目 */
    @Excel(name = "题目")
    private String question;

    /** 选项 */
    @Excel(name = "选项")
    private String option;

    /** 答案 */
    @Excel(name = "答案")
    private String answer;

    /** 类型1 单选 2多选 */
    @Excel(name = "类型1 单选 2多选")
    private Long type;

    /** 难度等级 */
    @Excel(name = "难度等级")
    private String levels;

    /** 解析 */
    @Excel(name = "解析")
    private String jiexi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }
    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setQuestion(String question) 
    {
        this.question = question;
    }

    public String getQuestion() 
    {
        return question;
    }
    public void setOption(String option) 
    {
        this.option = option;
    }

    public String getOption() 
    {
        return option;
    }
    public void setAnswer(String answer) 
    {
        this.answer = answer;
    }

    public String getAnswer() 
    {
        return answer;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setLevels(String levels) 
    {
        this.levels = levels;
    }

    public String getLevels() 
    {
        return levels;
    }
    public void setJiexi(String jiexi) 
    {
        this.jiexi = jiexi;
    }

    public String getJiexi() 
    {
        return jiexi;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typeId", getTypeId())
            .append("groupId", getGroupId())
            .append("question", getQuestion())
            .append("option", getOption())
            .append("answer", getAnswer())
            .append("type", getType())
            .append("levels", getLevels())
            .append("jiexi", getJiexi())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
