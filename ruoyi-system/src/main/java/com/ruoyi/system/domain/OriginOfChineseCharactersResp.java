package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 汉字起源对象 origin_of_chinese_characters
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public class OriginOfChineseCharactersResp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字典key */
    @Excel(name = "字典key")
    private String dictionaryChildrenKey;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 类别 */
    @Excel(name = "类别")
    private String typeSlug;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String videoSrc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String audioSrc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    private Integer isShow;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDictionaryChildrenKey(String dictionaryChildrenKey) 
    {
        this.dictionaryChildrenKey = dictionaryChildrenKey;
    }

    public String getDictionaryChildrenKey() 
    {
        return dictionaryChildrenKey;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setTypeSlug(String typeSlug) 
    {
        this.typeSlug = typeSlug;
    }

    public String getTypeSlug() 
    {
        return typeSlug;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setVideoSrc(String videoSrc) 
    {
        this.videoSrc = videoSrc;
    }

    public String getVideoSrc() 
    {
        return videoSrc;
    }
    public void setAudioSrc(String audioSrc) 
    {
        this.audioSrc = audioSrc;
    }

    public String getAudioSrc() 
    {
        return audioSrc;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dictionaryChildrenKey", getDictionaryChildrenKey())
            .append("title", getTitle())
            .append("typeSlug", getTypeSlug())
            .append("image", getImage())
            .append("content", getContent())
            .append("videoSrc", getVideoSrc())
            .append("audioSrc", getAudioSrc())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
                .append("isShow", getIsShow())
            .toString();
    }
}
