package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 文言文对象 classical
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public class ClassicalResp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private Long xuhao;

    /** 类型（1.古文初始2.必背名篇3课外扩展） */
    @Excel(name = "类型", readConverterExp = "1=.古文初始2.必背名篇3课外扩展")
    private Long type;

    /** 书籍标题 */
    @Excel(name = "书籍标题")
    private String title;

    /** 作者 */
    @Excel(name = "作者")
    private String name;

    /** 年代 */
    @Excel(name = "年代")
    private String years;

    /** 动画 */
    @Excel(name = "动画")
    private String animationVideoSrc;

    /** 音频 */
    @Excel(name = "音频")
    private String animationAudioSrc;

    /** 译文 */
    @Excel(name = "译文")
    private String translation;

    /** 正文数据 */
    @Excel(name = "正文数据")
    private String content;

    /** 拼音数据 */
    @Excel(name = "拼音数据")
    private String pinyin;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    private Integer isShow;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setXuhao(Long xuhao) 
    {
        this.xuhao = xuhao;
    }

    public Long getXuhao() 
    {
        return xuhao;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setYears(String years) 
    {
        this.years = years;
    }

    public String getYears() 
    {
        return years;
    }
    public void setAnimationVideoSrc(String animationVideoSrc) 
    {
        this.animationVideoSrc = animationVideoSrc;
    }

    public String getAnimationVideoSrc() 
    {
        return animationVideoSrc;
    }
    public void setAnimationAudioSrc(String animationAudioSrc) 
    {
        this.animationAudioSrc = animationAudioSrc;
    }

    public String getAnimationAudioSrc() 
    {
        return animationAudioSrc;
    }
    public void setTranslation(String translation) 
    {
        this.translation = translation;
    }

    public String getTranslation() 
    {
        return translation;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPinyin(String pinyin) 
    {
        this.pinyin = pinyin;
    }

    public String getPinyin() 
    {
        return pinyin;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("xuhao", getXuhao())
            .append("type", getType())
            .append("title", getTitle())
            .append("name", getName())
            .append("years", getYears())
            .append("animationVideoSrc", getAnimationVideoSrc())
            .append("animationAudioSrc", getAnimationAudioSrc())
            .append("translation", getTranslation())
            .append("content", getContent())
            .append("pinyin", getPinyin())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
                .append("isShow", getIsShow())
            .toString();
    }
}
