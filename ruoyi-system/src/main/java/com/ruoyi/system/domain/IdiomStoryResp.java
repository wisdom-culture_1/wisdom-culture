package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 成语典故对象 idiom_story
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public class IdiomStoryResp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字典分类 */
    @Excel(name = "字典分类")
    private String dictionaryChildrenKey;

    /** 成语名称 */
    @Excel(name = "成语名称")
    private String idiom;

    /** 成语拼音 */
    @Excel(name = "成语拼音")
    private String idiomPinyin;

    /** 成语视频 */
    @Excel(name = "成语视频")
    private String idiomVideoSrc;

    /** 成语图片 */
    @Excel(name = "成语图片")
    private String idiomImageSrc;

    /** 成语音频 */
    @Excel(name = "成语音频")
    private String idiomAudioSrc;

    /** 成语释义 */
    @Excel(name = "成语释义")
    private String interpretation;

    /** 成语出处 */
    @Excel(name = "成语出处")
    private String source;

    /** 成语示例 */
    @Excel(name = "成语示例")
    private String examples;

    /** 近义词 */
    @Excel(name = "近义词")
    private String similar;

    /** 反义词 */
    @Excel(name = "反义词")
    private String antonym;

    /** 成语难度级别 */
    @Excel(name = "成语难度级别")
    private Long level;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    private Long isShow;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDictionaryChildrenKey(String dictionaryChildrenKey) 
    {
        this.dictionaryChildrenKey = dictionaryChildrenKey;
    }

    public String getDictionaryChildrenKey() 
    {
        return dictionaryChildrenKey;
    }
    public void setIdiom(String idiom) 
    {
        this.idiom = idiom;
    }

    public String getIdiom() 
    {
        return idiom;
    }
    public void setIdiomPinyin(String idiomPinyin) 
    {
        this.idiomPinyin = idiomPinyin;
    }

    public String getIdiomPinyin() 
    {
        return idiomPinyin;
    }
    public void setIdiomVideoSrc(String idiomVideoSrc) 
    {
        this.idiomVideoSrc = idiomVideoSrc;
    }

    public String getIdiomVideoSrc() 
    {
        return idiomVideoSrc;
    }
    public void setIdiomImageSrc(String idiomImageSrc) 
    {
        this.idiomImageSrc = idiomImageSrc;
    }

    public String getIdiomImageSrc() 
    {
        return idiomImageSrc;
    }
    public void setIdiomAudioSrc(String idiomAudioSrc) 
    {
        this.idiomAudioSrc = idiomAudioSrc;
    }

    public String getIdiomAudioSrc() 
    {
        return idiomAudioSrc;
    }
    public void setInterpretation(String interpretation) 
    {
        this.interpretation = interpretation;
    }

    public String getInterpretation() 
    {
        return interpretation;
    }
    public void setSource(String source) 
    {
        this.source = source;
    }

    public String getSource() 
    {
        return source;
    }
    public void setExamples(String examples) 
    {
        this.examples = examples;
    }

    public String getExamples() 
    {
        return examples;
    }
    public void setSimilar(String similar) 
    {
        this.similar = similar;
    }

    public String getSimilar() 
    {
        return similar;
    }
    public void setAntonym(String antonym) 
    {
        this.antonym = antonym;
    }

    public String getAntonym() 
    {
        return antonym;
    }
    public void setLevel(Long level) 
    {
        this.level = level;
    }

    public Long getLevel() 
    {
        return level;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public Long getIsShow() {
        return isShow;
    }

    public void setIsShow(Long isShow) {
        this.isShow = isShow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dictionaryChildrenKey", getDictionaryChildrenKey())
            .append("idiom", getIdiom())
            .append("idiomPinyin", getIdiomPinyin())
            .append("idiomVideoSrc", getIdiomVideoSrc())
            .append("idiomImageSrc", getIdiomImageSrc())
            .append("idiomAudioSrc", getIdiomAudioSrc())
            .append("interpretation", getInterpretation())
            .append("source", getSource())
            .append("examples", getExamples())
            .append("similar", getSimilar())
            .append("antonym", getAntonym())
            .append("level", getLevel())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
                .append("isShow", getIsShow())
            .toString();
    }
}
