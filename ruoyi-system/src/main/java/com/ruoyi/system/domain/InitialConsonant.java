package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 声母 韵母对象 initial_consonant
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
public class InitialConsonant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字母 */
    @Excel(name = "字母")
    private String letter;

    /** 发音要领 */
    @Excel(name = "发音要领")
    private String pronunciation;

    /** 书写动画 */
    @Excel(name = "书写动画")
    private String pronunciationSrc;

    /** 音频 */
    @Excel(name = "音频")
    private String pronunciationAudioSrc;

    /** 拼一拼 */
    @Excel(name = "拼一拼")
    private String spell;

    /** 拼一拼图片 */
    @Excel(name = "拼一拼图片")
    private String spellSrc;

    /** 读一读 */
    @Excel(name = "读一读")
    private String read;

    /** 发音动画 */
    @Excel(name = "发音动画")
    private String readSrc;

    /** 读一读音频 */
    @Excel(name = "读一读音频")
    private String readAudioSrc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    /** 1 声母 2 韵母 */
    @Excel(name = "1 声母 2 韵母")
    private Integer type;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setLetter(String letter)
    {
        this.letter = letter;
    }

    public String getLetter() 
    {
        return letter;
    }
    public void setPronunciation(String pronunciation) 
    {
        this.pronunciation = pronunciation;
    }

    public String getPronunciation() 
    {
        return pronunciation;
    }
    public void setPronunciationSrc(String pronunciationSrc) 
    {
        this.pronunciationSrc = pronunciationSrc;
    }

    public String getPronunciationSrc() 
    {
        return pronunciationSrc;
    }
    public void setPronunciationAudioSrc(String pronunciationAudioSrc) 
    {
        this.pronunciationAudioSrc = pronunciationAudioSrc;
    }

    public String getPronunciationAudioSrc() 
    {
        return pronunciationAudioSrc;
    }
    public void setSpell(String spell) 
    {
        this.spell = spell;
    }

    public String getSpell() 
    {
        return spell;
    }
    public void setSpellSrc(String spellSrc) 
    {
        this.spellSrc = spellSrc;
    }

    public String getSpellSrc() 
    {
        return spellSrc;
    }
    public void setRead(String read) 
    {
        this.read = read;
    }

    public String getRead() 
    {
        return read;
    }
    public void setReadSrc(String readSrc) 
    {
        this.readSrc = readSrc;
    }

    public String getReadSrc() 
    {
        return readSrc;
    }
    public void setReadAudioSrc(String readAudioSrc) 
    {
        this.readAudioSrc = readAudioSrc;
    }

    public String getReadAudioSrc() 
    {
        return readAudioSrc;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("letter", getLetter())
            .append("pronunciation", getPronunciation())
            .append("pronunciationSrc", getPronunciationSrc())
            .append("pronunciationAudioSrc", getPronunciationAudioSrc())
            .append("spell", getSpell())
            .append("spellSrc", getSpellSrc())
            .append("read", getRead())
            .append("readSrc", getReadSrc())
            .append("readAudioSrc", getReadAudioSrc())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .append("type", getType())
            .toString();
    }
}
