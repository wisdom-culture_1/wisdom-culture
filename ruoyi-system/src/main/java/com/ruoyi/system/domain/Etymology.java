package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 字源对象 etymology
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
public class Etymology extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字典key */
    @Excel(name = "字典key")
    private String dictionaryChildrenKey;

    /** 拼音 */
    @Excel(name = "拼音")
    private String pinyin;

    /** 1文字 2 图片 */
    @Excel(name = "1文字 2 图片")
    private Long type;

    /** 字源 */
    @Excel(name = "字源")
    private String etymology;

    /** 笔画数 */
    @Excel(name = "笔画数")
    private Long numberOfStrokes;

    /** 偏旁 */
    @Excel(name = "偏旁")
    private String side;

    /** 修饰图 */
    @Excel(name = "修饰图")
    private String modificationImageSrc;

    /** 演变图 */
    @Excel(name = "演变图")
    private String evolutionImageSrc;

    /** 视频 */
    @Excel(name = "视频")
    private String videoSrc;

    /** 解释 */
    @Excel(name = "解释")
    private String explain;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDictionaryChildrenKey(String dictionaryChildrenKey) 
    {
        this.dictionaryChildrenKey = dictionaryChildrenKey;
    }

    public String getDictionaryChildrenKey() 
    {
        return dictionaryChildrenKey;
    }
    public void setPinyin(String pinyin) 
    {
        this.pinyin = pinyin;
    }

    public String getPinyin() 
    {
        return pinyin;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setEtymology(String etymology) 
    {
        this.etymology = etymology;
    }

    public String getEtymology() 
    {
        return etymology;
    }
    public void setNumberOfStrokes(Long numberOfStrokes) 
    {
        this.numberOfStrokes = numberOfStrokes;
    }

    public Long getNumberOfStrokes() 
    {
        return numberOfStrokes;
    }
    public void setSide(String side) 
    {
        this.side = side;
    }

    public String getSide() 
    {
        return side;
    }
    public void setModificationImageSrc(String modificationImageSrc) 
    {
        this.modificationImageSrc = modificationImageSrc;
    }

    public String getModificationImageSrc() 
    {
        return modificationImageSrc;
    }
    public void setEvolutionImageSrc(String evolutionImageSrc) 
    {
        this.evolutionImageSrc = evolutionImageSrc;
    }

    public String getEvolutionImageSrc() 
    {
        return evolutionImageSrc;
    }
    public void setVideoSrc(String videoSrc) 
    {
        this.videoSrc = videoSrc;
    }

    public String getVideoSrc() 
    {
        return videoSrc;
    }
    public void setExplain(String explain) 
    {
        this.explain = explain;
    }

    public String getExplain() 
    {
        return explain;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dictionaryChildrenKey", getDictionaryChildrenKey())
            .append("pinyin", getPinyin())
            .append("type", getType())
            .append("etymology", getEtymology())
            .append("numberOfStrokes", getNumberOfStrokes())
            .append("side", getSide())
            .append("modificationImageSrc", getModificationImageSrc())
            .append("evolutionImageSrc", getEvolutionImageSrc())
            .append("videoSrc", getVideoSrc())
            .append("explain", getExplain())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
