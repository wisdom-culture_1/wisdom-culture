package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.Users;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface BizUsersMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Users selectUsersById(String id);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public Users selectUserByUserName(String userName);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param users 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Users> selectUsersList(Users users);

    public List<Users> getIdsNotInNewSystem();

    /**
     * 新增【请填写功能名称】
     * 
     * @param users 【请填写功能名称】
     * @return 结果
     */
    public int insertUsers(Users users);

    /**
     * 修改【请填写功能名称】
     * 
     * @param users 【请填写功能名称】
     * @return 结果
     */
    public int updateUsers(Users users);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteUsersById(String id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUsersByIds(String[] ids);


    void updateUsersByUserName(Users users);
}
