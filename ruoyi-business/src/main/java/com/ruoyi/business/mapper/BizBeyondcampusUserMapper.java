package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.BeyondcampusUser;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizBeyondcampusUserMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BeyondcampusUser selectBeyondcampusUserById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BeyondcampusUser> selectBeyondcampusUserList(BeyondcampusUser beyondcampusUser);

    /**
     * 新增【请填写功能名称】
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 结果
     */
    public int insertBeyondcampusUser(BeyondcampusUser beyondcampusUser);

    /**
     * 修改【请填写功能名称】
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 结果
     */
    public int updateBeyondcampusUser(BeyondcampusUser beyondcampusUser);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBeyondcampusUserById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBeyondcampusUserByIds(Long[] ids);
}
