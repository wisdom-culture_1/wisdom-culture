package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.Share;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizShareMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Share selectShareById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param share 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Share> selectShareList(Share share);

    /**
     * 新增【请填写功能名称】
     * 
     * @param share 【请填写功能名称】
     * @return 结果
     */
    public int insertShare(Share share);

    /**
     * 修改【请填写功能名称】
     * 
     * @param share 【请填写功能名称】
     * @return 结果
     */
    public int updateShare(Share share);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteShareById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteShareByIds(Long[] ids);
}
