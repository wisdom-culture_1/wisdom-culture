package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.WisdomExtend;

import java.util.List;

/**
 * 智慧元典扩展Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizWisdomExtendMapper
{
    /**
     * 查询智慧元典扩展
     * 
     * @param id 智慧元典扩展主键
     * @return 智慧元典扩展
     */
    public WisdomExtend selectWisdomExtendById(Long id);

    /**
     * 查询智慧元典扩展列表
     * 
     * @param wisdomExtend 智慧元典扩展
     * @return 智慧元典扩展集合
     */
    public List<WisdomExtend> selectWisdomExtendList(WisdomExtend wisdomExtend);

    /**
     * 新增智慧元典扩展
     * 
     * @param wisdomExtend 智慧元典扩展
     * @return 结果
     */
    public int insertWisdomExtend(WisdomExtend wisdomExtend);

    /**
     * 修改智慧元典扩展
     * 
     * @param wisdomExtend 智慧元典扩展
     * @return 结果
     */
    public int updateWisdomExtend(WisdomExtend wisdomExtend);

    /**
     * 删除智慧元典扩展
     * 
     * @param id 智慧元典扩展主键
     * @return 结果
     */
    public int deleteWisdomExtendById(Long id);

    /**
     * 批量删除智慧元典扩展
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWisdomExtendByIds(Long[] ids);
}
