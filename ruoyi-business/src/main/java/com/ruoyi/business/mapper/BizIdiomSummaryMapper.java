package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.entity.IdiomSummary;

/**
 * 成语汇总Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface BizIdiomSummaryMapper
{
    /**
     * 查询成语汇总
     * 
     * @param id 成语汇总主键
     * @return 成语汇总
     */
    public IdiomSummary selectIdiomSummaryById(Long id);

    Integer selectIdiomSummaryUserIdAndStatus(IdiomSummary idiomSummary);


    public IdiomSummary selectIdiomSummaryIdomIdAndUserId(IdiomSummary idiomSummary);

    public Integer selectIdiomSummaryByUserId(IdiomSummary idiomSummary);

    /**
     * 查询成语汇总列表
     * 
     * @param idiomSummary 成语汇总
     * @return 成语汇总集合
     */
    public List<IdiomSummary> selectIdiomSummaryList(IdiomSummary idiomSummary);

    /**
     * 新增成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    public int insertIdiomSummary(IdiomSummary idiomSummary);

    /**
     * 修改成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    public int updateIdiomSummary(IdiomSummary idiomSummary);

    /**
     * 修改成语汇总
     *
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    public int updateIdiomSummaryByIdiomIdAndUserId(IdiomSummary idiomSummary);
    /**
     * 删除成语汇总
     * 
     * @param id 成语汇总主键
     * @return 结果
     */
    public int deleteIdiomSummaryById(Long id);

    /**
     * 批量删除成语汇总
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIdiomSummaryByIds(Long[] ids);
}
