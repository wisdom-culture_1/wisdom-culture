package com.ruoyi.business.mapper;


import com.ruoyi.business.entity.StudyPlanModuleStayTime;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface StudyPlanModuleStayTimeMapper
{
    StudyPlanModuleStayTime selectStudyPlanModuleStayTimeVo(StudyPlanModuleStayTime studyPlanModuleStayTime);

    void insertStudyPlanModuleStayTime(StudyPlanModuleStayTime studyPlanModuleStayTime);
}
