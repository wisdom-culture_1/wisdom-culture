package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.WisdomNotes;

import java.util.List;

/**
 * 智慧元典注释Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizWisdomNotesMapper
{
    /**
     * 查询智慧元典注释
     * 
     * @param id 智慧元典注释主键
     * @return 智慧元典注释
     */
    public WisdomNotes selectWisdomNotesById(Long id);

    /**
     * 查询智慧元典注释列表
     * 
     * @param wisdomNotes 智慧元典注释
     * @return 智慧元典注释集合
     */
    public List<WisdomNotes> selectWisdomNotesList(WisdomNotes wisdomNotes);

    /**
     * 新增智慧元典注释
     * 
     * @param wisdomNotes 智慧元典注释
     * @return 结果
     */
    public int insertWisdomNotes(WisdomNotes wisdomNotes);

    /**
     * 修改智慧元典注释
     * 
     * @param wisdomNotes 智慧元典注释
     * @return 结果
     */
    public int updateWisdomNotes(WisdomNotes wisdomNotes);

    /**
     * 删除智慧元典注释
     * 
     * @param id 智慧元典注释主键
     * @return 结果
     */
    public int deleteWisdomNotesById(Long id);

    /**
     * 批量删除智慧元典注释
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWisdomNotesByIds(Long[] ids);
}
