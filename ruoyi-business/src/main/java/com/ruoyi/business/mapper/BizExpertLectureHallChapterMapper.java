package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.ExpertLectureHallChapter;

import java.util.List;

/**
 * 专家讲堂章节Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizExpertLectureHallChapterMapper
{
    /**
     * 查询专家讲堂章节
     * 
     * @param id 专家讲堂章节主键
     * @return 专家讲堂章节
     */
    public ExpertLectureHallChapter selectExpertLectureHallChapterById(Long id);

    /**
     * 查询专家讲堂章节列表
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 专家讲堂章节集合
     */
    public List<ExpertLectureHallChapter> selectExpertLectureHallChapterList(ExpertLectureHallChapter expertLectureHallChapter);

    /**
     * 新增专家讲堂章节
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 结果
     */
    public int insertExpertLectureHallChapter(ExpertLectureHallChapter expertLectureHallChapter);

    /**
     * 修改专家讲堂章节
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 结果
     */
    public int updateExpertLectureHallChapter(ExpertLectureHallChapter expertLectureHallChapter);

    /**
     * 删除专家讲堂章节
     * 
     * @param id 专家讲堂章节主键
     * @return 结果
     */
    public int deleteExpertLectureHallChapterById(Long id);

    /**
     * 批量删除专家讲堂章节
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExpertLectureHallChapterByIds(Long[] ids);
}
