package com.ruoyi.business.mapper;


import com.ruoyi.business.entity.GradePlan;
import com.ruoyi.business.entity.GradeStudyPlan;

import java.util.List;

/**
 * 学习计划Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-14
 */
public interface GradeStudyPlanMapper 
{
    /**
     * 查询学习计划
     * 
     * @param id 学习计划主键
     * @return 学习计划
     */
    public GradeStudyPlan selectGradeStudyPlanById(String id);

    /**
     * 查询学习计划列表
     * 
     * @param gradeStudyPlan 学习计划
     * @return 学习计划集合
     */
    public List<GradeStudyPlan> selectGradeStudyPlanList(GradeStudyPlan gradeStudyPlan);

    /**
     * 新增学习计划
     * 
     * @param gradeStudyPlan 学习计划
     * @return 结果
     */
    public int insertGradeStudyPlan(GradeStudyPlan gradeStudyPlan);

    /**
     * 修改学习计划
     * 
     * @param gradeStudyPlan 学习计划
     * @return 结果
     */
    public int updateGradeStudyPlan(GradeStudyPlan gradeStudyPlan);

    /**
     * 删除学习计划
     * 
     * @param id 学习计划主键
     * @return 结果
     */
    public int deleteGradeStudyPlanById(String id);

    /**
     * 批量删除学习计划
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGradeStudyPlanByIds(String[] ids);

    /**
     * 批量删除学习计划
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGradeStudyPlanByIds(Long ids);

    int deleteGradeStudyPlanByGrade(GradePlan gradePlan);
}
