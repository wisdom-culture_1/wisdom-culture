package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.entity.ModuleDict;

/**
 * 模块对应关系字典Mapper接口
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public interface ModuleDictMapper 
{
    /**
     * 查询模块对应关系字典
     * 
     * @param id 模块对应关系字典主键
     * @return 模块对应关系字典
     */
    public ModuleDict selectModuleDictById(Long id);

    /**
     * 查询模块对应关系字典列表
     * 
     * @param moduleDict 模块对应关系字典
     * @return 模块对应关系字典集合
     */
    public List<ModuleDict> selectModuleDictList(ModuleDict moduleDict);

    /**
     * 新增模块对应关系字典
     * 
     * @param moduleDict 模块对应关系字典
     * @return 结果
     */
    public int insertModuleDict(ModuleDict moduleDict);

    /**
     * 修改模块对应关系字典
     * 
     * @param moduleDict 模块对应关系字典
     * @return 结果
     */
    public int updateModuleDict(ModuleDict moduleDict);

    /**
     * 删除模块对应关系字典
     * 
     * @param id 模块对应关系字典主键
     * @return 结果
     */
    public int deleteModuleDictById(Long id);

    /**
     * 批量删除模块对应关系字典
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteModuleDictByIds(Long[] ids);
}
