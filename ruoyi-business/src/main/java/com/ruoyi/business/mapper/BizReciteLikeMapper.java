package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.ReciteLike;

import java.util.List;

/**
 * 背诵打卡点赞Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizReciteLikeMapper
{
    /**
     * 查询背诵打卡点赞
     * 
     * @param id 背诵打卡点赞主键
     * @return 背诵打卡点赞
     */
    public ReciteLike selectReciteLikeById(Long id);

    /**
     * 查询背诵打卡点赞
     *
     * @param reciteLike 背诵打卡
     * @return 背诵打卡点赞
     */
    public ReciteLike selectReciteLikeById(ReciteLike reciteLike);

    public Integer selectReciteLikeCount(ReciteLike reciteLike);

    /**
     * 查询背诵打卡点赞
     *
     * @param reciteLike 背诵打卡
     * @return 背诵打卡点赞
     */
    public ReciteLike selectReciteLikeByUserIdAndReciteId(ReciteLike reciteLike);

    /**
     * 查询背诵打卡点赞列表
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 背诵打卡点赞集合
     */
    public List<ReciteLike> selectReciteLikeList(ReciteLike reciteLike);

    /**
     * 新增背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    public int insertReciteLike(ReciteLike reciteLike);

    /**
     * 修改背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    public int updateReciteLike(ReciteLike reciteLike);

    /**
     * 修改背诵打卡点赞
     *
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    public int updateReciteLikeByReciteIdAndUserId(ReciteLike reciteLike);


    /**
     * 删除背诵打卡点赞
     * 
     * @param id 背诵打卡点赞主键
     * @return 结果
     */
    public int deleteReciteLikeById(Long id);

    /**
     * 批量删除背诵打卡点赞
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReciteLikeByIds(Long[] ids);
}
