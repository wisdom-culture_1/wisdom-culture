package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.entity.ReciteType;

/**
 * 背诵打卡类别Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizReciteTypeMapper
{
    /**
     * 查询背诵打卡类别
     * 
     * @param id 背诵打卡类别主键
     * @return 背诵打卡类别
     */
    public ReciteType selectReciteTypeById(Long id);

    /**
     * 查询背诵打卡类别列表
     * 
     * @param reciteType 背诵打卡类别
     * @return 背诵打卡类别集合
     */
    public List<ReciteType> selectReciteTypeList(ReciteType reciteType);

    /**
     * 新增背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    public int insertReciteType(ReciteType reciteType);

    /**
     * 修改背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    public int updateReciteType(ReciteType reciteType);

    /**
     * 删除背诵打卡类别
     * 
     * @param id 背诵打卡类别主键
     * @return 结果
     */
    public int deleteReciteTypeById(Long id);

    /**
     * 批量删除背诵打卡类别
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReciteTypeByIds(Long[] ids);
}
