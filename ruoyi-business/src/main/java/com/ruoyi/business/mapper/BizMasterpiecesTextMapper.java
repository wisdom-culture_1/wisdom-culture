package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.MasterpiecesText;

import java.util.List;

/**
 * 名著章节Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizMasterpiecesTextMapper
{
    /**
     * 查询名著章节
     * 
     * @param id 名著章节主键
     * @return 名著章节
     */
    public MasterpiecesText selectMasterpiecesTextById(Long id);

    /**
     * 查询名著章节列表
     * 
     * @param masterpiecesText 名著章节
     * @return 名著章节集合
     */
    public List<MasterpiecesText> selectMasterpiecesTextList(MasterpiecesText masterpiecesText);

    /**
     * 查询名著章节列表
     *
     * @param masterpiecesText 名著章节
     * @return 名著章节集合
     */
    public List<MasterpiecesText> selectMasterpiecesTextListWithoutBigText(MasterpiecesText masterpiecesText);

    /**
     * 新增名著章节
     * 
     * @param masterpiecesText 名著章节
     * @return 结果
     */
    public int insertMasterpiecesText(MasterpiecesText masterpiecesText);

    /**
     * 修改名著章节
     * 
     * @param masterpiecesText 名著章节
     * @return 结果
     */
    public int updateMasterpiecesText(MasterpiecesText masterpiecesText);

    /**
     * 删除名著章节
     * 
     * @param id 名著章节主键
     * @return 结果
     */
    public int deleteMasterpiecesTextById(Long id);

    /**
     * 批量删除名著章节
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMasterpiecesTextByIds(Long[] ids);

    List<MasterpiecesText> selectMasterpiecesTextByChapterId(MasterpiecesText masterpiecesText);
}
