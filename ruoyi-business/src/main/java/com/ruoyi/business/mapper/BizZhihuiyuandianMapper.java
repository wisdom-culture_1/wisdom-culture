package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.Zhihuiyuandian;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizZhihuiyuandianMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Zhihuiyuandian selectZhihuiyuandianById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zhihuiyuandian 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Zhihuiyuandian> selectZhihuiyuandianList(Zhihuiyuandian zhihuiyuandian);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zhihuiyuandian 【请填写功能名称】
     * @return 结果
     */
    public int insertZhihuiyuandian(Zhihuiyuandian zhihuiyuandian);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zhihuiyuandian 【请填写功能名称】
     * @return 结果
     */
    public int updateZhihuiyuandian(Zhihuiyuandian zhihuiyuandian);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZhihuiyuandianById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZhihuiyuandianByIds(Long[] ids);
}
