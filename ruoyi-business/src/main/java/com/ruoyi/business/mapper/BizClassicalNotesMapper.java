package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.ClassicalNotes;

import java.util.List;

/**
 * 文言文注释Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizClassicalNotesMapper
{
    /**
     * 查询文言文注释
     * 
     * @param id 文言文注释主键
     * @return 文言文注释
     */
    public ClassicalNotes selectClassicalNotesById(Long id);

    /**
     * 查询文言文注释列表
     * 
     * @param classicalNotes 文言文注释
     * @return 文言文注释集合
     */
    public List<ClassicalNotes> selectClassicalNotesList(ClassicalNotes classicalNotes);

    /**
     * 新增文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    public int insertClassicalNotes(ClassicalNotes classicalNotes);

    /**
     * 修改文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    public int updateClassicalNotes(ClassicalNotes classicalNotes);

    /**
     * 删除文言文注释
     * 
     * @param id 文言文注释主键
     * @return 结果
     */
    public int deleteClassicalNotesById(Long id);

    /**
     * 批量删除文言文注释
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteClassicalNotesByIds(Long[] ids);
}
