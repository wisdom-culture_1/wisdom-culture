package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.MyNotes;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizMyNotesMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MyNotes selectMyNotesById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param myNotes 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MyNotes> selectMyNotesList(MyNotes myNotes);

    /**
     * 新增【请填写功能名称】
     * 
     * @param myNotes 【请填写功能名称】
     * @return 结果
     */
    public int insertMyNotes(MyNotes myNotes);

    /**
     * 修改【请填写功能名称】
     * 
     * @param myNotes 【请填写功能名称】
     * @return 结果
     */
    public int updateMyNotes(MyNotes myNotes);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMyNotesById(String id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMyNotesByIds(Long[] ids);
}
