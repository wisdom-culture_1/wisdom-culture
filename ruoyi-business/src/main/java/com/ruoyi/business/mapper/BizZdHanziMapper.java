package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.entity.ZdHanzi;
import com.ruoyi.business.entity.ZdZbh;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface BizZdHanziMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ZdHanzi selectZdHanziByRefid(Long refid);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZdHanzi> selectZdHanziList(ZdHanzi zdHanzi);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 结果
     */
    public int insertZdHanzi(ZdHanzi zdHanzi);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 结果
     */
    public int updateZdHanzi(ZdHanzi zdHanzi);

    /**
     * 删除【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZdHanziByRefid(Long refid);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param refids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZdHanziByRefids(Long[] refids);

    List<ZdHanzi> selectStrokeCharacter(ZdZbh zdZbh);
}
