package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.ChapterProcess;

/**
 * 名著书籍Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizChapterProcessMapper
{

    public ChapterProcess selectChapterProcess(ChapterProcess process);

    public int insertChapterProcess(ChapterProcess process);

    public int updateChapterProcess(ChapterProcess process);
}
