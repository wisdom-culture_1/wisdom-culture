package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.CultureText;

import java.util.List;

/**
 * 中华文化内容Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizCultureTextMapper
{
    /**
     * 查询中华文化内容
     * 
     * @param id 中华文化内容主键
     * @return 中华文化内容
     */
    public CultureText selectCultureTextById(Long id);

    /**
     * 查询中华文化内容列表
     * 
     * @param cultureText 中华文化内容
     * @return 中华文化内容集合
     */
    public List<CultureText> selectCultureTextList(CultureText cultureText);

    /**
     * 新增中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    public int insertCultureText(CultureText cultureText);

    /**
     * 修改中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    public int updateCultureText(CultureText cultureText);

    /**
     * 删除中华文化内容
     * 
     * @param id 中华文化内容主键
     * @return 结果
     */
    public int deleteCultureTextById(Long id);

    /**
     * 批量删除中华文化内容
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCultureTextByIds(Long[] ids);
}
