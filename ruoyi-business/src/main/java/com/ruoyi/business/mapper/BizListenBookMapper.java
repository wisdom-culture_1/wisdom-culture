package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.ListenBook;

import java.util.List;

/**
 * 诗词磨耳朵Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizListenBookMapper
{
    /**
     * 查询诗词磨耳朵
     * 
     * @param id 诗词磨耳朵主键
     * @return 诗词磨耳朵
     */
    public ListenBook selectListenBookById(Long id);

    /**
     * 查询诗词磨耳朵列表
     * 
     * @param listenBook 诗词磨耳朵
     * @return 诗词磨耳朵集合
     */

    public List<ListenBook> selectListenBookList(ListenBook listenBook);

    /**
     * 新增诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    public int insertListenBook(ListenBook listenBook);

    /**
     * 修改诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    public int updateListenBook(ListenBook listenBook);

    /**
     * 删除诗词磨耳朵
     * 
     * @param id 诗词磨耳朵主键
     * @return 结果
     */
    public int deleteListenBookById(Long id);

    /**
     * 批量删除诗词磨耳朵
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteListenBookByIds(Long[] ids);
}
