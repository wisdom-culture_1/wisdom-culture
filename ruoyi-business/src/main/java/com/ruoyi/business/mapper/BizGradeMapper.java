package com.ruoyi.business.mapper;

import java.util.List;

import com.ruoyi.business.entity.Grade;

/**
 * 年级Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-12
 */
public interface BizGradeMapper
{
    /**
     * 查询年级
     * 
     * @param id 年级主键
     * @return 年级
     */
    public Grade selectGradeById(Long id);

    /**
     * 查询年级列表
     * 
     * @param grade 年级
     * @return 年级集合
     */
    public List<Grade> selectGradeList(Grade grade);

    /**
     * 新增年级
     * 
     * @param grade 年级
     * @return 结果
     */
    public int insertGrade(Grade grade);

    /**
     * 修改年级
     * 
     * @param grade 年级
     * @return 结果
     */
    public int updateGrade(Grade grade);

    /**
     * 删除年级
     * 
     * @param id 年级主键
     * @return 结果
     */
    public int deleteGradeById(Long id);

    /**
     * 批量删除年级
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGradeByIds(Long[] ids);
}
