package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.ZdZbh;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizZdZbhMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ZdZbh selectZdZbhByXuhao(Long xuhao);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdZbh 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZdZbh> selectZdZbhList(ZdZbh zdZbh);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdZbh 【请填写功能名称】
     * @return 结果
     */
    public int insertZdZbh(ZdZbh zdZbh);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdZbh 【请填写功能名称】
     * @return 结果
     */
    public int updateZdZbh(ZdZbh zdZbh);

    /**
     * 删除【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZdZbhByXuhao(Long xuhao);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param xuhaos 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZdZbhByXuhaos(Long[] xuhaos);
}
