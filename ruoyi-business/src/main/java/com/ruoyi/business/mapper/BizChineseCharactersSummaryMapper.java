package com.ruoyi.business.mapper;

import java.util.List;

import com.ruoyi.business.entity.ChineseCharactersSummary;

/**
 * 文字汇总Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface BizChineseCharactersSummaryMapper
{
    /**
     * 查询文字汇总
     * 
     * @param id 文字汇总主键
     * @return 文字汇总
     */
    public ChineseCharactersSummary selectChineseCharactersSummaryById(Long id);

    /**
     * 查询文字汇总
     *
     * @param chineseCharactersSummary 查询文字汇总
     * @return 文字汇总
     */
    public ChineseCharactersSummary selectByRefIdAndCharacId(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 查询文字汇总
     *
     * @param chineseCharactersSummary 查询文字汇总
     * @return 文字汇总
     */
    public Integer selectByUserId(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 查询文字汇总列表
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 文字汇总集合
     */
    public List<ChineseCharactersSummary> selectChineseCharactersSummaryList(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 新增文字汇总
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 结果
     */
    public int insertChineseCharactersSummary(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 修改文字汇总
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 结果
     */
    public int updateChineseCharactersSummary(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 修改文字汇总
     *
     * @param chineseCharactersSummary 文字汇总
     * @return 结果
     */
    public int updateByRefIdAndCharacId(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 删除文字汇总
     * 
     * @param id 文字汇总主键
     * @return 结果
     */
    public int deleteChineseCharactersSummaryById(Long id);

    /**
     * 根据用户删除文字汇总
     *
     * @param userId 用户id
     * @return 结果
     */
    public int deleteChineseCharactersSummaryByUserId(Long userId);

    /**
     * 批量删除文字汇总
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChineseCharactersSummaryByIds(Long[] ids);
}
