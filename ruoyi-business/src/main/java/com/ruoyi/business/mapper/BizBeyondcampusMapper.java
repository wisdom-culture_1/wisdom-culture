package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.Beyondcampus;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizBeyondcampusMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Beyondcampus selectBeyondcampusById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param beyondcampus 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Beyondcampus> selectBeyondcampusList(Beyondcampus beyondcampus);

    /**
     * 新增【请填写功能名称】
     * 
     * @param beyondcampus 【请填写功能名称】
     * @return 结果
     */
    public int insertBeyondcampus(Beyondcampus beyondcampus);

    /**
     * 修改【请填写功能名称】
     * 
     * @param beyondcampus 【请填写功能名称】
     * @return 结果
     */
    public int updateBeyondcampus(Beyondcampus beyondcampus);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBeyondcampusById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBeyondcampusByIds(Long[] ids);
}
