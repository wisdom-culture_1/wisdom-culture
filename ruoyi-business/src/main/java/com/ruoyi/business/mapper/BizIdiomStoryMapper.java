package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.entity.IdiomStory;
import com.ruoyi.business.entity.IdiomSummary;

/**
 * 成语典故Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface BizIdiomStoryMapper
{
    /**
     * 查询成语典故
     * 
     * @param id 成语典故主键
     * @return 成语典故
     */
    public IdiomStory selectIdiomStoryById(Long id);

    /**
     * 查询成语典故列表
     * 
     * @param idiomStory 成语典故
     * @return 成语典故集合
     */
    public List<IdiomStory> selectIdiomStoryList(IdiomStory idiomStory);

    /**
     * 新增成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    public int insertIdiomStory(IdiomStory idiomStory);

    /**
     * 修改成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    public int updateIdiomStory(IdiomStory idiomStory);

    /**
     * 删除成语典故
     * 
     * @param id 成语典故主键
     * @return 结果
     */
    public int deleteIdiomStoryById(Long id);

    /**
     * 批量删除成语典故
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIdiomStoryByIds(Long[] ids);

    List<IdiomStory> selectIdiomStoryJoinSummaryByStatus(IdiomSummary idiomSummary);
}
