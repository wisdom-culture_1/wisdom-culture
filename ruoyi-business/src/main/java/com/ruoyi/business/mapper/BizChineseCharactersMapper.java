package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.entity.ChineseCharacters;
import org.apache.ibatis.annotations.Param;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface BizChineseCharactersMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ChineseCharacters selectChineseCharactersByRefid(Long refid);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ChineseCharacters> selectChineseCharactersList(ChineseCharacters chineseCharacters);

    /**
     * 新增【请填写功能名称】
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 结果
     */
    public int insertChineseCharacters(ChineseCharacters chineseCharacters);

    /**
     * 修改【请填写功能名称】
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 结果
     */
    public int updateChineseCharacters(ChineseCharacters chineseCharacters);

    /**
     * 删除【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteChineseCharactersByRefid(Long refid);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param refids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChineseCharactersByRefids(Long[] refids);

    public int selectCountFromCharacter();

    public List<Integer> selectAllFromCharacter(@Param("character") String character);

    List<ChineseCharacters> selectChineseCharactersByRefids(List<Integer> list);

    ChineseCharacters characterRefidByCharacter(String zi);
}
