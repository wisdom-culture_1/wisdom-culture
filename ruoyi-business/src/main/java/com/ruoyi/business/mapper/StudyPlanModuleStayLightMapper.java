package com.ruoyi.business.mapper;


import com.ruoyi.business.entity.StudyPlanModuleLight;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface StudyPlanModuleStayLightMapper
{
    List<StudyPlanModuleLight> selectStudyPlanModuleLight(StudyPlanModuleLight studyPlanModuleLight);
    List<StudyPlanModuleLight> selectStudyPlanModuleLightByUserId(StudyPlanModuleLight studyPlanModuleLight);

    void insertStudyPlanModuleLight(StudyPlanModuleLight studyPlanModuleLight);

    void updateStudyPlanModuleLight(StudyPlanModuleLight studyPlanModuleLight);

}
