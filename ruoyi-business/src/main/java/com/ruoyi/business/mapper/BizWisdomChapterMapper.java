package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.WisdomChapter;
import com.ruoyi.business.entity.WisdomChapterResp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 智慧元典章节Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizWisdomChapterMapper
{
    /**
     * 查询智慧元典章节
     * 
     * @param id 智慧元典章节主键
     * @return 智慧元典章节
     */
    public WisdomChapter selectWisdomChapterById(Long id);

    /**
     * 查询智慧元典章节列表
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 智慧元典章节集合
     */
    public List<WisdomChapter> selectWisdomChapterList(WisdomChapter wisdomChapter);

    /**
     * 查询智慧元典章节列表（仅列表）
     *
     * @param wisdomChapter 智慧元典章节
     * @return 智慧元典章节集合
     */
    public List<WisdomChapter> selectWisdomChapterListWithoutBigText(WisdomChapter wisdomChapter);

    /**
     * 新增智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    public int insertWisdomChapter(WisdomChapter wisdomChapter);

    /**
     * 修改智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    public int updateWisdomChapter(WisdomChapter wisdomChapter);

    /**
     * 删除智慧元典章节
     * 
     * @param id 智慧元典章节主键
     * @return 结果
     */
    public int deleteWisdomChapterById(Long id);

    /**
     * 批量删除智慧元典章节
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWisdomChapterByIds(Long[] ids);

    List<WisdomChapterResp> selectWisdomChapterListAll(@Param("gradeId") Long gradeId,
                                                       @Param("module") Long module,
                                                       @Param("childModule") Long childModule,
                                                       @Param("type") Long type,
                                                       @Param("detailName")String detailName
                                                       );

    List<WisdomChapter> selectWisdomChapterListIsDynamic(@Param("gradeId") Long gradeId,
                                                         @Param("module") Long module,
                                                         @Param("childModule") Long childModule,
                                                         @Param("type") Long type,
                                                         @Param("detailName")String detailName);
}
