package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.Textbook;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizTextbookMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Textbook selectTextbookById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param textbook 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Textbook> selectTextbookList(Textbook textbook);

    /**
     * 新增【请填写功能名称】
     * 
     * @param textbook 【请填写功能名称】
     * @return 结果
     */
    public int insertTextbook(Textbook textbook);

    /**
     * 修改【请填写功能名称】
     * 
     * @param textbook 【请填写功能名称】
     * @return 结果
     */
    public int updateTextbook(Textbook textbook);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTextbookById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTextbookByIds(Long[] ids);
}
