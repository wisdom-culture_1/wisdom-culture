package com.ruoyi.business.mapper;

import java.util.List;
import com.ruoyi.business.entity.ZdBushou;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface BizZdBushouMapper
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ZdBushou selectZdBushouByXuhao(Long xuhao);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdBushou 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZdBushou> selectZdBushouList(ZdBushou zdBushou);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdBushou 【请填写功能名称】
     * @return 结果
     */
    public int insertZdBushou(ZdBushou zdBushou);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdBushou 【请填写功能名称】
     * @return 结果
     */
    public int updateZdBushou(ZdBushou zdBushou);

    /**
     * 删除【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZdBushouByXuhao(Long xuhao);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param xuhaos 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZdBushouByXuhaos(Long[] xuhaos);
}
