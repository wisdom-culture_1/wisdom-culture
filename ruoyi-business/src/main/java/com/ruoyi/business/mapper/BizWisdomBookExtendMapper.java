package com.ruoyi.business.mapper;

import com.ruoyi.business.entity.WisdomBookExtend;

import java.util.List;

/**
 * 智慧元典书籍扩展Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface BizWisdomBookExtendMapper
{
    /**
     * 查询智慧元典书籍扩展
     * 
     * @param id 智慧元典书籍扩展主键
     * @return 智慧元典书籍扩展
     */
    public WisdomBookExtend selectWisdomBookExtendById(Long id);

    /**
     * 查询智慧元典书籍扩展列表
     * 
     * @param wisdomBookExtend 智慧元典书籍扩展
     * @return 智慧元典书籍扩展集合
     */
    public List<WisdomBookExtend> selectWisdomBookExtendList(WisdomBookExtend wisdomBookExtend);

    /**
     * 新增智慧元典书籍扩展
     * 
     * @param wisdomBookExtend 智慧元典书籍扩展
     * @return 结果
     */
    public int insertWisdomBookExtend(WisdomBookExtend wisdomBookExtend);

    /**
     * 修改智慧元典书籍扩展
     * 
     * @param wisdomBookExtend 智慧元典书籍扩展
     * @return 结果
     */
    public int updateWisdomBookExtend(WisdomBookExtend wisdomBookExtend);

    /**
     * 删除智慧元典书籍扩展
     * 
     * @param id 智慧元典书籍扩展主键
     * @return 结果
     */
    public int deleteWisdomBookExtendById(Long id);

    /**
     * 批量删除智慧元典书籍扩展
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWisdomBookExtendByIds(Long[] ids);
}
