package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ChineseCharacters;
import com.ruoyi.business.mapper.BizChineseCharactersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizZdBushouMapper;
import com.ruoyi.business.entity.ZdBushou;
import com.ruoyi.business.service.IZdBushouService;
import org.springframework.util.CollectionUtils;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizZdBushouServiceImpl implements IZdBushouService
{
    @Autowired
    private BizZdBushouMapper bizZdBushouMapper;

    @Autowired
    private BizChineseCharactersMapper bizChineseCharactersMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ZdBushou selectZdBushouByXuhao(Long xuhao)
    {
        return bizZdBushouMapper.selectZdBushouByXuhao(xuhao);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdBushou 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ZdBushou> selectZdBushouList(ZdBushou zdBushou)
    {
        List<ZdBushou> zdBushous = bizZdBushouMapper.selectZdBushouList(zdBushou);
        zdBushous.forEach(item -> {
            ChineseCharacters chineseCharacters = new ChineseCharacters();
            chineseCharacters.setCharacter(item.getZi());
            List<ChineseCharacters> chineseCharacters1 = bizChineseCharactersMapper.selectChineseCharactersList(chineseCharacters);
            if(!CollectionUtils.isEmpty(chineseCharacters1)){
                item.setRefid(String.valueOf(chineseCharacters1.get(0).getRefid()));
            }
        });
        return zdBushous;
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdBushou 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZdBushou(ZdBushou zdBushou)
    {
        return bizZdBushouMapper.insertZdBushou(zdBushou);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdBushou 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZdBushou(ZdBushou zdBushou)
    {
        return bizZdBushouMapper.updateZdBushou(zdBushou);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param xuhaos 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdBushouByXuhaos(Long[] xuhaos)
    {
        return bizZdBushouMapper.deleteZdBushouByXuhaos(xuhaos);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdBushouByXuhao(Long xuhao)
    {
        return bizZdBushouMapper.deleteZdBushouByXuhao(xuhao);
    }
}
