package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Classical;
import com.ruoyi.business.entity.ClassicalNotes;
import com.ruoyi.business.entity.ClassicalText;
import com.ruoyi.business.mapper.BizClassicalMapper;
import com.ruoyi.business.mapper.BizClassicalNotesMapper;
import com.ruoyi.business.mapper.BizClassicalTextMapper;
import com.ruoyi.business.service.IClassicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * 文言文Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizClassicalServiceImpl implements IClassicalService
{
    @Autowired
    private BizClassicalMapper bizClassicalMapper;
    @Autowired
    private BizClassicalNotesMapper bizClassicalNotesMapper;
    @Autowired
    private BizClassicalTextMapper bizClassicalTextMapper;

    /**
     * 查询文言文
     * 
     * @param id 文言文主键
     * @return 文言文
     */
    @Override
    public Classical selectClassicalById(Long id)
    {
        return bizClassicalMapper.selectClassicalById(id);
    }

    /**
     * 查询文言文列表
     * 
     * @param classical 文言文
     * @return 文言文
     */
    @Override
    public List<Classical> selectClassicalList(Classical classical)
    {
        return bizClassicalMapper.selectClassicalList(classical);
    }

    @Override
    public Classical classicalDetail(Classical classical) {
        List<Classical> classicals = bizClassicalMapper.selectClassicalList(classical);
        Classical classical1 = null;
        if(!CollectionUtils.isEmpty(classicals)){
            classical1 = classicals.get(0);
            ClassicalNotes classicalNotes = new ClassicalNotes();
            classicalNotes.setXuhao(classical.getXuhao());
            List<ClassicalNotes> classicalNotes1 = bizClassicalNotesMapper.selectClassicalNotesList(classicalNotes);
            classical1.setClassicalnotes(classicalNotes1);

            ClassicalText classicalText = new ClassicalText();
            classicalText.setXuhao(classical.getXuhao());
            List<ClassicalText> classicalTexts = bizClassicalTextMapper.selectClassicalTextList(classicalText);
            classical1.setClassicaltext(classicalTexts);
        }
        return classical1;
    }

    /**
     * 新增文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    @Override
    public int insertClassical(Classical classical)
    {
        return bizClassicalMapper.insertClassical(classical);
    }

    /**
     * 修改文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    @Override
    public int updateClassical(Classical classical)
    {
        return bizClassicalMapper.updateClassical(classical);
    }

    /**
     * 批量删除文言文
     * 
     * @param ids 需要删除的文言文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalByIds(Long[] ids)
    {
        return bizClassicalMapper.deleteClassicalByIds(ids);
    }

    /**
     * 删除文言文信息
     * 
     * @param id 文言文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalById(Long id)
    {
        return bizClassicalMapper.deleteClassicalById(id);
    }
}
