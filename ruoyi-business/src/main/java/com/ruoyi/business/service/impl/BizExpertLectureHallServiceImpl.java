package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ExpertLectureHall;
import com.ruoyi.business.mapper.BizExpertLectureHallMapper;
import com.ruoyi.business.service.IExpertLectureHallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 专家讲堂视频Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizExpertLectureHallServiceImpl implements IExpertLectureHallService
{
    @Autowired
    private BizExpertLectureHallMapper bizExpertLectureHallMapper;

    /**
     * 查询专家讲堂视频
     * 
     * @param id 专家讲堂视频主键
     * @return 专家讲堂视频
     */
    @Override
    public ExpertLectureHall selectExpertLectureHallById(Long id)
    {
        return bizExpertLectureHallMapper.selectExpertLectureHallById(id);
    }

    /**
     * 查询专家讲堂视频列表
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 专家讲堂视频
     */
    @Override
    public List<ExpertLectureHall> selectExpertLectureHallList(ExpertLectureHall expertLectureHall)
    {
        List<ExpertLectureHall> expertLectureHalls = bizExpertLectureHallMapper.selectExpertLectureHallList(expertLectureHall);
        expertLectureHalls.forEach(item -> {
            item.setImageSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+item.getImageSrc());
        });

        return expertLectureHalls;
    }

    /**
     * 新增专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    @Override
    public int insertExpertLectureHall(ExpertLectureHall expertLectureHall)
    {
        return bizExpertLectureHallMapper.insertExpertLectureHall(expertLectureHall);
    }

    /**
     * 修改专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    @Override
    public int updateExpertLectureHall(ExpertLectureHall expertLectureHall)
    {
        return bizExpertLectureHallMapper.updateExpertLectureHall(expertLectureHall);
    }

    /**
     * 批量删除专家讲堂视频
     * 
     * @param ids 需要删除的专家讲堂视频主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallByIds(Long[] ids)
    {
        return bizExpertLectureHallMapper.deleteExpertLectureHallByIds(ids);
    }

    /**
     * 删除专家讲堂视频信息
     * 
     * @param id 专家讲堂视频主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallById(Long id)
    {
        return bizExpertLectureHallMapper.deleteExpertLectureHallById(id);
    }
}
