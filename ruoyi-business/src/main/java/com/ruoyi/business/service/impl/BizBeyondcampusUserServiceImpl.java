package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Beyondcampus;
import com.ruoyi.business.entity.BeyondcampusUnion;
import com.ruoyi.business.entity.BeyondcampusUser;
import com.ruoyi.business.mapper.BizBeyondcampusMapper;
import com.ruoyi.business.mapper.BizBeyondcampusUserMapper;
import com.ruoyi.business.service.IBeyondcampusUserService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizBeyondcampusUserServiceImpl implements IBeyondcampusUserService
{
    @Autowired
    private BizBeyondcampusUserMapper bizBeyondcampusUserMapper;


    @Autowired
    private BizBeyondcampusMapper bizBeyondcampusMapper;
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public BeyondcampusUser selectBeyondcampusUserById(Long id)
    {
        return bizBeyondcampusUserMapper.selectBeyondcampusUserById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * /mynotes
     * @param beyondcampusUser 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<BeyondcampusUser> selectBeyondcampusUserList(BeyondcampusUser beyondcampusUser)
    {
        return bizBeyondcampusUserMapper.selectBeyondcampusUserList(beyondcampusUser);
    }

    @Override
    public BeyondcampusUnion selectBeyondcampusAndMyList(BeyondcampusUser beyondcampusUser) {
        BeyondcampusUnion beyondcampusUnion = new BeyondcampusUnion();
        // todo userid
        beyondcampusUser.setuId(SecurityUtils.getUserId());
        List<BeyondcampusUser> beyondcampusUsers = bizBeyondcampusUserMapper.selectBeyondcampusUserList(beyondcampusUser);
        beyondcampusUnion.setModule(beyondcampusUsers);

        Beyondcampus beyondcampus = new Beyondcampus();
        beyondcampus.setGradeId(beyondcampusUser.getGradeId());
        List<Beyondcampus> beyondcampuses = bizBeyondcampusMapper.selectBeyondcampusList(beyondcampus);
        beyondcampusUnion.setList(beyondcampuses);
        return beyondcampusUnion;
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBeyondcampusUser(BeyondcampusUser beyondcampusUser)
    {
        return bizBeyondcampusUserMapper.insertBeyondcampusUser(beyondcampusUser);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBeyondcampusUser(BeyondcampusUser beyondcampusUser)
    {
        return bizBeyondcampusUserMapper.updateBeyondcampusUser(beyondcampusUser);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBeyondcampusUserByIds(Long[] ids)
    {
        return bizBeyondcampusUserMapper.deleteBeyondcampusUserByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBeyondcampusUserById(Long id)
    {
        return bizBeyondcampusUserMapper.deleteBeyondcampusUserById(id);
    }
}
