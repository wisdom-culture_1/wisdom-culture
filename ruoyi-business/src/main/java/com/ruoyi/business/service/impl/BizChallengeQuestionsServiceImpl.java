package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ChallengeQuestions;
import com.ruoyi.business.mapper.BizChallengeCompleteMapper;
import com.ruoyi.business.mapper.BizChallengeQuestionsMapper;
import com.ruoyi.business.service.IChallengeQuestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 其他挑战（成语诗词....）Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@Service
public class BizChallengeQuestionsServiceImpl implements IChallengeQuestionsService
{
    @Autowired
    private BizChallengeQuestionsMapper bizChallengeQuestionsMapper;

    @Autowired
    private BizChallengeCompleteMapper challengeCompleteMapper;

    /**
     * 查询其他挑战（成语诗词....）
     * 
     * @param id 其他挑战（成语诗词....）主键
     * @return 其他挑战（成语诗词....）
     */
    @Override
    public ChallengeQuestions selectChallengeQuestionsById(Long id)
    {
        return bizChallengeQuestionsMapper.selectChallengeQuestionsById(id);
    }

    /**
     * 查询其他挑战（成语诗词....）列表
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 其他挑战（成语诗词....）
     */
    @Override
    public List<ChallengeQuestions> selectChallengeQuestionsList(ChallengeQuestions challengeQuestions)
    {
        return bizChallengeQuestionsMapper.selectChallengeQuestionsList(challengeQuestions);
    }

    /**
     * 新增其他挑战（成语诗词....）
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 结果
     */
    @Override
    public int insertChallengeQuestions(ChallengeQuestions challengeQuestions)
    {
        return bizChallengeQuestionsMapper.insertChallengeQuestions(challengeQuestions);
    }

    /**
     * 修改其他挑战（成语诗词....）
     * 
     * @param challengeQuestions 其他挑战（成语诗词....）
     * @return 结果
     */
    @Override
    public int updateChallengeQuestions(ChallengeQuestions challengeQuestions)
    {
        return bizChallengeQuestionsMapper.updateChallengeQuestions(challengeQuestions);
    }

    /**
     * 批量删除其他挑战（成语诗词....）
     * 
     * @param ids 需要删除的其他挑战（成语诗词....）主键
     * @return 结果
     */
    @Override
    public int deleteChallengeQuestionsByIds(Long[] ids)
    {
        return bizChallengeQuestionsMapper.deleteChallengeQuestionsByIds(ids);
    }

    /**
     * 删除其他挑战（成语诗词....）信息
     * 
     * @param id 其他挑战（成语诗词....）主键
     * @return 结果
     */
    @Override
    public int deleteChallengeQuestionsById(Long id)
    {
        return bizChallengeQuestionsMapper.deleteChallengeQuestionsById(id);
    }
}
