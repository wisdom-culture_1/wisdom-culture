package com.ruoyi.business.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.business.entity.CharacterSummaryRsp;
import com.ruoyi.business.entity.ChineseCharacters;
import com.ruoyi.business.entity.NewWordsSummaryRsp;

import javax.xml.stream.events.Characters;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IChineseCharactersService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ChineseCharacters selectChineseCharactersByRefid(Long refid);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ChineseCharacters> selectChineseCharactersList(ChineseCharacters chineseCharacters);

    /**
     * 新增【请填写功能名称】
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 结果
     */
    public int insertChineseCharacters(ChineseCharacters chineseCharacters);

    /**
     * 修改【请填写功能名称】
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 结果
     */
    public int updateChineseCharacters(ChineseCharacters chineseCharacters);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param refids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteChineseCharactersByRefids(Long[] refids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param refid 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteChineseCharactersByRefid(Long refid);


    public NewWordsSummaryRsp selectNewWordListSummary(String between, String level);

    public NewWordsSummaryRsp selectNewWordListSummaryAll(Long gradeId,
                                                          Long module,
                                                          Long childModule);

    public NewWordsSummaryRsp selectNewWordListSummaryIsDynamic(Long gradeId,
                                                          Long module,
                                                          Long childModule);

    public List<ChineseCharacters> selectNewWordsList(String between,Integer status,String key);

}
