package com.ruoyi.business.service;

import com.ruoyi.business.entity.Img;

import java.util.List;

/**
 * 图片Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IImgService 
{
    /**
     * 查询图片
     * 
     * @param id 图片主键
     * @return 图片
     */
    public Img selectImgById(Long id);

    /**
     * 查询图片列表
     * 
     * @param img 图片
     * @return 图片集合
     */
    public List<Img> selectImgList(Img img);

    /**
     * 新增图片
     * 
     * @param img 图片
     * @return 结果
     */
    public int insertImg(Img img);

    /**
     * 修改图片
     * 
     * @param img 图片
     * @return 结果
     */
    public int updateImg(Img img);

    /**
     * 批量删除图片
     * 
     * @param ids 需要删除的图片主键集合
     * @return 结果
     */
    public int deleteImgByIds(Long[] ids);

    /**
     * 删除图片信息
     * 
     * @param id 图片主键
     * @return 结果
     */
    public int deleteImgById(Long id);
}
