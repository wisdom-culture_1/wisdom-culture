package com.ruoyi.business.service;

import com.ruoyi.business.entity.PoetrySense;

import java.util.List;

/**
 * 诗词常识Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IPoetrySenseService 
{
    /**
     * 查询诗词常识
     * 
     * @param id 诗词常识主键
     * @return 诗词常识
     */
    public PoetrySense selectPoetrySenseById(Long id);

    /**
     * 查询诗词常识列表
     * 
     * @param poetrySense 诗词常识
     * @return 诗词常识集合
     */
    public List<PoetrySense> selectPoetrySenseList(PoetrySense poetrySense);

    /**
     * 新增诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    public int insertPoetrySense(PoetrySense poetrySense);

    /**
     * 修改诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    public int updatePoetrySense(PoetrySense poetrySense);

    /**
     * 批量删除诗词常识
     * 
     * @param ids 需要删除的诗词常识主键集合
     * @return 结果
     */
    public int deletePoetrySenseByIds(Long[] ids);

    /**
     * 删除诗词常识信息
     * 
     * @param id 诗词常识主键
     * @return 结果
     */
    public int deletePoetrySenseById(Long id);
}
