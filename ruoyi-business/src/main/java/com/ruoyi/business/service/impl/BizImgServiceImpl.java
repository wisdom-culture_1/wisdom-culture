package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Img;
import com.ruoyi.business.mapper.BizImgMapper;
import com.ruoyi.business.service.IImgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 图片Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizImgServiceImpl implements IImgService
{
    @Autowired
    private BizImgMapper bizImgMapper;

    /**
     * 查询图片
     * 
     * @param id 图片主键
     * @return 图片
     */
    @Override
    public Img selectImgById(Long id)
    {
        return bizImgMapper.selectImgById(id);
    }

    /**
     * 查询图片列表
     * 
     * @param img 图片
     * @return 图片
     */
    @Override
    public List<Img> selectImgList(Img img)
    {
        return bizImgMapper.selectImgList(img);
    }

    /**
     * 新增图片
     * 
     * @param img 图片
     * @return 结果
     */
    @Override
    public int insertImg(Img img)
    {
        return bizImgMapper.insertImg(img);
    }

    /**
     * 修改图片
     * 
     * @param img 图片
     * @return 结果
     */
    @Override
    public int updateImg(Img img)
    {
        return bizImgMapper.updateImg(img);
    }

    /**
     * 批量删除图片
     * 
     * @param ids 需要删除的图片主键
     * @return 结果
     */
    @Override
    public int deleteImgByIds(Long[] ids)
    {
        return bizImgMapper.deleteImgByIds(ids);
    }

    /**
     * 删除图片信息
     * 
     * @param id 图片主键
     * @return 结果
     */
    @Override
    public int deleteImgById(Long id)
    {
        return bizImgMapper.deleteImgById(id);
    }
}
