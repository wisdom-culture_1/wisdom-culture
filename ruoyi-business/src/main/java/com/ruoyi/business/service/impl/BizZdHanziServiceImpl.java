package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ZdZbh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizZdHanziMapper;
import com.ruoyi.business.entity.ZdHanzi;
import com.ruoyi.business.service.IZdHanziService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizZdHanziServiceImpl implements IZdHanziService
{
    @Autowired
    private BizZdHanziMapper bizZdHanziMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ZdHanzi selectZdHanziByRefid(Long refid)
    {
        return bizZdHanziMapper.selectZdHanziByRefid(refid);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ZdHanzi> selectZdHanziList(ZdHanzi zdHanzi)
    {
        return bizZdHanziMapper.selectZdHanziList(zdHanzi);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZdHanzi(ZdHanzi zdHanzi)
    {
        return bizZdHanziMapper.insertZdHanzi(zdHanzi);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZdHanzi(ZdHanzi zdHanzi)
    {
        return bizZdHanziMapper.updateZdHanzi(zdHanzi);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param refids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdHanziByRefids(Long[] refids)
    {
        return bizZdHanziMapper.deleteZdHanziByRefids(refids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param refid 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdHanziByRefid(Long refid)
    {
        return bizZdHanziMapper.deleteZdHanziByRefid(refid);
    }

    @Override
    public List<ZdHanzi> selectStrokeCharacter(String begin, String bihua) {

        ZdZbh zdZbh = new ZdZbh();
        zdZbh.setBegin(Long.valueOf(begin));
        zdZbh.setBihua(Long.valueOf(bihua));

        return bizZdHanziMapper.selectStrokeCharacter(zdZbh);
    }
}
