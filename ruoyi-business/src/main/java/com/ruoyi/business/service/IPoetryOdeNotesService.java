package com.ruoyi.business.service;

import com.ruoyi.business.entity.PoetryOdeNotes;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IPoetryOdeNotesService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public PoetryOdeNotes selectPoetryOdeNotesById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param poetryOdeNotes 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<PoetryOdeNotes> selectPoetryOdeNotesList(PoetryOdeNotes poetryOdeNotes);

    /**
     * 新增【请填写功能名称】
     * 
     * @param poetryOdeNotes 【请填写功能名称】
     * @return 结果
     */
    public int insertPoetryOdeNotes(PoetryOdeNotes poetryOdeNotes);

    /**
     * 修改【请填写功能名称】
     * 
     * @param poetryOdeNotes 【请填写功能名称】
     * @return 结果
     */
    public int updatePoetryOdeNotes(PoetryOdeNotes poetryOdeNotes);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deletePoetryOdeNotesByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deletePoetryOdeNotesById(Long id);
}
