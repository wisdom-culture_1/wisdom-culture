package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Share;
import com.ruoyi.business.mapper.BizShareMapper;
import com.ruoyi.business.service.IShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizShareServiceImpl implements IShareService
{
    @Autowired
    private BizShareMapper bizShareMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Share selectShareById(Long id)
    {
        return bizShareMapper.selectShareById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param share 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Share> selectShareList(Share share)
    {
        return bizShareMapper.selectShareList(share);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param share 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertShare(Share share)
    {
        return bizShareMapper.insertShare(share);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param share 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateShare(Share share)
    {
        return bizShareMapper.updateShare(share);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteShareByIds(Long[] ids)
    {
        return bizShareMapper.deleteShareByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteShareById(Long id)
    {
        return bizShareMapper.deleteShareById(id);
    }
}
