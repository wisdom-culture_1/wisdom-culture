package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ClassicalText;
import com.ruoyi.business.mapper.BizClassicalTextMapper;
import com.ruoyi.business.service.IClassicalTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 文言文正文Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizClassicalTextServiceImpl implements IClassicalTextService
{
    @Autowired
    private BizClassicalTextMapper bizClassicalTextMapper;

    /**
     * 查询文言文正文
     * 
     * @param id 文言文正文主键
     * @return 文言文正文
     */
    @Override
    public ClassicalText selectClassicalTextById(Long id)
    {
        return bizClassicalTextMapper.selectClassicalTextById(id);
    }

    /**
     * 查询文言文正文列表
     * 
     * @param classicalText 文言文正文
     * @return 文言文正文
     */
    @Override
    public List<ClassicalText> selectClassicalTextList(ClassicalText classicalText)
    {
        return bizClassicalTextMapper.selectClassicalTextList(classicalText);
    }

    /**
     * 新增文言文正文
     * 
     * @param classicalText 文言文正文
     * @return 结果
     */
    @Override
    public int insertClassicalText(ClassicalText classicalText)
    {
        return bizClassicalTextMapper.insertClassicalText(classicalText);
    }

    /**
     * 修改文言文正文
     * 
     * @param classicalText 文言文正文
     * @return 结果
     */
    @Override
    public int updateClassicalText(ClassicalText classicalText)
    {
        return bizClassicalTextMapper.updateClassicalText(classicalText);
    }

    /**
     * 批量删除文言文正文
     * 
     * @param ids 需要删除的文言文正文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalTextByIds(Long[] ids)
    {
        return bizClassicalTextMapper.deleteClassicalTextByIds(ids);
    }

    /**
     * 删除文言文正文信息
     * 
     * @param id 文言文正文主键
     * @return 结果
     */
    @Override
    public int deleteClassicalTextById(Long id)
    {
        return bizClassicalTextMapper.deleteClassicalTextById(id);
    }
}
