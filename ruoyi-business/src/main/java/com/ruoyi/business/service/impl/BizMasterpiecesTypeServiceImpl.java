package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.MasterpiecesType;
import com.ruoyi.business.mapper.BizMasterpiecesTypeMapper;
import com.ruoyi.business.service.IMasterpiecesTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 名著阅读分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizMasterpiecesTypeServiceImpl implements IMasterpiecesTypeService
{
    @Autowired
    private BizMasterpiecesTypeMapper bizMasterpiecesTypeMapper;

    /**
     * 查询名著阅读分类
     * 
     * @param id 名著阅读分类主键
     * @return 名著阅读分类
     */
    @Override
    public MasterpiecesType selectMasterpiecesTypeById(Long id)
    {
        return bizMasterpiecesTypeMapper.selectMasterpiecesTypeById(id);
    }

    /**
     * 查询名著阅读分类列表
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 名著阅读分类
     */
    @Override
    public List<MasterpiecesType> selectMasterpiecesTypeList(MasterpiecesType masterpiecesType)
    {
        return bizMasterpiecesTypeMapper.selectMasterpiecesTypeList(masterpiecesType);
    }

    /**
     * 新增名著阅读分类
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 结果
     */
    @Override
    public int insertMasterpiecesType(MasterpiecesType masterpiecesType)
    {
        return bizMasterpiecesTypeMapper.insertMasterpiecesType(masterpiecesType);
    }

    /**
     * 修改名著阅读分类
     * 
     * @param masterpiecesType 名著阅读分类
     * @return 结果
     */
    @Override
    public int updateMasterpiecesType(MasterpiecesType masterpiecesType)
    {
        return bizMasterpiecesTypeMapper.updateMasterpiecesType(masterpiecesType);
    }

    /**
     * 批量删除名著阅读分类
     * 
     * @param ids 需要删除的名著阅读分类主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTypeByIds(Long[] ids)
    {
        return bizMasterpiecesTypeMapper.deleteMasterpiecesTypeByIds(ids);
    }

    /**
     * 删除名著阅读分类信息
     * 
     * @param id 名著阅读分类主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTypeById(Long id)
    {
        return bizMasterpiecesTypeMapper.deleteMasterpiecesTypeById(id);
    }
}
