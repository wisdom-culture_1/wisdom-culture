package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.WisdomBookExtend;
import com.ruoyi.business.mapper.BizWisdomBookExtendMapper;
import com.ruoyi.business.service.IWisdomBookExtendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 智慧元典书籍扩展Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizWisdomBookExtendServiceImpl implements IWisdomBookExtendService
{
    @Autowired
    private BizWisdomBookExtendMapper bizWisdomBookExtendMapper;

    /**
     * 查询智慧元典书籍扩展
     * 
     * @param id 智慧元典书籍扩展主键
     * @return 智慧元典书籍扩展
     */
    @Override
    public WisdomBookExtend selectWisdomBookExtendById(Long id)
    {
        return bizWisdomBookExtendMapper.selectWisdomBookExtendById(id);
    }

    /**
     * 查询智慧元典书籍扩展列表
     * 
     * @param wisdomBookExtend 智慧元典书籍扩展
     * @return 智慧元典书籍扩展
     */
    @Override
    public List<WisdomBookExtend> selectWisdomBookExtendList(WisdomBookExtend wisdomBookExtend)
    {
        return bizWisdomBookExtendMapper.selectWisdomBookExtendList(wisdomBookExtend);
    }

    /**
     * 新增智慧元典书籍扩展
     * 
     * @param wisdomBookExtend 智慧元典书籍扩展
     * @return 结果
     */
    @Override
    public int insertWisdomBookExtend(WisdomBookExtend wisdomBookExtend)
    {
        return bizWisdomBookExtendMapper.insertWisdomBookExtend(wisdomBookExtend);
    }

    /**
     * 修改智慧元典书籍扩展
     * 
     * @param wisdomBookExtend 智慧元典书籍扩展
     * @return 结果
     */
    @Override
    public int updateWisdomBookExtend(WisdomBookExtend wisdomBookExtend)
    {
        return bizWisdomBookExtendMapper.updateWisdomBookExtend(wisdomBookExtend);
    }

    /**
     * 批量删除智慧元典书籍扩展
     * 
     * @param ids 需要删除的智慧元典书籍扩展主键
     * @return 结果
     */
    @Override
    public int deleteWisdomBookExtendByIds(Long[] ids)
    {
        return bizWisdomBookExtendMapper.deleteWisdomBookExtendByIds(ids);
    }

    /**
     * 删除智慧元典书籍扩展信息
     * 
     * @param id 智慧元典书籍扩展主键
     * @return 结果
     */
    @Override
    public int deleteWisdomBookExtendById(Long id)
    {
        return bizWisdomBookExtendMapper.deleteWisdomBookExtendById(id);
    }
}
