package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Zhihuiyuandian;
import com.ruoyi.business.mapper.BizZhihuiyuandianMapper;
import com.ruoyi.business.service.IZhihuiyuandianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizZhihuiyuandianServiceImpl implements IZhihuiyuandianService
{
    @Autowired
    private BizZhihuiyuandianMapper bizZhihuiyuandianMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Zhihuiyuandian selectZhihuiyuandianById(Long id)
    {
        return bizZhihuiyuandianMapper.selectZhihuiyuandianById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zhihuiyuandian 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Zhihuiyuandian> selectZhihuiyuandianList(Zhihuiyuandian zhihuiyuandian)
    {
        return bizZhihuiyuandianMapper.selectZhihuiyuandianList(zhihuiyuandian);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zhihuiyuandian 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZhihuiyuandian(Zhihuiyuandian zhihuiyuandian)
    {
        return bizZhihuiyuandianMapper.insertZhihuiyuandian(zhihuiyuandian);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zhihuiyuandian 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZhihuiyuandian(Zhihuiyuandian zhihuiyuandian)
    {
        return bizZhihuiyuandianMapper.updateZhihuiyuandian(zhihuiyuandian);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZhihuiyuandianByIds(Long[] ids)
    {
        return bizZhihuiyuandianMapper.deleteZhihuiyuandianByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZhihuiyuandianById(Long id)
    {
        return bizZhihuiyuandianMapper.deleteZhihuiyuandianById(id);
    }
}
