package com.ruoyi.business.service;

import com.ruoyi.business.entity.Classical;

import java.util.List;

/**
 * 文言文Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IClassicalService 
{
    /**
     * 查询文言文
     * 
     * @param id 文言文主键
     * @return 文言文
     */
    public Classical selectClassicalById(Long id);

    /**
     * 查询文言文列表
     * 
     * @param classical 文言文
     * @return 文言文集合
     */
    public List<Classical> selectClassicalList(Classical classical);

    /**
     * 查询文言文列表
     *
     * @param classical 文言文
     * @return 文言文集合
     */
    public Classical classicalDetail(Classical classical);

    /**
     * 新增文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    public int insertClassical(Classical classical);

    /**
     * 修改文言文
     * 
     * @param classical 文言文
     * @return 结果
     */
    public int updateClassical(Classical classical);

    /**
     * 批量删除文言文
     * 
     * @param ids 需要删除的文言文主键集合
     * @return 结果
     */
    public int deleteClassicalByIds(Long[] ids);

    /**
     * 删除文言文信息
     * 
     * @param id 文言文主键
     * @return 结果
     */
    public int deleteClassicalById(Long id);
}
