package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.MasterpiecesText;
import com.ruoyi.business.mapper.BizMasterpiecesTextMapper;
import com.ruoyi.business.service.IMasterpiecesTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 名著章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizMasterpiecesTextServiceImpl implements IMasterpiecesTextService
{
    @Autowired
    private BizMasterpiecesTextMapper bizMasterpiecesTextMapper;

    /**
     * 查询名著章节
     * 
     * @param id 名著章节主键
     * @return 名著章节
     */
    @Override
    public MasterpiecesText selectMasterpiecesTextById(Long id)
    {
        return bizMasterpiecesTextMapper.selectMasterpiecesTextById(id);
    }

    /**
     * 查询名著章节列表
     * 
     * @param masterpiecesText 名著章节
     * @return 名著章节
     */
    @Override
    public List<MasterpiecesText> selectMasterpiecesTextList(MasterpiecesText masterpiecesText)
    {
        return bizMasterpiecesTextMapper.selectMasterpiecesTextByChapterId(masterpiecesText);
    }

    /**
     * 新增名著章节
     * 
     * @param masterpiecesText 名著章节
     * @return 结果
     */
    @Override
    public int insertMasterpiecesText(MasterpiecesText masterpiecesText)
    {
        return bizMasterpiecesTextMapper.insertMasterpiecesText(masterpiecesText);
    }

    /**
     * 修改名著章节
     * 
     * @param masterpiecesText 名著章节
     * @return 结果
     */
    @Override
    public int updateMasterpiecesText(MasterpiecesText masterpiecesText)
    {
        return bizMasterpiecesTextMapper.updateMasterpiecesText(masterpiecesText);
    }

    /**
     * 批量删除名著章节
     * 
     * @param ids 需要删除的名著章节主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTextByIds(Long[] ids)
    {
        return bizMasterpiecesTextMapper.deleteMasterpiecesTextByIds(ids);
    }

    /**
     * 删除名著章节信息
     * 
     * @param id 名著章节主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesTextById(Long id)
    {
        return bizMasterpiecesTextMapper.deleteMasterpiecesTextById(id);
    }
}
