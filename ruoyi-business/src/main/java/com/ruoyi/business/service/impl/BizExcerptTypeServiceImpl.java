package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ExcerptType;
import com.ruoyi.business.mapper.BizExcerptTypeMapper;
import com.ruoyi.business.service.IExcerptTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 金句摘抄分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizExcerptTypeServiceImpl implements IExcerptTypeService
{
    @Autowired
    private BizExcerptTypeMapper bizExcerptTypeMapper;

    /**
     * 查询金句摘抄分类
     * 
     * @param id 金句摘抄分类主键
     * @return 金句摘抄分类
     */
    @Override
    public ExcerptType selectExcerptTypeById(Long id)
    {
        return bizExcerptTypeMapper.selectExcerptTypeById(id);
    }

    /**
     * 查询金句摘抄分类列表
     * 
     * @param excerptType 金句摘抄分类
     * @return 金句摘抄分类
     */
    @Override
    public List<ExcerptType> selectExcerptTypeList(ExcerptType excerptType)
    {
        List<ExcerptType> excerptTypes = bizExcerptTypeMapper.selectExcerptTypeList(excerptType);
        excerptTypes.forEach(item -> {
            ExcerptType excerptType1 = new ExcerptType();
            excerptType1.setPid(item.getId());
            List<ExcerptType> excerptTypes1 = bizExcerptTypeMapper.selectExcerptTypeList(excerptType1);
            item.setChild(excerptTypes1);
        });

        return excerptTypes;
    }

    /**
     * 新增金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    @Override
    public int insertExcerptType(ExcerptType excerptType)
    {
        return bizExcerptTypeMapper.insertExcerptType(excerptType);
    }

    /**
     * 修改金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    @Override
    public int updateExcerptType(ExcerptType excerptType)
    {
        return bizExcerptTypeMapper.updateExcerptType(excerptType);
    }

    /**
     * 批量删除金句摘抄分类
     * 
     * @param ids 需要删除的金句摘抄分类主键
     * @return 结果
     */
    @Override
    public int deleteExcerptTypeByIds(Long[] ids)
    {
        return bizExcerptTypeMapper.deleteExcerptTypeByIds(ids);
    }

    /**
     * 删除金句摘抄分类信息
     * 
     * @param id 金句摘抄分类主键
     * @return 结果
     */
    @Override
    public int deleteExcerptTypeById(Long id)
    {
        return bizExcerptTypeMapper.deleteExcerptTypeById(id);
    }
}
