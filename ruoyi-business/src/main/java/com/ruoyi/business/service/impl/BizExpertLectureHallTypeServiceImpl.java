package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ExpertLectureHallType;
import com.ruoyi.business.mapper.BizExpertLectureHallTypeMapper;
import com.ruoyi.business.service.IExpertLectureHallTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 专家讲堂分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizExpertLectureHallTypeServiceImpl implements IExpertLectureHallTypeService
{
    @Autowired
    private BizExpertLectureHallTypeMapper bizExpertLectureHallTypeMapper;

    /**
     * 查询专家讲堂分类
     * 
     * @param id 专家讲堂分类主键
     * @return 专家讲堂分类
     */
    @Override
    public ExpertLectureHallType selectExpertLectureHallTypeById(Long id)
    {
        return bizExpertLectureHallTypeMapper.selectExpertLectureHallTypeById(id);
    }

    /**
     * 查询专家讲堂分类列表
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 专家讲堂分类
     */
    @Override
    public List<ExpertLectureHallType> selectExpertLectureHallTypeList(ExpertLectureHallType expertLectureHallType)
    {
        return bizExpertLectureHallTypeMapper.selectExpertLectureHallTypeList(expertLectureHallType);
    }

    /**
     * 新增专家讲堂分类
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 结果
     */
    @Override
    public int insertExpertLectureHallType(ExpertLectureHallType expertLectureHallType)
    {
        return bizExpertLectureHallTypeMapper.insertExpertLectureHallType(expertLectureHallType);
    }

    /**
     * 修改专家讲堂分类
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 结果
     */
    @Override
    public int updateExpertLectureHallType(ExpertLectureHallType expertLectureHallType)
    {
        return bizExpertLectureHallTypeMapper.updateExpertLectureHallType(expertLectureHallType);
    }

    /**
     * 批量删除专家讲堂分类
     * 
     * @param ids 需要删除的专家讲堂分类主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallTypeByIds(Long[] ids)
    {
        return bizExpertLectureHallTypeMapper.deleteExpertLectureHallTypeByIds(ids);
    }

    /**
     * 删除专家讲堂分类信息
     * 
     * @param id 专家讲堂分类主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallTypeById(Long id)
    {
        return bizExpertLectureHallTypeMapper.deleteExpertLectureHallTypeById(id);
    }
}
