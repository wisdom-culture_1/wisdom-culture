package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.Affiche;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IAfficheService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Affiche selectAfficheById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param affiche 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Affiche> selectAfficheList(Affiche affiche);

    /**
     * 新增【请填写功能名称】
     * 
     * @param affiche 【请填写功能名称】
     * @return 结果
     */
    public int insertAffiche(Affiche affiche);

    /**
     * 修改【请填写功能名称】
     * 
     * @param affiche 【请填写功能名称】
     * @return 结果
     */
    public int updateAffiche(Affiche affiche);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteAfficheByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteAfficheById(Long id);
}
