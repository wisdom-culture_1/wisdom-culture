package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.CultureType;
import com.ruoyi.business.mapper.BizCultureTypeMapper;
import com.ruoyi.business.service.ICultureTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 中华文化分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizCultureTypeServiceImpl implements ICultureTypeService
{
    @Autowired
    private BizCultureTypeMapper bizCultureTypeMapper;

    /**
     * 查询中华文化分类
     * 
     * @param id 中华文化分类主键
     * @return 中华文化分类
     */
    @Override
    public CultureType selectCultureTypeById(Long id)
    {
        return bizCultureTypeMapper.selectCultureTypeById(id);
    }

    /**
     * 查询中华文化分类列表
     * 
     * @param cultureType 中华文化分类
     * @return 中华文化分类
     */
    @Override
    public List<CultureType> selectCultureTypeList(CultureType cultureType)
    {
        return bizCultureTypeMapper.selectCultureTypeList(cultureType);
    }

    /**
     * 新增中华文化分类
     * 
     * @param cultureType 中华文化分类
     * @return 结果
     */
    @Override
    public int insertCultureType(CultureType cultureType)
    {
        return bizCultureTypeMapper.insertCultureType(cultureType);
    }

    /**
     * 修改中华文化分类
     * 
     * @param cultureType 中华文化分类
     * @return 结果
     */
    @Override
    public int updateCultureType(CultureType cultureType)
    {
        return bizCultureTypeMapper.updateCultureType(cultureType);
    }

    /**
     * 批量删除中华文化分类
     * 
     * @param ids 需要删除的中华文化分类主键
     * @return 结果
     */
    @Override
    public int deleteCultureTypeByIds(Long[] ids)
    {
        return bizCultureTypeMapper.deleteCultureTypeByIds(ids);
    }

    /**
     * 删除中华文化分类信息
     * 
     * @param id 中华文化分类主键
     * @return 结果
     */
    @Override
    public int deleteCultureTypeById(Long id)
    {
        return bizCultureTypeMapper.deleteCultureTypeById(id);
    }
}
