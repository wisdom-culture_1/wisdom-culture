package com.ruoyi.business.service;

import com.ruoyi.business.entity.Etymology;

import java.util.List;

/**
 * 字源Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IEtymologyService 
{
    /**
     * 查询字源
     * 
     * @param id 字源主键
     * @return 字源
     */
    public Etymology selectEtymologyById(Long id);

    /**
     * 查询字源列表
     * 
     * @param etymology 字源
     * @return 字源集合
     */
    public List<Etymology> selectEtymologyList(Etymology etymology);



    /**
     * 新增字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    public int insertEtymology(Etymology etymology);

    /**
     * 修改字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    public int updateEtymology(Etymology etymology);

    /**
     * 批量删除字源
     * 
     * @param ids 需要删除的字源主键集合
     * @return 结果
     */
    public int deleteEtymologyByIds(Long[] ids);

    /**
     * 删除字源信息
     * 
     * @param id 字源主键
     * @return 结果
     */
    public int deleteEtymologyById(Long id);
}
