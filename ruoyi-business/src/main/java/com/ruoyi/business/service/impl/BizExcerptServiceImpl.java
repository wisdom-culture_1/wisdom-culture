package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Excerpt;
import com.ruoyi.business.entity.ExcerptComment;
import com.ruoyi.business.entity.ExcerptLike;
import com.ruoyi.business.mapper.BizExcerptCommentMapper;
import com.ruoyi.business.mapper.BizExcerptLikeMapper;
import com.ruoyi.business.mapper.BizExcerptMapper;
import com.ruoyi.business.service.IExcerptService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * 金句摘抄Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizExcerptServiceImpl implements IExcerptService
{
    @Autowired
    private BizExcerptMapper excerptMapper;

    @Autowired
    private BizExcerptCommentMapper bizExcerptCommentMapper;

    @Autowired
    private BizExcerptLikeMapper bizExcerptLikeMapper;

    /**
     * 查询金句摘抄
     * 
     * @param id 金句摘抄主键
     * @return 金句摘抄
     */
    @Override
    public Excerpt selectExcerptById(Long id)
    {
        Excerpt excerpt = excerptMapper.selectExcerptById(id);
        getExcerptLikeAndCommentNum(excerpt);
        return excerpt;
    }

    /**
 * 查询金句摘抄列表
 *
 * @param excerpt 金句摘抄
 * @return 金句摘抄
 */
@Override
public List<Excerpt> selectExcerptList(Excerpt excerpt)
{
    excerpt.setSource(excerpt.getTitle());
    List<Excerpt> excerpts = excerptMapper.selectExcerptList(excerpt);
    excerpts.forEach(item ->{
        getExcerptLikeAndCommentNum(item);
    });
    return excerpts;
}

    private void getExcerptLikeAndCommentNum(Excerpt item) {
        ExcerptComment comment = new ExcerptComment();
        comment.setExcerptId(item.getId());
        List<ExcerptComment> excerptComments = bizExcerptCommentMapper.selectExcerptCommentList(comment);
        if(!CollectionUtils.isEmpty(excerptComments)){
            item.setCommentNumber(Integer.toUnsignedLong(excerptComments.size()));
        }

        ExcerptLike excerptLike = new ExcerptLike();
        excerptLike.setExcerptId(item.getId());
        List<ExcerptLike> excerptLikes = bizExcerptLikeMapper.selectExcerptLikeList(excerptLike);
        if(!CollectionUtils.isEmpty(excerptLikes)){
            item.setLikeNumber(Integer.toUnsignedLong(excerptLikes.size()));
        }


        ExcerptLike excerptLike1 = new ExcerptLike();
        excerptLike1.setExcerptId(item.getId());
        excerptLike1.setUserId(SecurityUtils.getUserId());
        List<ExcerptLike> excerptLikes1 = bizExcerptLikeMapper.selectExcerptLikeList(excerptLike1);
        if(!CollectionUtils.isEmpty(excerptLikes1)){
            item.setIs_like(String.valueOf(excerptLikes1.get(0).getIsLike()));
        }

    }

    /**
     * 新增金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    @Override
    public int insertExcerpt(Excerpt excerpt)
    {
        return excerptMapper.insertExcerpt(excerpt);
    }

    /**
     * 修改金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    @Override
    public int updateExcerpt(Excerpt excerpt)
    {
        return excerptMapper.updateExcerpt(excerpt);
    }

    /**
     * 批量删除金句摘抄
     * 
     * @param ids 需要删除的金句摘抄主键
     * @return 结果
     */
    @Override
    public int deleteExcerptByIds(Long[] ids)
    {
        return excerptMapper.deleteExcerptByIds(ids);
    }

    /**
     * 删除金句摘抄信息
     * 
     * @param id 金句摘抄主键
     * @return 结果
     */
    @Override
    public int deleteExcerptById(Long id)
    {
        return excerptMapper.deleteExcerptById(id);
    }
}
