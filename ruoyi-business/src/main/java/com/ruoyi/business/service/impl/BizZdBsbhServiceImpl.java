package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizZdBsbhMapper;
import com.ruoyi.business.entity.ZdBsbh;
import com.ruoyi.business.service.IZdBsbhService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizZdBsbhServiceImpl implements IZdBsbhService
{
    @Autowired
    private BizZdBsbhMapper bizZdBsbhMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param bu 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ZdBsbh selectZdBsbhByBu(String bu)
    {
        return bizZdBsbhMapper.selectZdBsbhByBu(bu);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdBsbh 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ZdBsbh> selectZdBsbhList(ZdBsbh zdBsbh)
    {
        return bizZdBsbhMapper.selectZdBsbhList(zdBsbh);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdBsbh 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZdBsbh(ZdBsbh zdBsbh)
    {
        return bizZdBsbhMapper.insertZdBsbh(zdBsbh);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdBsbh 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZdBsbh(ZdBsbh zdBsbh)
    {
        return bizZdBsbhMapper.updateZdBsbh(zdBsbh);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param bus 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdBsbhByBus(String[] bus)
    {
        return bizZdBsbhMapper.deleteZdBsbhByBus(bus);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param bu 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdBsbhByBu(String bu)
    {
        return bizZdBsbhMapper.deleteZdBsbhByBu(bu);
    }
}
