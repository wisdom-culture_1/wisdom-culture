package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Guide;
import com.ruoyi.business.mapper.BizGuideMapper;
import com.ruoyi.business.service.IGuideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizGuideServiceImpl implements IGuideService
{
    @Autowired
    private BizGuideMapper bizGuideMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Guide selectGuideById(Long id)
    {
        return bizGuideMapper.selectGuideById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param guide 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Guide> selectGuideList(Guide guide)
    {
        return bizGuideMapper.selectGuideList(guide);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param guide 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertGuide(Guide guide)
    {
        return bizGuideMapper.insertGuide(guide);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param guide 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateGuide(Guide guide)
    {
        return bizGuideMapper.updateGuide(guide);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteGuideByIds(Long[] ids)
    {
        return bizGuideMapper.deleteGuideByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteGuideById(Long id)
    {
        return bizGuideMapper.deleteGuideById(id);
    }
}
