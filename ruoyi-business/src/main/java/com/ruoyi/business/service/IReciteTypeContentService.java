package com.ruoyi.business.service;

import com.ruoyi.business.entity.ReciteTypeContent;

import java.util.List;

/**
 * 背诵打卡类别内容Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IReciteTypeContentService 
{
    /**
     * 查询背诵打卡类别内容
     * 
     * @param id 背诵打卡类别内容主键
     * @return 背诵打卡类别内容
     */
    public ReciteTypeContent selectReciteTypeContentById(Long id);

    /**
     * 查询背诵打卡类别内容列表
     * 
     * @param reciteTypeContent 背诵打卡类别内容
     * @return 背诵打卡类别内容集合
     */
    public List<ReciteTypeContent> selectReciteTypeContentList(ReciteTypeContent reciteTypeContent);

    /**
     * 新增背诵打卡类别内容
     * 
     * @param reciteTypeContent 背诵打卡类别内容
     * @return 结果
     */
    public int insertReciteTypeContent(ReciteTypeContent reciteTypeContent);

    /**
     * 修改背诵打卡类别内容
     * 
     * @param reciteTypeContent 背诵打卡类别内容
     * @return 结果
     */
    public int updateReciteTypeContent(ReciteTypeContent reciteTypeContent);

    /**
     * 批量删除背诵打卡类别内容
     * 
     * @param ids 需要删除的背诵打卡类别内容主键集合
     * @return 结果
     */
    public int deleteReciteTypeContentByIds(Long[] ids);

    /**
     * 删除背诵打卡类别内容信息
     * 
     * @param id 背诵打卡类别内容主键
     * @return 结果
     */
    public int deleteReciteTypeContentById(Long id);
}
