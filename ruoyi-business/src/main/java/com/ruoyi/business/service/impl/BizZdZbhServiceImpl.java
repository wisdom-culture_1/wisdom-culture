package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ZdZbh;
import com.ruoyi.business.mapper.BizZdZbhMapper;
import com.ruoyi.business.service.IZdZbhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizZdZbhServiceImpl implements IZdZbhService
{
    @Autowired
    private BizZdZbhMapper bizZdZbhMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ZdZbh selectZdZbhByXuhao(Long xuhao)
    {
        return bizZdZbhMapper.selectZdZbhByXuhao(xuhao);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdZbh 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ZdZbh> selectZdZbhList(ZdZbh zdZbh)
    {
        return bizZdZbhMapper.selectZdZbhList(zdZbh);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdZbh 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZdZbh(ZdZbh zdZbh)
    {
        return bizZdZbhMapper.insertZdZbh(zdZbh);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdZbh 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZdZbh(ZdZbh zdZbh)
    {
        return bizZdZbhMapper.updateZdZbh(zdZbh);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param xuhaos 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdZbhByXuhaos(Long[] xuhaos)
    {
        return bizZdZbhMapper.deleteZdZbhByXuhaos(xuhaos);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdZbhByXuhao(Long xuhao)
    {
        return bizZdZbhMapper.deleteZdZbhByXuhao(xuhao);
    }
}
