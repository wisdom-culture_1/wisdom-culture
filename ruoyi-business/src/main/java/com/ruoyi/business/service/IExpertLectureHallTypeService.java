package com.ruoyi.business.service;

import com.ruoyi.business.entity.ExpertLectureHallType;

import java.util.List;

/**
 * 专家讲堂分类Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IExpertLectureHallTypeService 
{
    /**
     * 查询专家讲堂分类
     * 
     * @param id 专家讲堂分类主键
     * @return 专家讲堂分类
     */
    public ExpertLectureHallType selectExpertLectureHallTypeById(Long id);

    /**
     * 查询专家讲堂分类列表
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 专家讲堂分类集合
     */
    public List<ExpertLectureHallType> selectExpertLectureHallTypeList(ExpertLectureHallType expertLectureHallType);

    /**
     * 新增专家讲堂分类
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 结果
     */
    public int insertExpertLectureHallType(ExpertLectureHallType expertLectureHallType);

    /**
     * 修改专家讲堂分类
     * 
     * @param expertLectureHallType 专家讲堂分类
     * @return 结果
     */
    public int updateExpertLectureHallType(ExpertLectureHallType expertLectureHallType);

    /**
     * 批量删除专家讲堂分类
     * 
     * @param ids 需要删除的专家讲堂分类主键集合
     * @return 结果
     */
    public int deleteExpertLectureHallTypeByIds(Long[] ids);

    /**
     * 删除专家讲堂分类信息
     * 
     * @param id 专家讲堂分类主键
     * @return 结果
     */
    public int deleteExpertLectureHallTypeById(Long id);
}
