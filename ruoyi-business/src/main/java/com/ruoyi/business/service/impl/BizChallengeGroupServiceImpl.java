package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ChallengeComplete;
import com.ruoyi.business.entity.ChallengeGroup;
import com.ruoyi.business.mapper.BizChallengeCompleteMapper;
import com.ruoyi.business.mapper.BizChallengeGroupMapper;
import com.ruoyi.business.service.IChallengeGroupService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 挑战组别Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@Service
public class BizChallengeGroupServiceImpl implements IChallengeGroupService
{
    @Autowired
    private BizChallengeGroupMapper bizChallengeGroupMapper;
    @Autowired
    private BizChallengeCompleteMapper challengeCompleteMapper;

    /**
     * 查询挑战组别
     * 
     * @param id 挑战组别主键
     * @return 挑战组别
     */
    @Override
    public ChallengeGroup selectChallengeGroupById(Long id)
    {
        return bizChallengeGroupMapper.selectChallengeGroupById(id);
    }

    /**
     * 查询挑战组别列表
     * 
     * @param challengeGroup 挑战组别
     * @return 挑战组别
     */
    @Override
    public List<ChallengeGroup> selectChallengeGroupList(ChallengeGroup challengeGroup)
    {
        List<ChallengeGroup> challengeGroups = bizChallengeGroupMapper.selectChallengeGroupList(challengeGroup);

        challengeGroups.forEach(item -> {
            ChallengeComplete challengeComplete = new ChallengeComplete();
            challengeComplete.setuId(SecurityUtils.getUserId());
            challengeComplete.setTypeId(challengeGroup.getTypeId());
            challengeComplete.setGroupId(item.getId());

            ChallengeComplete challengeComplete1 = challengeCompleteMapper.selectLatestChallengeCompliete(challengeComplete);
            if(challengeComplete1 != null && challengeComplete1.getAchievement().intValue() >= 60){
                item.setIs_pass(Long.valueOf(2));
            }else{
                item.setIs_pass(Long.valueOf(1));
            }
        });

        return challengeGroups;
    }

    /**
     * 新增挑战组别
     * 
     * @param challengeGroup 挑战组别
     * @return 结果
     */
    @Override
    public int insertChallengeGroup(ChallengeGroup challengeGroup)
    {
        return bizChallengeGroupMapper.insertChallengeGroup(challengeGroup);
    }

    /**
     * 修改挑战组别
     * 
     * @param challengeGroup 挑战组别
     * @return 结果
     */
    @Override
    public int updateChallengeGroup(ChallengeGroup challengeGroup)
    {
        return bizChallengeGroupMapper.updateChallengeGroup(challengeGroup);
    }

    /**
     * 批量删除挑战组别
     * 
     * @param ids 需要删除的挑战组别主键
     * @return 结果
     */
    @Override
    public int deleteChallengeGroupByIds(Long[] ids)
    {
        return bizChallengeGroupMapper.deleteChallengeGroupByIds(ids);
    }

    /**
     * 删除挑战组别信息
     * 
     * @param id 挑战组别主键
     * @return 结果
     */
    @Override
    public int deleteChallengeGroupById(Long id)
    {
        return bizChallengeGroupMapper.deleteChallengeGroupById(id);
    }
}
