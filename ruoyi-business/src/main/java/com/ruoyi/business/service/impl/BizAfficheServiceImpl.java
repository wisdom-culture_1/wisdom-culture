package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizAfficheMapper;
import com.ruoyi.business.entity.Affiche;
import com.ruoyi.business.service.IAfficheService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizAfficheServiceImpl implements IAfficheService
{
    @Autowired
    private BizAfficheMapper bizAfficheMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Affiche selectAfficheById(Long id)
    {
        return bizAfficheMapper.selectAfficheById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param affiche 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Affiche> selectAfficheList(Affiche affiche)
    {
        return bizAfficheMapper.selectAfficheList(affiche);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param affiche 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertAffiche(Affiche affiche)
    {
        return bizAfficheMapper.insertAffiche(affiche);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param affiche 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateAffiche(Affiche affiche)
    {
        return bizAfficheMapper.updateAffiche(affiche);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAfficheByIds(Long[] ids)
    {
        return bizAfficheMapper.deleteAfficheByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAfficheById(Long id)
    {
        return bizAfficheMapper.deleteAfficheById(id);
    }
}
