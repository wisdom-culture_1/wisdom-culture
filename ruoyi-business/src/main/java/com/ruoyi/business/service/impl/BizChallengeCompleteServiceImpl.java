package com.ruoyi.business.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.business.entity.ChallengeComplete;
import com.ruoyi.business.mapper.BizChallengeCompleteMapper;
import com.ruoyi.business.service.IChallengeCompleteService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 挑战结果Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@Service
public class BizChallengeCompleteServiceImpl implements IChallengeCompleteService
{
    @Autowired
    private BizChallengeCompleteMapper challengeCompleteMapper;

    /**
     * 查询挑战结果
     * 
     * @param id 挑战结果主键
     * @return 挑战结果
     */
    @Override
    public ChallengeComplete selectChallengeCompleteById(Long id)
    {
        return challengeCompleteMapper.selectChallengeCompleteById(id);
    }

    /**
     * 查询挑战结果列表
     * 
     * @param challengeComplete 挑战结果
     * @return 挑战结果
     */
    @Override
    public List<ChallengeComplete> selectChallengeCompleteList(ChallengeComplete challengeComplete)
    {
        return challengeCompleteMapper.selectChallengeCompleteList(challengeComplete);
    }

    /**
     * 新增挑战结果
     * 
     * @param challengeComplete 挑战结果
     * @return 结果
     */
    @Override
    public int insertChallengeComplete(ChallengeComplete challengeComplete)
    {
        challengeComplete.setuId(SecurityUtils.getUserId());
        challengeComplete.setCreatedAt(new Date());
        challengeComplete.setUpdatedAt(new Date());
        return challengeCompleteMapper.insertChallengeComplete(challengeComplete);
    }

    /**
     * 修改挑战结果
     * 
     * @param challengeComplete 挑战结果
     * @return 结果
     */
    @Override
    public int updateChallengeComplete(ChallengeComplete challengeComplete)
    {
        return challengeCompleteMapper.updateChallengeComplete(challengeComplete);
    }

    /**
     * 批量删除挑战结果
     * 
     * @param ids 需要删除的挑战结果主键
     * @return 结果
     */
    @Override
    public int deleteChallengeCompleteByIds(Long[] ids)
    {
        return challengeCompleteMapper.deleteChallengeCompleteByIds(ids);
    }

    /**
     * 删除挑战结果信息
     * 
     * @param id 挑战结果主键
     * @return 结果
     */
    @Override
    public int deleteChallengeCompleteById(Long id)
    {
        return challengeCompleteMapper.deleteChallengeCompleteById(id);
    }
}
