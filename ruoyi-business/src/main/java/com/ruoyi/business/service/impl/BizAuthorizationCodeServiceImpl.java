package com.ruoyi.business.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizAuthorizationCodeMapper;
import com.ruoyi.business.entity.AuthorizationCode;
import com.ruoyi.business.service.IAuthorizationCodeService;
import org.springframework.util.CollectionUtils;

/**
 * 授权码Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizAuthorizationCodeServiceImpl implements IAuthorizationCodeService
{
    @Autowired
    private BizAuthorizationCodeMapper authorizationCodeMapper;

    /**
     * 查询授权码
     * 
     * @param id 授权码主键
     * @return 授权码
     */
    @Override
    public AuthorizationCode selectAuthorizationCodeById(Long id)
    {
        return authorizationCodeMapper.selectAuthorizationCodeById(id);
    }

    /**
     * 查询授权码列表
     * 
     * @param authorizationCode 授权码
     * @return 授权码
     */
    @Override
    public List<AuthorizationCode> selectAuthorizationCodeList(AuthorizationCode authorizationCode)
    {
        return authorizationCodeMapper.selectAuthorizationCodeList(authorizationCode);
    }

    /**
     * 新增授权码
     * 
     * @param authorizationCode 授权码
     * @return 结果
     */
    @Override
    public int insertAuthorizationCode(AuthorizationCode authorizationCode)
    {
        AuthorizationCode code = new AuthorizationCode();
        code.setCode(authorizationCode.getCode());
        List<AuthorizationCode> authorizationCodes = authorizationCodeMapper.selectAuthorizationCodeList(authorizationCode);
        if(!CollectionUtils.isEmpty(authorizationCodes)){

        }

        if(StringUtils.isNotBlank(authorizationCode.getIsUse())){
            String isUse = authorizationCode.getIsUse().trim();
            authorizationCode.setIsUse(isUse);
            if(isUse.equals("是")){
                authorizationCode.setIsUse("1");
            }

            if(isUse.equals("否")){
                authorizationCode.setIsUse("0");
            }

        }
        return authorizationCodeMapper.insertAuthorizationCode(authorizationCode);
    }

    /**
     * 修改授权码
     * 
     * @param authorizationCode 授权码
     * @return 结果
     */
    @Override
    public int updateAuthorizationCode(AuthorizationCode authorizationCode)
    {
        return authorizationCodeMapper.updateAuthorizationCode(authorizationCode);
    }

    /**
     * 批量删除授权码
     * 
     * @param ids 需要删除的授权码主键
     * @return 结果
     */
    @Override
    public int deleteAuthorizationCodeByIds(Long[] ids)
    {
        return authorizationCodeMapper.deleteAuthorizationCodeByIds(ids);
    }

    /**
     * 删除授权码信息
     * 
     * @param id 授权码主键
     * @return 结果
     */
    @Override
    public int deleteAuthorizationCodeById(Long id)
    {
        return authorizationCodeMapper.deleteAuthorizationCodeById(id);
    }

    @Override
    public AuthorizationCode getRandomCode() {
        return authorizationCodeMapper.getRandomCode();
    }

//    public static void main(String[] args) {
//        UUID uuid = UUID.randomUUID();
//        String hexString = uuid.toString().replace("-", "").substring(0,16).toUpperCase();
//        System.out.println(hexString);
//    }
}
