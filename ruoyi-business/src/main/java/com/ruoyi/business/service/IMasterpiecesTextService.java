package com.ruoyi.business.service;

import com.ruoyi.business.entity.MasterpiecesText;

import java.util.List;

/**
 * 名著章节Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IMasterpiecesTextService 
{
    /**
     * 查询名著章节
     * 
     * @param id 名著章节主键
     * @return 名著章节
     */
    public MasterpiecesText selectMasterpiecesTextById(Long id);

    /**
     * 查询名著章节列表
     * 
     * @param masterpiecesText 名著章节
     * @return 名著章节集合
     */
    public List<MasterpiecesText> selectMasterpiecesTextList(MasterpiecesText masterpiecesText);

    /**
     * 新增名著章节
     * 
     * @param masterpiecesText 名著章节
     * @return 结果
     */
    public int insertMasterpiecesText(MasterpiecesText masterpiecesText);

    /**
     * 修改名著章节
     * 
     * @param masterpiecesText 名著章节
     * @return 结果
     */
    public int updateMasterpiecesText(MasterpiecesText masterpiecesText);

    /**
     * 批量删除名著章节
     * 
     * @param ids 需要删除的名著章节主键集合
     * @return 结果
     */
    public int deleteMasterpiecesTextByIds(Long[] ids);

    /**
     * 删除名著章节信息
     * 
     * @param id 名著章节主键
     * @return 结果
     */
    public int deleteMasterpiecesTextById(Long id);
}
