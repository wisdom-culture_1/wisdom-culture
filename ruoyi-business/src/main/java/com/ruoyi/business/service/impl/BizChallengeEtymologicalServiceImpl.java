package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ChallengeEtymological;
import com.ruoyi.business.mapper.BizChallengeEtymologicalMapper;
import com.ruoyi.business.service.IChallengeEtymologicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@Service
public class BizChallengeEtymologicalServiceImpl implements IChallengeEtymologicalService
{
    @Autowired
    private BizChallengeEtymologicalMapper bizChallengeEtymologicalMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ChallengeEtymological selectChallengeEtymologicalById(Long id)
    {
        return bizChallengeEtymologicalMapper.selectChallengeEtymologicalById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param challengeEtymological 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ChallengeEtymological> selectChallengeEtymologicalList(ChallengeEtymological challengeEtymological)
    {
        return bizChallengeEtymologicalMapper.selectChallengeEtymologicalList(challengeEtymological);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param challengeEtymological 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertChallengeEtymological(ChallengeEtymological challengeEtymological)
    {
        return bizChallengeEtymologicalMapper.insertChallengeEtymological(challengeEtymological);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param challengeEtymological 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateChallengeEtymological(ChallengeEtymological challengeEtymological)
    {
        return bizChallengeEtymologicalMapper.updateChallengeEtymological(challengeEtymological);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteChallengeEtymologicalByIds(Long[] ids)
    {
        return bizChallengeEtymologicalMapper.deleteChallengeEtymologicalByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteChallengeEtymologicalById(Long id)
    {
        return bizChallengeEtymologicalMapper.deleteChallengeEtymologicalById(id);
    }
}
