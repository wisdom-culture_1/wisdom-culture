package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.OriginOfChineseCharacters;

/**
 * 汉字起源Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IOriginOfChineseCharactersService 
{
    /**
     * 查询汉字起源
     * 
     * @param id 汉字起源主键
     * @return 汉字起源
     */
    public OriginOfChineseCharacters selectOriginOfChineseCharactersById(Long id);

    /**
     * 查询汉字起源列表
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 汉字起源集合
     */
    public List<OriginOfChineseCharacters> selectOriginOfChineseCharactersList(OriginOfChineseCharacters originOfChineseCharacters);

    /**
     * 新增汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    public int insertOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters);

    /**
     * 修改汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    public int updateOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters);

    /**
     * 批量删除汉字起源
     * 
     * @param ids 需要删除的汉字起源主键集合
     * @return 结果
     */
    public int deleteOriginOfChineseCharactersByIds(Long[] ids);

    /**
     * 删除汉字起源信息
     * 
     * @param id 汉字起源主键
     * @return 结果
     */
    public int deleteOriginOfChineseCharactersById(Long id);
}
