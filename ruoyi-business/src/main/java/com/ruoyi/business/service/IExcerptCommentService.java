package com.ruoyi.business.service;

import com.ruoyi.business.entity.ExcerptComment;

import java.util.List;

/**
 * 金句摘抄评论Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IExcerptCommentService 
{
    /**
     * 查询金句摘抄评论
     * 
     * @param id 金句摘抄评论主键
     * @return 金句摘抄评论
     */
    public ExcerptComment selectExcerptCommentById(Long id);

    /**
     * 查询金句摘抄评论列表
     * 
     * @param excerptComment 金句摘抄评论
     * @return 金句摘抄评论集合
     */
    public List<ExcerptComment> selectExcerptCommentList(ExcerptComment excerptComment);

    /**
     * 新增金句摘抄评论
     * 
     * @param excerptComment 金句摘抄评论
     * @return 结果
     */
    public int insertExcerptComment(ExcerptComment excerptComment);

    /**
     * 修改金句摘抄评论
     * 
     * @param excerptComment 金句摘抄评论
     * @return 结果
     */
    public int updateExcerptComment(ExcerptComment excerptComment);

    /**
     * 批量删除金句摘抄评论
     * 
     * @param ids 需要删除的金句摘抄评论主键集合
     * @return 结果
     */
    public int deleteExcerptCommentByIds(ExcerptComment excerptComment);

    /**
     * 删除金句摘抄评论信息
     * 
     * @param id 金句摘抄评论主键
     * @return 结果
     */
    public int deleteExcerptCommentById(Long id);
}
