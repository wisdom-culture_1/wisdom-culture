package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.ZdBsbh;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IZdBsbhService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param bu 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ZdBsbh selectZdBsbhByBu(String bu);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdBsbh 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZdBsbh> selectZdBsbhList(ZdBsbh zdBsbh);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdBsbh 【请填写功能名称】
     * @return 结果
     */
    public int insertZdBsbh(ZdBsbh zdBsbh);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdBsbh 【请填写功能名称】
     * @return 结果
     */
    public int updateZdBsbh(ZdBsbh zdBsbh);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param bus 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteZdBsbhByBus(String[] bus);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param bu 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZdBsbhByBu(String bu);
}
