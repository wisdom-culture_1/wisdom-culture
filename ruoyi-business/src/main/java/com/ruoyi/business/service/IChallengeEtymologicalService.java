package com.ruoyi.business.service;

import com.ruoyi.business.entity.ChallengeEtymological;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
public interface IChallengeEtymologicalService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ChallengeEtymological selectChallengeEtymologicalById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param challengeEtymological 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ChallengeEtymological> selectChallengeEtymologicalList(ChallengeEtymological challengeEtymological);

    /**
     * 新增【请填写功能名称】
     * 
     * @param challengeEtymological 【请填写功能名称】
     * @return 结果
     */
    public int insertChallengeEtymological(ChallengeEtymological challengeEtymological);

    /**
     * 修改【请填写功能名称】
     * 
     * @param challengeEtymological 【请填写功能名称】
     * @return 结果
     */
    public int updateChallengeEtymological(ChallengeEtymological challengeEtymological);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteChallengeEtymologicalByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteChallengeEtymologicalById(Long id);
}
