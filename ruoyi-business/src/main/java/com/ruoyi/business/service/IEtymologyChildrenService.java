package com.ruoyi.business.service;

import com.ruoyi.business.entity.EtymologyChildren;

import java.util.List;

/**
 * 字源示例Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IEtymologyChildrenService 
{
    /**
     * 查询字源示例
     * 
     * @param id 字源示例主键
     * @return 字源示例
     */
    public EtymologyChildren selectEtymologyChildrenById(Long id);

    /**
     * 查询字源示例列表
     * 
     * @param etymologyChildren 字源示例
     * @return 字源示例集合
     */
    public List<EtymologyChildren> selectEtymologyChildrenList(EtymologyChildren etymologyChildren);

    /**
     * 新增字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    public int insertEtymologyChildren(EtymologyChildren etymologyChildren);

    /**
     * 修改字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    public int updateEtymologyChildren(EtymologyChildren etymologyChildren);

    /**
     * 批量删除字源示例
     * 
     * @param ids 需要删除的字源示例主键集合
     * @return 结果
     */
    public int deleteEtymologyChildrenByIds(Long[] ids);

    /**
     * 删除字源示例信息
     * 
     * @param id 字源示例主键
     * @return 结果
     */
    public int deleteEtymologyChildrenById(Long id);
}
