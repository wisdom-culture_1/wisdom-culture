package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.ZdOrderpy;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IZdOrderpyService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param head 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ZdOrderpy selectZdOrderpyByHead(String head);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdOrderpy 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZdOrderpy> selectZdOrderpyList(ZdOrderpy zdOrderpy);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdOrderpy 【请填写功能名称】
     * @return 结果
     */
    public int insertZdOrderpy(ZdOrderpy zdOrderpy);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdOrderpy 【请填写功能名称】
     * @return 结果
     */
    public int updateZdOrderpy(ZdOrderpy zdOrderpy);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param heads 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteZdOrderpyByHeads(String[] heads);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param head 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZdOrderpyByHead(String head);
}
