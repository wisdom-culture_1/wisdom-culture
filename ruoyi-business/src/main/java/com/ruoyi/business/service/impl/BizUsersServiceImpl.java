package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizUsersMapper;
import com.ruoyi.business.service.IUsersService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizUsersServiceImpl implements IUsersService
{
    @Autowired
    private BizUsersMapper bizUsersMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Users selectUsersById(String id)
    {
        return bizUsersMapper.selectUsersById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param users 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Users> selectUsersList(Users users)
    {
        return bizUsersMapper.selectUsersList(users);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param users 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertUsers(Users users)
    {
        return bizUsersMapper.insertUsers(users);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param users 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateUsers(Users users)
    {
        return bizUsersMapper.updateUsers(users);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteUsersByIds(String[] ids)
    {
        return bizUsersMapper.deleteUsersByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteUsersById(String id)
    {
        return bizUsersMapper.deleteUsersById(id);
    }

    @Override
    public void updateUsersByUserName(Users users) {
        bizUsersMapper.updateUsersByUserName(users);
    }
}
