package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.InitialConsonant;
import com.ruoyi.business.mapper.BizInitialConsonantMapper;
import com.ruoyi.business.service.IInitialConsonantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 声母 韵母Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizInitialConsonantServiceImpl implements IInitialConsonantService
{
    @Autowired
    private BizInitialConsonantMapper bizInitialConsonantMapper;

    /**
     * 查询声母 韵母
     * 
     * @param id 声母 韵母主键
     * @return 声母 韵母
     */
    @Override
    public InitialConsonant selectInitialConsonantById(Long id)
    {
        return bizInitialConsonantMapper.selectInitialConsonantById(id);
    }

    /**
     * 查询声母 韵母列表
     * 
     * @param initialConsonant 声母 韵母
     * @return 声母 韵母
     */
    @Override
    public List<InitialConsonant> selectInitialConsonantList(InitialConsonant initialConsonant)
    {
        return bizInitialConsonantMapper.selectInitialConsonantList(initialConsonant);
    }

    /**
     * 新增声母 韵母
     * 
     * @param initialConsonant 声母 韵母
     * @return 结果
     */
    @Override
    public int insertInitialConsonant(InitialConsonant initialConsonant)
    {
        return bizInitialConsonantMapper.insertInitialConsonant(initialConsonant);
    }

    /**
     * 修改声母 韵母
     * 
     * @param initialConsonant 声母 韵母
     * @return 结果
     */
    @Override
    public int updateInitialConsonant(InitialConsonant initialConsonant)
    {
        return bizInitialConsonantMapper.updateInitialConsonant(initialConsonant);
    }

    /**
     * 批量删除声母 韵母
     * 
     * @param ids 需要删除的声母 韵母主键
     * @return 结果
     */
    @Override
    public int deleteInitialConsonantByIds(Long[] ids)
    {
        return bizInitialConsonantMapper.deleteInitialConsonantByIds(ids);
    }

    /**
     * 删除声母 韵母信息
     * 
     * @param id 声母 韵母主键
     * @return 结果
     */
    @Override
    public int deleteInitialConsonantById(Long id)
    {
        return bizInitialConsonantMapper.deleteInitialConsonantById(id);
    }
}
