package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ExcerptLike;
import com.ruoyi.business.mapper.BizExcerptLikeMapper;
import com.ruoyi.business.service.IExcerptLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 金句摘抄点赞Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizExcerptLikeServiceImpl implements IExcerptLikeService
{
    @Autowired
    private BizExcerptLikeMapper bizExcerptLikeMapper;

    /**
     * 查询金句摘抄点赞
     * 
     * @param id 金句摘抄点赞主键
     * @return 金句摘抄点赞
     */
    @Override
    public ExcerptLike selectExcerptLikeById(Long id)
    {
        return bizExcerptLikeMapper.selectExcerptLikeById(id);
    }

    /**
     * 查询金句摘抄点赞列表
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 金句摘抄点赞
     */
    @Override
    public List<ExcerptLike> selectExcerptLikeList(ExcerptLike excerptLike)
    {
        return bizExcerptLikeMapper.selectExcerptLikeList(excerptLike);
    }

    /**
     * 新增金句摘抄点赞
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 结果
     */
    @Override
    public int insertExcerptLike(ExcerptLike excerptLike)
    {
        ExcerptLike excerptLike1 = bizExcerptLikeMapper.selectExcerptLikeByUserIdAndExcerptId(excerptLike);
        if(excerptLike1 != null){
            return bizExcerptLikeMapper.updateExcerptLike(excerptLike);
        }else{
            return bizExcerptLikeMapper.insertExcerptLike(excerptLike);

        }
    }

    /**
     * 修改金句摘抄点赞
     * 
     * @param excerptLike 金句摘抄点赞
     * @return 结果
     */
    @Override
    public int updateExcerptLike(ExcerptLike excerptLike)
    {
        return bizExcerptLikeMapper.updateExcerptLike(excerptLike);
    }

    /**
     * 批量删除金句摘抄点赞
     * 
     * @param ids 需要删除的金句摘抄点赞主键
     * @return 结果
     */
    @Override
    public int deleteExcerptLikeByIds(Long[] ids)
    {
        return bizExcerptLikeMapper.deleteExcerptLikeByIds(ids);
    }

    /**
     * 删除金句摘抄点赞信息
     * 
     * @param id 金句摘抄点赞主键
     * @return 结果
     */
    @Override
    public int deleteExcerptLikeById(Long id)
    {
        return bizExcerptLikeMapper.deleteExcerptLikeById(id);
    }
}
