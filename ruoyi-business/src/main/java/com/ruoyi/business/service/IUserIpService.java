package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.UserIp;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IUserIpService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public UserIp selectUserIpById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param userIp 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<UserIp> selectUserIpList(UserIp userIp);

    /**
     * 新增【请填写功能名称】
     * 
     * @param userIp 【请填写功能名称】
     * @return 结果
     */
    public int insertUserIp(UserIp userIp);

    /**
     * 修改【请填写功能名称】
     * 
     * @param userIp 【请填写功能名称】
     * @return 结果
     */
    public int updateUserIp(UserIp userIp);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteUserIpByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteUserIpById(Long id);
}
