package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.GradePlan;
import com.ruoyi.business.entity.GradeStudyPlan;
import com.ruoyi.business.mapper.GradeStudyPlanMapper;
import com.ruoyi.business.service.IGradeStudyPlanService;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 学习计划Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-14
 */
@Service
public class GradeStudyPlanServiceImpl implements IGradeStudyPlanService
{
    @Autowired
    private GradeStudyPlanMapper gradeStudyPlanMapper;

    /**
     * 查询学习计划
     * 
     * @param id 学习计划主键
     * @return 学习计划
     */
    @Override
    public GradeStudyPlan selectGradeStudyPlanById(String id)
    {
        return gradeStudyPlanMapper.selectGradeStudyPlanById(id);
    }

    /**
     * 查询学习计划列表
     * 
     * @param gradeStudyPlan 学习计划
     * @return 学习计划
     */
    @Override
    public List<GradeStudyPlan> selectGradeStudyPlanList(GradeStudyPlan gradeStudyPlan)
    {
        return gradeStudyPlanMapper.selectGradeStudyPlanList(gradeStudyPlan);
    }

    /**
     * 新增学习计划
     * 
     * @param gradeStudyPlan 学习计划
     * @return 结果
     */
    @Override
    public int insertGradeStudyPlan(GradeStudyPlan gradeStudyPlan)
    {
        gradeStudyPlan.setCreateTime(DateUtils.getNowDate());
        return gradeStudyPlanMapper.insertGradeStudyPlan(gradeStudyPlan);
    }

    /**
     * 修改学习计划
     * 
     * @param gradeStudyPlan 学习计划
     * @return 结果
     */
    @Override
    public int updateGradeStudyPlan(GradeStudyPlan gradeStudyPlan)
    {
        gradeStudyPlan.setUpdateTime(DateUtils.getNowDate());
        return gradeStudyPlanMapper.updateGradeStudyPlan(gradeStudyPlan);
    }

    /**
     * 批量删除学习计划
     * 
     * @param ids 需要删除的学习计划主键
     * @return 结果
     */
    @Override
    public int deleteGradeStudyPlanByIds(String[] ids)
    {
        return gradeStudyPlanMapper.deleteGradeStudyPlanByIds(ids);
    }

    @Override
    public int deleteGradeStudyPlanByGrade(GradePlan gradePlan) {
        return gradeStudyPlanMapper.deleteGradeStudyPlanByGrade(gradePlan);
    }

    /**
     * 删除学习计划信息
     * 
     * @param id 学习计划主键
     * @return 结果
     */
    @Override
    public int deleteGradeStudyPlanById(String id)
    {
        return gradeStudyPlanMapper.deleteGradeStudyPlanById(id);
    }
}
