package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Grade;
import com.ruoyi.business.mapper.BizGradeMapper;
import com.ruoyi.business.service.IGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 年级Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-12
 */
@Service
public class BizGradeServiceImpl implements IGradeService
{
    @Autowired
    private BizGradeMapper bizGradeMapper;

    /**
     * 查询年级
     * 
     * @param id 年级主键
     * @return 年级
     */
    @Override
    public Grade selectGradeById(Long id)
    {
        return bizGradeMapper.selectGradeById(id);
    }

    /**
     * 查询年级列表
     * 
     * @param grade 年级
     * @return 年级
     */
    @Override
    public List<Grade> selectGradeList(Grade grade)
    {
        return bizGradeMapper.selectGradeList(grade);
    }

    /**
     * 新增年级
     * 
     * @param grade 年级
     * @return 结果
     */
    @Override
    public int insertGrade(Grade grade)
    {
        return bizGradeMapper.insertGrade(grade);
    }

    /**
     * 修改年级
     * 
     * @param grade 年级
     * @return 结果
     */
    @Override
    public int updateGrade(Grade grade)
    {
        return bizGradeMapper.updateGrade(grade);
    }

    /**
     * 批量删除年级
     * 
     * @param ids 需要删除的年级主键
     * @return 结果
     */
    @Override
    public int deleteGradeByIds(Long[] ids)
    {
        return bizGradeMapper.deleteGradeByIds(ids);
    }

    /**
     * 删除年级信息
     * 
     * @param id 年级主键
     * @return 结果
     */
    @Override
    public int deleteGradeById(Long id)
    {
        return bizGradeMapper.deleteGradeById(id);
    }
}
