package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.PoetrySense;
import com.ruoyi.business.mapper.BizPoetrySenseMapper;
import com.ruoyi.business.service.IPoetrySenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 诗词常识Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizPoetrySenseServiceImpl implements IPoetrySenseService
{
    @Autowired
    private BizPoetrySenseMapper bizPoetrySenseMapper;

    /**
     * 查询诗词常识
     * 
     * @param id 诗词常识主键
     * @return 诗词常识
     */
    @Override
    public PoetrySense selectPoetrySenseById(Long id)
    {
        return bizPoetrySenseMapper.selectPoetrySenseById(id);
    }

    /**
     * 查询诗词常识列表
     * 
     * @param poetrySense 诗词常识
     * @return 诗词常识
     */
    @Override
    public List<PoetrySense> selectPoetrySenseList(PoetrySense poetrySense)
    {
        return bizPoetrySenseMapper.selectPoetrySenseList(poetrySense);
    }

    /**
     * 新增诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    @Override
    public int insertPoetrySense(PoetrySense poetrySense)
    {
        return bizPoetrySenseMapper.insertPoetrySense(poetrySense);
    }

    /**
     * 修改诗词常识
     * 
     * @param poetrySense 诗词常识
     * @return 结果
     */
    @Override
    public int updatePoetrySense(PoetrySense poetrySense)
    {
        return bizPoetrySenseMapper.updatePoetrySense(poetrySense);
    }

    /**
     * 批量删除诗词常识
     * 
     * @param ids 需要删除的诗词常识主键
     * @return 结果
     */
    @Override
    public int deletePoetrySenseByIds(Long[] ids)
    {
        return bizPoetrySenseMapper.deletePoetrySenseByIds(ids);
    }

    /**
     * 删除诗词常识信息
     * 
     * @param id 诗词常识主键
     * @return 结果
     */
    @Override
    public int deletePoetrySenseById(Long id)
    {
        return bizPoetrySenseMapper.deletePoetrySenseById(id);
    }
}
