package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.DictionaryChildren;
import com.ruoyi.business.mapper.BizDictionaryChildrenMapper;
import com.ruoyi.business.service.IDictionaryChildrenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizDictionaryChildrenServiceImpl implements IDictionaryChildrenService
{
    @Autowired
    private BizDictionaryChildrenMapper bizDictionaryChildrenMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public DictionaryChildren selectDictionaryChildrenById(String id)
    {
        return bizDictionaryChildrenMapper.selectDictionaryChildrenById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dictionaryChildren 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<DictionaryChildren> selectDictionaryChildrenList(DictionaryChildren dictionaryChildren)
    {
        return bizDictionaryChildrenMapper.selectDictionaryChildrenList(dictionaryChildren);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param dictionaryChildren 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDictionaryChildren(DictionaryChildren dictionaryChildren)
    {
        return bizDictionaryChildrenMapper.insertDictionaryChildren(dictionaryChildren);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param dictionaryChildren 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDictionaryChildren(DictionaryChildren dictionaryChildren)
    {
        return bizDictionaryChildrenMapper.updateDictionaryChildren(dictionaryChildren);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDictionaryChildrenByIds(String[] ids)
    {
        return bizDictionaryChildrenMapper.deleteDictionaryChildrenByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDictionaryChildrenById(String id)
    {
        return bizDictionaryChildrenMapper.deleteDictionaryChildrenById(id);
    }
}
