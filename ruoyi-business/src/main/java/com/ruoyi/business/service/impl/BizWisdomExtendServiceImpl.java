package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.WisdomExtend;
import com.ruoyi.business.mapper.BizWisdomExtendMapper;
import com.ruoyi.business.service.IWisdomExtendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 智慧元典扩展Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizWisdomExtendServiceImpl implements IWisdomExtendService
{
    @Autowired
    private BizWisdomExtendMapper bizWisdomExtendMapper;

    /**
     * 查询智慧元典扩展
     * 
     * @param id 智慧元典扩展主键
     * @return 智慧元典扩展
     */
    @Override
    public WisdomExtend selectWisdomExtendById(Long id)
    {
        return bizWisdomExtendMapper.selectWisdomExtendById(id);
    }

    /**
     * 查询智慧元典扩展列表
     * 
     * @param wisdomExtend 智慧元典扩展
     * @return 智慧元典扩展
     */
    @Override
    public List<WisdomExtend> selectWisdomExtendList(WisdomExtend wisdomExtend)
    {
        return bizWisdomExtendMapper.selectWisdomExtendList(wisdomExtend);
    }

    /**
     * 新增智慧元典扩展
     * 
     * @param wisdomExtend 智慧元典扩展
     * @return 结果
     */
    @Override
    public int insertWisdomExtend(WisdomExtend wisdomExtend)
    {
        return bizWisdomExtendMapper.insertWisdomExtend(wisdomExtend);
    }

    /**
     * 修改智慧元典扩展
     * 
     * @param wisdomExtend 智慧元典扩展
     * @return 结果
     */
    @Override
    public int updateWisdomExtend(WisdomExtend wisdomExtend)
    {
        return bizWisdomExtendMapper.updateWisdomExtend(wisdomExtend);
    }

    /**
     * 批量删除智慧元典扩展
     * 
     * @param ids 需要删除的智慧元典扩展主键
     * @return 结果
     */
    @Override
    public int deleteWisdomExtendByIds(Long[] ids)
    {
        return bizWisdomExtendMapper.deleteWisdomExtendByIds(ids);
    }

    /**
     * 删除智慧元典扩展信息
     * 
     * @param id 智慧元典扩展主键
     * @return 结果
     */
    @Override
    public int deleteWisdomExtendById(Long id)
    {
        return bizWisdomExtendMapper.deleteWisdomExtendById(id);
    }
}
