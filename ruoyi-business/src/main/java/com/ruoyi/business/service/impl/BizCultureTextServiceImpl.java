package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.CultureText;
import com.ruoyi.business.mapper.BizCultureTextMapper;
import com.ruoyi.business.service.ICultureTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 中华文化内容Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizCultureTextServiceImpl implements ICultureTextService
{
    @Autowired
    private BizCultureTextMapper bizCultureTextMapper;

    /**
     * 查询中华文化内容
     * 
     * @param id 中华文化内容主键
     * @return 中华文化内容
     */
    @Override
    public CultureText selectCultureTextById(Long id)
    {
        return bizCultureTextMapper.selectCultureTextById(id);
    }

    /**
     * 查询中华文化内容列表
     * 
     * @param cultureText 中华文化内容
     * @return 中华文化内容
     */
    @Override
    public List<CultureText> selectCultureTextList(CultureText cultureText)
    {
        return bizCultureTextMapper.selectCultureTextList(cultureText);
    }

    /**
     * 新增中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    @Override
    public int insertCultureText(CultureText cultureText)
    {
        return bizCultureTextMapper.insertCultureText(cultureText);
    }

    /**
     * 修改中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    @Override
    public int updateCultureText(CultureText cultureText)
    {
        return bizCultureTextMapper.updateCultureText(cultureText);
    }

    /**
     * 批量删除中华文化内容
     * 
     * @param ids 需要删除的中华文化内容主键
     * @return 结果
     */
    @Override
    public int deleteCultureTextByIds(Long[] ids)
    {
        return bizCultureTextMapper.deleteCultureTextByIds(ids);
    }

    /**
     * 删除中华文化内容信息
     * 
     * @param id 中华文化内容主键
     * @return 结果
     */
    @Override
    public int deleteCultureTextById(Long id)
    {
        return bizCultureTextMapper.deleteCultureTextById(id);
    }
}
