package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.WisdomText;
import com.ruoyi.business.mapper.BizWisdomTextMapper;
import com.ruoyi.business.service.IWisdomTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 智慧元典正文Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizWisdomTextServiceImpl implements IWisdomTextService
{
    @Autowired
    private BizWisdomTextMapper bizWisdomTextMapper;

    /**
     * 查询智慧元典正文
     * 
     * @param id 智慧元典正文主键
     * @return 智慧元典正文
     */
    @Override
    public WisdomText selectWisdomTextById(Long id)
    {
        return bizWisdomTextMapper.selectWisdomTextById(id);
    }

    /**
     * 查询智慧元典正文列表
     * 
     * @param wisdomText 智慧元典正文
     * @return 智慧元典正文
     */
    @Override
    public List<WisdomText> selectWisdomTextList(WisdomText wisdomText)
    {
        return bizWisdomTextMapper.selectWisdomTextList(wisdomText);
    }

    /**
     * 新增智慧元典正文
     * 
     * @param wisdomText 智慧元典正文
     * @return 结果
     */
    @Override
    public int insertWisdomText(WisdomText wisdomText)
    {
        return bizWisdomTextMapper.insertWisdomText(wisdomText);
    }

    /**
     * 修改智慧元典正文
     * 
     * @param wisdomText 智慧元典正文
     * @return 结果
     */
    @Override
    public int updateWisdomText(WisdomText wisdomText)
    {
        return bizWisdomTextMapper.updateWisdomText(wisdomText);
    }

    /**
     * 批量删除智慧元典正文
     * 
     * @param ids 需要删除的智慧元典正文主键
     * @return 结果
     */
    @Override
    public int deleteWisdomTextByIds(Long[] ids)
    {
        return bizWisdomTextMapper.deleteWisdomTextByIds(ids);
    }

    /**
     * 删除智慧元典正文信息
     * 
     * @param id 智慧元典正文主键
     * @return 结果
     */
    @Override
    public int deleteWisdomTextById(Long id)
    {
        return bizWisdomTextMapper.deleteWisdomTextById(id);
    }
}
