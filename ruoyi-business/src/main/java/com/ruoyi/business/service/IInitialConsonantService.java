package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.InitialConsonant;

/**
 * 声母 韵母Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IInitialConsonantService 
{
    /**
     * 查询声母 韵母
     * 
     * @param id 声母 韵母主键
     * @return 声母 韵母
     */
    public InitialConsonant selectInitialConsonantById(Long id);

    /**
     * 查询声母 韵母列表
     * 
     * @param initialConsonant 声母 韵母
     * @return 声母 韵母集合
     */
    public List<InitialConsonant> selectInitialConsonantList(InitialConsonant initialConsonant);

    /**
     * 新增声母 韵母
     * 
     * @param initialConsonant 声母 韵母
     * @return 结果
     */
    public int insertInitialConsonant(InitialConsonant initialConsonant);

    /**
     * 修改声母 韵母
     * 
     * @param initialConsonant 声母 韵母
     * @return 结果
     */
    public int updateInitialConsonant(InitialConsonant initialConsonant);

    /**
     * 批量删除声母 韵母
     * 
     * @param ids 需要删除的声母 韵母主键集合
     * @return 结果
     */
    public int deleteInitialConsonantByIds(Long[] ids);

    /**
     * 删除声母 韵母信息
     * 
     * @param id 声母 韵母主键
     * @return 结果
     */
    public int deleteInitialConsonantById(Long id);
}
