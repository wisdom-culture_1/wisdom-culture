package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.IdiomSummary;
import com.ruoyi.business.entity.IdiomSummaryRsp;

/**
 * 成语汇总Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IIdiomSummaryService 
{
    /**
     * 查询成语汇总
     * 
     * @param id 成语汇总主键
     * @return 成语汇总
     */
    public IdiomSummary selectIdiomSummaryById(Long id);


    /**
     * 查询成语汇总
     *
     * @param id 成语汇总主键
     * @return 成语汇总
     */
    public IdiomSummaryRsp selectIdiomSummaryUserIdAndStatus();

    /**
     * 查询成语汇总列表
     * 
     * @param idiomSummary 成语汇总
     * @return 成语汇总集合
     */
    public List<IdiomSummary> selectIdiomSummaryList(IdiomSummary idiomSummary);

    /**
     * 新增成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    public int insertIdiomSummary(IdiomSummary idiomSummary);

    /**
     * 修改成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    public int updateIdiomSummary(IdiomSummary idiomSummary);

    /**
     * 批量删除成语汇总
     * 
     * @param ids 需要删除的成语汇总主键集合
     * @return 结果
     */
    public int deleteIdiomSummaryByIds(Long[] ids);

    /**
     * 删除成语汇总信息
     * 
     * @param id 成语汇总主键
     * @return 结果
     */
    public int deleteIdiomSummaryById(Long id);
}
