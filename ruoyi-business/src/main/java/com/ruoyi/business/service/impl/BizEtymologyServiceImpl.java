package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ChineseCharacters;
import com.ruoyi.business.entity.Etymology;
import com.ruoyi.business.entity.EtymologyChildren;
import com.ruoyi.business.mapper.BizChineseCharactersMapper;
import com.ruoyi.business.mapper.BizEtymologyChildrenMapper;
import com.ruoyi.business.mapper.BizEtymologyMapper;
import com.ruoyi.business.service.IEtymologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 字源Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizEtymologyServiceImpl implements IEtymologyService
{
    @Autowired
    private BizEtymologyMapper bizEtymologyMapper;

    @Autowired
    private BizEtymologyChildrenMapper bizEtymologyChildrenMapper;

    @Autowired
    private BizChineseCharactersMapper bizChineseCharactersMapper;

    /**
     * 查询字源
     * 
     * @param id 字源主键
     * @return 字源
     */
    @Override
    public Etymology selectEtymologyById(Long id)
    {
        Etymology etymology = bizEtymologyMapper.selectEtymologyById(id);
        EtymologyChildren etymologyChildren = new EtymologyChildren();
        etymologyChildren.setEtymologyId(etymology.getId());
        List<EtymologyChildren> etymologyChildren1 = bizEtymologyChildrenMapper.selectEtymologyChildrenList(etymologyChildren);

        // 联查字典表，获取相同汉字放到child 的 evolutionImageSrc 字段中
        for (EtymologyChildren children : etymologyChildren1) {
            ChineseCharacters chineseCharacters = new ChineseCharacters();
            chineseCharacters.setCharacter(children.getExampleWord());
            List<ChineseCharacters> chineseCharacters1 = bizChineseCharactersMapper.selectChineseCharactersList(chineseCharacters);
//            List childPinyinList = bizEtymologyChildrenMapper.selectPinyinById(children.getId());
            //如果 chineseCharacters1 不为空
            if (!chineseCharacters1.isEmpty()){
                children.setEvolutionImageSrc(chineseCharacters1.get(0).getEvolutionImageSrc());
            }
        }

        etymology.setEtymologyChild(etymologyChildren1);

        return etymology;
    }

    /**
     * 查询字源列表
     * 
     * @param etymology 字源
     * @return 字源
     */
    @Override
    public List<Etymology> selectEtymologyList(Etymology etymology)
    {
        return bizEtymologyMapper.selectEtymologyList(etymology);
    }

    /**
     * 新增字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    @Override
    public int insertEtymology(Etymology etymology)
    {
        return bizEtymologyMapper.insertEtymology(etymology);
    }

    /**
     * 修改字源
     * 
     * @param etymology 字源
     * @return 结果
     */
    @Override
    public int updateEtymology(Etymology etymology)
    {
        return bizEtymologyMapper.updateEtymology(etymology);
    }

    /**
     * 批量删除字源
     * 
     * @param ids 需要删除的字源主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyByIds(Long[] ids)
    {
        return bizEtymologyMapper.deleteEtymologyByIds(ids);
    }

    /**
     * 删除字源信息
     * 
     * @param id 字源主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyById(Long id)
    {
        return bizEtymologyMapper.deleteEtymologyById(id);
    }
}
