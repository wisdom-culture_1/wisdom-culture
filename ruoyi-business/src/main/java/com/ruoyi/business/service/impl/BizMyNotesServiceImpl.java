package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.MyNotes;
import com.ruoyi.business.mapper.BizMyNotesMapper;
import com.ruoyi.business.service.IMyNotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizMyNotesServiceImpl implements IMyNotesService
{
    @Autowired
    private BizMyNotesMapper bizMyNotesMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MyNotes selectMyNotesById(Long id)
    {
        return bizMyNotesMapper.selectMyNotesById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param myNotes 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MyNotes> selectMyNotesList(MyNotes myNotes)
    {
        return bizMyNotesMapper.selectMyNotesList(myNotes);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param myNotes 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMyNotes(MyNotes myNotes)
    {
        return bizMyNotesMapper.insertMyNotes(myNotes);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param myNotes 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMyNotes(MyNotes myNotes)
    {
        return bizMyNotesMapper.updateMyNotes(myNotes);
    }

    @Override
    public int deleteMyNotesByIds(String id) {
        return 0;
    }


    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMyNotesById(String id)
    {
        return bizMyNotesMapper.deleteMyNotesById(id);
    }
}
