package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.*;
import com.ruoyi.business.mapper.BizChineseCharactersSummaryMapper;
import com.ruoyi.business.mapper.BizHelpCenterMapper;
import com.ruoyi.business.mapper.BizIdiomSummaryMapper;
import com.ruoyi.business.mapper.BizReciteMapper;
import com.ruoyi.business.service.IHelpCenterService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 帮助中心Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-12
 */
@Service
public class BizHelpCenterServiceImpl implements IHelpCenterService
{
    @Autowired
    private BizHelpCenterMapper helpCenterMapper;
    @Autowired
    BizChineseCharactersSummaryMapper bizChineseCharactersSummaryMapper;
    @Autowired
    BizIdiomSummaryMapper bizIdiomSummaryMapper;
    @Autowired
    BizReciteMapper bizReciteMapper;

    /**
     * 查询帮助中心
     * 
     * @param id 帮助中心主键
     * @return 帮助中心
     */
    @Override
    public HelpCenter selectHelpCenterById(Long id)
    {
        return helpCenterMapper.selectHelpCenterById(id);
    }

    /**
     * 查询帮助中心列表
     * 
     * @param helpCenter 帮助中心
     * @return 帮助中心
     */
    @Override
    public List<HelpCenter> selectHelpCenterList(HelpCenter helpCenter)
    {
        return helpCenterMapper.selectHelpCenterList(helpCenter);
    }

    /**
     * 新增帮助中心
     * 
     * @param helpCenter 帮助中心
     * @return 结果
     */
    @Override
    public int insertHelpCenter(HelpCenter helpCenter)
    {
        return helpCenterMapper.insertHelpCenter(helpCenter);
    }

    /**
     * 修改帮助中心
     * 
     * @param helpCenter 帮助中心
     * @return 结果
     */
    @Override
    public int updateHelpCenter(HelpCenter helpCenter)
    {
        return helpCenterMapper.updateHelpCenter(helpCenter);
    }

    /**
     * 批量删除帮助中心
     * 
     * @param ids 需要删除的帮助中心主键
     * @return 结果
     */
    @Override
    public int deleteHelpCenterByIds(Long[] ids)
    {
        return helpCenterMapper.deleteHelpCenterByIds(ids);
    }

    /**
     * 删除帮助中心信息
     * 
     * @param id 帮助中心主键
     * @return 结果
     */
    @Override
    public int deleteHelpCenterById(Long id)
    {
        return helpCenterMapper.deleteHelpCenterById(id);
    }

    @Override
    public AccountCenter center() {
        AccountCenter center = new AccountCenter();

        ChineseCharactersSummary summary=new ChineseCharactersSummary();
        // todo userId
        summary.setUserId(SecurityUtils.getUserId());
        Integer integer = bizChineseCharactersSummaryMapper.selectByUserId(summary);
        center.setCharacter(integer);

        IdiomSummary idiomSummary = new IdiomSummary();
        idiomSummary.setUserId(SecurityUtils.getUserId());
        Integer integer1 = bizIdiomSummaryMapper.selectIdiomSummaryByUserId(idiomSummary);
        center.setIdiom(integer1);

        Recite recite = new Recite();
        recite.setUserId(SecurityUtils.getUserId());
        Integer integer2 = bizReciteMapper.selectReciteByUserId(recite);
        center.setArticle(integer2);

        return center;
    }
}
