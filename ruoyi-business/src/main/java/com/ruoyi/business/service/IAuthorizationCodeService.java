package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.AuthorizationCode;

/**
 * 授权码Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IAuthorizationCodeService 
{
    /**
     * 查询授权码
     * 
     * @param id 授权码主键
     * @return 授权码
     */
    public AuthorizationCode selectAuthorizationCodeById(Long id);

    /**
     * 查询授权码列表
     * 
     * @param authorizationCode 授权码
     * @return 授权码集合
     */
    public List<AuthorizationCode> selectAuthorizationCodeList(AuthorizationCode authorizationCode);

    /**
     * 新增授权码
     * 
     * @param authorizationCode 授权码
     * @return 结果
     */
    public int insertAuthorizationCode(AuthorizationCode authorizationCode);

    /**
     * 修改授权码
     * 
     * @param authorizationCode 授权码
     * @return 结果
     */
    public int updateAuthorizationCode(AuthorizationCode authorizationCode);

    /**
     * 批量删除授权码
     * 
     * @param ids 需要删除的授权码主键集合
     * @return 结果
     */
    public int deleteAuthorizationCodeByIds(Long[] ids);

    /**
     * 删除授权码信息
     * 
     * @param id 授权码主键
     * @return 结果
     */
    public int deleteAuthorizationCodeById(Long id);

    AuthorizationCode getRandomCode();
}
