package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Beyondcampus;
import com.ruoyi.business.mapper.BizBeyondcampusMapper;
import com.ruoyi.business.service.IBeyondcampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizBeyondcampusServiceImpl implements IBeyondcampusService
{
    @Autowired
    private BizBeyondcampusMapper bizBeyondcampusMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Beyondcampus selectBeyondcampusById(Long id)
    {
        return bizBeyondcampusMapper.selectBeyondcampusById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param beyondcampus 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Beyondcampus> selectBeyondcampusList(Beyondcampus beyondcampus)
    {
        return bizBeyondcampusMapper.selectBeyondcampusList(beyondcampus);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param beyondcampus 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBeyondcampus(Beyondcampus beyondcampus)
    {
        return bizBeyondcampusMapper.insertBeyondcampus(beyondcampus);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param beyondcampus 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBeyondcampus(Beyondcampus beyondcampus)
    {
        return bizBeyondcampusMapper.updateBeyondcampus(beyondcampus);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBeyondcampusByIds(Long[] ids)
    {
        return bizBeyondcampusMapper.deleteBeyondcampusByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBeyondcampusById(Long id)
    {
        return bizBeyondcampusMapper.deleteBeyondcampusById(id);
    }
}
