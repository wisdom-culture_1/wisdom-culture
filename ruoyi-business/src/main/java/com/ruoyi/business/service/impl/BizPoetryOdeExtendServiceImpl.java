package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.PoetryOdeExtend;
import com.ruoyi.business.mapper.BizPoetryOdeExtendMapper;
import com.ruoyi.business.service.IPoetryOdeExtendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 诗词歌赋扩展Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizPoetryOdeExtendServiceImpl implements IPoetryOdeExtendService
{
    @Autowired
    private BizPoetryOdeExtendMapper bizPoetryOdeExtendMapper;

    /**
     * 查询诗词歌赋扩展
     * 
     * @param id 诗词歌赋扩展主键
     * @return 诗词歌赋扩展
     */
    @Override
    public PoetryOdeExtend selectPoetryOdeExtendById(Long id)
    {
        return bizPoetryOdeExtendMapper.selectPoetryOdeExtendById(id);
    }

    /**
     * 查询诗词歌赋扩展列表
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 诗词歌赋扩展
     */
    @Override
    public List<PoetryOdeExtend> selectPoetryOdeExtendList(PoetryOdeExtend poetryOdeExtend)
    {
        return bizPoetryOdeExtendMapper.selectPoetryOdeExtendList(poetryOdeExtend);
    }

    /**
     * 新增诗词歌赋扩展
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 结果
     */
    @Override
    public int insertPoetryOdeExtend(PoetryOdeExtend poetryOdeExtend)
    {
        return bizPoetryOdeExtendMapper.insertPoetryOdeExtend(poetryOdeExtend);
    }

    /**
     * 修改诗词歌赋扩展
     * 
     * @param poetryOdeExtend 诗词歌赋扩展
     * @return 结果
     */
    @Override
    public int updatePoetryOdeExtend(PoetryOdeExtend poetryOdeExtend)
    {
        return bizPoetryOdeExtendMapper.updatePoetryOdeExtend(poetryOdeExtend);
    }

    /**
     * 批量删除诗词歌赋扩展
     * 
     * @param ids 需要删除的诗词歌赋扩展主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeExtendByIds(Long[] ids)
    {
        return bizPoetryOdeExtendMapper.deletePoetryOdeExtendByIds(ids);
    }

    /**
     * 删除诗词歌赋扩展信息
     * 
     * @param id 诗词歌赋扩展主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeExtendById(Long id)
    {
        return bizPoetryOdeExtendMapper.deletePoetryOdeExtendById(id);
    }
}
