package com.ruoyi.business.service;

import com.ruoyi.business.entity.ExcerptType;

import java.util.List;

/**
 * 金句摘抄分类Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IExcerptTypeService 
{
    /**
     * 查询金句摘抄分类
     * 
     * @param id 金句摘抄分类主键
     * @return 金句摘抄分类
     */
    public ExcerptType selectExcerptTypeById(Long id);

    /**
     * 查询金句摘抄分类列表
     * 
     * @param excerptType 金句摘抄分类
     * @return 金句摘抄分类集合
     */
    public List<ExcerptType> selectExcerptTypeList(ExcerptType excerptType);

    /**
     * 新增金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    public int insertExcerptType(ExcerptType excerptType);

    /**
     * 修改金句摘抄分类
     * 
     * @param excerptType 金句摘抄分类
     * @return 结果
     */
    public int updateExcerptType(ExcerptType excerptType);

    /**
     * 批量删除金句摘抄分类
     * 
     * @param ids 需要删除的金句摘抄分类主键集合
     * @return 结果
     */
    public int deleteExcerptTypeByIds(Long[] ids);

    /**
     * 删除金句摘抄分类信息
     * 
     * @param id 金句摘抄分类主键
     * @return 结果
     */
    public int deleteExcerptTypeById(Long id);
}
