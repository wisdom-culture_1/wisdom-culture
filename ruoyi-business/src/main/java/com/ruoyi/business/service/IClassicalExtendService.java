package com.ruoyi.business.service;

import com.ruoyi.business.entity.ClassicalExtend;

import java.util.List;

/**
 * 文言名篇扩展Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IClassicalExtendService 
{
    /**
     * 查询文言名篇扩展
     * 
     * @param id 文言名篇扩展主键
     * @return 文言名篇扩展
     */
    public ClassicalExtend selectClassicalExtendById(Long id);

    /**
     * 查询文言名篇扩展列表
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 文言名篇扩展集合
     */
    public List<ClassicalExtend> selectClassicalExtendList(ClassicalExtend classicalExtend);

    /**
     * 新增文言名篇扩展
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 结果
     */
    public int insertClassicalExtend(ClassicalExtend classicalExtend);

    /**
     * 修改文言名篇扩展
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 结果
     */
    public int updateClassicalExtend(ClassicalExtend classicalExtend);

    /**
     * 批量删除文言名篇扩展
     * 
     * @param ids 需要删除的文言名篇扩展主键集合
     * @return 结果
     */
    public int deleteClassicalExtendByIds(Long[] ids);

    /**
     * 删除文言名篇扩展信息
     * 
     * @param id 文言名篇扩展主键
     * @return 结果
     */
    public int deleteClassicalExtendById(Long id);
}
