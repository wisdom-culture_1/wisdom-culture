package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.ModuleDictMapper;
import com.ruoyi.business.entity.ModuleDict;
import com.ruoyi.business.service.IModuleDictService;

/**
 * 模块对应关系字典Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
@Service
public class ModuleDictServiceImpl implements IModuleDictService 
{
    @Autowired
    private ModuleDictMapper moduleDictMapper;

    /**
     * 查询模块对应关系字典
     * 
     * @param id 模块对应关系字典主键
     * @return 模块对应关系字典
     */
    @Override
    public ModuleDict selectModuleDictById(Long id)
    {
        return moduleDictMapper.selectModuleDictById(id);
    }

    /**
     * 查询模块对应关系字典列表
     * 
     * @param moduleDict 模块对应关系字典
     * @return 模块对应关系字典
     */
    @Override
    public List<ModuleDict> selectModuleDictList(ModuleDict moduleDict)
    {
        return moduleDictMapper.selectModuleDictList(moduleDict);
    }

    /**
     * 新增模块对应关系字典
     * 
     * @param moduleDict 模块对应关系字典
     * @return 结果
     */
    @Override
    public int insertModuleDict(ModuleDict moduleDict)
    {
        return moduleDictMapper.insertModuleDict(moduleDict);
    }

    /**
     * 修改模块对应关系字典
     * 
     * @param moduleDict 模块对应关系字典
     * @return 结果
     */
    @Override
    public int updateModuleDict(ModuleDict moduleDict)
    {
        return moduleDictMapper.updateModuleDict(moduleDict);
    }

    /**
     * 批量删除模块对应关系字典
     * 
     * @param ids 需要删除的模块对应关系字典主键
     * @return 结果
     */
    @Override
    public int deleteModuleDictByIds(Long[] ids)
    {
        return moduleDictMapper.deleteModuleDictByIds(ids);
    }

    /**
     * 删除模块对应关系字典信息
     * 
     * @param id 模块对应关系字典主键
     * @return 结果
     */
    @Override
    public int deleteModuleDictById(Long id)
    {
        return moduleDictMapper.deleteModuleDictById(id);
    }
}
