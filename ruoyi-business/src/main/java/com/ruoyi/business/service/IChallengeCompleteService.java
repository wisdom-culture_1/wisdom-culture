package com.ruoyi.business.service;

import com.ruoyi.business.entity.ChallengeComplete;

import java.util.List;

/**
 * 挑战结果Service接口
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
public interface IChallengeCompleteService 
{
    /**
     * 查询挑战结果
     * 
     * @param id 挑战结果主键
     * @return 挑战结果
     */
    public ChallengeComplete selectChallengeCompleteById(Long id);

    /**
     * 查询挑战结果列表
     * 
     * @param challengeComplete 挑战结果
     * @return 挑战结果集合
     */
    public List<ChallengeComplete> selectChallengeCompleteList(ChallengeComplete challengeComplete);

    /**
     * 新增挑战结果
     * 
     * @param challengeComplete 挑战结果
     * @return 结果
     */
    public int insertChallengeComplete(ChallengeComplete challengeComplete);

    /**
     * 修改挑战结果
     * 
     * @param challengeComplete 挑战结果
     * @return 结果
     */
    public int updateChallengeComplete(ChallengeComplete challengeComplete);

    /**
     * 批量删除挑战结果
     * 
     * @param ids 需要删除的挑战结果主键集合
     * @return 结果
     */
    public int deleteChallengeCompleteByIds(Long[] ids);

    /**
     * 删除挑战结果信息
     * 
     * @param id 挑战结果主键
     * @return 结果
     */
    public int deleteChallengeCompleteById(Long id);
}
