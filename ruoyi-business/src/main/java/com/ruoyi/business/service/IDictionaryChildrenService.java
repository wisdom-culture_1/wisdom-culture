package com.ruoyi.business.service;

import com.ruoyi.business.entity.DictionaryChildren;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IDictionaryChildrenService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public DictionaryChildren selectDictionaryChildrenById(String id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dictionaryChildren 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<DictionaryChildren> selectDictionaryChildrenList(DictionaryChildren dictionaryChildren);

    /**
     * 新增【请填写功能名称】
     * 
     * @param dictionaryChildren 【请填写功能名称】
     * @return 结果
     */
    public int insertDictionaryChildren(DictionaryChildren dictionaryChildren);

    /**
     * 修改【请填写功能名称】
     * 
     * @param dictionaryChildren 【请填写功能名称】
     * @return 结果
     */
    public int updateDictionaryChildren(DictionaryChildren dictionaryChildren);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteDictionaryChildrenByIds(String[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDictionaryChildrenById(String id);
}
