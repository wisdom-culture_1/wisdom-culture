package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Recite;
import com.ruoyi.business.mapper.BizReciteMapper;
import com.ruoyi.business.service.IReciteService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 背诵打卡Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizReciteServiceImpl implements IReciteService
{
    @Autowired
    private BizReciteMapper bizReciteMapper;

    /**
     * 查询背诵打卡
     * 
     * @param id 背诵打卡主键
     * @return 背诵打卡
     */
    @Override
    public Recite selectReciteById(Long id)
    {
        return bizReciteMapper.selectReciteById(id);
    }

    /**
     * 查询背诵打卡列表
     * 
     * @param recite 背诵打卡
     * @return 背诵打卡
     */
    @Override
    public List<Recite> selectReciteList(Recite recite)
    {
        return bizReciteMapper.selectReciteList(recite);
    }

    /**
     * 新增背诵打卡
     * 
     * @param recite 背诵打卡
     * @return 结果
     */
    @Override
    public int insertRecite(Recite recite)
    {
        return bizReciteMapper.insertRecite(recite);
    }

    /**
     * 修改背诵打卡
     * 
     * @param recite 背诵打卡
     * @return 结果
     */
    @Override
    public int updateRecite(Recite recite)
    {
        return bizReciteMapper.updateRecite(recite);
    }

    /**
     * 批量删除背诵打卡
     * 
     * @param ids 需要删除的背诵打卡主键
     * @return 结果
     */
    @Override
    public int deleteReciteByIds(Long[] ids)
    {
        return bizReciteMapper.deleteReciteByIds(ids);
    }

    /**
     * 删除背诵打卡信息
     * 
     * @param id 背诵打卡主键
     * @return 结果
     */
    @Override
    public int deleteReciteById(Long id)
    {
        return bizReciteMapper.deleteReciteById(id);
    }
}
