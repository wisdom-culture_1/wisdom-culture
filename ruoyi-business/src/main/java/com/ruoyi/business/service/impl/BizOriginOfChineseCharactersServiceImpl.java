package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizOriginOfChineseCharactersMapper;
import com.ruoyi.business.entity.OriginOfChineseCharacters;
import com.ruoyi.business.service.IOriginOfChineseCharactersService;

/**
 * 汉字起源Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizOriginOfChineseCharactersServiceImpl implements IOriginOfChineseCharactersService
{
    @Autowired
    private BizOriginOfChineseCharactersMapper bizOriginOfChineseCharactersMapper;

    /**
     * 查询汉字起源
     * 
     * @param id 汉字起源主键
     * @return 汉字起源
     */
    @Override
    public OriginOfChineseCharacters selectOriginOfChineseCharactersById(Long id)
    {
        return bizOriginOfChineseCharactersMapper.selectOriginOfChineseCharactersById(id);
    }

    /**
     * 查询汉字起源列表
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 汉字起源
     */
    @Override
    public List<OriginOfChineseCharacters> selectOriginOfChineseCharactersList(OriginOfChineseCharacters originOfChineseCharacters)
    {
        List<OriginOfChineseCharacters> originOfChineseCharacters1 = bizOriginOfChineseCharactersMapper.selectOriginOfChineseCharactersList(originOfChineseCharacters);
//        originOfChineseCharacters1.forEach(item ->{
//            item.setAudioSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+item.getAudioSrc());
//        });
        return originOfChineseCharacters1;
    }

    /**
     * 新增汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    @Override
    public int insertOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters)
    {
        return bizOriginOfChineseCharactersMapper.insertOriginOfChineseCharacters(originOfChineseCharacters);
    }

    /**
     * 修改汉字起源
     * 
     * @param originOfChineseCharacters 汉字起源
     * @return 结果
     */
    @Override
    public int updateOriginOfChineseCharacters(OriginOfChineseCharacters originOfChineseCharacters)
    {
        return bizOriginOfChineseCharactersMapper.updateOriginOfChineseCharacters(originOfChineseCharacters);
    }

    /**
     * 批量删除汉字起源
     * 
     * @param ids 需要删除的汉字起源主键
     * @return 结果
     */
    @Override
    public int deleteOriginOfChineseCharactersByIds(Long[] ids)
    {
        return bizOriginOfChineseCharactersMapper.deleteOriginOfChineseCharactersByIds(ids);
    }

    /**
     * 删除汉字起源信息
     * 
     * @param id 汉字起源主键
     * @return 结果
     */
    @Override
    public int deleteOriginOfChineseCharactersById(Long id)
    {
        return bizOriginOfChineseCharactersMapper.deleteOriginOfChineseCharactersById(id);
    }
}
