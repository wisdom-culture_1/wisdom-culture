package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.IdiomStory;
import com.ruoyi.business.entity.IdiomSummary;
import com.ruoyi.business.mapper.BizIdiomStoryMapper;
import com.ruoyi.business.service.IIdiomStoryService;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 成语典故Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizIdiomStoryServiceImpl implements IIdiomStoryService
{
    @Autowired
    private BizIdiomStoryMapper bizIdiomStoryMapper;

    /**
     * 查询成语典故
     * 
     * @param id 成语典故主键
     * @return 成语典故
     */
    @Override
    public IdiomStory selectIdiomStoryById(Long id)
    {
        return bizIdiomStoryMapper.selectIdiomStoryById(id);
    }

    /**
     * 查询成语典故列表
     * 
     * @param idiomStory 成语典故
     * @return 成语典故
     */
    @Override
    public List<IdiomStory> selectIdiomStoryList(IdiomStory idiomStory)
    {
        if(StringUtils.isNotBlank(idiomStory.getStatus())){
            IdiomSummary idiomSummary = new IdiomSummary();
            idiomSummary.setUserId(SecurityUtils.getUserId());
            idiomSummary.setStatus(Long.valueOf(idiomStory.getStatus()));
            idiomSummary.setIdiom(idiomStory.getIdiom());
            return bizIdiomStoryMapper.selectIdiomStoryJoinSummaryByStatus(idiomSummary);
        }

//        idiomStory.setIdiom(idiomStory.getQ());
        return bizIdiomStoryMapper.selectIdiomStoryList(idiomStory);
    }

    /**
     * 新增成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    @Override
    public int insertIdiomStory(IdiomStory idiomStory)
    {
        return bizIdiomStoryMapper.insertIdiomStory(idiomStory);
    }

    /**
     * 修改成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    @Override
    public int updateIdiomStory(IdiomStory idiomStory)
    {
        return bizIdiomStoryMapper.updateIdiomStory(idiomStory);
    }

    /**
     * 批量删除成语典故
     * 
     * @param ids 需要删除的成语典故主键
     * @return 结果
     */
    @Override
    public int deleteIdiomStoryByIds(Long[] ids)
    {
        return bizIdiomStoryMapper.deleteIdiomStoryByIds(ids);
    }

    /**
     * 删除成语典故信息
     * 
     * @param id 成语典故主键
     * @return 结果
     */
    @Override
    public int deleteIdiomStoryById(Long id)
    {
        return bizIdiomStoryMapper.deleteIdiomStoryById(id);
    }
}
