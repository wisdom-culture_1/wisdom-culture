package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.ZdPinyin;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IZdPinyinService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ZdPinyin selectZdPinyinByXuhao(String xuhao);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdPinyin 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZdPinyin> selectZdPinyinList(ZdPinyin zdPinyin);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdPinyin 【请填写功能名称】
     * @return 结果
     */
    public int insertZdPinyin(ZdPinyin zdPinyin);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdPinyin 【请填写功能名称】
     * @return 结果
     */
    public int updateZdPinyin(ZdPinyin zdPinyin);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param xuhaos 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteZdPinyinByXuhaos(String[] xuhaos);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZdPinyinByXuhao(String xuhao);
}
