package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.IdiomSummary;
import com.ruoyi.business.entity.IdiomSummaryRsp;
import com.ruoyi.business.mapper.BizIdiomSummaryMapper;
import com.ruoyi.business.service.IIdiomSummaryService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 成语汇总Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizIdiomSummaryServiceImpl implements IIdiomSummaryService
{
    @Autowired
    private BizIdiomSummaryMapper bizIdiomSummaryMapper;

    /**
     * 查询成语汇总
     * 
     * @param id 成语汇总主键
     * @return 成语汇总
     */
    @Override
    public IdiomSummary selectIdiomSummaryById(Long id)
    {
        return bizIdiomSummaryMapper.selectIdiomSummaryById(id);
    }

    @Override
    public IdiomSummaryRsp selectIdiomSummaryUserIdAndStatus() {

        IdiomSummaryRsp rsp = new IdiomSummaryRsp();
        // todo userid
        IdiomSummary idiomSummary = new IdiomSummary();
        idiomSummary.setUserId(SecurityUtils.getUserId());
        idiomSummary.setStatus(Long.valueOf(1));
        Integer alreadyCount = bizIdiomSummaryMapper.selectIdiomSummaryUserIdAndStatus(idiomSummary);
        rsp.setAlready(Long.valueOf(alreadyCount));
        idiomSummary.setStatus(Long.valueOf(0));
        Integer incapableCount = bizIdiomSummaryMapper.selectIdiomSummaryUserIdAndStatus(idiomSummary);
        rsp.setIncapable(Long.valueOf(incapableCount));
        return rsp;
    }

    /**
     * 查询成语汇总列表
     * 
     * @param idiomSummary 成语汇总
     * @return 成语汇总
     */
    @Override
    public List<IdiomSummary> selectIdiomSummaryList(IdiomSummary idiomSummary)
    {
        return bizIdiomSummaryMapper.selectIdiomSummaryList(idiomSummary);
    }

    /**
     * 新增成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    @Override
    public int insertIdiomSummary(IdiomSummary idiomSummary)
    {
        // todo userid
        idiomSummary.setUserId(SecurityUtils.getUserId());
        IdiomSummary idiomSummary1 = bizIdiomSummaryMapper.selectIdiomSummaryIdomIdAndUserId(idiomSummary);
        if(null != idiomSummary1){
            return bizIdiomSummaryMapper.updateIdiomSummaryByIdiomIdAndUserId(idiomSummary);
        }else{
            return bizIdiomSummaryMapper.insertIdiomSummary(idiomSummary);
        }
    }

    /**
     * 修改成语汇总
     * 
     * @param idiomSummary 成语汇总
     * @return 结果
     */
    @Override
    public int updateIdiomSummary(IdiomSummary idiomSummary)
    {
        return bizIdiomSummaryMapper.updateIdiomSummary(idiomSummary);
    }

    /**
     * 批量删除成语汇总
     * 
     * @param ids 需要删除的成语汇总主键
     * @return 结果
     */
    @Override
    public int deleteIdiomSummaryByIds(Long[] ids)
    {
        return bizIdiomSummaryMapper.deleteIdiomSummaryByIds(ids);
    }

    /**
     * 删除成语汇总信息
     * 
     * @param id 成语汇总主键
     * @return 结果
     */
    @Override
    public int deleteIdiomSummaryById(Long id)
    {
        return bizIdiomSummaryMapper.deleteIdiomSummaryById(id);
    }
}
