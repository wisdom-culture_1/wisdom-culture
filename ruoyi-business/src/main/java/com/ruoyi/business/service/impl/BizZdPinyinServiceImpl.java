package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizZdPinyinMapper;
import com.ruoyi.business.entity.ZdPinyin;
import com.ruoyi.business.service.IZdPinyinService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizZdPinyinServiceImpl implements IZdPinyinService
{
    @Autowired
    private BizZdPinyinMapper bizZdPinyinMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ZdPinyin selectZdPinyinByXuhao(String xuhao)
    {
        return bizZdPinyinMapper.selectZdPinyinByXuhao(xuhao);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdPinyin 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ZdPinyin> selectZdPinyinList(ZdPinyin zdPinyin)
    {
        return bizZdPinyinMapper.selectZdPinyinList(zdPinyin);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdPinyin 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZdPinyin(ZdPinyin zdPinyin)
    {
        return bizZdPinyinMapper.insertZdPinyin(zdPinyin);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdPinyin 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZdPinyin(ZdPinyin zdPinyin)
    {
        return bizZdPinyinMapper.updateZdPinyin(zdPinyin);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param xuhaos 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdPinyinByXuhaos(String[] xuhaos)
    {
        return bizZdPinyinMapper.deleteZdPinyinByXuhaos(xuhaos);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param xuhao 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdPinyinByXuhao(String xuhao)
    {
        return bizZdPinyinMapper.deleteZdPinyinByXuhao(xuhao);
    }
}
