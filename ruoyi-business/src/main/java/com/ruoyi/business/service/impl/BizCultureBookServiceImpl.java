package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.CultureBook;
import com.ruoyi.business.mapper.BizCultureBookMapper;
import com.ruoyi.business.service.ICultureBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 中华文化书籍Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizCultureBookServiceImpl implements ICultureBookService
{
    @Autowired
    private BizCultureBookMapper bizCultureBookMapper;

    /**
     * 查询中华文化书籍
     * 
     * @param id 中华文化书籍主键
     * @return 中华文化书籍
     */
    @Override
    public CultureBook selectCultureBookById(Long id)
    {
        return bizCultureBookMapper.selectCultureBookById(id);
    }

    /**
     * 查询中华文化书籍列表
     * 
     * @param cultureBook 中华文化书籍
     * @return 中华文化书籍
     */
    @Override
    public List<CultureBook> selectCultureBookList(CultureBook cultureBook)
    {
        return bizCultureBookMapper.selectCultureBookList(cultureBook);
    }

    /**
     * 新增中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    @Override
    public int insertCultureBook(CultureBook cultureBook)
    {
        return bizCultureBookMapper.insertCultureBook(cultureBook);
    }

    /**
     * 修改中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    @Override
    public int updateCultureBook(CultureBook cultureBook)
    {
        return bizCultureBookMapper.updateCultureBook(cultureBook);
    }

    /**
     * 批量删除中华文化书籍
     * 
     * @param ids 需要删除的中华文化书籍主键
     * @return 结果
     */
    @Override
    public int deleteCultureBookByIds(Long[] ids)
    {
        return bizCultureBookMapper.deleteCultureBookByIds(ids);
    }

    /**
     * 删除中华文化书籍信息
     * 
     * @param id 中华文化书籍主键
     * @return 结果
     */
    @Override
    public int deleteCultureBookById(Long id)
    {
        return bizCultureBookMapper.deleteCultureBookById(id);
    }
}
