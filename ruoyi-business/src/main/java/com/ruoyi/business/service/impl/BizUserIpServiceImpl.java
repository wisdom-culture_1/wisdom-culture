package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizUserIpMapper;
import com.ruoyi.business.entity.UserIp;
import com.ruoyi.business.service.IUserIpService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizUserIpServiceImpl implements IUserIpService
{
    @Autowired
    private BizUserIpMapper bizUserIpMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public UserIp selectUserIpById(Long id)
    {
        return bizUserIpMapper.selectUserIpById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param userIp 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<UserIp> selectUserIpList(UserIp userIp)
    {
        return bizUserIpMapper.selectUserIpList(userIp);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param userIp 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertUserIp(UserIp userIp)
    {
        return bizUserIpMapper.insertUserIp(userIp);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param userIp 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateUserIp(UserIp userIp)
    {
        return bizUserIpMapper.updateUserIp(userIp);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteUserIpByIds(Long[] ids)
    {
        return bizUserIpMapper.deleteUserIpByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteUserIpById(Long id)
    {
        return bizUserIpMapper.deleteUserIpById(id);
    }
}
