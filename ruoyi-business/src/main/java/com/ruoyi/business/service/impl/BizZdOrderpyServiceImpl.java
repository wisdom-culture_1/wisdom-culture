package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizZdOrderpyMapper;
import com.ruoyi.business.entity.ZdOrderpy;
import com.ruoyi.business.service.IZdOrderpyService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizZdOrderpyServiceImpl implements IZdOrderpyService
{
    @Autowired
    private BizZdOrderpyMapper bizZdOrderpyMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param head 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ZdOrderpy selectZdOrderpyByHead(String head)
    {
        return bizZdOrderpyMapper.selectZdOrderpyByHead(head);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdOrderpy 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ZdOrderpy> selectZdOrderpyList(ZdOrderpy zdOrderpy)
    {
        return bizZdOrderpyMapper.selectZdOrderpyList(zdOrderpy);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdOrderpy 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZdOrderpy(ZdOrderpy zdOrderpy)
    {
        return bizZdOrderpyMapper.insertZdOrderpy(zdOrderpy);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdOrderpy 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZdOrderpy(ZdOrderpy zdOrderpy)
    {
        return bizZdOrderpyMapper.updateZdOrderpy(zdOrderpy);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param heads 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdOrderpyByHeads(String[] heads)
    {
        return bizZdOrderpyMapper.deleteZdOrderpyByHeads(heads);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param head 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteZdOrderpyByHead(String head)
    {
        return bizZdOrderpyMapper.deleteZdOrderpyByHead(head);
    }
}
