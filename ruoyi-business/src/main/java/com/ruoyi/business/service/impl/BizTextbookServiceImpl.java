package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Textbook;
import com.ruoyi.business.mapper.BizTextbookMapper;
import com.ruoyi.business.service.ITextbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizTextbookServiceImpl implements ITextbookService
{
    @Autowired
    private BizTextbookMapper bizTextbookMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Textbook selectTextbookById(Long id)
    {
        return bizTextbookMapper.selectTextbookById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param textbook 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Textbook> selectTextbookList(Textbook textbook)
    {
        return bizTextbookMapper.selectTextbookList(textbook);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param textbook 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTextbook(Textbook textbook)
    {
        return bizTextbookMapper.insertTextbook(textbook);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param textbook 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTextbook(Textbook textbook)
    {
        return bizTextbookMapper.updateTextbook(textbook);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTextbookByIds(Long[] ids)
    {
        return bizTextbookMapper.deleteTextbookByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTextbookById(Long id)
    {
        return bizTextbookMapper.deleteTextbookById(id);
    }
}
