package com.ruoyi.business.service;

import java.util.List;

import com.ruoyi.business.entity.GradePlan;
import com.ruoyi.business.entity.GradeStudyPlanDetail;

/**
 * 学习计划（细化到文章）Service接口
 * 
 * @author ruoyi
 * @date 2024-06-01
 */
public interface IGradeStudyPlanDetailService 
{
    /**
     * 查询学习计划（细化到文章）
     * 
     * @param id 学习计划（细化到文章）主键
     * @return 学习计划（细化到文章）
     */
    public GradeStudyPlanDetail selectGradeStudyPlanDetailById(Long id);

    /**
     * 查询学习计划（细化到文章）列表
     * 
     * @param gradeStudyPlanDetail 学习计划（细化到文章）
     * @return 学习计划（细化到文章）集合
     */
    public List<GradeStudyPlanDetail> selectGradeStudyPlanDetailList(GradeStudyPlanDetail gradeStudyPlanDetail);

    /**
     * 新增学习计划（细化到文章）
     * 
     * @param gradeStudyPlanDetail 学习计划（细化到文章）
     * @return 结果
     */
    public int insertGradeStudyPlanDetail(GradeStudyPlanDetail gradeStudyPlanDetail);

    /**
     * 修改学习计划（细化到文章）
     * 
     * @param gradeStudyPlanDetail 学习计划（细化到文章）
     * @return 结果
     */
    public int updateGradeStudyPlanDetail(GradeStudyPlanDetail gradeStudyPlanDetail);

    /**
     * 批量删除学习计划（细化到文章）
     * 
     * @param ids 需要删除的学习计划（细化到文章）主键集合
     * @return 结果
     */
    public int deleteGradeStudyPlanDetailByIds(Long[] ids);

    /**
     * 删除学习计划（细化到文章）信息
     * 
     * @param id 学习计划（细化到文章）主键
     * @return 结果
     */
    public int deleteGradeStudyPlanDetailById(Long id);

    /**
     * 批量删除
     */
    public int deleteGradeStudyPlanDetailByGrade(GradeStudyPlanDetail gradeStudyPlanDetail);
}
