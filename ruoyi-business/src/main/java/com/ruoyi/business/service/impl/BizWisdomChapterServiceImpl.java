package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.WisdomChapter;
import com.ruoyi.business.entity.WisdomNotes;
import com.ruoyi.business.entity.WisdomText;
import com.ruoyi.business.mapper.BizWisdomChapterMapper;
import com.ruoyi.business.mapper.BizWisdomNotesMapper;
import com.ruoyi.business.mapper.BizWisdomTextMapper;
import com.ruoyi.business.service.IWisdomChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 智慧元典章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizWisdomChapterServiceImpl implements IWisdomChapterService
{
    @Autowired
    private BizWisdomChapterMapper bizWisdomChapterMapper;
    @Autowired
    private BizWisdomNotesMapper bizWisdomNotesMapper;
    @Autowired
    private BizWisdomTextMapper bizWisdomTextMapper;

    /**
     * 查询智慧元典章节
     * 
     * @param id 智慧元典章节主键
     * @return 智慧元典章节
     */
    @Override
    public WisdomChapter selectWisdomChapterById(Long id)
    {
        return bizWisdomChapterMapper.selectWisdomChapterById(id);
    }

    /**
     * 查询智慧元典章节列表
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 智慧元典章节
     */
    @Override
    public List<WisdomChapter> selectWisdomChapterList(WisdomChapter wisdomChapter)
    {
        List<WisdomChapter> wisdomChapters = bizWisdomChapterMapper.selectWisdomChapterList(wisdomChapter);
//        wisdomChapters.forEach(item -> {
//            item.setAudioSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+item.getAudioSrc());
//        });
        return wisdomChapters;
    }


    /**
     * 查询智慧元典章节列表（仅列表）
     *
     * @param wisdomChapter 智慧元典章节
     * @return 智慧元典章节列表
     */
    @Override
    public List<WisdomChapter> selectWisdomChapterListSample(WisdomChapter wisdomChapter) {
//        List<WisdomChapter> wisdomChapters = bizWisdomChapterMapper.selectWisdomChapterList(wisdomChapter);
        List<WisdomChapter> wisdomChapters = bizWisdomChapterMapper.selectWisdomChapterListWithoutBigText(wisdomChapter);
        return wisdomChapters;
    }

    @Override
    public  List<WisdomChapter> selectWisdomByXuhao(WisdomChapter wisdomChapter) {
        List<WisdomChapter> wisdomChapters = bizWisdomChapterMapper.selectWisdomChapterList(wisdomChapter);

        wisdomChapters.forEach(item -> {
//            item.setAudioSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+item.getAudioSrc());
            WisdomNotes wisdomNotes = new WisdomNotes();
            wisdomNotes.setXuhao(wisdomChapter.getXuhao());
            List<WisdomNotes> wisdomNotes1 = bizWisdomNotesMapper.selectWisdomNotesList(wisdomNotes);
            item.setWisdomnotes(wisdomNotes1);

            WisdomText wisdomText = new WisdomText();
            wisdomText.setXuhao(wisdomChapter.getXuhao());
            List<WisdomText> wisdomTexts = bizWisdomTextMapper.selectWisdomTextList(wisdomText);
            item.setWisdomtext(wisdomTexts);
        });


        return wisdomChapters;
    }

    /**
     * 新增智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    @Override
    public int insertWisdomChapter(WisdomChapter wisdomChapter)
    {
        return bizWisdomChapterMapper.insertWisdomChapter(wisdomChapter);
    }

    /**
     * 修改智慧元典章节
     * 
     * @param wisdomChapter 智慧元典章节
     * @return 结果
     */
    @Override
    public int updateWisdomChapter(WisdomChapter wisdomChapter)
    {
        return bizWisdomChapterMapper.updateWisdomChapter(wisdomChapter);
    }

    /**
     * 批量删除智慧元典章节
     * 
     * @param ids 需要删除的智慧元典章节主键
     * @return 结果
     */
    @Override
    public int deleteWisdomChapterByIds(Long[] ids)
    {
        return bizWisdomChapterMapper.deleteWisdomChapterByIds(ids);
    }

    /**
     * 删除智慧元典章节信息
     * 
     * @param id 智慧元典章节主键
     * @return 结果
     */
    @Override
    public int deleteWisdomChapterById(Long id)
    {
        return bizWisdomChapterMapper.deleteWisdomChapterById(id);
    }
}
