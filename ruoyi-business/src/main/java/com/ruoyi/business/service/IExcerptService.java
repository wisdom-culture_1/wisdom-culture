package com.ruoyi.business.service;

import com.ruoyi.business.entity.Excerpt;

import java.util.List;

/**
 * 金句摘抄Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IExcerptService 
{
    /**
     * 查询金句摘抄
     * 
     * @param id 金句摘抄主键
     * @return 金句摘抄
     */
    public Excerpt selectExcerptById(Long id);

    /**
     * 查询金句摘抄列表
     * 
     * @param excerpt 金句摘抄
     * @return 金句摘抄集合
     */
    public List<Excerpt> selectExcerptList(Excerpt excerpt);

    /**
     * 新增金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    public int insertExcerpt(Excerpt excerpt);

    /**
     * 修改金句摘抄
     * 
     * @param excerpt 金句摘抄
     * @return 结果
     */
    public int updateExcerpt(Excerpt excerpt);

    /**
     * 批量删除金句摘抄
     * 
     * @param ids 需要删除的金句摘抄主键集合
     * @return 结果
     */
    public int deleteExcerptByIds(Long[] ids);

    /**
     * 删除金句摘抄信息
     * 
     * @param id 金句摘抄主键
     * @return 结果
     */
    public int deleteExcerptById(Long id);
}
