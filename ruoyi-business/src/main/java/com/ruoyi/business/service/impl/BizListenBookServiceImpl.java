package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ListenBook;
import com.ruoyi.business.mapper.BizListenBookMapper;
import com.ruoyi.business.service.IListenBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 诗词磨耳朵Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizListenBookServiceImpl implements IListenBookService
{
    @Autowired
    private BizListenBookMapper bizListenBookMapper;

    /**
     * 查询诗词磨耳朵
     * 
     * @param id 诗词磨耳朵主键
     * @return 诗词磨耳朵
     */
    @Override
    public ListenBook selectListenBookById(Long id)
    {
        return bizListenBookMapper.selectListenBookById(id);
    }

    /**
     * 查询诗词磨耳朵列表
     * 
     * @param listenBook 诗词磨耳朵
     * @return 诗词磨耳朵
     */
    @Override
    public List<ListenBook> selectListenBookList(ListenBook listenBook)
    {
        return bizListenBookMapper.selectListenBookList(listenBook);
    }

    /**
     * 新增诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    @Override
    public int insertListenBook(ListenBook listenBook)
    {
        return bizListenBookMapper.insertListenBook(listenBook);
    }

    /**
     * 修改诗词磨耳朵
     * 
     * @param listenBook 诗词磨耳朵
     * @return 结果
     */
    @Override
    public int updateListenBook(ListenBook listenBook)
    {
        return bizListenBookMapper.updateListenBook(listenBook);
    }

    /**
     * 批量删除诗词磨耳朵
     * 
     * @param ids 需要删除的诗词磨耳朵主键
     * @return 结果
     */
    @Override
    public int deleteListenBookByIds(Long[] ids)
    {
        return bizListenBookMapper.deleteListenBookByIds(ids);
    }

    /**
     * 删除诗词磨耳朵信息
     * 
     * @param id 诗词磨耳朵主键
     * @return 结果
     */
    @Override
    public int deleteListenBookById(Long id)
    {
        return bizListenBookMapper.deleteListenBookById(id);
    }
}
