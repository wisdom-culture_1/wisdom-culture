package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.Set;
import com.ruoyi.business.mapper.BizSetMapper;
import com.ruoyi.business.service.ISetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizSetServiceImpl implements ISetService
{
    @Autowired
    private BizSetMapper bizSetMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Set selectSetById(Long id)
    {
        return bizSetMapper.selectSetById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param set 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Set> selectSetList(Set set)
    {
        return bizSetMapper.selectSetList(set);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param set 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertSet(Set set)
    {
        return bizSetMapper.insertSet(set);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param set 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateSet(Set set)
    {
        return bizSetMapper.updateSet(set);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteSetByIds(Long[] ids)
    {
        return bizSetMapper.deleteSetByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteSetById(Long id)
    {
        return bizSetMapper.deleteSetById(id);
    }
}
