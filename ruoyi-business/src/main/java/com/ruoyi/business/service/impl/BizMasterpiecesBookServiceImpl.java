package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.MasterpiecesBook;
import com.ruoyi.business.entity.MasterpiecesChapters;
import com.ruoyi.business.mapper.BizMasterpiecesBookMapper;
import com.ruoyi.business.mapper.BizMasterpiecesChaptersMapper;
import com.ruoyi.business.service.IMasterpiecesBookService;
import com.ruoyi.common.utils.SecurityUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 名著书籍Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizMasterpiecesBookServiceImpl implements IMasterpiecesBookService
{
    @Autowired
    private BizMasterpiecesBookMapper bizMasterpiecesBookMapper;

    @Autowired
    private BizMasterpiecesChaptersMapper bizMasterpiecesChaptersMapper;

    /**
     * 查询名著书籍
     * 
     * @param id 名著书籍主键
     * @return 名著书籍
     */
    @Override
    public MasterpiecesBook selectMasterpiecesBookById(Long id)
    {
        MasterpiecesBook masterpiecesBook = bizMasterpiecesBookMapper.selectMasterpiecesBookById(id);
        MasterpiecesChapters masterpiecesChapters = new MasterpiecesChapters();
        masterpiecesChapters.setBookId(id);
        masterpiecesChapters.setUserId(SecurityUtils.getUserId());
        masterpiecesChapters.setPid(Long.valueOf(0));
        List<MasterpiecesChapters> masterpiecesChapters1 = bizMasterpiecesChaptersMapper.selectMasterpiecesChaptersAndText(masterpiecesChapters);
//        List<MasterpiecesChapters> masterpiecesChapters2 = Lists.newArrayList();
        masterpiecesBook.setList(masterpiecesChapters1);
        if(id == 26 || id ==68){
            masterpiecesChapters1.forEach(item ->{
//                masterpiecesChapters2.add(item);
                MasterpiecesChapters masterpiecesChapters3 = new MasterpiecesChapters();
                masterpiecesChapters3.setBookId(id);
                masterpiecesChapters3.setPid(item.getId());
                masterpiecesChapters3.setUserId(SecurityUtils.getUserId());
                List<MasterpiecesChapters> masterpiecesChapters4 = bizMasterpiecesChaptersMapper.selectMasterpiecesChaptersAndText(masterpiecesChapters3);
                if(CollectionUtils.isNotEmpty(masterpiecesChapters4)){
                    item.setChild(masterpiecesChapters4);
                }
            });
//            masterpiecesBook.setList(masterpiecesChapters2);
        }

        return masterpiecesBook;
    }

    /**
     * 查询名著书籍列表
     * 
     * @param masterpiecesBook 名著书籍
     * @return 名著书籍
     */
    @Override
    public List<MasterpiecesBook> selectMasterpiecesBookList(MasterpiecesBook masterpiecesBook)
    {
        return bizMasterpiecesBookMapper.selectMasterpiecesBookList(masterpiecesBook);
    }

    /**
     * 新增名著书籍
     * 
     * @param masterpiecesBook 名著书籍
     * @return 结果
     */
    @Override
    public int insertMasterpiecesBook(MasterpiecesBook masterpiecesBook)
    {
        return bizMasterpiecesBookMapper.insertMasterpiecesBook(masterpiecesBook);
    }

    /**
     * 修改名著书籍
     * 
     * @param masterpiecesBook 名著书籍
     * @return 结果
     */
    @Override
    public int updateMasterpiecesBook(MasterpiecesBook masterpiecesBook)
    {
        return bizMasterpiecesBookMapper.updateMasterpiecesBook(masterpiecesBook);
    }

    /**
     * 批量删除名著书籍
     * 
     * @param ids 需要删除的名著书籍主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesBookByIds(Long[] ids)
    {
        return bizMasterpiecesBookMapper.deleteMasterpiecesBookByIds(ids);
    }

    /**
     * 删除名著书籍信息
     * 
     * @param id 名著书籍主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesBookById(Long id)
    {
        return bizMasterpiecesBookMapper.deleteMasterpiecesBookById(id);
    }
}
