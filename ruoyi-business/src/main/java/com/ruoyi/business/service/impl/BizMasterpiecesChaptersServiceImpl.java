package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.MasterpiecesChapters;
import com.ruoyi.business.mapper.BizMasterpiecesChaptersMapper;
import com.ruoyi.business.service.IMasterpiecesChaptersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 名著章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizMasterpiecesChaptersServiceImpl implements IMasterpiecesChaptersService
{
    @Autowired
    private BizMasterpiecesChaptersMapper bizMasterpiecesChaptersMapper;

    /**
     * 查询名著章节
     * 
     * @param id 名著章节主键
     * @return 名著章节
     */
    @Override
    public MasterpiecesChapters selectMasterpiecesChaptersById(Long id)
    {
        return bizMasterpiecesChaptersMapper.selectMasterpiecesChaptersById(id);
    }

    /**
     * 查询名著章节列表
     * 
     * @param masterpiecesChapters 名著章节
     * @return 名著章节
     */
    @Override
    public List<MasterpiecesChapters> selectMasterpiecesChaptersList(MasterpiecesChapters masterpiecesChapters)
    {
        return bizMasterpiecesChaptersMapper.selectMasterpiecesChaptersList(masterpiecesChapters);
    }

    /**
     * 新增名著章节
     * 
     * @param masterpiecesChapters 名著章节
     * @return 结果
     */
    @Override
    public int insertMasterpiecesChapters(MasterpiecesChapters masterpiecesChapters)
    {
        return bizMasterpiecesChaptersMapper.insertMasterpiecesChapters(masterpiecesChapters);
    }

    /**
     * 修改名著章节
     * 
     * @param masterpiecesChapters 名著章节
     * @return 结果
     */
    @Override
    public int updateMasterpiecesChapters(MasterpiecesChapters masterpiecesChapters)
    {
        return bizMasterpiecesChaptersMapper.updateMasterpiecesChapters(masterpiecesChapters);
    }

    /**
     * 批量删除名著章节
     * 
     * @param ids 需要删除的名著章节主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesChaptersByIds(Long[] ids)
    {
        return bizMasterpiecesChaptersMapper.deleteMasterpiecesChaptersByIds(ids);
    }

    /**
     * 删除名著章节信息
     * 
     * @param id 名著章节主键
     * @return 结果
     */
    @Override
    public int deleteMasterpiecesChaptersById(Long id)
    {
        return bizMasterpiecesChaptersMapper.deleteMasterpiecesChaptersById(id);
    }
}
