package com.ruoyi.business.service;

import com.ruoyi.business.entity.ExpertLectureHall;

import java.util.List;

/**
 * 专家讲堂视频Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IExpertLectureHallService 
{
    /**
     * 查询专家讲堂视频
     * 
     * @param id 专家讲堂视频主键
     * @return 专家讲堂视频
     */
    public ExpertLectureHall selectExpertLectureHallById(Long id);

    /**
     * 查询专家讲堂视频列表
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 专家讲堂视频集合
     */
    public List<ExpertLectureHall> selectExpertLectureHallList(ExpertLectureHall expertLectureHall);

    /**
     * 新增专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    public int insertExpertLectureHall(ExpertLectureHall expertLectureHall);

    /**
     * 修改专家讲堂视频
     * 
     * @param expertLectureHall 专家讲堂视频
     * @return 结果
     */
    public int updateExpertLectureHall(ExpertLectureHall expertLectureHall);

    /**
     * 批量删除专家讲堂视频
     * 
     * @param ids 需要删除的专家讲堂视频主键集合
     * @return 结果
     */
    public int deleteExpertLectureHallByIds(Long[] ids);

    /**
     * 删除专家讲堂视频信息
     * 
     * @param id 专家讲堂视频主键
     * @return 结果
     */
    public int deleteExpertLectureHallById(Long id);
}
