package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.SystemConfig;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface ISystemConfigService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public SystemConfig selectSystemConfigById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param systemConfig 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<SystemConfig> selectSystemConfigList(SystemConfig systemConfig);

    /**
     * 新增【请填写功能名称】
     * 
     * @param systemConfig 【请填写功能名称】
     * @return 结果
     */
    public int insertSystemConfig(SystemConfig systemConfig);

    /**
     * 修改【请填写功能名称】
     * 
     * @param systemConfig 【请填写功能名称】
     * @return 结果
     */
    public int updateSystemConfig(SystemConfig systemConfig);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteSystemConfigByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteSystemConfigById(Long id);
}
