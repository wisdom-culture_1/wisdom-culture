package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.WisdomBook;
import com.ruoyi.business.mapper.BizWisdomBookMapper;
import com.ruoyi.business.service.IWisdomBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 智慧元典书籍Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizWisdomBookServiceImpl implements IWisdomBookService
{
    @Autowired
    private BizWisdomBookMapper bizWisdomBookMapper;

    /**
     * 查询智慧元典书籍
     * 
     * @param id 智慧元典书籍主键
     * @return 智慧元典书籍
     */
    @Override
    public WisdomBook selectWisdomBookById(Long id)
    {
        return bizWisdomBookMapper.selectWisdomBookById(id);
    }

    /**
     * 查询智慧元典书籍列表
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 智慧元典书籍
     */
    @Override
    public List<WisdomBook> selectWisdomBookList(WisdomBook wisdomBook)
    {
        return bizWisdomBookMapper.selectWisdomBookList(wisdomBook);
    }

    /**
     * 新增智慧元典书籍
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 结果
     */
    @Override
    public int insertWisdomBook(WisdomBook wisdomBook)
    {
        return bizWisdomBookMapper.insertWisdomBook(wisdomBook);
    }

    /**
     * 修改智慧元典书籍
     * 
     * @param wisdomBook 智慧元典书籍
     * @return 结果
     */
    @Override
    public int updateWisdomBook(WisdomBook wisdomBook)
    {
        return bizWisdomBookMapper.updateWisdomBook(wisdomBook);
    }

    /**
     * 批量删除智慧元典书籍
     * 
     * @param ids 需要删除的智慧元典书籍主键
     * @return 结果
     */
    @Override
    public int deleteWisdomBookByIds(Long[] ids)
    {
        return bizWisdomBookMapper.deleteWisdomBookByIds(ids);
    }

    /**
     * 删除智慧元典书籍信息
     * 
     * @param id 智慧元典书籍主键
     * @return 结果
     */
    @Override
    public int deleteWisdomBookById(Long id)
    {
        return bizWisdomBookMapper.deleteWisdomBookById(id);
    }
}
