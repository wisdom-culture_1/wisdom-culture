package com.ruoyi.business.service;

import com.ruoyi.business.entity.WisdomText;

import java.util.List;

/**
 * 智慧元典正文Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IWisdomTextService 
{
    /**
     * 查询智慧元典正文
     * 
     * @param id 智慧元典正文主键
     * @return 智慧元典正文
     */
    public WisdomText selectWisdomTextById(Long id);

    /**
     * 查询智慧元典正文列表
     * 
     * @param wisdomText 智慧元典正文
     * @return 智慧元典正文集合
     */
    public List<WisdomText> selectWisdomTextList(WisdomText wisdomText);

    /**
     * 新增智慧元典正文
     * 
     * @param wisdomText 智慧元典正文
     * @return 结果
     */
    public int insertWisdomText(WisdomText wisdomText);

    /**
     * 修改智慧元典正文
     * 
     * @param wisdomText 智慧元典正文
     * @return 结果
     */
    public int updateWisdomText(WisdomText wisdomText);

    /**
     * 批量删除智慧元典正文
     * 
     * @param ids 需要删除的智慧元典正文主键集合
     * @return 结果
     */
    public int deleteWisdomTextByIds(Long[] ids);

    /**
     * 删除智慧元典正文信息
     * 
     * @param id 智慧元典正文主键
     * @return 结果
     */
    public int deleteWisdomTextById(Long id);
}
