package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.PoetryOde;
import com.ruoyi.business.entity.PoetryOdeNotes;
import com.ruoyi.business.mapper.BizPoetryOdeMapper;
import com.ruoyi.business.mapper.BizPoetryOdeNotesMapper;
import com.ruoyi.business.service.IPoetryOdeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 诗词歌赋Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizPoetryOdeServiceImpl implements IPoetryOdeService
{
    @Autowired
    private BizPoetryOdeMapper bizPoetryOdeMapper;

    @Autowired
    private BizPoetryOdeNotesMapper bizPoetryOdeNotesMapper;

    /**
     * 查询诗词歌赋
     * 
     * @param id 诗词歌赋主键
     * @return 诗词歌赋
     */
    @Override
    public PoetryOde selectPoetryOdeById(Long id)
    {
        PoetryOde poetryOde = bizPoetryOdeMapper.selectPoetryOdeById(id);
        PoetryOdeNotes poetryOdeNotes = new PoetryOdeNotes();
        poetryOdeNotes.setPoetryOdeId(id);
        List<PoetryOdeNotes> poetryOdeNotes1 = bizPoetryOdeNotesMapper.selectPoetryOdeNotesList(poetryOdeNotes);
        poetryOde.setOdenodes(poetryOdeNotes1);
        return poetryOde;
    }

    /**
     * 查询诗词歌赋列表
     * 
     * @param poetryOde 诗词歌赋
     * @return 诗词歌赋
     */
    @Override
    public List<PoetryOde> selectPoetryOdeList(PoetryOde poetryOde)
    {
        return bizPoetryOdeMapper.selectPoetryOdeList(poetryOde);
    }

    /**
     * 新增诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    @Override
    public int insertPoetryOde(PoetryOde poetryOde)
    {
        return bizPoetryOdeMapper.insertPoetryOde(poetryOde);
    }

    /**
     * 修改诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    @Override
    public int updatePoetryOde(PoetryOde poetryOde)
    {
        return bizPoetryOdeMapper.updatePoetryOde(poetryOde);
    }

    /**
     * 批量删除诗词歌赋
     * 
     * @param ids 需要删除的诗词歌赋主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeByIds(Long[] ids)
    {
        return bizPoetryOdeMapper.deletePoetryOdeByIds(ids);
    }

    /**
     * 删除诗词歌赋信息
     * 
     * @param id 诗词歌赋主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeById(Long id)
    {
        return bizPoetryOdeMapper.deletePoetryOdeById(id);
    }
}
