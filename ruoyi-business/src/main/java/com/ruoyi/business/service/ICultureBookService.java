package com.ruoyi.business.service;

import com.ruoyi.business.entity.CultureBook;

import java.util.List;

/**
 * 中华文化书籍Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface ICultureBookService 
{
    /**
     * 查询中华文化书籍
     * 
     * @param id 中华文化书籍主键
     * @return 中华文化书籍
     */
    public CultureBook selectCultureBookById(Long id);

    /**
     * 查询中华文化书籍列表
     * 
     * @param cultureBook 中华文化书籍
     * @return 中华文化书籍集合
     */
    public List<CultureBook> selectCultureBookList(CultureBook cultureBook);

    /**
     * 新增中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    public int insertCultureBook(CultureBook cultureBook);

    /**
     * 修改中华文化书籍
     * 
     * @param cultureBook 中华文化书籍
     * @return 结果
     */
    public int updateCultureBook(CultureBook cultureBook);

    /**
     * 批量删除中华文化书籍
     * 
     * @param ids 需要删除的中华文化书籍主键集合
     * @return 结果
     */
    public int deleteCultureBookByIds(Long[] ids);

    /**
     * 删除中华文化书籍信息
     * 
     * @param id 中华文化书籍主键
     * @return 结果
     */
    public int deleteCultureBookById(Long id);
}
