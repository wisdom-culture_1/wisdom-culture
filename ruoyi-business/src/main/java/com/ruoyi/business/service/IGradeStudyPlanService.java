package com.ruoyi.business.service;


import com.ruoyi.business.entity.GradePlan;
import com.ruoyi.business.entity.GradeStudyPlan;

import java.util.List;

/**
 * 学习计划Service接口
 * 
 * @author ruoyi
 * @date 2024-04-14
 */
public interface IGradeStudyPlanService 
{
    /**
     * 查询学习计划
     * 
     * @param id 学习计划主键
     * @return 学习计划
     */
    public GradeStudyPlan selectGradeStudyPlanById(String id);

    /**
     * 查询学习计划列表
     * 
     * @param gradeStudyPlan 学习计划
     * @return 学习计划集合
     */
    public List<GradeStudyPlan> selectGradeStudyPlanList(GradeStudyPlan gradeStudyPlan);

    /**
     * 新增学习计划
     * 
     * @param gradeStudyPlan 学习计划
     * @return 结果
     */
    public int insertGradeStudyPlan(GradeStudyPlan gradeStudyPlan);

    /**
     * 修改学习计划
     * 
     * @param gradeStudyPlan 学习计划
     * @return 结果
     */
    public int updateGradeStudyPlan(GradeStudyPlan gradeStudyPlan);

    /**
     * 批量删除学习计划
     * 
     * @param ids 需要删除的学习计划主键集合
     * @return 结果
     */
    public int deleteGradeStudyPlanByIds(String[] ids);

    /**
     * 批量删除学习计划
     *
     * @param ids 需要删除的学习计划主键集合
     * @return 结果
     */
    public int deleteGradeStudyPlanByGrade(GradePlan plan);

    /**
     * 删除学习计划信息
     * 
     * @param id 学习计划主键
     * @return 结果
     */
    public int deleteGradeStudyPlanById(String id);
}
