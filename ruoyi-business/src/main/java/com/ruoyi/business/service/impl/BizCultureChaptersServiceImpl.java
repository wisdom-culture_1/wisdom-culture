package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.CultureChapters;
import com.ruoyi.business.mapper.BizCultureChaptersMapper;
import com.ruoyi.business.service.ICultureChaptersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文化章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizCultureChaptersServiceImpl implements ICultureChaptersService
{
    @Autowired
    private BizCultureChaptersMapper bizCultureChaptersMapper;

    /**
     * 查询文化章节
     * 
     * @param id 文化章节主键
     * @return 文化章节
     */
    @Override
    public CultureChapters selectCultureChaptersById(Long id)
    {
        return bizCultureChaptersMapper.selectCultureChaptersById(id);
    }

    /**
     * 查询文化章节列表
     * 
     * @param cultureChapters 文化章节
     * @return 文化章节
     */
    @Override
    public List<CultureChapters> selectCultureChaptersList(CultureChapters cultureChapters)
    {

        List<CultureChapters> cultureChapters1 = bizCultureChaptersMapper.selectCultureChaptersList(cultureChapters);
        cultureChapters1.forEach(item -> {
            CultureChapters cultureChapters2 = new CultureChapters();
            cultureChapters2.setPid(item.getId());
            List<CultureChapters> cultureChapters3 = bizCultureChaptersMapper.selectCultureChaptersList(cultureChapters2);
            item.setChild(cultureChapters3);
        });

//        return bizCultureChaptersMapper.selectCultureChaptersList(cultureChapters);
        return cultureChapters1;
    }

    /**
     * 新增文化章节
     * 
     * @param cultureChapters 文化章节
     * @return 结果
     */
    @Override
    public int insertCultureChapters(CultureChapters cultureChapters)
    {
        return bizCultureChaptersMapper.insertCultureChapters(cultureChapters);
    }

    /**
     * 修改文化章节
     * 
     * @param cultureChapters 文化章节
     * @return 结果
     */
    @Override
    public int updateCultureChapters(CultureChapters cultureChapters)
    {
        return bizCultureChaptersMapper.updateCultureChapters(cultureChapters);
    }

    /**
     * 批量删除文化章节
     * 
     * @param ids 需要删除的文化章节主键
     * @return 结果
     */
    @Override
    public int deleteCultureChaptersByIds(Long[] ids)
    {
        return bizCultureChaptersMapper.deleteCultureChaptersByIds(ids);
    }

    /**
     * 删除文化章节信息
     * 
     * @param id 文化章节主键
     * @return 结果
     */
    @Override
    public int deleteCultureChaptersById(Long id)
    {
        return bizCultureChaptersMapper.deleteCultureChaptersById(id);
    }
}
