package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.Alphabets;

/**
 * 字母Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IAlphabetsService 
{
    /**
     * 查询字母
     * 
     * @param id 字母主键
     * @return 字母
     */
    public Alphabets selectAlphabetsById(Long id);

    /**
     * 查询字母列表
     * 
     * @param alphabets 字母
     * @return 字母集合
     */
    public List<Alphabets> selectAlphabetsList(Alphabets alphabets);

    /**
     * 新增字母
     * 
     * @param alphabets 字母
     * @return 结果
     */
    public int insertAlphabets(Alphabets alphabets);

    /**
     * 修改字母
     * 
     * @param alphabets 字母
     * @return 结果
     */
    public int updateAlphabets(Alphabets alphabets);

    /**
     * 批量删除字母
     * 
     * @param ids 需要删除的字母主键集合
     * @return 结果
     */
    public int deleteAlphabetsByIds(Long[] ids);

    /**
     * 删除字母信息
     * 
     * @param id 字母主键
     * @return 结果
     */
    public int deleteAlphabetsById(Long id);
}
