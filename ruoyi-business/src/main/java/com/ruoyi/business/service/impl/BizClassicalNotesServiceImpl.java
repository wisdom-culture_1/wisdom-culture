package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ClassicalNotes;
import com.ruoyi.business.mapper.BizClassicalNotesMapper;
import com.ruoyi.business.service.IClassicalNotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文言文注释Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizClassicalNotesServiceImpl implements IClassicalNotesService
{
    @Autowired
    private BizClassicalNotesMapper bizClassicalNotesMapper;

    /**
     * 查询文言文注释
     * 
     * @param id 文言文注释主键
     * @return 文言文注释
     */
    @Override
    public ClassicalNotes selectClassicalNotesById(Long id)
    {
        return bizClassicalNotesMapper.selectClassicalNotesById(id);
    }

    /**
     * 查询文言文注释列表
     * 
     * @param classicalNotes 文言文注释
     * @return 文言文注释
     */
    @Override
    public List<ClassicalNotes> selectClassicalNotesList(ClassicalNotes classicalNotes)
    {
        return bizClassicalNotesMapper.selectClassicalNotesList(classicalNotes);
    }

    /**
     * 新增文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    @Override
    public int insertClassicalNotes(ClassicalNotes classicalNotes)
    {
        return bizClassicalNotesMapper.insertClassicalNotes(classicalNotes);
    }

    /**
     * 修改文言文注释
     * 
     * @param classicalNotes 文言文注释
     * @return 结果
     */
    @Override
    public int updateClassicalNotes(ClassicalNotes classicalNotes)
    {
        return bizClassicalNotesMapper.updateClassicalNotes(classicalNotes);
    }

    /**
     * 批量删除文言文注释
     * 
     * @param ids 需要删除的文言文注释主键
     * @return 结果
     */
    @Override
    public int deleteClassicalNotesByIds(Long[] ids)
    {
        return bizClassicalNotesMapper.deleteClassicalNotesByIds(ids);
    }

    /**
     * 删除文言文注释信息
     * 
     * @param id 文言文注释主键
     * @return 结果
     */
    @Override
    public int deleteClassicalNotesById(Long id)
    {
        return bizClassicalNotesMapper.deleteClassicalNotesById(id);
    }
}
