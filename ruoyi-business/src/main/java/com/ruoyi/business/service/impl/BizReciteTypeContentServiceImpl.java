package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ReciteTypeContent;
import com.ruoyi.business.mapper.BizReciteTypeContentMapper;
import com.ruoyi.business.service.IReciteTypeContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 背诵打卡类别内容Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizReciteTypeContentServiceImpl implements IReciteTypeContentService
{
    @Autowired
    private BizReciteTypeContentMapper bizReciteTypeContentMapper;

    /**
     * 查询背诵打卡类别内容
     * 
     * @param id 背诵打卡类别内容主键
     * @return 背诵打卡类别内容
     */
    @Override
    public ReciteTypeContent selectReciteTypeContentById(Long id)
    {
        return bizReciteTypeContentMapper.selectReciteTypeContentById(id);
    }

    /**
     * 查询背诵打卡类别内容列表
     * 
     * @param reciteTypeContent 背诵打卡类别内容
     * @return 背诵打卡类别内容
     */
    @Override
    public List<ReciteTypeContent> selectReciteTypeContentList(ReciteTypeContent reciteTypeContent)
    {
        return bizReciteTypeContentMapper.selectReciteTypeContentList(reciteTypeContent);
    }

    /**
     * 新增背诵打卡类别内容
     * 
     * @param reciteTypeContent 背诵打卡类别内容
     * @return 结果
     */
    @Override
    public int insertReciteTypeContent(ReciteTypeContent reciteTypeContent)
    {
        return bizReciteTypeContentMapper.insertReciteTypeContent(reciteTypeContent);
    }

    /**
     * 修改背诵打卡类别内容
     * 
     * @param reciteTypeContent 背诵打卡类别内容
     * @return 结果
     */
    @Override
    public int updateReciteTypeContent(ReciteTypeContent reciteTypeContent)
    {
        return bizReciteTypeContentMapper.updateReciteTypeContent(reciteTypeContent);
    }

    /**
     * 批量删除背诵打卡类别内容
     * 
     * @param ids 需要删除的背诵打卡类别内容主键
     * @return 结果
     */
    @Override
    public int deleteReciteTypeContentByIds(Long[] ids)
    {
        return bizReciteTypeContentMapper.deleteReciteTypeContentByIds(ids);
    }

    /**
     * 删除背诵打卡类别内容信息
     * 
     * @param id 背诵打卡类别内容主键
     * @return 结果
     */
    @Override
    public int deleteReciteTypeContentById(Long id)
    {
        return bizReciteTypeContentMapper.deleteReciteTypeContentById(id);
    }
}
