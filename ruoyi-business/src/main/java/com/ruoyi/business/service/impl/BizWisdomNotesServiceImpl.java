package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.WisdomNotes;
import com.ruoyi.business.mapper.BizWisdomNotesMapper;
import com.ruoyi.business.service.IWisdomNotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 智慧元典注释Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizWisdomNotesServiceImpl implements IWisdomNotesService
{
    @Autowired
    private BizWisdomNotesMapper bizWisdomNotesMapper;

    /**
     * 查询智慧元典注释
     * 
     * @param id 智慧元典注释主键
     * @return 智慧元典注释
     */
    @Override
    public WisdomNotes selectWisdomNotesById(Long id)
    {
        return bizWisdomNotesMapper.selectWisdomNotesById(id);
    }

    /**
     * 查询智慧元典注释列表
     * 
     * @param wisdomNotes 智慧元典注释
     * @return 智慧元典注释
     */
    @Override
    public List<WisdomNotes> selectWisdomNotesList(WisdomNotes wisdomNotes)
    {
        return bizWisdomNotesMapper.selectWisdomNotesList(wisdomNotes);
    }

    /**
     * 新增智慧元典注释
     * 
     * @param wisdomNotes 智慧元典注释
     * @return 结果
     */
    @Override
    public int insertWisdomNotes(WisdomNotes wisdomNotes)
    {
        return bizWisdomNotesMapper.insertWisdomNotes(wisdomNotes);
    }

    /**
     * 修改智慧元典注释
     * 
     * @param wisdomNotes 智慧元典注释
     * @return 结果
     */
    @Override
    public int updateWisdomNotes(WisdomNotes wisdomNotes)
    {
        return bizWisdomNotesMapper.updateWisdomNotes(wisdomNotes);
    }

    /**
     * 批量删除智慧元典注释
     * 
     * @param ids 需要删除的智慧元典注释主键
     * @return 结果
     */
    @Override
    public int deleteWisdomNotesByIds(Long[] ids)
    {
        return bizWisdomNotesMapper.deleteWisdomNotesByIds(ids);
    }

    /**
     * 删除智慧元典注释信息
     * 
     * @param id 智慧元典注释主键
     * @return 结果
     */
    @Override
    public int deleteWisdomNotesById(Long id)
    {
        return bizWisdomNotesMapper.deleteWisdomNotesById(id);
    }
}
