package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ExcerptComment;
import com.ruoyi.business.entity.Users;
import com.ruoyi.business.mapper.BizExcerptCommentMapper;
import com.ruoyi.business.mapper.BizUsersMapper;
import com.ruoyi.business.service.IExcerptCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 金句摘抄评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizExcerptCommentServiceImpl implements IExcerptCommentService
{
    @Autowired
    private BizExcerptCommentMapper bizExcerptCommentMapper;

    @Autowired
    private BizUsersMapper bizUsersMapper;

    /**
     * 查询金句摘抄评论
     * 
     * @param id 金句摘抄评论主键
     * @return 金句摘抄评论
     */
    @Override
    public ExcerptComment selectExcerptCommentById(Long id)
    {
        return bizExcerptCommentMapper.selectExcerptCommentById(id);
    }

    /**
     * 查询金句摘抄评论列表
     * 
     * @param excerptComment 金句摘抄评论
     * @return 金句摘抄评论
     */
    @Override
    public List<ExcerptComment> selectExcerptCommentList(ExcerptComment excerptComment)
    {
        List<ExcerptComment> excerptComments = bizExcerptCommentMapper.selectExcerptCommentList(excerptComment);
        excerptComments.forEach(item -> {
            Users users = bizUsersMapper.selectUsersById(String.valueOf(item.getUserId()));
            item.setUser(users);
        });
        return excerptComments;
    }

    /**
     * 新增金句摘抄评论
     * 
     * @param excerptComment 金句摘抄评论
     * @return 结果
     */
    @Override
    public int insertExcerptComment(ExcerptComment excerptComment)
    {
        return bizExcerptCommentMapper.insertExcerptComment(excerptComment);
    }

    /**
     * 修改金句摘抄评论
     * 
     * @param excerptComment 金句摘抄评论
     * @return 结果
     */
    @Override
    public int updateExcerptComment(ExcerptComment excerptComment)
    {
        return bizExcerptCommentMapper.updateExcerptComment(excerptComment);
    }

    @Override
    public int deleteExcerptCommentByIds(ExcerptComment excerptComment) {
        return bizExcerptCommentMapper.deleteExcerptCommentById(excerptComment.getId());
    }


    /**
     * 删除金句摘抄评论信息
     * 
     * @param id 金句摘抄评论主键
     * @return 结果
     */
    @Override
    public int deleteExcerptCommentById(Long id)
    {
        return bizExcerptCommentMapper.deleteExcerptCommentById(id);
    }
}
