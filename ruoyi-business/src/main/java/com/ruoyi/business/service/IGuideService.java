package com.ruoyi.business.service;

import com.ruoyi.business.entity.Guide;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IGuideService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Guide selectGuideById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param guide 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Guide> selectGuideList(Guide guide);

    /**
     * 新增【请填写功能名称】
     * 
     * @param guide 【请填写功能名称】
     * @return 结果
     */
    public int insertGuide(Guide guide);

    /**
     * 修改【请填写功能名称】
     * 
     * @param guide 【请填写功能名称】
     * @return 结果
     */
    public int updateGuide(Guide guide);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteGuideByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteGuideById(Long id);
}
