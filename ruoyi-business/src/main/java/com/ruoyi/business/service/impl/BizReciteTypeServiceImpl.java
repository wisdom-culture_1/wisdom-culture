package com.ruoyi.business.service.impl;

import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.Recite;
import com.ruoyi.business.entity.ReciteType;
import com.ruoyi.business.entity.ReciteTypeContent;
import com.ruoyi.business.mapper.BizReciteMapper;
import com.ruoyi.business.mapper.BizReciteTypeContentMapper;
import com.ruoyi.business.mapper.BizReciteTypeMapper;
import com.ruoyi.business.service.IReciteTypeService;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 背诵打卡类别Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizReciteTypeServiceImpl implements IReciteTypeService
{
    private static final Logger log = LoggerFactory.getLogger(BizReciteTypeServiceImpl.class);

    @Resource
    private BizReciteTypeMapper bizReciteTypeMapper;
    @Resource
    private BizReciteMapper bizReciteMapper;
    @Resource
    private BizReciteTypeContentMapper bizReciteTypeContentMapper;

    /**
     * 查询背诵打卡类别
     * 
     * @param id 背诵打卡类别主键
     * @return 背诵打卡类别
     */
    @Override
    public ReciteType selectReciteTypeById(Long id)
    {
        return bizReciteTypeMapper.selectReciteTypeById(id);
    }

    /**
     * 查询背诵打卡类别列表
     * 
     * @param reciteType 背诵打卡类别
     * @return 背诵打卡类别
     */
    @Override
    public List<ReciteType> selectReciteTypeList(ReciteType reciteType)
    {
        List<ReciteType> reciteTypes = bizReciteTypeMapper.selectReciteTypeList(reciteType);

        reciteTypes.forEach(item ->{
            ReciteType reciteType1 = new ReciteType();
            reciteType1.setPid(item.getId());
            List<ReciteType> reciteTypes1 = bizReciteTypeMapper.selectReciteTypeList(reciteType1);
            reciteTypes1.forEach(itemChild -> {
                if(null != itemChild.getReciteTypeContentId()){
                    Recite recite = new Recite();
                    recite.setTypeId(itemChild.getReciteTypeContentId());
                    log.info("BizReciteTypeServiceImpl selectReciteTypeList userid is :{},typeId is:{}",SecurityUtils.getUserId(),itemChild.getReciteTypeContentId());
                    recite.setUserId(SecurityUtils.getUserId());
                    List<Recite> recites = bizReciteMapper.selectReciteList(recite);
                    log.info("BizReciteTypeServiceImpl selectReciteTypeList recites is :{}",recites.size());
                    if(CollectionUtils.isNotEmpty(recites)){
                        itemChild.setReciteFlag("1");
                    }
                }
            });
            item.setChild(reciteTypes1);
        });

        return reciteTypes;
    }

    /**
     * 新增背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    @Override
    public int insertReciteType(ReciteType reciteType)
    {
        return bizReciteTypeMapper.insertReciteType(reciteType);
    }

    /**
     * 修改背诵打卡类别
     * 
     * @param reciteType 背诵打卡类别
     * @return 结果
     */
    @Override
    public int updateReciteType(ReciteType reciteType)
    {
        return bizReciteTypeMapper.updateReciteType(reciteType);
    }

    /**
     * 批量删除背诵打卡类别
     * 
     * @param ids 需要删除的背诵打卡类别主键
     * @return 结果
     */
    @Override
    public int deleteReciteTypeByIds(Long[] ids)
    {
        return bizReciteTypeMapper.deleteReciteTypeByIds(ids);
    }

    /**
     * 删除背诵打卡类别信息
     * 
     * @param id 背诵打卡类别主键
     * @return 结果
     */
    @Override
    public int deleteReciteTypeById(Long id)
    {
        return bizReciteTypeMapper.deleteReciteTypeById(id);
    }
}
