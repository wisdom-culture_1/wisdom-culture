package com.ruoyi.business.service;

import com.ruoyi.business.entity.CultureText;

import java.util.List;

/**
 * 中华文化内容Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface ICultureTextService 
{
    /**
     * 查询中华文化内容
     * 
     * @param id 中华文化内容主键
     * @return 中华文化内容
     */
    public CultureText selectCultureTextById(Long id);

    /**
     * 查询中华文化内容列表
     * 
     * @param cultureText 中华文化内容
     * @return 中华文化内容集合
     */
    public List<CultureText> selectCultureTextList(CultureText cultureText);

    /**
     * 新增中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    public int insertCultureText(CultureText cultureText);

    /**
     * 修改中华文化内容
     * 
     * @param cultureText 中华文化内容
     * @return 结果
     */
    public int updateCultureText(CultureText cultureText);

    /**
     * 批量删除中华文化内容
     * 
     * @param ids 需要删除的中华文化内容主键集合
     * @return 结果
     */
    public int deleteCultureTextByIds(Long[] ids);

    /**
     * 删除中华文化内容信息
     * 
     * @param id 中华文化内容主键
     * @return 结果
     */
    public int deleteCultureTextById(Long id);
}
