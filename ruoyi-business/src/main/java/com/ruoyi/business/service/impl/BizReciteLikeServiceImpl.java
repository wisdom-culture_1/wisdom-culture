package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ReciteLike;
import com.ruoyi.business.mapper.BizReciteLikeMapper;
import com.ruoyi.business.service.IReciteLikeService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 背诵打卡点赞Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizReciteLikeServiceImpl implements IReciteLikeService
{
    @Resource
    private BizReciteLikeMapper bizReciteLikeMapper;

    /**
     * 查询背诵打卡点赞
     * 
     * @param id 背诵打卡点赞主键
     * @return 背诵打卡点赞
     */
    @Override
    public ReciteLike selectReciteLikeById(Long id)
    {
        return bizReciteLikeMapper.selectReciteLikeById(id);
    }

    /**
     * 查询背诵打卡点赞列表
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 背诵打卡点赞
     */
    @Override
    public List<ReciteLike> selectReciteLikeList(ReciteLike reciteLike)
    {
        return bizReciteLikeMapper.selectReciteLikeList(reciteLike);
    }

    /**
     * 新增背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    @Override
    public int insertReciteLike(ReciteLike reciteLike)
    {
        // todo 要加上user_id
        reciteLike.setUserId(SecurityUtils.getUserId());
        ReciteLike reciteLike1 = bizReciteLikeMapper.selectReciteLikeByUserIdAndReciteId(reciteLike);
        if(null != reciteLike1){
            return bizReciteLikeMapper.updateReciteLikeByReciteIdAndUserId(reciteLike);
        }else{
            return bizReciteLikeMapper.insertReciteLike(reciteLike);
        }
    }

    /**
     * 修改背诵打卡点赞
     * 
     * @param reciteLike 背诵打卡点赞
     * @return 结果
     */
    @Override
    public int updateReciteLike(ReciteLike reciteLike)
    {
        return bizReciteLikeMapper.updateReciteLike(reciteLike);
    }

    /**
     * 批量删除背诵打卡点赞
     * 
     * @param ids 需要删除的背诵打卡点赞主键
     * @return 结果
     */
    @Override
    public int deleteReciteLikeByIds(Long[] ids)
    {
        return bizReciteLikeMapper.deleteReciteLikeByIds(ids);
    }

    /**
     * 删除背诵打卡点赞信息
     * 
     * @param id 背诵打卡点赞主键
     * @return 结果
     */
    @Override
    public int deleteReciteLikeById(Long id)
    {
        return bizReciteLikeMapper.deleteReciteLikeById(id);
    }
}
