package com.ruoyi.business.service;

import com.ruoyi.business.entity.PoetryOde;

import java.util.List;

/**
 * 诗词歌赋Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IPoetryOdeService 
{
    /**
     * 查询诗词歌赋
     * 
     * @param id 诗词歌赋主键
     * @return 诗词歌赋
     */
    public PoetryOde selectPoetryOdeById(Long id);

    /**
     * 查询诗词歌赋列表
     * 
     * @param poetryOde 诗词歌赋
     * @return 诗词歌赋集合
     */
    public List<PoetryOde> selectPoetryOdeList(PoetryOde poetryOde);

    /**
     * 新增诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    public int insertPoetryOde(PoetryOde poetryOde);

    /**
     * 修改诗词歌赋
     * 
     * @param poetryOde 诗词歌赋
     * @return 结果
     */
    public int updatePoetryOde(PoetryOde poetryOde);

    /**
     * 批量删除诗词歌赋
     * 
     * @param ids 需要删除的诗词歌赋主键集合
     * @return 结果
     */
    public int deletePoetryOdeByIds(Long[] ids);

    /**
     * 删除诗词歌赋信息
     * 
     * @param id 诗词歌赋主键
     * @return 结果
     */
    public int deletePoetryOdeById(Long id);
}
