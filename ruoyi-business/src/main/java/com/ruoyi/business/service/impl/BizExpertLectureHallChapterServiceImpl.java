package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ExpertLectureHallChapter;
import com.ruoyi.business.mapper.BizExpertLectureHallChapterMapper;
import com.ruoyi.business.service.IExpertLectureHallChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 专家讲堂章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizExpertLectureHallChapterServiceImpl implements IExpertLectureHallChapterService
{
    @Autowired
    private BizExpertLectureHallChapterMapper bizExpertLectureHallChapterMapper;

    /**
     * 查询专家讲堂章节
     * 
     * @param id 专家讲堂章节主键
     * @return 专家讲堂章节
     */
    @Override
    public ExpertLectureHallChapter selectExpertLectureHallChapterById(Long id)
    {
        return bizExpertLectureHallChapterMapper.selectExpertLectureHallChapterById(id);
    }

    /**
     * 查询专家讲堂章节列表
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 专家讲堂章节
     */
    @Override
    public List<ExpertLectureHallChapter> selectExpertLectureHallChapterList(ExpertLectureHallChapter expertLectureHallChapter)
    {
        return bizExpertLectureHallChapterMapper.selectExpertLectureHallChapterList(expertLectureHallChapter);
    }

    /**
     * 新增专家讲堂章节
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 结果
     */
    @Override
    public int insertExpertLectureHallChapter(ExpertLectureHallChapter expertLectureHallChapter)
    {
        return bizExpertLectureHallChapterMapper.insertExpertLectureHallChapter(expertLectureHallChapter);
    }

    /**
     * 修改专家讲堂章节
     * 
     * @param expertLectureHallChapter 专家讲堂章节
     * @return 结果
     */
    @Override
    public int updateExpertLectureHallChapter(ExpertLectureHallChapter expertLectureHallChapter)
    {
        return bizExpertLectureHallChapterMapper.updateExpertLectureHallChapter(expertLectureHallChapter);
    }

    /**
     * 批量删除专家讲堂章节
     * 
     * @param ids 需要删除的专家讲堂章节主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallChapterByIds(Long[] ids)
    {
        return bizExpertLectureHallChapterMapper.deleteExpertLectureHallChapterByIds(ids);
    }

    /**
     * 删除专家讲堂章节信息
     * 
     * @param id 专家讲堂章节主键
     * @return 结果
     */
    @Override
    public int deleteExpertLectureHallChapterById(Long id)
    {
        return bizExpertLectureHallChapterMapper.deleteExpertLectureHallChapterById(id);
    }
}
