package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ClassicalExtend;
import com.ruoyi.business.mapper.BizClassicalExtendMapper;
import com.ruoyi.business.service.IClassicalExtendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文言名篇扩展Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizClassicalExtendServiceImpl implements IClassicalExtendService
{
    @Autowired
    private BizClassicalExtendMapper bizClassicalExtendMapper;

    /**
     * 查询文言名篇扩展
     * 
     * @param id 文言名篇扩展主键
     * @return 文言名篇扩展
     */
    @Override
    public ClassicalExtend selectClassicalExtendById(Long id)
    {
        return bizClassicalExtendMapper.selectClassicalExtendById(id);
    }

    /**
     * 查询文言名篇扩展列表
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 文言名篇扩展
     */
    @Override
    public List<ClassicalExtend> selectClassicalExtendList(ClassicalExtend classicalExtend)
    {
        return bizClassicalExtendMapper.selectClassicalExtendList(classicalExtend);
    }

    /**
     * 新增文言名篇扩展
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 结果
     */
    @Override
    public int insertClassicalExtend(ClassicalExtend classicalExtend)
    {
        return bizClassicalExtendMapper.insertClassicalExtend(classicalExtend);
    }

    /**
     * 修改文言名篇扩展
     * 
     * @param classicalExtend 文言名篇扩展
     * @return 结果
     */
    @Override
    public int updateClassicalExtend(ClassicalExtend classicalExtend)
    {
        return bizClassicalExtendMapper.updateClassicalExtend(classicalExtend);
    }

    /**
     * 批量删除文言名篇扩展
     * 
     * @param ids 需要删除的文言名篇扩展主键
     * @return 结果
     */
    @Override
    public int deleteClassicalExtendByIds(Long[] ids)
    {
        return bizClassicalExtendMapper.deleteClassicalExtendByIds(ids);
    }

    /**
     * 删除文言名篇扩展信息
     * 
     * @param id 文言名篇扩展主键
     * @return 结果
     */
    @Override
    public int deleteClassicalExtendById(Long id)
    {
        return bizClassicalExtendMapper.deleteClassicalExtendById(id);
    }
}
