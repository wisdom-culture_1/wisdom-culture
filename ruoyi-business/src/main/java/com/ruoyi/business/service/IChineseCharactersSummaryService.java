package com.ruoyi.business.service;

import com.ruoyi.business.entity.ChineseCharactersSummary;

import java.util.List;

/**
 * 文字汇总Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IChineseCharactersSummaryService 
{
    /**
     * 查询文字汇总
     * 
     * @param id 文字汇总主键
     * @return 文字汇总
     */
    public ChineseCharactersSummary selectChineseCharactersSummaryById(Long id);

    /**
     * 查询文字汇总列表
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 文字汇总集合
     */
    public List<ChineseCharactersSummary> selectChineseCharactersSummaryList(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 新增文字汇总
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 结果
     */
    public int insertChineseCharactersSummary(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 修改文字汇总
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 结果
     */
    public int updateChineseCharactersSummary(ChineseCharactersSummary chineseCharactersSummary);

    /**
     * 批量删除文字汇总
     * 
     * @param ids 需要删除的文字汇总主键集合
     * @return 结果
     */
    public int deleteChineseCharactersSummaryByIds(Long[] ids);

    /**
     * 删除文字汇总信息
     * 
     * @param id 文字汇总主键
     * @return 结果
     */
    public int deleteChineseCharactersSummaryById(Long id);
}
