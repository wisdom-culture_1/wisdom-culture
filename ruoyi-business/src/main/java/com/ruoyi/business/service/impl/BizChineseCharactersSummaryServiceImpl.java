package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.ChineseCharactersSummary;
import com.ruoyi.business.mapper.BizChineseCharactersSummaryMapper;
import com.ruoyi.business.service.IChineseCharactersSummaryService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文字汇总Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizChineseCharactersSummaryServiceImpl implements IChineseCharactersSummaryService
{
    @Autowired
    private BizChineseCharactersSummaryMapper bizChineseCharactersSummaryMapper;

    /**
     * 查询文字汇总
     * 
     * @param id 文字汇总主键
     * @return 文字汇总
     */
    @Override
    public ChineseCharactersSummary selectChineseCharactersSummaryById(Long id)
    {
        return bizChineseCharactersSummaryMapper.selectChineseCharactersSummaryById(id);
    }

    /**
     * 查询文字汇总列表
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 文字汇总
     */
    @Override
    public List<ChineseCharactersSummary> selectChineseCharactersSummaryList(ChineseCharactersSummary chineseCharactersSummary)
    {
        return bizChineseCharactersSummaryMapper.selectChineseCharactersSummaryList(chineseCharactersSummary);
    }

    /**
     * 新增文字汇总
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 结果
     */
    @Override
    public int insertChineseCharactersSummary(ChineseCharactersSummary chineseCharactersSummary)
    {

        // todo userid
        chineseCharactersSummary.setUserId(SecurityUtils.getUserId());
        ChineseCharactersSummary chineseCharactersSummary1 = bizChineseCharactersSummaryMapper.selectByRefIdAndCharacId(chineseCharactersSummary);
        if(null != chineseCharactersSummary1){
           return bizChineseCharactersSummaryMapper.updateByRefIdAndCharacId(chineseCharactersSummary);
        }
        return bizChineseCharactersSummaryMapper.insertChineseCharactersSummary(chineseCharactersSummary);
    }

    /**
     * 修改文字汇总
     * 
     * @param chineseCharactersSummary 文字汇总
     * @return 结果
     */
    @Override
    public int updateChineseCharactersSummary(ChineseCharactersSummary chineseCharactersSummary)
    {
        return bizChineseCharactersSummaryMapper.updateChineseCharactersSummary(chineseCharactersSummary);
    }

    /**
     * 批量删除文字汇总
     * 
     * @param ids 需要删除的文字汇总主键
     * @return 结果
     */
    @Override
    public int deleteChineseCharactersSummaryByIds(Long[] ids)
    {
        return bizChineseCharactersSummaryMapper.deleteChineseCharactersSummaryByIds(ids);
    }

    /**
     * 删除文字汇总信息
     * 
     * @param id 文字汇总主键
     * @return 结果
     */
    @Override
    public int deleteChineseCharactersSummaryById(Long id)
    {
        return bizChineseCharactersSummaryMapper.deleteChineseCharactersSummaryById(id);
    }
}
