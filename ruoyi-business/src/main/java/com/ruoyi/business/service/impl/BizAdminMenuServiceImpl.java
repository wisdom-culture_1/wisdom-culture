package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizAdminMenuMapper;
import com.ruoyi.business.entity.AdminMenu;
import com.ruoyi.business.service.IAdminMenuService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizAdminMenuServiceImpl implements IAdminMenuService
{
    @Autowired
    private BizAdminMenuMapper adminMenuMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public AdminMenu selectAdminMenuById(String id)
    {
        return adminMenuMapper.selectAdminMenuById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param adminMenu 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<AdminMenu> selectAdminMenuList(AdminMenu adminMenu)
    {
        return adminMenuMapper.selectAdminMenuList(adminMenu);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param adminMenu 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertAdminMenu(AdminMenu adminMenu)
    {
        return adminMenuMapper.insertAdminMenu(adminMenu);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param adminMenu 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateAdminMenu(AdminMenu adminMenu)
    {
        return adminMenuMapper.updateAdminMenu(adminMenu);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAdminMenuByIds(String[] ids)
    {
        return adminMenuMapper.deleteAdminMenuByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAdminMenuById(String id)
    {
        return adminMenuMapper.deleteAdminMenuById(id);
    }
}
