package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.GradeStudyPlanDetailMapper;
import com.ruoyi.business.entity.GradeStudyPlanDetail;
import com.ruoyi.business.service.IGradeStudyPlanDetailService;

/**
 * 学习计划（细化到文章）Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-01
 */
@Service
public class GradeStudyPlanDetailServiceImpl implements IGradeStudyPlanDetailService 
{
    @Autowired
    private GradeStudyPlanDetailMapper gradeStudyPlanDetailMapper;

    /**
     * 查询学习计划（细化到文章）
     * 
     * @param id 学习计划（细化到文章）主键
     * @return 学习计划（细化到文章）
     */
    @Override
    public GradeStudyPlanDetail selectGradeStudyPlanDetailById(Long id)
    {
        return gradeStudyPlanDetailMapper.selectGradeStudyPlanDetailById(id);
    }

    /**
     * 查询学习计划（细化到文章）列表
     * 
     * @param gradeStudyPlanDetail 学习计划（细化到文章）
     * @return 学习计划（细化到文章）
     */
    @Override
    public List<GradeStudyPlanDetail> selectGradeStudyPlanDetailList(GradeStudyPlanDetail gradeStudyPlanDetail)
    {
        return gradeStudyPlanDetailMapper.selectGradeStudyPlanDetailList(gradeStudyPlanDetail);
    }

    /**
     * 新增学习计划（细化到文章）
     * 
     * @param gradeStudyPlanDetail 学习计划（细化到文章）
     * @return 结果
     */
    @Override
    public int insertGradeStudyPlanDetail(GradeStudyPlanDetail gradeStudyPlanDetail)
    {
        return gradeStudyPlanDetailMapper.insertGradeStudyPlanDetail(gradeStudyPlanDetail);
    }

    /**
     * 修改学习计划（细化到文章）
     * 
     * @param gradeStudyPlanDetail 学习计划（细化到文章）
     * @return 结果
     */
    @Override
    public int updateGradeStudyPlanDetail(GradeStudyPlanDetail gradeStudyPlanDetail)
    {
        return gradeStudyPlanDetailMapper.updateGradeStudyPlanDetail(gradeStudyPlanDetail);
    }

    /**
     * 批量删除学习计划（细化到文章）
     * 
     * @param ids 需要删除的学习计划（细化到文章）主键
     * @return 结果
     */
    @Override
    public int deleteGradeStudyPlanDetailByIds(Long[] ids)
    {
        return gradeStudyPlanDetailMapper.deleteGradeStudyPlanDetailByIds(ids);
    }

    /**
     * 删除学习计划（细化到文章）信息
     * 
     * @param id 学习计划（细化到文章）主键
     * @return 结果
     */
    @Override
    public int deleteGradeStudyPlanDetailById(Long id)
    {
        return gradeStudyPlanDetailMapper.deleteGradeStudyPlanDetailById(id);
    }

    @Override
    public int deleteGradeStudyPlanDetailByGrade(GradeStudyPlanDetail gradeStudyPlanDetail) {
        return gradeStudyPlanDetailMapper.deleteGradeStudyPlanDetailByGrade(gradeStudyPlanDetail);
    }
}
