package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizSystemConfigMapper;
import com.ruoyi.business.entity.SystemConfig;
import com.ruoyi.business.service.ISystemConfigService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizSystemConfigServiceImpl implements ISystemConfigService
{
    @Autowired
    private BizSystemConfigMapper bizSystemConfigMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public SystemConfig selectSystemConfigById(Long id)
    {
        return bizSystemConfigMapper.selectSystemConfigById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param systemConfig 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<SystemConfig> selectSystemConfigList(SystemConfig systemConfig)
    {
        return bizSystemConfigMapper.selectSystemConfigList(systemConfig);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param systemConfig 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertSystemConfig(SystemConfig systemConfig)
    {
        return bizSystemConfigMapper.insertSystemConfig(systemConfig);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param systemConfig 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateSystemConfig(SystemConfig systemConfig)
    {
        return bizSystemConfigMapper.updateSystemConfig(systemConfig);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteSystemConfigByIds(Long[] ids)
    {
        return bizSystemConfigMapper.deleteSystemConfigByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteSystemConfigById(Long id)
    {
        return bizSystemConfigMapper.deleteSystemConfigById(id);
    }
}
