package com.ruoyi.business.service;

import com.ruoyi.business.entity.BeyondcampusUnion;
import com.ruoyi.business.entity.BeyondcampusUser;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public interface IBeyondcampusUserService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BeyondcampusUser selectBeyondcampusUserById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BeyondcampusUser> selectBeyondcampusUserList(BeyondcampusUser beyondcampusUser);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param beyondcampusUser 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public BeyondcampusUnion selectBeyondcampusAndMyList(BeyondcampusUser beyondcampusUser);

    /**
     * 新增【请填写功能名称】
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 结果
     */
    public int insertBeyondcampusUser(BeyondcampusUser beyondcampusUser);

    /**
     * 修改【请填写功能名称】
     * 
     * @param beyondcampusUser 【请填写功能名称】
     * @return 结果
     */
    public int updateBeyondcampusUser(BeyondcampusUser beyondcampusUser);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteBeyondcampusUserByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBeyondcampusUserById(Long id);
}
