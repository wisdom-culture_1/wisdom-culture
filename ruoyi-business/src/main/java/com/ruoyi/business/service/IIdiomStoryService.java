package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.IdiomStory;

/**
 * 成语典故Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IIdiomStoryService 
{
    /**
     * 查询成语典故
     * 
     * @param id 成语典故主键
     * @return 成语典故
     */
    public IdiomStory selectIdiomStoryById(Long id);

    /**
     * 查询成语典故列表
     * 
     * @param idiomStory 成语典故
     * @return 成语典故集合
     */
    public List<IdiomStory> selectIdiomStoryList(IdiomStory idiomStory);

    /**
     * 新增成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    public int insertIdiomStory(IdiomStory idiomStory);

    /**
     * 修改成语典故
     * 
     * @param idiomStory 成语典故
     * @return 结果
     */
    public int updateIdiomStory(IdiomStory idiomStory);

    /**
     * 批量删除成语典故
     * 
     * @param ids 需要删除的成语典故主键集合
     * @return 结果
     */
    public int deleteIdiomStoryByIds(Long[] ids);

    /**
     * 删除成语典故信息
     * 
     * @param id 成语典故主键
     * @return 结果
     */
    public int deleteIdiomStoryById(Long id);
}
