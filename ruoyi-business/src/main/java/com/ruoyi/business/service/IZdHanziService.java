package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.ZdHanzi;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IZdHanziService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public ZdHanzi selectZdHanziByRefid(Long refid);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ZdHanzi> selectZdHanziList(ZdHanzi zdHanzi);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 结果
     */
    public int insertZdHanzi(ZdHanzi zdHanzi);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zdHanzi 【请填写功能名称】
     * @return 结果
     */
    public int updateZdHanzi(ZdHanzi zdHanzi);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param refids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteZdHanziByRefids(Long[] refids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param refid 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteZdHanziByRefid(Long refid);

    List<ZdHanzi> selectStrokeCharacter(String begin, String bihua);
}
