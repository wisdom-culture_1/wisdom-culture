package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.entity.AdminMenu;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public interface IAdminMenuService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public AdminMenu selectAdminMenuById(String id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param adminMenu 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<AdminMenu> selectAdminMenuList(AdminMenu adminMenu);

    /**
     * 新增【请填写功能名称】
     * 
     * @param adminMenu 【请填写功能名称】
     * @return 结果
     */
    public int insertAdminMenu(AdminMenu adminMenu);

    /**
     * 修改【请填写功能名称】
     * 
     * @param adminMenu 【请填写功能名称】
     * @return 结果
     */
    public int updateAdminMenu(AdminMenu adminMenu);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteAdminMenuByIds(String[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteAdminMenuById(String id);
}
