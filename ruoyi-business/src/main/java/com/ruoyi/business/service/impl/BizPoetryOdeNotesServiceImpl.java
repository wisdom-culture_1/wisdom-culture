package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.PoetryOdeNotes;
import com.ruoyi.business.mapper.BizPoetryOdeNotesMapper;
import com.ruoyi.business.service.IPoetryOdeNotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizPoetryOdeNotesServiceImpl implements IPoetryOdeNotesService
{
    @Autowired
    private BizPoetryOdeNotesMapper bizPoetryOdeNotesMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public PoetryOdeNotes selectPoetryOdeNotesById(Long id)
    {
        return bizPoetryOdeNotesMapper.selectPoetryOdeNotesById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param poetryOdeNotes 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<PoetryOdeNotes> selectPoetryOdeNotesList(PoetryOdeNotes poetryOdeNotes)
    {
        return bizPoetryOdeNotesMapper.selectPoetryOdeNotesList(poetryOdeNotes);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param poetryOdeNotes 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertPoetryOdeNotes(PoetryOdeNotes poetryOdeNotes)
    {
        return bizPoetryOdeNotesMapper.insertPoetryOdeNotes(poetryOdeNotes);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param poetryOdeNotes 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updatePoetryOdeNotes(PoetryOdeNotes poetryOdeNotes)
    {
        return bizPoetryOdeNotesMapper.updatePoetryOdeNotes(poetryOdeNotes);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeNotesByIds(Long[] ids)
    {
        return bizPoetryOdeNotesMapper.deletePoetryOdeNotesByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deletePoetryOdeNotesById(Long id)
    {
        return bizPoetryOdeNotesMapper.deletePoetryOdeNotesById(id);
    }
}
