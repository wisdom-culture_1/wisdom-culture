package com.ruoyi.business.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.business.entity.*;
import com.ruoyi.business.mapper.BizChineseCharactersSummaryMapper;
import com.ruoyi.business.mapper.BizZdPinyinMapper;
import com.ruoyi.business.mapper.GradeStudyPlanDetailMapper;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BizChineseCharactersMapper;
import com.ruoyi.business.service.IChineseCharactersService;
import org.springframework.util.CollectionUtils;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@Service
public class BizChineseCharactersServiceImpl implements IChineseCharactersService
{
    @Autowired
    private BizChineseCharactersMapper bizChineseCharactersMapper;

    @Autowired
    private BizChineseCharactersSummaryMapper bizChineseCharactersSummaryMapper;

    @Autowired
    private GradeStudyPlanDetailMapper gradeStudyPlanDetailMapper;

    @Autowired
    private BizZdPinyinMapper bizZdPinyinMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param refid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public ChineseCharacters selectChineseCharactersByRefid(Long refid)
    {
        ChineseCharacters chineseCharacters = bizChineseCharactersMapper.selectChineseCharactersByRefid(refid);
        if(null != chineseCharacters){
            // 查询拼音
            ZdPinyin zdPinyin = new ZdPinyin();
            zdPinyin.setRefid(chineseCharacters.getRefid().toString());
            List<ZdPinyin> zdPinyins = bizZdPinyinMapper.selectZdPinyinList(zdPinyin);
            if (!CollectionUtils.isEmpty(zdPinyins)){
                String[] strings = new String[zdPinyins.size()];
                for (int i = 0; i < zdPinyins.size(); i++) {
                    strings[i] = "http://sns-oss.oss-cn-beijing.aliyuncs.com/"+zdPinyins.get(i).getDuyin();
                    chineseCharacters.setPronunciationAudioSrcList(strings);
                }
//                chineseCharacters.setPronunciationAudioSrc(zdPinyins.get(0).getDuyin());
            }

            chineseCharacters.setEtymologyImageSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+chineseCharacters.getEtymologyImageSrc());
            chineseCharacters.setEvolutionImageSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+chineseCharacters.getEvolutionImageSrc());
            chineseCharacters.setCharacterVideoSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+chineseCharacters.getCharacterVideoSrc());
//            chineseCharacters.setPronunciationAudioSrc("http://sns-oss.oss-cn-beijing.aliyuncs.com/"+chineseCharacters.getPronunciationAudioSrc());
            if(null != chineseCharacters.getPronunciation()){
                chineseCharacters.setPronunciationList(chineseCharacters.getPronunciation().split("/"));
            }
        }
        return chineseCharacters;
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<ChineseCharacters> selectChineseCharactersList(ChineseCharacters chineseCharacters)
    {
        return bizChineseCharactersMapper.selectChineseCharactersList(chineseCharacters);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertChineseCharacters(ChineseCharacters chineseCharacters)
    {
        return bizChineseCharactersMapper.insertChineseCharacters(chineseCharacters);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param chineseCharacters 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateChineseCharacters(ChineseCharacters chineseCharacters)
    {
        return bizChineseCharactersMapper.updateChineseCharacters(chineseCharacters);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param refids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteChineseCharactersByRefids(Long[] refids)
    {
        return bizChineseCharactersMapper.deleteChineseCharactersByRefids(refids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param refid 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteChineseCharactersByRefid(Long refid)
    {
        return bizChineseCharactersMapper.deleteChineseCharactersByRefid(refid);
    }

    @Override
    public NewWordsSummaryRsp selectNewWordListSummary(String between, String level) {

        // 查询用户汉字信息
        ChineseCharactersSummary chineseCharactersSummary = new ChineseCharactersSummary();
        chineseCharactersSummary.setUserId(SecurityUtils.getUserId());
        List<ChineseCharactersSummary> chineseCharactersSummaries = bizChineseCharactersSummaryMapper.selectChineseCharactersSummaryList(chineseCharactersSummary);

        NewWordsSummaryRsp newWordsSummaryRsp = new NewWordsSummaryRsp();
//        查询所有的汉字id
        List<Integer> chineseCharacters = bizChineseCharactersMapper.selectAllFromCharacter(null);
        int size = chineseCharacters.size();
        Map<String ,CharacterSummaryRsp> characterSummaryRspMap = new LinkedHashMap<String, CharacterSummaryRsp>();
        CharacterSummaryRsp characterSummaryRsp = null;
        newWordsSummaryRsp.setAlready_count(Long.valueOf(0));
        Long alreadyCount = Long.valueOf(0);
        if(size > 0){
            newWordsSummaryRsp.setCharacters_count(Long.valueOf(size));
            if(null != level && level.equals("2")){
                // level =2 比例是50
                String[] split = between.split("-");
                Integer max = Integer.valueOf(split[1]);
                if(size <= Integer.valueOf(split[1])){
                    max = size;
                }
                for(int i = Integer.valueOf(split[0]); i<= max; i = i + 50){
                    int nextIntevalCount = i + 49;
                    characterSummaryRsp = new CharacterSummaryRsp();

                    characterSummaryRsp.setAlready(Long.valueOf(0));
                    characterSummaryRsp.setIncapable(Long.valueOf(0));
                    List<Integer> integers = new ArrayList<>();
                    if(i+49 >= size){
                        integers = chineseCharacters.subList(i - 1, size);
                    }else{
                        integers = chineseCharacters.subList(i - 1, i + 49);
                    }

                    for (ChineseCharactersSummary item : chineseCharactersSummaries) {
                        for (Integer character : integers) {
                            if (character.intValue() == item.getChineseCharactersId().intValue()) {
                                if(item.getStatus() == 1){
                                    ++alreadyCount;
                                    characterSummaryRsp.setAlready(characterSummaryRsp.getAlready() +1);
                                }else{
                                    characterSummaryRsp.setIncapable(characterSummaryRsp.getIncapable()+1);
                                }
                            }
                        }
                    }


                    characterSummaryRspMap.put(i+"-"+nextIntevalCount,characterSummaryRsp);
                }
            }else{// level = 1 区间是500
                for(int i = 1; i<= size; i = i + 500){
                    characterSummaryRsp = new CharacterSummaryRsp();
                    int nextIntevalCount = i + 499;
                    characterSummaryRsp.setAlready(Long.valueOf(0));
                    characterSummaryRsp.setIncapable(Long.valueOf(0));
                    List<Integer> integersLevel1 = new ArrayList<>();
                    if(i+499 >= size){
                        integersLevel1 = chineseCharacters.subList(i - 1, size);
                    }else{
                        integersLevel1 = chineseCharacters.subList(i - 1, i + 499);
                    }
                    for (ChineseCharactersSummary item : chineseCharactersSummaries) {
                        for (Integer characterLevel1 : integersLevel1) {
                            if (characterLevel1.intValue() == item.getChineseCharactersId().intValue()) {
                                if(item.getStatus() == 1){
                                    ++alreadyCount;
                                    characterSummaryRsp.setAlready(characterSummaryRsp.getAlready() +1);
                                }else{
                                    characterSummaryRsp.setIncapable(characterSummaryRsp.getIncapable()+1);
                                }
                            }
                        }
                    }
                    characterSummaryRspMap.put(i+"-"+nextIntevalCount,characterSummaryRsp);
                }
            }
        }

        newWordsSummaryRsp.setList(characterSummaryRspMap);
        newWordsSummaryRsp.setAlready_count(alreadyCount);
        return newWordsSummaryRsp;
    }


    @Override
    public NewWordsSummaryRsp selectNewWordListSummaryAll(Long gradeId,
                                                          Long module,
                                                          Long childModule) {
        NewWordsSummaryRsp newWordsSummaryRsp = new NewWordsSummaryRsp();
//        查询所有的汉字id
        List<Integer> chineseCharacters = bizChineseCharactersMapper.selectAllFromCharacter(null);
        int size = chineseCharacters.size();
        Map<String ,CharacterSummaryRsp> characterSummaryRspMap = new LinkedHashMap<String, CharacterSummaryRsp>();
        CharacterSummaryRsp characterSummaryRsp = null;// 初始化返回值
        newWordsSummaryRsp.setAlready_count(Long.valueOf(0));// 初始化总数
        if(size > 0){
            newWordsSummaryRsp.setCharacters_count(Long.valueOf(size));// 总数
            // level = 1 区间是500
            for(int i = 1; i<= size; i = i + 500){
                characterSummaryRsp = new CharacterSummaryRsp();
                int nextIntevalCount = i + 499;// 当前范围尾数
                characterSummaryRsp.setAlready(null); // 初始化 会
                characterSummaryRsp.setIncapable(null);// 初始化 不会
                characterSummaryRsp.setIsShow(0); // 初始化是否显示

                // 获取是否显示
                GradeStudyPlanDetail gradeStudyPlanDetail = new GradeStudyPlanDetail();
                gradeStudyPlanDetail.setGradeId(gradeId);
                gradeStudyPlanDetail.setModule(module);
                gradeStudyPlanDetail.setChildModule(childModule);
                gradeStudyPlanDetail.setDetailId(i+"-"+nextIntevalCount);// 以首位数字来判断
                List<GradeStudyPlanDetail> gradeStudyPlanDetails = gradeStudyPlanDetailMapper.selectGradeStudyPlanDetailList(gradeStudyPlanDetail);
                if (!gradeStudyPlanDetails.isEmpty()){
                    characterSummaryRsp.setIsShow(1);
                }
                characterSummaryRspMap.put(i+"-"+nextIntevalCount,characterSummaryRsp);
            }
        }
        newWordsSummaryRsp.setList(characterSummaryRspMap);
        newWordsSummaryRsp.setAlready_count(null);// 完成总数
        return newWordsSummaryRsp;
    }


    @Override
    public NewWordsSummaryRsp selectNewWordListSummaryIsDynamic(Long gradeId,
                                                                Long module,
                                                                Long childModule) {
        // 查询用户汉字信息
        ChineseCharactersSummary chineseCharactersSummary = new ChineseCharactersSummary();
        chineseCharactersSummary.setUserId(SecurityUtils.getUserId());
        List<ChineseCharactersSummary> chineseCharactersSummaries = bizChineseCharactersSummaryMapper.selectChineseCharactersSummaryList(chineseCharactersSummary);

        NewWordsSummaryRsp newWordsSummaryRsp = new NewWordsSummaryRsp();
//        查询所有的汉字id
        List<Integer> chineseCharacters = bizChineseCharactersMapper.selectAllFromCharacter(null);
        int size = chineseCharacters.size();
        Map<String ,CharacterSummaryRsp> characterSummaryRspMap = new LinkedHashMap<String, CharacterSummaryRsp>();
        CharacterSummaryRsp characterSummaryRsp = null;// 初始化返回值
        newWordsSummaryRsp.setAlready_count(Long.valueOf(0));// 初始化总数
        Long alreadyCount = Long.valueOf(0);// 完成总数
        if(size > 0){
            newWordsSummaryRsp.setCharacters_count(Long.valueOf(size));// 总数
            // level = 1 区间是500
            for(int i = 1; i<= size; i = i + 500){
                characterSummaryRsp = new CharacterSummaryRsp();
                int nextIntevalCount = i + 499;// 当前范围尾数
                characterSummaryRsp.setAlready(Long.valueOf(0)); // 初始化 会
                characterSummaryRsp.setIncapable(Long.valueOf(0));// 初始化 不会
                characterSummaryRsp.setIsShow(1); // 初始化是否显示
                List<Integer> integersLevel1 = new ArrayList<>();

                // 获取是否显示
                GradeStudyPlanDetail gradeStudyPlanDetail = new GradeStudyPlanDetail();
                gradeStudyPlanDetail.setGradeId(gradeId);
                gradeStudyPlanDetail.setModule(module);
                gradeStudyPlanDetail.setChildModule(childModule);
                gradeStudyPlanDetail.setDetailId(i+"-"+nextIntevalCount);// 以首位数字来判断
                List<GradeStudyPlanDetail> gradeStudyPlanDetails = gradeStudyPlanDetailMapper.selectGradeStudyPlanDetailList(gradeStudyPlanDetail);
                if (gradeStudyPlanDetails.isEmpty()){
                    // 跳过此轮循环
                    continue;
                }

                if(i+499 >= size){
                    integersLevel1 = chineseCharacters.subList(i - 1, size);
                }else{
                    integersLevel1 = chineseCharacters.subList(i - 1, i + 499);
                }
                // 标记完成项
                for (ChineseCharactersSummary item : chineseCharactersSummaries) {
                    for (Integer characterLevel1 : integersLevel1) {
                        if (characterLevel1.intValue() == item.getChineseCharactersId().intValue()) {
                            if(item.getStatus() == 1){
                                ++alreadyCount;
                                characterSummaryRsp.setAlready(characterSummaryRsp.getAlready() +1);
                            }else{
                                characterSummaryRsp.setIncapable(characterSummaryRsp.getIncapable()+1);
                            }
                        }
                    }
                }
                characterSummaryRspMap.put(i+"-"+nextIntevalCount,characterSummaryRsp);
            }
        }
        newWordsSummaryRsp.setList(characterSummaryRspMap);
        newWordsSummaryRsp.setAlready_count(alreadyCount);
        return newWordsSummaryRsp;
    }

    @Override
    public List<ChineseCharacters> selectNewWordsList(String between,Integer status,String key) {
        // 查出所有汉字总数
        List<Integer> chineseCharacters = bizChineseCharactersMapper.selectAllFromCharacter(key);
        int size = chineseCharacters.size();

        List<Integer> characters = new ArrayList<Integer>();
        String[] split = between.split("-");
        if(Integer.valueOf(split[1])<=size){
            characters = chineseCharacters.subList(Integer.valueOf(split[0])-1, Integer.valueOf(split[1]));
        }else {
            characters = chineseCharacters.subList(Integer.valueOf(split[0])-1, size);
        }

        // 返回区间内所有的汉字，如1-50区间的汉字
        List<ChineseCharacters> chineseCharacters1 = new ArrayList<>();
        if(null == status){
            chineseCharacters1 = bizChineseCharactersMapper.selectChineseCharactersByRefids(characters);
            if (!CollectionUtils.isEmpty(chineseCharacters1) && StringUtils.isNotBlank(key)) {
                chineseCharacters1 = chineseCharacters1.stream().filter(item -> item.getCharacter().equals(key)).collect(Collectors.toList());
            }
            return chineseCharacters1;
        }

        // 查出当前用户所有会与不会的汉字记录
        ChineseCharactersSummary chineseCharactersSummary = new ChineseCharactersSummary();
        chineseCharactersSummary.setUserId(SecurityUtils.getUserId());
        List<ChineseCharactersSummary> chineseCharactersSummaries = bizChineseCharactersSummaryMapper.selectChineseCharactersSummaryList(chineseCharactersSummary);

        // 返回区间内会或不会的，如1-50区间内会和不会的汉字
        List<Integer> charactersAlreadOrNot = new ArrayList<Integer>();

        for (ChineseCharactersSummary item : chineseCharactersSummaries) {
            for (Integer characterLevel1 : characters) {
                if (characterLevel1.intValue() == item.getChineseCharactersId().intValue()) {
                    if(status == item.getStatus().intValue()){
                        charactersAlreadOrNot.add(item.getChineseCharactersId().intValue());
                    }
                }
            }
        }
        if(!CollectionUtils.isEmpty(charactersAlreadOrNot)){
            chineseCharacters1 = bizChineseCharactersMapper.selectChineseCharactersByRefids(charactersAlreadOrNot);
        }
        return chineseCharacters1;
    }
}
