package com.ruoyi.business.service.impl;

import java.util.List;

import com.ruoyi.business.entity.EtymologyChildren;
import com.ruoyi.business.mapper.BizEtymologyChildrenMapper;
import com.ruoyi.business.service.IEtymologyChildrenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 字源示例Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@Service
public class BizEtymologyChildrenServiceImpl implements IEtymologyChildrenService
{
    @Autowired
    private BizEtymologyChildrenMapper bizEtymologyChildrenMapper;

    /**
     * 查询字源示例
     * 
     * @param id 字源示例主键
     * @return 字源示例
     */
    @Override
    public EtymologyChildren selectEtymologyChildrenById(Long id)
    {
        return bizEtymologyChildrenMapper.selectEtymologyChildrenById(id);
    }

    /**
     * 查询字源示例列表
     * 
     * @param etymologyChildren 字源示例
     * @return 字源示例
     */
    @Override
    public List<EtymologyChildren> selectEtymologyChildrenList(EtymologyChildren etymologyChildren)
    {
        return bizEtymologyChildrenMapper.selectEtymologyChildrenList(etymologyChildren);
    }

    /**
     * 新增字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    @Override
    public int insertEtymologyChildren(EtymologyChildren etymologyChildren)
    {
        return bizEtymologyChildrenMapper.insertEtymologyChildren(etymologyChildren);
    }

    /**
     * 修改字源示例
     * 
     * @param etymologyChildren 字源示例
     * @return 结果
     */
    @Override
    public int updateEtymologyChildren(EtymologyChildren etymologyChildren)
    {
        return bizEtymologyChildrenMapper.updateEtymologyChildren(etymologyChildren);
    }

    /**
     * 批量删除字源示例
     * 
     * @param ids 需要删除的字源示例主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyChildrenByIds(Long[] ids)
    {
        return bizEtymologyChildrenMapper.deleteEtymologyChildrenByIds(ids);
    }

    /**
     * 删除字源示例信息
     * 
     * @param id 字源示例主键
     * @return 结果
     */
    @Override
    public int deleteEtymologyChildrenById(Long id)
    {
        return bizEtymologyChildrenMapper.deleteEtymologyChildrenById(id);
    }
}
