package com.ruoyi.business.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 my_notes
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class MyNotes extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 笔记标识 */
    private Long id;

    private Long notes_id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long uId;

    /** 笔记标题 */
    @Excel(name = "笔记标题")
    @JsonProperty("notes_title")
    private String notesTitle;

    /** 笔记内容 */
    @Excel(name = "笔记内容")
    @JsonProperty("my_notes")
    private String myNotes;

    /** 笔记排序 */
    @Excel(name = "笔记排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setuId(Long uId) 
    {
        this.uId = uId;
    }

    public Long getuId() 
    {
        return uId;
    }
    public void setNotesTitle(String notesTitle) 
    {
        this.notesTitle = notesTitle;
    }

    public String getNotesTitle() 
    {
        return notesTitle;
    }
    public void setMyNotes(String myNotes) 
    {
        this.myNotes = myNotes;
    }

    public String getMyNotes() 
    {
        return myNotes;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public Long getNotes_id() {
        return notes_id;
    }

    public void setNotes_id(Long notes_id) {
        this.notes_id = notes_id;
    }

    @Override
    public String toString() {
        return "MyNotes{" +
                "id=" + id +
                ", notes_id='" + notes_id + '\'' +
                ", uId=" + uId +
                ", notesTitle='" + notesTitle + '\'' +
                ", myNotes='" + myNotes + '\'' +
                ", sort=" + sort +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
