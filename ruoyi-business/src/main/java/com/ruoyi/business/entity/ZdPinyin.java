package com.ruoyi.business.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 zd_pinyin
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class ZdPinyin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String xuhao;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String refid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String zi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pinyin;

    private String pronunciationAudioSrc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pyj;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String py;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String head;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String diaohao;

    /**  读音 */
    @Excel(name = " 读音")
    private String duyin;

    public void setXuhao(String xuhao) 
    {
        this.xuhao = xuhao;
    }

    public String getXuhao() 
    {
        return xuhao;
    }
    public void setRefid(String refid) 
    {
        this.refid = refid;
    }

    public String getRefid() 
    {
        return refid;
    }
    public void setZi(String zi) 
    {
        this.zi = zi;
    }

    public String getZi() 
    {
        return zi;
    }
    public void setPinyin(String pinyin) 
    {
        this.pinyin = pinyin;
    }

    public String getPinyin() 
    {
        return pinyin;
    }
    public void setPyj(String pyj) 
    {
        this.pyj = pyj;
    }

    public String getPyj() 
    {
        return pyj;
    }
    public void setPy(String py) 
    {
        this.py = py;
    }

    public String getPy() 
    {
        return py;
    }
    public void setHead(String head) 
    {
        this.head = head;
    }

    public String getHead() 
    {
        return head;
    }
    public void setDiaohao(String diaohao) 
    {
        this.diaohao = diaohao;
    }

    public String getDiaohao() 
    {
        return diaohao;
    }
    public void setDuyin(String duyin) 
    {
        this.duyin = duyin;
    }

    public String getDuyin() 
    {
        return duyin;
    }

    public String getPronunciationAudioSrc() {
        return pronunciationAudioSrc;
    }

    public void setPronunciationAudioSrc(String pronunciationAudioSrc) {
        this.pronunciationAudioSrc = pronunciationAudioSrc;
    }

    @Override
    public String toString() {
        return "ZdPinyin{" +
                "xuhao='" + xuhao + '\'' +
                ", refid='" + refid + '\'' +
                ", zi='" + zi + '\'' +
                ", pinyin='" + pinyin + '\'' +
                ", pronunciationAudioSrc='" + pronunciationAudioSrc + '\'' +
                ", pyj='" + pyj + '\'' +
                ", py='" + py + '\'' +
                ", head='" + head + '\'' +
                ", diaohao='" + diaohao + '\'' +
                ", duyin='" + duyin + '\'' +
                '}';
    }
}
