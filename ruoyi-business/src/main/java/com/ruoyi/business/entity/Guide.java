package com.ruoyi.business.entity;

import java.util.Date;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 guide
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class Guide extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 指导建议标识 */
    private Long id;

    /** 指导建议标题 */
    @Excel(name = "指导建议标题")
    @JSONField(name = "guide_title")
    private String guideTitle;

    /** 指导建议内容 */
    @Excel(name = "指导建议内容")
    private String guide;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGuideTitle(String guideTitle) 
    {
        this.guideTitle = guideTitle;
    }

    public String getGuideTitle() 
    {
        return guideTitle;
    }
    public void setGuide(String guide) 
    {
        this.guide = guide;
    }

    public String getGuide() 
    {
        return guide;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("guideTitle", getGuideTitle())
            .append("guide", getGuide())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
