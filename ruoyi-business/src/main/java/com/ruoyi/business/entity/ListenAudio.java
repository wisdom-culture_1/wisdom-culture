package com.ruoyi.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 诗词磨耳朵对象 listen_book
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class ListenAudio extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String audio_src;

    private String title;

    public String getAudio_src() {
        return audio_src;
    }

    public void setAudio_src(String audio_src) {
        this.audio_src = audio_src;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
