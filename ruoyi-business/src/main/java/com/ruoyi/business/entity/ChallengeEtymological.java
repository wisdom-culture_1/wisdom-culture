package com.ruoyi.business.entity;

import java.util.Date;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 challenge_etymological
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
public class ChallengeEtymological extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 分组id */
    @Excel(name = "分组id")
    @JsonProperty("group_id")
    @JSONField(name = "group_id")
    private Long groupId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JSONField(name = "type_id")
    private Long typeId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String etymologicalText;

    /** 字源图 */
    @Excel(name = "字源图")
    private String etymologicalImageSrc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String wordText;

    /** 字图 */
    @Excel(name = "字图")
    private String wordImageSrc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }
    public void setEtymologicalText(String etymologicalText) 
    {
        this.etymologicalText = etymologicalText;
    }

    public String getEtymologicalText() 
    {
        return etymologicalText;
    }
    public void setEtymologicalImageSrc(String etymologicalImageSrc) 
    {
        this.etymologicalImageSrc = etymologicalImageSrc;
    }

    public String getEtymologicalImageSrc() 
    {
        return etymologicalImageSrc;
    }
    public void setWordText(String wordText) 
    {
        this.wordText = wordText;
    }

    public String getWordText() 
    {
        return wordText;
    }
    public void setWordImageSrc(String wordImageSrc) 
    {
        this.wordImageSrc = wordImageSrc;
    }

    public String getWordImageSrc() 
    {
        return wordImageSrc;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("groupId", getGroupId())
            .append("typeId", getTypeId())
            .append("etymologicalText", getEtymologicalText())
            .append("etymologicalImageSrc", getEtymologicalImageSrc())
            .append("wordText", getWordText())
            .append("wordImageSrc", getWordImageSrc())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
