package com.ruoyi.business.entity;

import java.util.Date;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 金句摘抄对象 excerpt
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class Excerpt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 摘要标识 */
    private Long id;

    /** 分类id */
    @Excel(name = "分类id")
    @JSONField(name = "type_id")
    private Long typeId;

    /** 出处 */
    @Excel(name = "出处")
    private String source;

    /** 作者 */
    @Excel(name = "作者")
    private String author;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 章节内容 */
    @Excel(name = "章节内容")
    private String content;

    private String is_like;

    /** 评论数 */
    @Excel(name = "评论数")
    @JsonProperty("comment_number")
    @JSONField(name = "comment_number")
    private Long commentNumber;

    /** 点赞数 */
    @Excel(name = "点赞数")
    @JsonProperty("like_number")
    @JSONField(name = "like_number")
    private Long likeNumber;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }
    public void setSource(String source) 
    {
        this.source = source;
    }

    public String getSource() 
    {
        return source;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setCommentNumber(Long commentNumber) 
    {
        this.commentNumber = commentNumber;
    }

    public Long getCommentNumber() 
    {
        return commentNumber;
    }
    public void setLikeNumber(Long likeNumber) 
    {
        this.likeNumber = likeNumber;
    }

    public Long getLikeNumber() 
    {
        return likeNumber;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    @Override
    public String toString() {
        return "Excerpt{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", source='" + source + '\'' +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", is_like='" + is_like + '\'' +
                ", commentNumber=" + commentNumber +
                ", likeNumber=" + likeNumber +
                ", sort=" + sort +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
