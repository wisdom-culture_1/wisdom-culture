package com.ruoyi.business.entity;

import java.util.Date;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文言名篇扩展对象 classical_extend
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class ClassicalExtend extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 序号 */
    @Excel(name = "序号")
    private Long xuhao;

    /** 扩展标题 */
    @Excel(name = "扩展标题")
    private String title;

    /** 扩展分类 */
    @Excel(name = "扩展分类")
    @JsonProperty("type_name")
    @JSONField(name = "type_name")
    private String typeName;

    /** 扩展图片 */
    @Excel(name = "扩展图片")
    @JsonProperty("extend_image_src")
    @JSONField(name = "extend_image_src")
    private String extendImageSrc;

    /** 扩展内容 */
    @Excel(name = "扩展内容")
    private String content;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setXuhao(Long xuhao) 
    {
        this.xuhao = xuhao;
    }

    public Long getXuhao() 
    {
        return xuhao;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setTypeName(String typeName) 
    {
        this.typeName = typeName;
    }

    public String getTypeName() 
    {
        return typeName;
    }
    public void setExtendImageSrc(String extendImageSrc) 
    {
        this.extendImageSrc = extendImageSrc;
    }

    public String getExtendImageSrc() 
    {
        return extendImageSrc;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("xuhao", getXuhao())
            .append("title", getTitle())
            .append("typeName", getTypeName())
            .append("extendImageSrc", getExtendImageSrc())
            .append("content", getContent())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
