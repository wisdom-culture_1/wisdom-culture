package com.ruoyi.business.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学习计划对象 grade_study_plan
 * 
 * @author ruoyi
 * @date 2024-04-14
 */
public class GradeStudyPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 年级名称 */
    @Excel(name = "年级名称")
    private String gradeName;

    /** 年级 */
    @Excel(name = "年级")
    private Long gradeId;

    /** 学习计划模块 1:掌握拼音，2：认识汉字，3：阅读量，4、培养表达 */
    @Excel(name = "学习计划模块 1:掌握拼音，2：认识汉字，3：阅读量，4、培养表达")
    private Long module;

    /** moule为1：child_module 1：字母表，2：声母表，3：韵母
			  moule为2：child_module 1：速斩生字，2：生词闯大关
			  moule为3：child_module 1：成语典故，2：人物外貌，3：人物类型，4：人物心理，5：人物心情
									 6：人物品质，7：言辞表达，8：为人处世，9：描写景物，10：描写四季
									 11：描写气候，12：十二生肖，13：其他动物 14：其他 15： 对比、比较
									 16：社会生活 17、社会斗争 18教育学习 19 政治法律 20 军事经济 21 成语结构
									 22： 近反义词 23：成语故事 24：语文大纲 25：四大名著 26：其他名著 27：出自名人
									 28：其他和人有关
									 
			moule为4：child_module 1：国学启蒙，2：一年级背诵篇目，3：二年级背诵篇目，4：三年级背诵篇目，5：四年级背诵篇目	
								   6：五年级背诵篇目，7：六年级背诵篇目，8：初一背诵篇目，9：初二背诵篇目，10：初三背诵篇目 */
    @Excel(name = "")
    private Long childModule;

    private String moduleNickname;
    private String childModuleNickname;

    private String pic;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGradeName(String gradeName) 
    {
        this.gradeName = gradeName;
    }

    public String getGradeName() 
    {
        return gradeName;
    }
    public void setGradeId(Long gradeId) 
    {
        this.gradeId = gradeId;
    }

    public Long getGradeId() 
    {
        return gradeId;
    }
    public void setModule(Long module) 
    {
        this.module = module;
    }

    public Long getModule() 
    {
        return module;
    }
    public void setChildModule(Long childModule) 
    {
        this.childModule = childModule;
    }

    public Long getChildModule() 
    {
        return childModule;
    }

    public String getPic() {
		return pic;
	}

    public void setPic(String pic) {
		this.pic = pic;
	}

    public String getModuleNickname() {
		return moduleNickname;
	}

    public void setModuleNickname(String moduleNickname) {
		this.moduleNickname = moduleNickname;
	}

    public String getChildModuleNickname() {
		return childModuleNickname;
	}

    public void setChildModuleNickname(String childModuleNickname) {
		this.childModuleNickname = childModuleNickname;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gradeName", getGradeName())
            .append("gradeId", getGradeId())
            .append("module", getModule())
            .append("childModule", getChildModule())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("pic", getPic())
                .append("moduleNickname", getModuleNickname())
                .append("childModuleNickname", getChildModuleNickname())
            .toString();
    }
}
