package com.ruoyi.business.entity;

import java.util.Date;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 字母对象 alphabets
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class Alphabets extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字典id */
    @Excel(name = "字典id")
    private Long dictionaryChildrenKey;

    /** 字母 */
    @Excel(name = "字母")
    private String letter;

    /** 字母对应汉字 */
    @Excel(name = "字母对应汉字")
    private String word;

    /** 字母音频 */
    @Excel(name = "字母音频")
    @JsonProperty("audio_frequency")
    @JSONField(name = "audio_frequency")
    private String audioFrequency;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDictionaryChildrenKey(Long dictionaryChildrenKey) 
    {
        this.dictionaryChildrenKey = dictionaryChildrenKey;
    }

    public Long getDictionaryChildrenKey() 
    {
        return dictionaryChildrenKey;
    }
    public void setLetter(String letter) 
    {
        this.letter = letter;
    }

    public String getLetter() 
    {
        return letter;
    }
    public void setWord(String word) 
    {
        this.word = word;
    }

    public String getWord() 
    {
        return word;
    }
    public void setAudioFrequency(String audioFrequency) 
    {
        this.audioFrequency = audioFrequency;
    }

    public String getAudioFrequency() 
    {
        return audioFrequency;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dictionaryChildrenKey", getDictionaryChildrenKey())
            .append("letter", getLetter())
            .append("word", getWord())
            .append("audioFrequency", getAudioFrequency())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
