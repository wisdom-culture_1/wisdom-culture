package com.ruoyi.business.entity;


public class ReciteDeleteVo {

    Long id;

    Long typeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        return "reciteDeleteVo{" +
                "id=" + id +
                ", typeId=" + typeId +
                '}';
    }
}
