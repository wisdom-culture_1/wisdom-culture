package com.ruoyi.business.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 zd_bsbh
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class ZdBsbh extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 部首 */
    @Excel(name = "部首")
    private String bu;

    /** 部首展示 */
    @Excel(name = "部首展示")
    private String bushow;

    /** 笔画 */
    @Excel(name = "笔画")
    private Long bihua;

    public void setBu(String bu) 
    {
        this.bu = bu;
    }

    public String getBu() 
    {
        return bu;
    }
    public void setBushow(String bushow) 
    {
        this.bushow = bushow;
    }

    public String getBushow() 
    {
        return bushow;
    }
    public void setBihua(Long bihua) 
    {
        this.bihua = bihua;
    }

    public Long getBihua() 
    {
        return bihua;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("bu", getBu())
            .append("bushow", getBushow())
            .append("bihua", getBihua())
            .toString();
    }
}
