package com.ruoyi.business.entity;

import java.util.Date;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 背诵打卡对象 recite
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class Recite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 分类id */
    @Excel(name = "分类id")
    @JSONField(name = "type_id")
    @JsonProperty("type_id")
    private Long typeId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 音频地址 */
    @Excel(name = "音频地址")
    @JsonProperty("audio_src")
    @JSONField(name = "audio_src")
    private String audioSrc;

    /** 音频时长 */
    @Excel(name = "音频时长")
    @JSONField(name = "long")
    private Long longTime;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    private WxInfo userwx;

    private Long like_count;

    private String log_user;

    public Long getLike_count() {
        return like_count;
    }

    public void setLike_count(Long like_count) {
        this.like_count = like_count;
    }

    public WxInfo getUserwx() {
        return userwx;
    }

    public void setUserwx(WxInfo userwx) {
        this.userwx = userwx;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setAudioSrc(String audioSrc) 
    {
        this.audioSrc = audioSrc;
    }

    public String getAudioSrc() 
    {
        return audioSrc;
    }
    public void setLong(Long longTime)
    {
        this.longTime = longTime;
    }

    public Long getLong() 
    {
        return longTime;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public Long getLongTime() {
        return longTime;
    }

    public void setLongTime(Long longTime) {
        this.longTime = longTime;
    }

    public String getLog_user() {
        return log_user;
    }

    public void setLog_user(String log_user) {
        this.log_user = log_user;
    }

    @Override
    public String toString() {
        return "Recite{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", userId=" + userId +
                ", audioSrc='" + audioSrc + '\'' +
                ", longTime=" + longTime +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", userwx=" + userwx +
                ", like_count=" + like_count +
                ", log_user='" + log_user + '\'' +
                '}';
    }
}
