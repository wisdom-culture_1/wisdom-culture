package com.ruoyi.business.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 users
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class Users extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 授权码 */
    @Excel(name = "授权码")
    private String code;

    /** 到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expirationTime;

    /** 注册昵称 */
    @Excel(name = "注册昵称")
    private String nickname;

    /** 孩子姓名 */
    @Excel(name = "孩子姓名")
    private String childrenName;

    /** 孩子昵称 */
    @Excel(name = "孩子昵称")
    private String childrenNickname;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 年级 */
    @Excel(name = "年级")
    private Long gradeId;

    /** 1上半学期  2下半学期 */
    @Excel(name = "1上半学期  2下半学期")
    private Long greadUad;

    /** 微信头像地址 */
    @Excel(name = "微信头像地址")
    private String wxImg;

    /** 微信昵称 */
    @Excel(name = "微信昵称")
    private String wxName;

    /** 微信OPENID */
    @Excel(name = "微信OPENID")
    private String wxOpenid;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setExpirationTime(Date expirationTime) 
    {
        this.expirationTime = expirationTime;
    }

    public Date getExpirationTime() 
    {
        return expirationTime;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return nickname;
    }
    public void setChildrenName(String childrenName) 
    {
        this.childrenName = childrenName;
    }

    public String getChildrenName() 
    {
        return childrenName;
    }
    public void setChildrenNickname(String childrenNickname) 
    {
        this.childrenNickname = childrenNickname;
    }

    public String getChildrenNickname() 
    {
        return childrenNickname;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setGradeId(Long gradeId) 
    {
        this.gradeId = gradeId;
    }

    public Long getGradeId() 
    {
        return gradeId;
    }
    public void setGreadUad(Long greadUad) 
    {
        this.greadUad = greadUad;
    }

    public Long getGreadUad() 
    {
        return greadUad;
    }
    public void setWxImg(String wxImg) 
    {
        this.wxImg = wxImg;
    }

    public String getWxImg() 
    {
        return wxImg;
    }
    public void setWxName(String wxName) 
    {
        this.wxName = wxName;
    }

    public String getWxName() 
    {
        return wxName;
    }
    public void setWxOpenid(String wxOpenid) 
    {
        this.wxOpenid = wxOpenid;
    }

    public String getWxOpenid() 
    {
        return wxOpenid;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("code", getCode())
            .append("expirationTime", getExpirationTime())
            .append("nickname", getNickname())
            .append("childrenName", getChildrenName())
            .append("childrenNickname", getChildrenNickname())
            .append("address", getAddress())
            .append("phone", getPhone())
            .append("email", getEmail())
            .append("gradeId", getGradeId())
            .append("greadUad", getGreadUad())
            .append("wxImg", getWxImg())
            .append("wxName", getWxName())
            .append("wxOpenid", getWxOpenid())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
