package com.ruoyi.business.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 授权码对象 authorization_code
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class AuthorizationCode extends BaseEntity
{
    private static final long serialVersionUID = 1L;



    /** $column.columnComment */
    private Long id;

    /** 生成批次 */
    @Excel(name = "生成批次")
    private String title;

    /** 授权码 */
    @Excel(name = "授权码")
    private String code;

    /** 状态：0：未使用 1:已使用 2: 已到期 -1:暂停使用 */
    @Excel(name = "状态：0：未使用 1:已使用 2: 已到期 -1:暂停使用")
    private String isUse;

    /** 会员期限 */
    @Excel(name = "用户名")
    private String userName;

    /** 会员期限 */
    @Excel(name = "会员期限")
    private Long userTerm;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    /** 会员开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "会员开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 会员结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "会员结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 批次新增 */
    private Integer count;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setIsUse(String isUse)
    {
        this.isUse = isUse;
    }

    public String getIsUse()
    {
        return isUse;
    }
    public void setUserTerm(Long userTerm) 
    {
        this.userTerm = userTerm;
    }

    public Long getUserTerm() 
    {
        return userTerm;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }
    public void setStartDate(Date startDate) 
    {
        this.startDate = startDate;
    }

    public Date getStartDate() 
    {
        return startDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "AuthorizationCode{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", code='" + code + '\'' +
                ", isUse='" + isUse + '\'' +
                ", userName='" + userName + '\'' +
                ", userTerm=" + userTerm +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", count=" + count +
                '}';
    }
}
