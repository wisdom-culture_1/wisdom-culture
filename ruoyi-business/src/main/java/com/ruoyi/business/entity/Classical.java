package com.ruoyi.business.entity;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文言文对象 classical
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class Classical extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 序号 */
    @Excel(name = "序号")
    private Long xuhao;

    /** 类型（1.古文初始2.必背名篇3课外扩展） */
    @Excel(name = "类型", readConverterExp = "1=.古文初始2.必背名篇3课外扩展")
    private Long type;

    /** 书籍标题 */
    @Excel(name = "书籍标题")
    private String title;

    /** 作者 */
    @Excel(name = "作者")
    private String name;

    /** 年代 */
    @Excel(name = "年代")
    private String years;

    /** 动画 */
    @Excel(name = "动画")
    @JsonProperty("animation_video_src")
    @JSONField(name = "animation_video_src")
    private String animationVideoSrc;

    /** 音频 */
    @Excel(name = "音频")
    @JsonProperty("animation_audio_src")
    @JSONField(name = "animation_audio_src")
    private String animationAudioSrc;

    /** 译文 */
    @Excel(name = "译文")
    private String translation;

    /** 正文数据 */
    @Excel(name = "正文数据")
    private String content;

    /** 拼音数据 */
    @Excel(name = "拼音数据")
    private String pinyin;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    private List<ClassicalNotes> classicalnotes;

    private List<ClassicalText> classicaltext;

    public List<ClassicalNotes> getClassicalnotes() {
        return classicalnotes;
    }

    public void setClassicalnotes(List<ClassicalNotes> classicalnotes) {
        this.classicalnotes = classicalnotes;
    }

    public List<ClassicalText> getClassicaltext() {
        return classicaltext;
    }

    public void setClassicaltext(List<ClassicalText> classicaltext) {
        this.classicaltext = classicaltext;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setXuhao(Long xuhao) 
    {
        this.xuhao = xuhao;
    }

    public Long getXuhao() 
    {
        return xuhao;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setYears(String years) 
    {
        this.years = years;
    }

    public String getYears() 
    {
        return years;
    }
    public void setAnimationVideoSrc(String animationVideoSrc) 
    {
        this.animationVideoSrc = animationVideoSrc;
    }

    public String getAnimationVideoSrc() 
    {
        return animationVideoSrc;
    }
    public void setAnimationAudioSrc(String animationAudioSrc) 
    {
        this.animationAudioSrc = animationAudioSrc;
    }

    public String getAnimationAudioSrc() 
    {
        return animationAudioSrc;
    }
    public void setTranslation(String translation) 
    {
        this.translation = translation;
    }

    public String getTranslation() 
    {
        return translation;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPinyin(String pinyin) 
    {
        this.pinyin = pinyin;
    }

    public String getPinyin() 
    {
        return pinyin;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("xuhao", getXuhao())
            .append("type", getType())
            .append("title", getTitle())
            .append("name", getName())
            .append("years", getYears())
            .append("animationVideoSrc", getAnimationVideoSrc())
            .append("animationAudioSrc", getAnimationAudioSrc())
            .append("translation", getTranslation())
            .append("content", getContent())
            .append("pinyin", getPinyin())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
