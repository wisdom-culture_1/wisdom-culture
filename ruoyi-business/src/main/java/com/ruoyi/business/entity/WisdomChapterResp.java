package com.ruoyi.business.entity;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 智慧元典章节对象 wisdom_chapter
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class WisdomChapterResp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private Long xuhao;

    /** 书籍id */
    @Excel(name = "书籍id")
    private Long bookId;

    /** 类型（1.国学启蒙 2.思想元典） */
    @Excel(name = "类型", readConverterExp = "1=.国学启蒙,2=.思想元典")
    private Long type;

    /** 章节标题 */
    @Excel(name = "章节标题")
    private String title;

    /** 正文 */
    @Excel(name = "正文")
    private String content;

    /** 译文 */
    @Excel(name = "译文")
    private String translation;

    /** 音频 */
    @Excel(name = "音频")
    @JsonProperty("audio_src")
    @JSONField(name = "audio_src")
    private String audioSrc;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    private Integer isShow;

    private List<WisdomText> wisdomtext;

    private List<WisdomNotes> wisdomnotes;

    public List<WisdomText> getWisdomtext() {
        return wisdomtext;
    }

    public void setWisdomtext(List<WisdomText> wisdomtext) {
        this.wisdomtext = wisdomtext;
    }

    public List<WisdomNotes> getWisdomnotes() {
        return wisdomnotes;
    }

    public void setWisdomnotes(List<WisdomNotes> wisdomnotes) {
        this.wisdomnotes = wisdomnotes;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setXuhao(Long xuhao) 
    {
        this.xuhao = xuhao;
    }

    public Long getXuhao() 
    {
        return xuhao;
    }
    public void setBookId(Long bookId) 
    {
        this.bookId = bookId;
    }

    public Long getBookId() 
    {
        return bookId;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setTranslation(String translation) 
    {
        this.translation = translation;
    }

    public String getTranslation() 
    {
        return translation;
    }
    public void setAudioSrc(String audioSrc) 
    {
        this.audioSrc = audioSrc;
    }

    public String getAudioSrc() 
    {
        return audioSrc;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("xuhao", getXuhao())
            .append("bookId", getBookId())
            .append("type", getType())
            .append("title", getTitle())
            .append("content", getContent())
            .append("translation", getTranslation())
            .append("audioSrc", getAudioSrc())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
                .append("isShow", getIsShow())
            .toString();
    }
}
