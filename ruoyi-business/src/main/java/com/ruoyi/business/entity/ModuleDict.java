package com.ruoyi.business.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 模块对应关系字典对象 module_dict
 * 
 * @author ruoyi
 * @date 2024-06-22
 */
public class ModuleDict extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 模块id */
    @Excel(name = "模块id")
    private Long module;

    /** 子模块id */
    @Excel(name = "子模块id")
    private Long childModule;

    /** 模块名 */
    @Excel(name = "模块名")
    private String moduleName;

    @Excel(name = "子模块名")
    private String childModuleName;

    /** 是否可配置内容 */
    @Excel(name = "是否可配置内容")
    private Long dynamic;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setModule(Long module) 
    {
        this.module = module;
    }

    public Long getModule() 
    {
        return module;
    }
    public void setChildModule(Long childModule) 
    {
        this.childModule = childModule;
    }

    public Long getChildModule() 
    {
        return childModule;
    }
    public void setModuleName(String moduleName) 
    {
        this.moduleName = moduleName;
    }

    public String getModuleName() 
    {
        return moduleName;
    }
    public void setDynamic(Long dynamic) 
    {
        this.dynamic = dynamic;
    }

    public Long getDynamic() 
    {
        return dynamic;
    }

    public String getChildModuleName() {
        return childModuleName;
    }

    public void setChildModuleName(String childModuleName) {
        this.childModuleName = childModuleName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("module", getModule())
            .append("childModule", getChildModule())
            .append("moduleName", getModuleName())
            .append("dynamic", getDynamic())
                .append("childModuleName", getChildModuleName())
            .toString();
    }
}
