package com.ruoyi.business.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 zd_bushou
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class ZdBushou extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long xuhao;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String zi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String bu;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sbh;

    private String refid;

    public String getRefid() {
        return refid;
    }

    public void setRefid(String refid) {
        this.refid = refid;
    }

    public void setXuhao(Long xuhao)
    {
        this.xuhao = xuhao;
    }

    public Long getXuhao() 
    {
        return xuhao;
    }
    public void setZi(String zi) 
    {
        this.zi = zi;
    }

    public String getZi() 
    {
        return zi;
    }
    public void setBu(String bu) 
    {
        this.bu = bu;
    }

    public String getBu() 
    {
        return bu;
    }
    public void setSbh(Long sbh) 
    {
        this.sbh = sbh;
    }

    public Long getSbh() 
    {
        return sbh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("xuhao", getXuhao())
            .append("zi", getZi())
            .append("bu", getBu())
            .append("sbh", getSbh())
            .toString();
    }
}
