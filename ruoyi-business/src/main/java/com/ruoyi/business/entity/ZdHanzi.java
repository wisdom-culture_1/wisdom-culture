package com.ruoyi.business.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 zd_hanzi
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class ZdHanzi extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long refid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String zitou;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fjzi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String zi;

    public void setRefid(Long refid) 
    {
        this.refid = refid;
    }

    public Long getRefid() 
    {
        return refid;
    }
    public void setZitou(String zitou) 
    {
        this.zitou = zitou;
    }

    public String getZitou() 
    {
        return zitou;
    }
    public void setFjzi(String fjzi) 
    {
        this.fjzi = fjzi;
    }

    public String getFjzi() 
    {
        return fjzi;
    }
    public void setZi(String zi) 
    {
        this.zi = zi;
    }

    public String getZi() 
    {
        return zi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("refid", getRefid())
            .append("zitou", getZitou())
            .append("fjzi", getFjzi())
            .append("zi", getZi())
            .toString();
    }
}
