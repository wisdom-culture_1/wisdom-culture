package com.ruoyi.business.entity;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 字源对象 etymology
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class Etymology extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字典key */
    @Excel(name = "字典key")
    private String dictionaryChildrenKey;

    /** 拼音 */
    @Excel(name = "拼音")
    private String pinyin;

    /** 1文字 2 图片 */
    @Excel(name = "1文字 2 图片")
    private Long type;

    /** 字源 */
    @Excel(name = "字源")
    private String etymology;

    /** 笔画数 */
    @Excel(name = "笔画数")
    private Long numberOfStrokes;

    /** 偏旁 */
    @Excel(name = "偏旁")
    private String side;

    /** 修饰图 */
    @Excel(name = "修饰图")
    @JSONField(name = "modification_image_src")
    private String modificationImageSrc;

    /** 演变图 */
    @Excel(name = "演变图")
    @JSONField(name = "evolution_image_src")
    private String evolutionImageSrc;

    /** 视频 */
    @Excel(name = "视频")
    @JSONField(name = "video_src")
    private String videoSrc;

    /** 解释 */
    @Excel(name = "解释")
    private String explain;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    private List<EtymologyChildren> etymologyChild;

    public List<EtymologyChildren> getEtymologyChild() {
        return etymologyChild;
    }

    public void setEtymologyChild(List<EtymologyChildren> etymologyChild) {
        this.etymologyChild = etymologyChild;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDictionaryChildrenKey(String dictionaryChildrenKey) 
    {
        this.dictionaryChildrenKey = dictionaryChildrenKey;
    }

    public String getDictionaryChildrenKey() 
    {
        return dictionaryChildrenKey;
    }
    public void setPinyin(String pinyin) 
    {
        this.pinyin = pinyin;
    }

    public String getPinyin() 
    {
        return pinyin;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setEtymology(String etymology) 
    {
        this.etymology = etymology;
    }

    public String getEtymology() 
    {
        return etymology;
    }
    public void setNumberOfStrokes(Long numberOfStrokes) 
    {
        this.numberOfStrokes = numberOfStrokes;
    }

    public Long getNumberOfStrokes() 
    {
        return numberOfStrokes;
    }
    public void setSide(String side) 
    {
        this.side = side;
    }

    public String getSide() 
    {
        return side;
    }
    public void setModificationImageSrc(String modificationImageSrc) 
    {
        this.modificationImageSrc = modificationImageSrc;
    }

    public String getModificationImageSrc() 
    {
        return modificationImageSrc;
    }
    public void setEvolutionImageSrc(String evolutionImageSrc) 
    {
        this.evolutionImageSrc = evolutionImageSrc;
    }

    public String getEvolutionImageSrc() 
    {
        return evolutionImageSrc;
    }
    public void setVideoSrc(String videoSrc) 
    {
        this.videoSrc = videoSrc;
    }

    public String getVideoSrc() 
    {
        return videoSrc;
    }
    public void setExplain(String explain) 
    {
        this.explain = explain;
    }

    public String getExplain() 
    {
        return explain;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dictionaryChildrenKey", getDictionaryChildrenKey())
            .append("pinyin", getPinyin())
            .append("type", getType())
            .append("etymology", getEtymology())
            .append("numberOfStrokes", getNumberOfStrokes())
            .append("side", getSide())
            .append("modificationImageSrc", getModificationImageSrc())
            .append("evolutionImageSrc", getEvolutionImageSrc())
            .append("videoSrc", getVideoSrc())
            .append("explain", getExplain())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
