package com.ruoyi.business.entity;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 chinese_characters
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class ChineseCharacters extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long refid;

    /** 字 */
    @Excel(name = "字")
    private String character;

    /** 字视频 */
    @Excel(name = "字视频")
    @JSONField(name = "character_video_src")
    private String characterVideoSrc;

    /** 字源图 */
    @Excel(name = "字源图")
    @JSONField(name = "etymology_image_src")
    private String etymologyImageSrc;

    /** 读音 */
    @Excel(name = "读音")
    private String pronunciation;

    private String[] pronunciationList;

    /** 读音音频 */
    @Excel(name = "读音音频")
    @JSONField(name = "pronunciation_audio_src")
    private String pronunciationAudioSrc;

    /** 读音音频 */
    private String[] pronunciationAudioSrcList;

    /** 演变图 */
    @Excel(name = "演变图")
    @JSONField(name = "evolution_image_src")
    private String evolutionImageSrc;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String ytzi;

    /** 释义 */
    @Excel(name = "释义")
    private String shiyi;

    /** 基本释义 */
    @Excel(name = "基本释义")
    private String yanbian;

    /** 完整释义 */
    @Excel(name = "完整释义")
    private String cysy;

    /** 成语典故 */
    @Excel(name = "成语典故")
    private String cy;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setRefid(Long refid) 
    {
        this.refid = refid;
    }

    public Long getRefid() 
    {
        return refid;
    }
    public void setCharacter(String character) 
    {
        this.character = character;
    }

    public String getCharacter() 
    {
        return character;
    }
    public void setCharacterVideoSrc(String characterVideoSrc) 
    {
        this.characterVideoSrc = characterVideoSrc;
    }

    public String getCharacterVideoSrc() 
    {
        return characterVideoSrc;
    }
    public void setEtymologyImageSrc(String etymologyImageSrc) 
    {
        this.etymologyImageSrc = etymologyImageSrc;
    }

    public String getEtymologyImageSrc() 
    {
        return etymologyImageSrc;
    }
    public void setPronunciation(String pronunciation) 
    {
        this.pronunciation = pronunciation;
    }

    public String getPronunciation() 
    {
        return pronunciation;
    }
    public void setPronunciationAudioSrc(String pronunciationAudioSrc) 
    {
        this.pronunciationAudioSrc = pronunciationAudioSrc;
    }

    public String getPronunciationAudioSrc() 
    {
        return pronunciationAudioSrc;
    }
    public void setEvolutionImageSrc(String evolutionImageSrc) 
    {
        this.evolutionImageSrc = evolutionImageSrc;
    }

    public String getEvolutionImageSrc() 
    {
        return evolutionImageSrc;
    }
    public void setYtzi(String ytzi) 
    {
        this.ytzi = ytzi;
    }

    public String getYtzi() 
    {
        return ytzi;
    }
    public void setShiyi(String shiyi) 
    {
        this.shiyi = shiyi;
    }

    public String getShiyi() 
    {
        return shiyi;
    }
    public void setYanbian(String yanbian) 
    {
        this.yanbian = yanbian;
    }

    public String getYanbian() 
    {
        return yanbian;
    }
    public void setCysy(String cysy) 
    {
        this.cysy = cysy;
    }

    public String getCysy() 
    {
        return cysy;
    }
    public void setCy(String cy) 
    {
        this.cy = cy;
    }

    public String getCy() 
    {
        return cy;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public String[] getPronunciationList() {
        return pronunciationList;
    }

    public void setPronunciationList(String[] pronunciationList) {
        this.pronunciationList = pronunciationList;
    }

    public String[] getPronunciationAudioSrcList() {
        return pronunciationAudioSrcList;
    }

    public void setPronunciationAudioSrcList(String[] pronunciationAudioSrcList) {
        this.pronunciationAudioSrcList = pronunciationAudioSrcList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("refid", getRefid())
            .append("character", getCharacter())
            .append("characterVideoSrc", getCharacterVideoSrc())
            .append("etymologyImageSrc", getEtymologyImageSrc())
            .append("pronunciation", getPronunciation())
            .append("pronunciationAudioSrc", getPronunciationAudioSrc())
            .append("evolutionImageSrc", getEvolutionImageSrc())
            .append("ytzi", getYtzi())
            .append("shiyi", getShiyi())
            .append("yanbian", getYanbian())
            .append("cysy", getCysy())
            .append("cy", getCy())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
                .append("pronunciationAudioSrcList", getPronunciationAudioSrcList())
            .toString();
    }
}
