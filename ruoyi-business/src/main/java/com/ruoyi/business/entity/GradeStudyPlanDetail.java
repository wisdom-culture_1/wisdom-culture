package com.ruoyi.business.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学习计划（细化到文章）对象 grade_study_plan_detail
 * 
 * @author ruoyi
 * @date 2024-06-01
 */
public class GradeStudyPlanDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 年级id */
    private Long gradeId;

    /** 学习计划模块 */
    @Excel(name = "学习计划模块")
    private Long module;

    /** 子模块 */
    @Excel(name = "子模块")
    private Long childModule;

    /** 细化文章id */
    @Excel(name = "细化文章id")
    private String detailId;

    private Integer pageNum;

    private Integer pageSize;

    public String detailName;

    public Integer getPageNum() {
		return pageNum;
	}

    public Integer getPageSize() {
		return pageSize;
	}

    public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

    public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

    public Long getGradeId() {
		return gradeId;
	}

    public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setModule(Long module) 
    {
        this.module = module;
    }

    public Long getModule() 
    {
        return module;
    }
    public void setChildModule(Long childModule) 
    {
        this.childModule = childModule;
    }

    public Long getChildModule() 
    {
        return childModule;
    }

    public String getDetailId() {
		return detailId;
	}

    public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

    public String getDetailName() {
		return detailName;
	}

    public void setDetailName(String detailName) {
		this.detailName = detailName;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
                .append("gradeId", getGradeId())
            .append("module", getModule())
            .append("childModule", getChildModule())
            .append("detailId", getDetailId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("pageNum", getPageNum())
                .append("pageSize", getPageSize())
                .append("detailName", getDetailName())
            .toString();
    }
}
