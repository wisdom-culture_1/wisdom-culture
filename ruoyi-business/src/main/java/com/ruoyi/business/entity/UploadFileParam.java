package com.ruoyi.business.entity;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 文言文对象 classical
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class UploadFileParam
{
    private static final long serialVersionUID = 1L;

    String audio_ms;

    public String getAudio_ms() {
        return audio_ms;
    }

    public void setAudio_ms(String audio_ms) {
        this.audio_ms = audio_ms;
    }

    @Override
    public String toString() {
        return "UploadFileParam{" +
                "audio_ms='" + audio_ms + '\'' +
                '}';
    }
}
