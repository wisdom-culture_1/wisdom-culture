package com.ruoyi.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 帮助中心对象 help_center
 * 
 * @author ruoyi
 * @date 2023-04-12
 */
public class AccountCenter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Integer article;

    /** 标题 */
    @Excel(name = "标题")
    private Integer character;

    /** 内容 */
    @Excel(name = "内容")
    private Integer idiom;

    public Integer getArticle() {
        return article;
    }

    public void setArticle(Integer article) {
        this.article = article;
    }

    public Integer getCharacter() {
        return character;
    }

    public void setCharacter(Integer character) {
        this.character = character;
    }

    public Integer getIdiom() {
        return idiom;
    }

    public void setIdiom(Integer idiom) {
        this.idiom = idiom;
    }

    @Override
    public String toString() {
        return "AccountCenter{" +
                "article=" + article +
                ", character=" + character +
                ", idiom=" + idiom +
                '}';
    }
}
