package com.ruoyi.business.entity;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 诗词歌赋对象 poetry_ode
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class PoetryOde extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 作者 */
    @Excel(name = "作者")
    private String author;

    /** 朝代 */
    @Excel(name = "朝代")
    private String dynasty;

    /** 正文 */
    @Excel(name = "正文")
    private String content;

    /** 译文内容 */
    @Excel(name = "译文内容")
    @JsonProperty("translation_content")
    @JSONField(name = "translation_content")
    private String translationContent;

    /** 译文图片 */
    @Excel(name = "译文图片")
    @JsonProperty("translation_image_src")
    @JSONField(name = "translation_image_src")
    private String translationImageSrc;

    /** 赏析内容 */
    @Excel(name = "赏析内容")
    @JsonProperty("appreciation_content")
    @JSONField(name = "appreciation_content")
    private String appreciationContent;

    /** 赏析图片 */
    @Excel(name = "赏析图片")
    @JsonProperty("appreciation_image_src")
    @JSONField(name = "appreciation_image_src")
    private String appreciationImageSrc;

    /** 扩展标题 */
    @Excel(name = "扩展标题")
    @JsonProperty("extend_title")
    @JSONField(name = "extend_title")
    private String extendTitle;

    /** 扩展内容 */
    @Excel(name = "扩展内容")
    @JsonProperty("extend_content")
    @JSONField(name = "extend_content")
    private String extendContent;

    /** 扩展图片 */
    @Excel(name = "扩展图片")
    @JsonProperty("extend_image_src")
    @JSONField(name = "extend_image_src")
    private String extendImageSrc;

    private List<PoetryOdeNotes> odenodes;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    /** 序号 */
    @Excel(name = "序号")
    private String xuhao;

    /** 音频 */
    @Excel(name = "音频")
    private String audioSrc;

    public List<PoetryOdeNotes> getOdenodes() {
        return odenodes;
    }

    public void setOdenodes(List<PoetryOdeNotes> odenodes) {
        this.odenodes = odenodes;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setDynasty(String dynasty) 
    {
        this.dynasty = dynasty;
    }

    public String getDynasty() 
    {
        return dynasty;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setTranslationContent(String translationContent) 
    {
        this.translationContent = translationContent;
    }

    public String getTranslationContent() 
    {
        return translationContent;
    }
    public void setTranslationImageSrc(String translationImageSrc) 
    {
        this.translationImageSrc = translationImageSrc;
    }

    public String getTranslationImageSrc() 
    {
        return translationImageSrc;
    }
    public void setAppreciationContent(String appreciationContent) 
    {
        this.appreciationContent = appreciationContent;
    }

    public String getAppreciationContent() 
    {
        return appreciationContent;
    }
    public void setAppreciationImageSrc(String appreciationImageSrc) 
    {
        this.appreciationImageSrc = appreciationImageSrc;
    }

    public String getAppreciationImageSrc() 
    {
        return appreciationImageSrc;
    }
    public void setExtendTitle(String extendTitle) 
    {
        this.extendTitle = extendTitle;
    }

    public String getExtendTitle() 
    {
        return extendTitle;
    }
    public void setExtendContent(String extendContent) 
    {
        this.extendContent = extendContent;
    }

    public String getExtendContent() 
    {
        return extendContent;
    }
    public void setExtendImageSrc(String extendImageSrc) 
    {
        this.extendImageSrc = extendImageSrc;
    }

    public String getExtendImageSrc() 
    {
        return extendImageSrc;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }
    public void setXuhao(String xuhao) 
    {
        this.xuhao = xuhao;
    }

    public String getXuhao() 
    {
        return xuhao;
    }
    public void setAudioSrc(String audioSrc) 
    {
        this.audioSrc = audioSrc;
    }

    public String getAudioSrc() 
    {
        return audioSrc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("author", getAuthor())
            .append("dynasty", getDynasty())
            .append("content", getContent())
            .append("translationContent", getTranslationContent())
            .append("translationImageSrc", getTranslationImageSrc())
            .append("appreciationContent", getAppreciationContent())
            .append("appreciationImageSrc", getAppreciationImageSrc())
            .append("extendTitle", getExtendTitle())
            .append("extendContent", getExtendContent())
            .append("extendImageSrc", getExtendImageSrc())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .append("xuhao", getXuhao())
            .append("audioSrc", getAudioSrc())
            .toString();
    }
}
