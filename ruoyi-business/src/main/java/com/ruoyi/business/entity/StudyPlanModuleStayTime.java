package com.ruoyi.business.entity;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 *
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class StudyPlanModuleStayTime
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /**
     * 用户id
     */
    private Long userId;

    /** 停留时长 ms*/
    private Integer stayTime;

    /** 学习计划模块 1:掌握拼音，2：认识汉字，3：阅读量，4、培养表达 */
    private Integer module;

    /** moule为1：child_module 1：字母表，2：声母表，3：韵母\r\n
     * moule为2：child_module 1：速斩生字，2：生词闯大关\r\n
     * moule为3：child_module 1：成语典故，2：人物外貌，3：人物类型，4：人物心理，5：人物心情\r\n
     * 6：人物品质，7：言辞表达，8：为人处世，9：描写景物，10：描写四季\r\n
     * 11：描写气候，12：十二生肖，13：其他动物 14：其他 15： 对比、比较\r\n
     * 16：社会生活 17、社会斗争 18教育学习 19 政治法律 20 军事经济 21 成语结构\r\n
     * 22： 近反义词 23：成语故事 24：语文大纲 25：四大名著 26：其他名著 27：出自名人\r\n
     * 28：其他和人有关\r\n
     * moule为4：child_module 1：国学启蒙，2：一年级背诵篇目，3：二年级背诵篇目，4：三年级背诵篇目，5：四年级背诵篇目	\r\n								   6：五年级背诵篇目，7：六年级背诵篇目，8：初一背诵篇目，9：初二背诵篇目，10：初三背诵篇目',
     */
    private Integer childModule;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getStayTime() {
        return stayTime;
    }

    public void setStayTime(Integer stayTime) {
        this.stayTime = stayTime;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getChildModule() {
        return childModule;
    }

    public void setChildModule(Integer childModule) {
        this.childModule = childModule;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "StudyPlanModuleStayTime{" +
                "id=" + id +
                ", userId=" + userId +
                ", stayTime=" + stayTime +
                ", module=" + module +
                ", childModule=" + childModule +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
