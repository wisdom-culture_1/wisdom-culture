package com.ruoyi.business.entity;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 成语汇总对象 idiom_summary
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class CharacterSummaryRsp
{
    private static final long serialVersionUID = 1L;

    private Long already = Long.valueOf(0);

    private Long incapable = Long.valueOf(0);

    public Integer isShow;

    public Long getAlready() {
        return already;
    }

    public void setAlready(Long already) {
        this.already = already;
    }

    public Long getIncapable() {
        return incapable;
    }

    public void setIncapable(Long incapable) {
        this.incapable = incapable;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    @Override
    public String toString() {
        return "IdiomSummaryRsp{" +
                "already=" + already +
                ", incapable=" + incapable +
                ", isShow=" + isShow +
                '}';
    }
}
