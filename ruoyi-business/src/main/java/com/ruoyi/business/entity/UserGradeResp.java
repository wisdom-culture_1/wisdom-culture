package com.ruoyi.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 年级对象 grade
 * 
 * @author ruoyi
 * @date 2023-04-12
 */
public class UserGradeResp
{

    private List<Grade> grade;

    private Long user_grade;

    public List<Grade> getGrade() {
        return grade;
    }

    public void setGrade(List<Grade> grade) {
        this.grade = grade;
    }

    public Long getUser_grade() {
        return user_grade;
    }

    public void setUser_grade(Long user_grade) {
        this.user_grade = user_grade;
    }

    @Override
    public String toString() {
        return "UserGradeResp{" +
                "grade=" + grade +
                ", user_grade=" + user_grade +
                '}';
    }
}
