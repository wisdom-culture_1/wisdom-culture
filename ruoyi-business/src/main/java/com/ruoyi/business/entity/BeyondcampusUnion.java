package com.ruoyi.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 【请填写功能名称】对象 beyondcampus
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class BeyondcampusUnion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private List<Beyondcampus> list;

    private List<BeyondcampusUser> module;

    public List<Beyondcampus> getList() {
        return list;
    }

    public void setList(List<Beyondcampus> list) {
        this.list = list;
    }

    public List<BeyondcampusUser> getModule() {
        return module;
    }

    public void setModule(List<BeyondcampusUser> module) {
        this.module = module;
    }
}
