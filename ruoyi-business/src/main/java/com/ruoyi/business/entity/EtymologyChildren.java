package com.ruoyi.business.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 字源示例对象 etymology_children
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class EtymologyChildren extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字源id */
    @Excel(name = "字源id")
    private Long etymologyId;

    /** 例字 */
    @Excel(name = "例字")
    private String exampleWord;

    /** 演变图 */
    @Excel(name = "演变图")
    private String evolutionImageSrc;

    /** 解释 */
    @Excel(name = "解释")
    private String explain;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date updatedAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String ziyuan;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEtymologyId(Long etymologyId) 
    {
        this.etymologyId = etymologyId;
    }

    public Long getEtymologyId() 
    {
        return etymologyId;
    }
    public void setExampleWord(String exampleWord) 
    {
        this.exampleWord = exampleWord;
    }

    public String getExampleWord() 
    {
        return exampleWord;
    }
    public void setEvolutionImageSrc(String evolutionImageSrc) 
    {
        this.evolutionImageSrc = evolutionImageSrc;
    }

    public String getEvolutionImageSrc() 
    {
        return evolutionImageSrc;
    }
    public void setExplain(String explain) 
    {
        this.explain = explain;
    }

    public String getExplain() 
    {
        return explain;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }
    public void setZiyuan(String ziyuan) 
    {
        this.ziyuan = ziyuan;
    }

    public String getZiyuan() 
    {
        return ziyuan;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("etymologyId", getEtymologyId())
            .append("exampleWord", getExampleWord())
            .append("evolutionImageSrc", getEvolutionImageSrc())
            .append("explain", getExplain())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .append("ziyuan", getZiyuan())
            .toString();
    }
}
