package com.ruoyi.business.entity;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 成语汇总对象 idiom_summary
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class IdiomSummaryRsp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long already;

    private Long incapable;

    public Long getAlready() {
        return already;
    }

    public void setAlready(Long already) {
        this.already = already;
    }

    public Long getIncapable() {
        return incapable;
    }

    public void setIncapable(Long incapable) {
        this.incapable = incapable;
    }

    @Override
    public String toString() {
        return "IdiomSummaryRsp{" +
                "already=" + already +
                ", incapable=" + incapable +
                '}';
    }
}
