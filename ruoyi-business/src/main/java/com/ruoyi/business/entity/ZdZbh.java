package com.ruoyi.business.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 zd_zbh
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class ZdZbh extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long xuhao;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String zi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long bihua;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long begin;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long end;

    public void setXuhao(Long xuhao) 
    {
        this.xuhao = xuhao;
    }

    public Long getXuhao() 
    {
        return xuhao;
    }
    public void setZi(String zi) 
    {
        this.zi = zi;
    }

    public String getZi() 
    {
        return zi;
    }
    public void setBihua(Long bihua) 
    {
        this.bihua = bihua;
    }

    public Long getBihua() 
    {
        return bihua;
    }
    public void setBegin(Long begin) 
    {
        this.begin = begin;
    }

    public Long getBegin() 
    {
        return begin;
    }
    public void setEnd(Long end) 
    {
        this.end = end;
    }

    public Long getEnd() 
    {
        return end;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("xuhao", getXuhao())
            .append("zi", getZi())
            .append("bihua", getBihua())
            .append("begin", getBegin())
            .append("end", getEnd())
            .toString();
    }
}
