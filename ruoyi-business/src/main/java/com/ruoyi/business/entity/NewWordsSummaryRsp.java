package com.ruoyi.business.entity;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 成语汇总对象 idiom_summary
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class NewWordsSummaryRsp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long already_count;

    private Long characters_count;

    private Map<String, CharacterSummaryRsp> list;

    public Long getAlready_count() {
        return already_count;
    }

    public void setAlready_count(Long already_count) {
        this.already_count = already_count;
    }

    public Long getCharacters_count() {
        return characters_count;
    }

    public void setCharacters_count(Long characters_count) {
        this.characters_count = characters_count;
    }

    public Map<String, CharacterSummaryRsp> getList() {
        return list;
    }

    public void setList(Map<String, CharacterSummaryRsp> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "NewWordsSummaryRsp{" +
                "already_count=" + already_count +
                ", characters_count=" + characters_count +
                ", list=" + list +
                '}';
    }
}
