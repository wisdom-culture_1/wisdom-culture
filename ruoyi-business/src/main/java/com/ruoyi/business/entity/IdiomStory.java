package com.ruoyi.business.entity;

import java.util.Date;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 成语典故对象 idiom_story
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
public class IdiomStory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 字典分类 */
    @Excel(name = "字典分类")
    @JSONField(name = "key")
    private String dictionaryChildrenKey;

    /** 成语名称 */
    @Excel(name = "成语名称")
    private String idiom;

    private String q;

    /** 成语拼音 */
    @Excel(name = "成语拼音")
    @JsonProperty("idiom_pinyin")
    @JSONField(name = "idiom_pinyin")
    private String idiomPinyin;

    /** 成语视频 */
    @Excel(name = "成语视频")
    @JsonProperty("idiom_video_src")
    @JSONField(name = "idiom_video_src")
    private String idiomVideoSrc;

    /** 成语图片 */
    @Excel(name = "成语图片")
    @JsonProperty("idiom_image_src")
    @JSONField(name = "idiom_image_src")
    private String idiomImageSrc;

    /** 成语音频 */
    @Excel(name = "成语音频")
    @JsonProperty("idiom_audio_src")
    @JSONField(name = "idiom_audio_src")
    private String idiomAudioSrc;

    /** 成语释义 */
    @Excel(name = "成语释义")
    private String interpretation;

    /** 成语出处 */
    @Excel(name = "成语出处")
    private String source;

    /** 成语示例 */
    @Excel(name = "成语示例")
    private String examples;

    /** 近义词 */
    @Excel(name = "近义词")
    private String similar;

    /** 反义词 */
    @Excel(name = "反义词")
    private String antonym;

    /** 成语难度级别 */
    @Excel(name = "成语难度级别")
    private Long level;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    private String status;

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDictionaryChildrenKey(String dictionaryChildrenKey) 
    {
        this.dictionaryChildrenKey = dictionaryChildrenKey;
    }

    public String getDictionaryChildrenKey() 
    {
        return dictionaryChildrenKey;
    }
    public void setIdiom(String idiom) 
    {
        this.idiom = idiom;
    }

    public String getIdiom() 
    {
        return idiom;
    }
    public void setIdiomPinyin(String idiomPinyin) 
    {
        this.idiomPinyin = idiomPinyin;
    }

    public String getIdiomPinyin() 
    {
        return idiomPinyin;
    }
    public void setIdiomVideoSrc(String idiomVideoSrc) 
    {
        this.idiomVideoSrc = idiomVideoSrc;
    }

    public String getIdiomVideoSrc() 
    {
        return idiomVideoSrc;
    }
    public void setIdiomImageSrc(String idiomImageSrc) 
    {
        this.idiomImageSrc = idiomImageSrc;
    }

    public String getIdiomImageSrc() 
    {
        return idiomImageSrc;
    }
    public void setIdiomAudioSrc(String idiomAudioSrc) 
    {
        this.idiomAudioSrc = idiomAudioSrc;
    }

    public String getIdiomAudioSrc() 
    {
        return idiomAudioSrc;
    }
    public void setInterpretation(String interpretation) 
    {
        this.interpretation = interpretation;
    }

    public String getInterpretation() 
    {
        return interpretation;
    }
    public void setSource(String source) 
    {
        this.source = source;
    }

    public String getSource() 
    {
        return source;
    }
    public void setExamples(String examples) 
    {
        this.examples = examples;
    }

    public String getExamples() 
    {
        return examples;
    }
    public void setSimilar(String similar) 
    {
        this.similar = similar;
    }

    public String getSimilar() 
    {
        return similar;
    }
    public void setAntonym(String antonym) 
    {
        this.antonym = antonym;
    }

    public String getAntonym() 
    {
        return antonym;
    }
    public void setLevel(Long level) 
    {
        this.level = level;
    }

    public Long getLevel() 
    {
        return level;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "IdiomStory{" +
                "id=" + id +
                ", dictionaryChildrenKey='" + dictionaryChildrenKey + '\'' +
                ", idiom='" + idiom + '\'' +
                ", q='" + q + '\'' +
                ", idiomPinyin='" + idiomPinyin + '\'' +
                ", idiomVideoSrc='" + idiomVideoSrc + '\'' +
                ", idiomImageSrc='" + idiomImageSrc + '\'' +
                ", idiomAudioSrc='" + idiomAudioSrc + '\'' +
                ", interpretation='" + interpretation + '\'' +
                ", source='" + source + '\'' +
                ", examples='" + examples + '\'' +
                ", similar='" + similar + '\'' +
                ", antonym='" + antonym + '\'' +
                ", level=" + level +
                ", sort=" + sort +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", status='" + status + '\'' +
                '}';
    }
}
