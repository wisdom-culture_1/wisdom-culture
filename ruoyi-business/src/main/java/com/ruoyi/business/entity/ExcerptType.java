package com.ruoyi.business.entity;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 金句摘抄分类对象 excerpt_type
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
public class ExcerptType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 摘要分类标识 */
    private Long id;

    /** 父摘要分类标识 */
    @Excel(name = "父摘要分类标识")
    private Long pid;

    /** 摘要分类名称 */
    @Excel(name = "摘要分类名称")
    @JsonProperty("type_title")
    @JSONField(name = "type_title")
    private String typeTitle;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    private List<ExcerptType> child;

    /** 新增时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "新增时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public List<ExcerptType> getChild() {
        return child;
    }

    public void setChild(List<ExcerptType> child) {
        this.child = child;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setTypeTitle(String typeTitle) 
    {
        this.typeTitle = typeTitle;
    }

    public String getTypeTitle() 
    {
        return typeTitle;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pid", getPid())
            .append("typeTitle", getTypeTitle())
            .append("sort", getSort())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
