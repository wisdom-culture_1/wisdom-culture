package com.ruoyi.business.entity;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 学习计划（细化到文章）对象 grade_study_plan_detail
 * 
 * @author ruoyi
 * @date 2024-06-01
 */
public class GradeStudyPlanDetailReq extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 年级id */
    private Long gradeId;

    /** 学习计划模块 */
    @Excel(name = "学习计划模块")
    private Long module;

    /** 子模块 */
    @Excel(name = "子模块")
    private Long childModule;

    /** 细化文章id */
    @Excel(name = "细化文章id")
    private List<String> detailIds;

    public Long getGradeId() {
		return gradeId;
	}

    public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setModule(Long module) 
    {
        this.module = module;
    }

    public Long getModule() 
    {
        return module;
    }
    public void setChildModule(Long childModule) 
    {
        this.childModule = childModule;
    }

    public Long getChildModule() 
    {
        return childModule;
    }

//    public List<Long> getDetailIds() {
//		return detailIds;
//	}
//
//    public void setDetailIds(List<Long> detailIds) {
//		this.detailIds = detailIds;
//	}

    public List<String> getDetailIds() {
		return detailIds;
	}

    public void setDetailIds(List<String> detailIds) {
		this.detailIds = detailIds;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
                .append("gradeId", getGradeId())
            .append("module", getModule())
            .append("childModule", getChildModule())
                .append("detailIds", getDetailIds())
            .toString();
    }
}
