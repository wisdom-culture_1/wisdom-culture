package com.ruoyi.common.core.domain.entity;

import com.ruoyi.common.annotation.Excel;

/**
 * 用户对象 sys_user
 * 
 * @author ruoyi
 */
public class ChangePasswdReq
{
    private static final long serialVersionUID = 1L;

    /** 用户账号 */
    @Excel(name = "登录名称")
    private String old_password;

    /** 用户昵称 */
    @Excel(name = "用户名称")
    private String password;

    /** 用户昵称 */
    @Excel(name = "用户名称")
    private String password_confirmation;

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    @Override
    public String toString() {
        return "ChangePasswdReq{" +
                "old_password='" + old_password + '\'' +
                ", password='" + password + '\'' +
                ", password_confirmation='" + password_confirmation + '\'' +
                '}';
    }
}
