import request from '@/utils/request'

// 查询子字典表列表
export function listDictionaryChildren(query) {
  return request({
    url: '/system/dictionaryChildren/list',
    method: 'get',
    params: query
  })
}

// 查询子字典表详细
export function getDictionaryChildren(id) {
  return request({
    url: '/system/dictionaryChildren/' + id,
    method: 'get'
  })
}

// 新增子字典表
export function addDictionaryChildren(data) {
  return request({
    url: '/system/dictionaryChildren',
    method: 'post',
    data: data
  })
}

// 修改子字典表
export function updateDictionaryChildren(data) {
  return request({
    url: '/system/dictionaryChildren',
    method: 'put',
    data: data
  })
}

// 删除子字典表
export function delDictionaryChildren(id) {
  return request({
    url: '/system/dictionaryChildren/' + id,
    method: 'delete'
  })
}
