import request from '@/utils/request'

// 查询中华文化书籍列表
export function listCulture_book(query) {
  return request({
    url: '/system/culture_book/list',
    method: 'get',
    params: query
  })
}

// 查询中华文化书籍详细
export function getCulture_book(id) {
  return request({
    url: '/system/culture_book/' + id,
    method: 'get'
  })
}

// 新增中华文化书籍
export function addCulture_book(data) {
  return request({
    url: '/system/culture_book',
    method: 'post',
    data: data
  })
}

// 修改中华文化书籍
export function updateCulture_book(data) {
  return request({
    url: '/system/culture_book',
    method: 'put',
    data: data
  })
}

// 删除中华文化书籍
export function delCulture_book(id) {
  return request({
    url: '/system/culture_book/' + id,
    method: 'delete'
  })
}
