import request from '@/utils/request'

// 查询诗词磨耳朵列表
export function listListen_book(query) {
  return request({
    url: '/system/listen_book/list',
    method: 'get',
    params: query
  })
}

// 查询诗词磨耳朵详细
export function getListen_book(id) {
  return request({
    url: '/system/listen_book/' + id,
    method: 'get'
  })
}

// 新增诗词磨耳朵
export function addListen_book(data) {
  return request({
    url: '/system/listen_book',
    method: 'post',
    data: data
  })
}

// 修改诗词磨耳朵
export function updateListen_book(data) {
  return request({
    url: '/system/listen_book',
    method: 'put',
    data: data
  })
}

// 删除诗词磨耳朵
export function delListen_book(id) {
  return request({
    url: '/system/listen_book/' + id,
    method: 'delete'
  })
}
