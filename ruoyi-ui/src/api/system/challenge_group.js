import request from '@/utils/request'

// 查询挑战组别列表
export function listChallenge_group(query) {
  return request({
    url: '/system/challenge_group/list',
    method: 'get',
    params: query
  })
}

// 查询挑战组别详细
export function getChallenge_group(id) {
  return request({
    url: '/system/challenge_group/' + id,
    method: 'get'
  })
}

// 新增挑战组别
export function addChallenge_group(data) {
  return request({
    url: '/system/challenge_group',
    method: 'post',
    data: data
  })
}

// 修改挑战组别
export function updateChallenge_group(data) {
  return request({
    url: '/system/challenge_group',
    method: 'put',
    data: data
  })
}

// 删除挑战组别
export function delChallenge_group(id) {
  return request({
    url: '/system/challenge_group/' + id,
    method: 'delete'
  })
}
