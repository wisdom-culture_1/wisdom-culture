import request from '@/utils/request'

// 查询背诵打卡列表
export function listRecite(query) {
  return request({
    url: '/system/recite/list',
    method: 'get',
    params: query
  })
}

// 查询背诵打卡详细
export function getRecite(id) {
  return request({
    url: '/system/recite/' + id,
    method: 'get'
  })
}

// 新增背诵打卡
export function addRecite(data) {
  return request({
    url: '/system/recite',
    method: 'post',
    data: data
  })
}

// 修改背诵打卡
export function updateRecite(data) {
  return request({
    url: '/system/recite',
    method: 'put',
    data: data
  })
}

// 删除背诵打卡
export function delRecite(id) {
  return request({
    url: '/system/recite/' + id,
    method: 'delete'
  })
}
