import request from '@/utils/request'

// 查询名著书籍列表
export function listMasterpieces_book(query) {
  return request({
    url: '/system/masterpieces_book/list',
    method: 'get',
    params: query
  })
}

// 查询名著书籍详细
export function getMasterpieces_book(id) {
  return request({
    url: '/system/masterpieces_book/' + id,
    method: 'get'
  })
}

// 新增名著书籍
export function addMasterpieces_book(data) {
  return request({
    url: '/system/masterpieces_book',
    method: 'post',
    data: data
  })
}

// 修改名著书籍
export function updateMasterpieces_book(data) {
  return request({
    url: '/system/masterpieces_book',
    method: 'put',
    data: data
  })
}

// 删除名著书籍
export function delMasterpieces_book(id) {
  return request({
    url: '/system/masterpieces_book/' + id,
    method: 'delete'
  })
}
