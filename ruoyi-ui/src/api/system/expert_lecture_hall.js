import request from '@/utils/request'

// 查询专家讲堂视频列表
export function listExpert_lecture_hall(query) {
  return request({
    url: '/system/expert_lecture_hall/list',
    method: 'get',
    params: query
  })
}

// 查询专家讲堂视频详细
export function getExpert_lecture_hall(id) {
  return request({
    url: '/system/expert_lecture_hall/' + id,
    method: 'get'
  })
}

// 新增专家讲堂视频
export function addExpert_lecture_hall(data) {
  return request({
    url: '/system/expert_lecture_hall',
    method: 'post',
    data: data
  })
}

// 修改专家讲堂视频
export function updateExpert_lecture_hall(data) {
  return request({
    url: '/system/expert_lecture_hall',
    method: 'put',
    data: data
  })
}

// 删除专家讲堂视频
export function delExpert_lecture_hall(id) {
  return request({
    url: '/system/expert_lecture_hall/' + id,
    method: 'delete'
  })
}
