import request from '@/utils/request'

// 查询成语典故列表
export function listStory(query) {
  return request({
    url: '/system/story/list',
    method: 'get',
    params: query
  })
}

// 查询成语典故详细
export function getStory(id) {
  return request({
    url: '/system/story/' + id,
    method: 'get'
  })
}

// 新增成语典故
export function addStory(data) {
  return request({
    url: '/system/story',
    method: 'post',
    data: data
  })
}

// 修改成语典故
export function updateStory(data) {
  return request({
    url: '/system/story',
    method: 'put',
    data: data
  })
}

// 删除成语典故
export function delStory(id) {
  return request({
    url: '/system/story/' + id,
    method: 'delete'
  })
}
