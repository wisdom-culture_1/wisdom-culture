import request from '@/utils/request'

// 查询诗词歌赋列表
export function listOde(query) {
  return request({
    url: '/system/ode/list',
    method: 'get',
    params: query
  })
}

// 查询诗词歌赋详细
export function getOde(id) {
  return request({
    url: '/system/ode/' + id,
    method: 'get'
  })
}

// 新增诗词歌赋
export function addOde(data) {
  return request({
    url: '/system/ode',
    method: 'post',
    data: data
  })
}

// 修改诗词歌赋
export function updateOde(data) {
  return request({
    url: '/system/ode',
    method: 'put',
    data: data
  })
}

// 删除诗词歌赋
export function delOde(id) {
  return request({
    url: '/system/ode/' + id,
    method: 'delete'
  })
}
