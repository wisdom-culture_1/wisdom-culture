import request from '@/utils/request'

// 查询名著章节列表
export function listMasterpieces_chapters(query) {
  return request({
    url: '/system/masterpieces_chapters/list',
    method: 'get',
    params: query
  })
}

// 查询名著章节详细
export function getMasterpieces_chapters(id) {
  return request({
    url: '/system/masterpieces_chapters/' + id,
    method: 'get'
  })
}

// 新增名著章节
export function addMasterpieces_chapters(data) {
  return request({
    url: '/system/masterpieces_chapters',
    method: 'post',
    data: data
  })
}

// 修改名著章节
export function updateMasterpieces_chapters(data) {
  return request({
    url: '/system/masterpieces_chapters',
    method: 'put',
    data: data
  })
}

// 删除名著章节
export function delMasterpieces_chapters(id) {
  return request({
    url: '/system/masterpieces_chapters/' + id,
    method: 'delete'
  })
}
