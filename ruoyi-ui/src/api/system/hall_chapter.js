import request from '@/utils/request'

// 查询专家讲堂章节列表
export function listHall_chapter(query) {
  return request({
    url: '/system/hall_chapter/list',
    method: 'get',
    params: query
  })
}

// 查询专家讲堂章节详细
export function getHall_chapter(id) {
  return request({
    url: '/system/hall_chapter/' + id,
    method: 'get'
  })
}

// 新增专家讲堂章节
export function addHall_chapter(data) {
  return request({
    url: '/system/hall_chapter',
    method: 'post',
    data: data
  })
}

// 修改专家讲堂章节
export function updateHall_chapter(data) {
  return request({
    url: '/system/hall_chapter',
    method: 'put',
    data: data
  })
}

// 删除专家讲堂章节
export function delHall_chapter(id) {
  return request({
    url: '/system/hall_chapter/' + id,
    method: 'delete'
  })
}
