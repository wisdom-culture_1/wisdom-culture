import request from '@/utils/request'

// 查询文言文注释列表
export function listNotes(query) {
  return request({
    url: '/system/notes/list',
    method: 'get',
    params: query
  })
}

// 查询文言文注释详细
export function getNotes(id) {
  return request({
    url: '/system/notes/' + id,
    method: 'get'
  })
}

// 新增文言文注释
export function addNotes(data) {
  return request({
    url: '/system/notes',
    method: 'post',
    data: data
  })
}

// 修改文言文注释
export function updateNotes(data) {
  return request({
    url: '/system/notes',
    method: 'put',
    data: data
  })
}

// 删除文言文注释
export function delNotes(id) {
  return request({
    url: '/system/notes/' + id,
    method: 'delete'
  })
}
