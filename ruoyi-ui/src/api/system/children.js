import request from '@/utils/request'

// 查询字源示例列表
export function listChildren(query) {
  return request({
    url: '/system/children/list',
    method: 'get',
    params: query
  })
}

// 查询字源示例详细
export function getChildren(id) {
  return request({
    url: '/system/children/' + id,
    method: 'get'
  })
}

// 新增字源示例
export function addChildren(data) {
  return request({
    url: '/system/children',
    method: 'post',
    data: data
  })
}

// 修改字源示例
export function updateChildren(data) {
  return request({
    url: '/system/children',
    method: 'put',
    data: data
  })
}

// 删除字源示例
export function delChildren(id) {
  return request({
    url: '/system/children/' + id,
    method: 'delete'
  })
}
