import request from '@/utils/request'

// 查询文言名篇扩展列表
export function listClassical_extend(query) {
  return request({
    url: '/system/classical_extend/list',
    method: 'get',
    params: query
  })
}

// 查询文言名篇扩展详细
export function getClassical_extend(id) {
  return request({
    url: '/system/classical_extend/' + id,
    method: 'get'
  })
}

// 新增文言名篇扩展
export function addClassical_extend(data) {
  return request({
    url: '/system/classical_extend',
    method: 'post',
    data: data
  })
}

// 修改文言名篇扩展
export function updateClassical_extend(data) {
  return request({
    url: '/system/classical_extend',
    method: 'put',
    data: data
  })
}

// 删除文言名篇扩展
export function delClassical_extend(id) {
  return request({
    url: '/system/classical_extend/' + id,
    method: 'delete'
  })
}
