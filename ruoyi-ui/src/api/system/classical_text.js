import request from '@/utils/request'

// 查询文言文正文列表
export function listClassical_text(query) {
  return request({
    url: '/system/classical_text/list',
    method: 'get',
    params: query
  })
}

// 查询文言文正文详细
export function getClassical_text(id) {
  return request({
    url: '/system/classical_text/' + id,
    method: 'get'
  })
}

// 新增文言文正文
export function addClassical_text(data) {
  return request({
    url: '/system/classical_text',
    method: 'post',
    data: data
  })
}

// 修改文言文正文
export function updateClassical_text(data) {
  return request({
    url: '/system/classical_text',
    method: 'put',
    data: data
  })
}

// 删除文言文正文
export function delClassical_text(id) {
  return request({
    url: '/system/classical_text/' + id,
    method: 'delete'
  })
}
