import request from '@/utils/request'

// 查询成语汇总列表
export function listSummary(query) {
  return request({
    url: '/system/summary/list',
    method: 'get',
    params: query
  })
}

// 查询成语汇总详细
export function getSummary(id) {
  return request({
    url: '/system/summary/' + id,
    method: 'get'
  })
}

// 新增成语汇总
export function addSummary(data) {
  return request({
    url: '/system/summary',
    method: 'post',
    data: data
  })
}

// 修改成语汇总
export function updateSummary(data) {
  return request({
    url: '/system/summary',
    method: 'put',
    data: data
  })
}

// 删除成语汇总
export function delSummary(id) {
  return request({
    url: '/system/summary/' + id,
    method: 'delete'
  })
}
