import request from '@/utils/request'

// 查询智慧元典书籍列表
export function listBook(query) {
  return request({
    url: '/system/book/list',
    method: 'get',
    params: query
  })
}

// 查询智慧元典书籍详细
export function getBook(id) {
  return request({
    url: '/system/book/' + id,
    method: 'get'
  })
}

// 新增智慧元典书籍
export function addBook(data) {
  return request({
    url: '/system/book',
    method: 'post',
    data: data
  })
}

// 修改智慧元典书籍
export function updateBook(data) {
  return request({
    url: '/system/book',
    method: 'put',
    data: data
  })
}

// 删除智慧元典书籍
export function delBook(id) {
  return request({
    url: '/system/book/' + id,
    method: 'delete'
  })
}
