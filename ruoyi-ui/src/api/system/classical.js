import request from '@/utils/request'

// 查询文言文列表
export function listClassical(query) {
  return request({
    url: '/system/classical/list',
    method: 'get',
    params: query
  })
}

// 查询文言文详细
export function getClassical(id) {
  return request({
    url: '/system/classical/' + id,
    method: 'get'
  })
}

// 新增文言文
export function addClassical(data) {
  return request({
    url: '/system/classical',
    method: 'post',
    data: data
  })
}

// 修改文言文
export function updateClassical(data) {
  return request({
    url: '/system/classical',
    method: 'put',
    data: data
  })
}

// 删除文言文
export function delClassical(id) {
  return request({
    url: '/system/classical/' + id,
    method: 'delete'
  })
}
