import request from '@/utils/request'

// 查询金句摘抄评论列表
export function listExcerpt_comment(query) {
  return request({
    url: '/system/excerpt_comment/list',
    method: 'get',
    params: query
  })
}

// 查询金句摘抄评论详细
export function getExcerpt_comment(id) {
  return request({
    url: '/system/excerpt_comment/' + id,
    method: 'get'
  })
}

// 新增金句摘抄评论
export function addExcerpt_comment(data) {
  return request({
    url: '/system/excerpt_comment',
    method: 'post',
    data: data
  })
}

// 修改金句摘抄评论
export function updateExcerpt_comment(data) {
  return request({
    url: '/system/excerpt_comment',
    method: 'put',
    data: data
  })
}

// 删除金句摘抄评论
export function delExcerpt_comment(id) {
  return request({
    url: '/system/excerpt_comment/' + id,
    method: 'delete'
  })
}
