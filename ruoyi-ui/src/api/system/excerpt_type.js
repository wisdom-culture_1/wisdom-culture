import request from '@/utils/request'

// 查询金句摘抄分类列表
export function listExcerpt_type(query) {
  return request({
    url: '/system/excerpt_type/list',
    method: 'get',
    params: query
  })
}

// 查询金句摘抄分类详细
export function getExcerpt_type(id) {
  return request({
    url: '/system/excerpt_type/' + id,
    method: 'get'
  })
}

// 新增金句摘抄分类
export function addExcerpt_type(data) {
  return request({
    url: '/system/excerpt_type',
    method: 'post',
    data: data
  })
}

// 修改金句摘抄分类
export function updateExcerpt_type(data) {
  return request({
    url: '/system/excerpt_type',
    method: 'put',
    data: data
  })
}

// 删除金句摘抄分类
export function delExcerpt_type(id) {
  return request({
    url: '/system/excerpt_type/' + id,
    method: 'delete'
  })
}
