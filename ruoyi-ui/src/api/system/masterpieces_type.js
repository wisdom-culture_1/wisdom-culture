import request from '@/utils/request'

// 查询名著阅读分类列表
export function listMasterpieces_type(query) {
  return request({
    url: '/system/masterpieces_type/list',
    method: 'get',
    params: query
  })
}

// 查询名著阅读分类详细
export function getMasterpieces_type(id) {
  return request({
    url: '/system/masterpieces_type/' + id,
    method: 'get'
  })
}

// 新增名著阅读分类
export function addMasterpieces_type(data) {
  return request({
    url: '/system/masterpieces_type',
    method: 'post',
    data: data
  })
}

// 修改名著阅读分类
export function updateMasterpieces_type(data) {
  return request({
    url: '/system/masterpieces_type',
    method: 'put',
    data: data
  })
}

// 删除名著阅读分类
export function delMasterpieces_type(id) {
  return request({
    url: '/system/masterpieces_type/' + id,
    method: 'delete'
  })
}
