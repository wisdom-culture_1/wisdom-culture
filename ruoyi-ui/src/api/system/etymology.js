import request from '@/utils/request'

// 查询字源列表
export function listEtymology(query) {
  return request({
    url: '/system/etymology/list',
    method: 'get',
    params: query
  })
}

// 查询字源详细
export function getEtymology(id) {
  return request({
    url: '/system/etymology/' + id,
    method: 'get'
  })
}

// 新增字源
export function addEtymology(data) {
  return request({
    url: '/system/etymology',
    method: 'post',
    data: data
  })
}

// 修改字源
export function updateEtymology(data) {
  return request({
    url: '/system/etymology',
    method: 'put',
    data: data
  })
}

// 删除字源
export function delEtymology(id) {
  return request({
    url: '/system/etymology/' + id,
    method: 'delete'
  })
}
