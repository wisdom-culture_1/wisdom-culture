import request from '@/utils/request'

// 查询声母 韵母列表
export function listConsonant(query) {
  return request({
    url: '/system/consonant/list',
    method: 'get',
    params: query
  })
}

// 查询声母 韵母详细
export function getConsonant(id) {
  return request({
    url: '/system/consonant/' + id,
    method: 'get'
  })
}

// 新增声母 韵母
export function addConsonant(data) {
  return request({
    url: '/system/consonant',
    method: 'post',
    data: data
  })
}

// 修改声母 韵母
export function updateConsonant(data) {
  return request({
    url: '/system/consonant',
    method: 'put',
    data: data
  })
}

// 删除声母 韵母
export function delConsonant(id) {
  return request({
    url: '/system/consonant/' + id,
    method: 'delete'
  })
}
