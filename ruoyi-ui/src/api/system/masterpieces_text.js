import request from '@/utils/request'

// 查询名著章节内容列表
export function listMasterpieces_text(query) {
  return request({
    url: '/system/masterpieces_text/list',
    method: 'get',
    params: query
  })
}

// 查询名著章节内容详细
export function getMasterpieces_text(id) {
  return request({
    url: '/system/masterpieces_text/' + id,
    method: 'get'
  })
}

// 新增名著章节内容
export function addMasterpieces_text(data) {
  return request({
    url: '/system/masterpieces_text',
    method: 'post',
    data: data
  })
}

// 修改名著章节内容
export function updateMasterpieces_text(data) {
  return request({
    url: '/system/masterpieces_text',
    method: 'put',
    data: data
  })
}

// 删除名著章节内容
export function delMasterpieces_text(id) {
  return request({
    url: '/system/masterpieces_text/' + id,
    method: 'delete'
  })
}
