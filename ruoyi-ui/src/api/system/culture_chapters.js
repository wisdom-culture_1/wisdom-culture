import request from '@/utils/request'

// 查询文化章节列表
export function listCulture_chapters(query) {
  return request({
    url: '/system/culture_chapters/list',
    method: 'get',
    params: query
  })
}

// 查询文化章节详细
export function getCulture_chapters(id) {
  return request({
    url: '/system/culture_chapters/' + id,
    method: 'get'
  })
}

// 新增文化章节
export function addCulture_chapters(data) {
  return request({
    url: '/system/culture_chapters',
    method: 'post',
    data: data
  })
}

// 修改文化章节
export function updateCulture_chapters(data) {
  return request({
    url: '/system/culture_chapters',
    method: 'put',
    data: data
  })
}

// 删除文化章节
export function delCulture_chapters(id) {
  return request({
    url: '/system/culture_chapters/' + id,
    method: 'delete'
  })
}
