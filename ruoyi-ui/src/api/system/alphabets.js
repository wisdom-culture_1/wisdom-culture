import request from '@/utils/request'

// 查询字母列表
export function listAlphabets(query) {
  return request({
    url: '/system/alphabets/list',
    method: 'get',
    params: query
  })
}

// 查询字母详细
export function getAlphabets(id) {
  return request({
    url: '/system/alphabets/' + id,
    method: 'get'
  })
}

// 新增字母
export function addAlphabets(data) {
  return request({
    url: '/system/alphabets',
    method: 'post',
    data: data
  })
}

// 修改字母
export function updateAlphabets(data) {
  return request({
    url: '/system/alphabets',
    method: 'put',
    data: data
  })
}

// 删除字母
export function delAlphabets(id) {
  return request({
    url: '/system/alphabets/' + id,
    method: 'delete'
  })
}
