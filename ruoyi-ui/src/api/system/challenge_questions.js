import request from '@/utils/request'

// 查询其他挑战（成语诗词....）列表
export function listChallenge_questions(query) {
  return request({
    url: '/system/challenge_questions/list',
    method: 'get',
    params: query
  })
}

// 查询其他挑战（成语诗词....）详细
export function getChallenge_questions(id) {
  return request({
    url: '/system/challenge_questions/' + id,
    method: 'get'
  })
}

// 新增其他挑战（成语诗词....）
export function addChallenge_questions(data) {
  return request({
    url: '/system/challenge_questions',
    method: 'post',
    data: data
  })
}

// 修改其他挑战（成语诗词....）
export function updateChallenge_questions(data) {
  return request({
    url: '/system/challenge_questions',
    method: 'put',
    data: data
  })
}

// 删除其他挑战（成语诗词....）
export function delChallenge_questions(id) {
  return request({
    url: '/system/challenge_questions/' + id,
    method: 'delete'
  })
}
