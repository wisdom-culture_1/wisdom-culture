import request from '@/utils/request'

// 查询汉字起源列表
export function listCharacters(query) {
  return request({
    url: '/system/characters/list',
    method: 'get',
    params: query
  })
}

// 查询汉字起源详细
export function getCharacters(id) {
  return request({
    url: '/system/characters/' + id,
    method: 'get'
  })
}

// 新增汉字起源
export function addCharacters(data) {
  return request({
    url: '/system/characters',
    method: 'post',
    data: data
  })
}

// 修改汉字起源
export function updateCharacters(data) {
  return request({
    url: '/system/characters',
    method: 'put',
    data: data
  })
}

// 删除汉字起源
export function delCharacters(id) {
  return request({
    url: '/system/characters/' + id,
    method: 'delete'
  })
}
