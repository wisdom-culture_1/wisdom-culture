import request from '@/utils/request'

// 查询背诵打卡类别列表
export function listRecite_type(query) {
  return request({
    url: '/system/recite_type/list',
    method: 'get',
    params: query
  })
}

// 查询背诵打卡类别详细
export function getRecite_type(id) {
  return request({
    url: '/system/recite_type/' + id,
    method: 'get'
  })
}

// 新增背诵打卡类别
export function addRecite_type(data) {
  return request({
    url: '/system/recite_type',
    method: 'post',
    data: data
  })
}

// 修改背诵打卡类别
export function updateRecite_type(data) {
  return request({
    url: '/system/recite_type',
    method: 'put',
    data: data
  })
}

// 删除背诵打卡类别
export function delRecite_type(id) {
  return request({
    url: '/system/recite_type/' + id,
    method: 'delete'
  })
}
