import request from '@/utils/request'

// 查询金句摘抄列表
export function listExcerpt(query) {
  return request({
    url: '/system/excerpt/list',
    method: 'get',
    params: query
  })
}

// 查询金句摘抄详细
export function getExcerpt(id) {
  return request({
    url: '/system/excerpt/' + id,
    method: 'get'
  })
}

// 新增金句摘抄
export function addExcerpt(data) {
  return request({
    url: '/system/excerpt',
    method: 'post',
    data: data
  })
}

// 修改金句摘抄
export function updateExcerpt(data) {
  return request({
    url: '/system/excerpt',
    method: 'put',
    data: data
  })
}

// 删除金句摘抄
export function delExcerpt(id) {
  return request({
    url: '/system/excerpt/' + id,
    method: 'delete'
  })
}
