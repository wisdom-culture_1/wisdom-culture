import request from '@/utils/request'

// 查询章节试听进度列表
export function listMasterpieces_chapters_process(query) {
  return request({
    url: '/system/masterpieces_chapters_process/list',
    method: 'get',
    params: query
  })
}

// 查询章节试听进度详细
export function getMasterpieces_chapters_process(id) {
  return request({
    url: '/system/masterpieces_chapters_process/' + id,
    method: 'get'
  })
}

// 新增章节试听进度
export function addMasterpieces_chapters_process(data) {
  return request({
    url: '/system/masterpieces_chapters_process',
    method: 'post',
    data: data
  })
}

// 修改章节试听进度
export function updateMasterpieces_chapters_process(data) {
  return request({
    url: '/system/masterpieces_chapters_process',
    method: 'put',
    data: data
  })
}

// 删除章节试听进度
export function delMasterpieces_chapters_process(id) {
  return request({
    url: '/system/masterpieces_chapters_process/' + id,
    method: 'delete'
  })
}
