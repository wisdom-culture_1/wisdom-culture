import request from '@/utils/request'

// 查询诗词常识列表
export function listSense(query) {
  return request({
    url: '/system/sense/list',
    method: 'get',
    params: query
  })
}

// 查询诗词常识详细
export function getSense(id) {
  return request({
    url: '/system/sense/' + id,
    method: 'get'
  })
}

// 新增诗词常识
export function addSense(data) {
  return request({
    url: '/system/sense',
    method: 'post',
    data: data
  })
}

// 修改诗词常识
export function updateSense(data) {
  return request({
    url: '/system/sense',
    method: 'put',
    data: data
  })
}

// 删除诗词常识
export function delSense(id) {
  return request({
    url: '/system/sense/' + id,
    method: 'delete'
  })
}
