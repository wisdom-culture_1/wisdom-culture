import request from '@/utils/request'

// 查询挑战结果列表
export function listChallenge_complete(query) {
  return request({
    url: '/system/challenge_complete/list',
    method: 'get',
    params: query
  })
}

// 查询挑战结果详细
export function getChallenge_complete(id) {
  return request({
    url: '/system/challenge_complete/' + id,
    method: 'get'
  })
}

// 新增挑战结果
export function addChallenge_complete(data) {
  return request({
    url: '/system/challenge_complete',
    method: 'post',
    data: data
  })
}

// 修改挑战结果
export function updateChallenge_complete(data) {
  return request({
    url: '/system/challenge_complete',
    method: 'put',
    data: data
  })
}

// 删除挑战结果
export function delChallenge_complete(id) {
  return request({
    url: '/system/challenge_complete/' + id,
    method: 'delete'
  })
}
