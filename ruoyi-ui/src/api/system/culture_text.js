import request from '@/utils/request'

// 查询中华文化内容列表
export function listCulture_text(query) {
  return request({
    url: '/system/culture_text/list',
    method: 'get',
    params: query
  })
}

// 查询中华文化内容详细
export function getCulture_text(id) {
  return request({
    url: '/system/culture_text/' + id,
    method: 'get'
  })
}

// 新增中华文化内容
export function addCulture_text(data) {
  return request({
    url: '/system/culture_text',
    method: 'post',
    data: data
  })
}

// 修改中华文化内容
export function updateCulture_text(data) {
  return request({
    url: '/system/culture_text',
    method: 'put',
    data: data
  })
}

// 删除中华文化内容
export function delCulture_text(id) {
  return request({
    url: '/system/culture_text/' + id,
    method: 'delete'
  })
}
