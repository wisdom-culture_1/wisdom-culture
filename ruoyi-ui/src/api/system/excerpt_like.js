import request from '@/utils/request'

// 查询金句摘抄点赞列表
export function listExcerpt_like(query) {
  return request({
    url: '/system/excerpt_like/list',
    method: 'get',
    params: query
  })
}

// 查询金句摘抄点赞详细
export function getExcerpt_like(id) {
  return request({
    url: '/system/excerpt_like/' + id,
    method: 'get'
  })
}

// 新增金句摘抄点赞
export function addExcerpt_like(data) {
  return request({
    url: '/system/excerpt_like',
    method: 'post',
    data: data
  })
}

// 修改金句摘抄点赞
export function updateExcerpt_like(data) {
  return request({
    url: '/system/excerpt_like',
    method: 'put',
    data: data
  })
}

// 删除金句摘抄点赞
export function delExcerpt_like(id) {
  return request({
    url: '/system/excerpt_like/' + id,
    method: 'delete'
  })
}
