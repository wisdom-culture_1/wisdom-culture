import request from '@/utils/request'

// 查询智慧元典正文列表
export function listText(query) {
  return request({
    url: '/system/text/list',
    method: 'get',
    params: query
  })
}

// 查询智慧元典正文详细
export function getText(id) {
  return request({
    url: '/system/text/' + id,
    method: 'get'
  })
}

// 新增智慧元典正文
export function addText(data) {
  return request({
    url: '/system/text',
    method: 'post',
    data: data
  })
}

// 修改智慧元典正文
export function updateText(data) {
  return request({
    url: '/system/text',
    method: 'put',
    data: data
  })
}

// 删除智慧元典正文
export function delText(id) {
  return request({
    url: '/system/text/' + id,
    method: 'delete'
  })
}
