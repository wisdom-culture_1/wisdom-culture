import request from '@/utils/request'

// 查询专家讲堂分类列表
export function listHall_type(query) {
  return request({
    url: '/system/hall_type/list',
    method: 'get',
    params: query
  })
}

// 查询专家讲堂分类详细
export function getHall_type(id) {
  return request({
    url: '/system/hall_type/' + id,
    method: 'get'
  })
}

// 新增专家讲堂分类
export function addHall_type(data) {
  return request({
    url: '/system/hall_type',
    method: 'post',
    data: data
  })
}

// 修改专家讲堂分类
export function updateHall_type(data) {
  return request({
    url: '/system/hall_type',
    method: 'put',
    data: data
  })
}

// 删除专家讲堂分类
export function delHall_type(id) {
  return request({
    url: '/system/hall_type/' + id,
    method: 'delete'
  })
}
