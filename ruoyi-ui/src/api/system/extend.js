import request from '@/utils/request'

// 查询诗词歌赋扩展列表
export function listExtend(query) {
  return request({
    url: '/system/extend/list',
    method: 'get',
    params: query
  })
}

// 查询诗词歌赋扩展详细
export function getExtend(id) {
  return request({
    url: '/system/extend/' + id,
    method: 'get'
  })
}

// 新增诗词歌赋扩展
export function addExtend(data) {
  return request({
    url: '/system/extend',
    method: 'post',
    data: data
  })
}

// 修改诗词歌赋扩展
export function updateExtend(data) {
  return request({
    url: '/system/extend',
    method: 'put',
    data: data
  })
}

// 删除诗词歌赋扩展
export function delExtend(id) {
  return request({
    url: '/system/extend/' + id,
    method: 'delete'
  })
}
