import request from '@/utils/request'

// 查询背诵打卡点赞列表
export function listRecite_like(query) {
  return request({
    url: '/system/recite_like/list',
    method: 'get',
    params: query
  })
}

// 查询背诵打卡点赞详细
export function getRecite_like(id) {
  return request({
    url: '/system/recite_like/' + id,
    method: 'get'
  })
}

// 新增背诵打卡点赞
export function addRecite_like(data) {
  return request({
    url: '/system/recite_like',
    method: 'post',
    data: data
  })
}

// 修改背诵打卡点赞
export function updateRecite_like(data) {
  return request({
    url: '/system/recite_like',
    method: 'put',
    data: data
  })
}

// 删除背诵打卡点赞
export function delRecite_like(id) {
  return request({
    url: '/system/recite_like/' + id,
    method: 'delete'
  })
}
