import request from '@/utils/request'

// 查询中华文化分类列表
export function listCulture_type(query) {
  return request({
    url: '/system/culture_type/list',
    method: 'get',
    params: query
  })
}

// 查询中华文化分类详细
export function getCulture_type(id) {
  return request({
    url: '/system/culture_type/' + id,
    method: 'get'
  })
}

// 新增中华文化分类
export function addCulture_type(data) {
  return request({
    url: '/system/culture_type',
    method: 'post',
    data: data
  })
}

// 修改中华文化分类
export function updateCulture_type(data) {
  return request({
    url: '/system/culture_type',
    method: 'put',
    data: data
  })
}

// 删除中华文化分类
export function delCulture_type(id) {
  return request({
    url: '/system/culture_type/' + id,
    method: 'delete'
  })
}
