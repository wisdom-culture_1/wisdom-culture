package com.ruoyi.framework.web.service;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.AuthorizationCode;
import com.ruoyi.business.entity.Users;
import com.ruoyi.business.mapper.BizAuthorizationCodeMapper;
import com.ruoyi.business.mapper.BizUsersMapper;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.framework.constants.RedisKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.model.RegisterBody;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.exception.user.CaptchaException;
import com.ruoyi.common.exception.user.CaptchaExpireException;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.framework.manager.factory.AsyncFactory;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 注册校验方法
 * 
 * @author ruoyi
 */
@Component
public class SysRegisterService
{
    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BizAuthorizationCodeMapper authorizationCodeMapper;
    @Autowired
    private BizUsersMapper usersMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;
    private static final Logger log = LoggerFactory.getLogger(SysRegisterService.class);

    /**
     * 注册
     */
    public String register(RegisterBody registerBody)
    {
        log.info("register---------"+JSON.toJSONString(registerBody));
        String msg = "",
                username = registerBody.getUsername(),
                password = registerBody.getPassword(),
                passwordConfirmation = registerBody.getPassword_confirmation();

        AuthorizationCode authorizationCode = new AuthorizationCode();
        authorizationCode.setCode(registerBody.getAuth_code());
        AuthorizationCode authorizationByCode = authorizationCodeMapper.selectAuthorizationAuthCode(authorizationCode);
        log.info("authorizationByCode---------"+JSON.toJSONString(authorizationByCode));

        if(null != authorizationByCode){
            // 1.1校验鉴权码，校验鉴权码是否被使用过
            if("1".equals(authorizationByCode.getIsUse())){
                msg = "激活码已使用";
                return msg;
            }

            if("-1".equals(authorizationByCode.getIsUse())){
                msg = "激活码已停用";
                return msg;
            }
        }
        else{
            msg = "激活码错误！";
            return msg;
        }

        SysUser users = new SysUser();
        users.setUserName(username);

        //2. 校验短信验证码
        String cacheKey = String.format(RedisKey.CHECK_CODE_KEY,username);
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
        log.info("register---------"+cacheValue);

        // 如果不为空，再判断是否是60秒以内重复发送，value格式 0122_8735679
        if(StringUtils.isNotBlank(cacheValue)){
            if(!registerBody.getMessageCode().equals(cacheValue.split("_")[0])){
                msg = "短信验证码错误！";
                log.info("短信验证码错误！");

                return msg;
            }else{
                redisTemplate.delete(cacheKey);
            }
        }else{
            // TODO 如果手机号没有传，不通过
            msg = "请输入正确的验证码！";
            return msg;
        }

        if (StringUtils.isEmpty(username))
        {
            msg = "用户名不能为空";
        }
        else if (StringUtils.isEmpty(password))
        {
            msg = "用户密码不能为空";
        }
        else if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            msg = "账户长度必须在2到20个字符之间";
        }
        else if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            msg = "密码长度必须在5到20个字符之间";
        }
        else if (UserConstants.NOT_UNIQUE.equals(userService.checkNewUserNameUnique(users)))
        {
            msg = "保存用户'" + username + "'失败，注册账号已存在";
        }
        else if(StringUtils.isNull(passwordConfirmation)){
            log.info("二次确认密码不能为空！");
            msg = "二次确认密码不能为空";
        }
        else
        {
            if(!password.equals(passwordConfirmation)){
                log.info("两次输入密码不一致，请确认！！");
                return  "两次输入密码不一致，请确认！";
            }
            users.setPassword(SecurityUtils.encryptPassword(password));
            users.setPhonenumber(username);
            users.setCode(registerBody.getAuth_code());
            users.setNickname(username);
            users.setCreateTime(new Date());
            // 计算到期时间更新到sys_user.expiration_time
            if(authorizationByCode.getStartDate() != null && authorizationByCode.getEndDate() != null){
                users.setExpirationTime(authorizationByCode.getEndDate());
            }else{
                Date date = addYear(new Date(), authorizationByCode.getUserTerm().intValue());
                users.setExpirationTime(date);
            }

            // TODO 要把对应的激活码更新为已使用
            boolean regFlag = userService.registerUser(users);
            if (!regFlag)
            {
                log.info("注册失败,请联系系统管理人员");
                msg = "注册失败,请联系系统管理人员";
            }
            else
            {
                authorizationByCode.setIsUse("1");
                authorizationByCode.setUpdatedAt(new Date());
                authorizationCodeMapper.updateAuthorizationCode(authorizationByCode);
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.REGISTER, MessageUtils.message("user.register.success")));

                // 同步新系统的用户到老系统中。
                Users oldSystemUser = new Users();
                oldSystemUser.setUsername(users.getUserName());
                oldSystemUser.setPassword(users.getPassword());
                oldSystemUser.setCode(users.getCode());
                oldSystemUser.setExpirationTime(users.getExpirationTime());
                oldSystemUser.setNickname(users.getNickname());
                oldSystemUser.setChildrenName(users.getChildrenName());
                oldSystemUser.setChildrenNickname(users.getChildrenNickname());
                oldSystemUser.setAddress(users.getAddress());
                oldSystemUser.setPhone(users.getPhonenumber());
                oldSystemUser.setEmail(users.getEmail());
                oldSystemUser.setGradeId(users.getGradeId());
                oldSystemUser.setGreadUad(users.getGreadUad());
                oldSystemUser.setWxImg(users.getWxImg());
                oldSystemUser.setWxName(users.getWxName());
                oldSystemUser.setWxOpenid(users.getWxOpenid());
                oldSystemUser.setCreatedAt(users.getCreateTime());
                oldSystemUser.setUpdatedAt(new Date());
                int i = usersMapper.insertUsers(oldSystemUser);
                if(i > 0){
                    log.info("sys new register user to old system : {}"+JSON.toJSONString(oldSystemUser));
                }
            }
        }

        return msg;
    }

    public  Date addYear (Date date, int year) {
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR,year);
        return new Date(cal.getTime().getTime());
    }

    /**
     * 校验验证码
     * 
     * @param username 用户名
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid)
    {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.nvl(username, "");
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null)
        {
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha))
        {
            throw new CaptchaException();
        }
    }
}
