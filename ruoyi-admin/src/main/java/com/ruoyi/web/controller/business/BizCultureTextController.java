package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.CultureText;
import com.ruoyi.business.service.ICultureTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 中华文化内容Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/culture")
public class BizCultureTextController extends BaseController
{
    @Autowired
    private ICultureTextService cultureTextService;

    /**
     * 查询中华文化内容列表
     */
//    @PreAuthorize("@ss.hasPermi('system:text:list')")
    @GetMapping("/detail")
    public AjaxResult list(CultureText cultureText)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<CultureText> list = cultureTextService.selectCultureTextList(cultureText);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出中华文化内容列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:export')")
//    @Log(title = "中华文化内容", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CultureText cultureText)
//    {
//        List<CultureText> list = cultureTextService.selectCultureTextList(cultureText);
//        ExcelUtil<CultureText> util = new ExcelUtil<CultureText>(CultureText.class);
//        util.exportExcel(response, list, "中华文化内容数据");
//    }

//    /**
//     * 获取中华文化内容详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(cultureTextService.selectCultureTextById(id));
//    }

//    /**
//     * 新增中华文化内容
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:add')")
//    @Log(title = "中华文化内容", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody CultureText cultureText)
//    {
//        return toAjax(cultureTextService.insertCultureText(cultureText));
//    }

//    /**
//     * 修改中华文化内容
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:edit')")
//    @Log(title = "中华文化内容", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody CultureText cultureText)
//    {
//        return toAjax(cultureTextService.updateCultureText(cultureText));
//    }

//    /**
//     * 删除中华文化内容
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:remove')")
//    @Log(title = "中华文化内容", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(cultureTextService.deleteCultureTextByIds(ids));
//    }
}
