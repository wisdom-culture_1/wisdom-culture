package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Classical;
import com.ruoyi.system.service.IClassicalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文言文Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/classical")
public class ClassicalController extends BaseController
{
    @Autowired
    private IClassicalService classicalService;

    /**
     * 查询文言文列表
     */
    @PreAuthorize("@ss.hasPermi('system:classical:list')")
    @GetMapping("/list")
    public TableDataInfo list(Classical classical)
    {
        startPage();
        List<Classical> list = classicalService.selectClassicalList(classical);
        return getDataTable(list);
    }

    /**
     * 导出文言文列表
     */
    @PreAuthorize("@ss.hasPermi('system:classical:export')")
    @Log(title = "文言文", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Classical classical)
    {
        List<Classical> list = classicalService.selectClassicalList(classical);
        ExcelUtil<Classical> util = new ExcelUtil<Classical>(Classical.class);
        util.exportExcel(response, list, "文言文数据");
    }

    /**
     * 获取文言文详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:classical:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(classicalService.selectClassicalById(id));
    }

    /**
     * 新增文言文
     */
    @PreAuthorize("@ss.hasPermi('system:classical:add')")
    @Log(title = "文言文", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Classical classical)
    {
        return toAjax(classicalService.insertClassical(classical));
    }

    /**
     * 修改文言文
     */
    @PreAuthorize("@ss.hasPermi('system:classical:edit')")
    @Log(title = "文言文", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Classical classical)
    {
        return toAjax(classicalService.updateClassical(classical));
    }

    /**
     * 删除文言文
     */
    @PreAuthorize("@ss.hasPermi('system:classical:remove')")
    @Log(title = "文言文", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(classicalService.deleteClassicalByIds(ids));
    }
}
