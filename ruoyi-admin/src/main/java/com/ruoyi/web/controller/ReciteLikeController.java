package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ReciteLike;
import com.ruoyi.system.service.IReciteLikeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 背诵打卡点赞Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/recite_like")
public class ReciteLikeController extends BaseController
{
    @Autowired
    private IReciteLikeService reciteLikeService;

    /**
     * 查询背诵打卡点赞列表
     */
    @PreAuthorize("@ss.hasPermi('system:recite_like:list')")
    @GetMapping("/list")
    public TableDataInfo list(ReciteLike reciteLike)
    {
        startPage();
        List<ReciteLike> list = reciteLikeService.selectReciteLikeList(reciteLike);
        return getDataTable(list);
    }

    /**
     * 导出背诵打卡点赞列表
     */
    @PreAuthorize("@ss.hasPermi('system:recite_like:export')")
    @Log(title = "背诵打卡点赞", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ReciteLike reciteLike)
    {
        List<ReciteLike> list = reciteLikeService.selectReciteLikeList(reciteLike);
        ExcelUtil<ReciteLike> util = new ExcelUtil<ReciteLike>(ReciteLike.class);
        util.exportExcel(response, list, "背诵打卡点赞数据");
    }

    /**
     * 获取背诵打卡点赞详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:recite_like:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(reciteLikeService.selectReciteLikeById(id));
    }

    /**
     * 新增背诵打卡点赞
     */
    @PreAuthorize("@ss.hasPermi('system:recite_like:add')")
    @Log(title = "背诵打卡点赞", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReciteLike reciteLike)
    {
        return toAjax(reciteLikeService.insertReciteLike(reciteLike));
    }

    /**
     * 修改背诵打卡点赞
     */
    @PreAuthorize("@ss.hasPermi('system:recite_like:edit')")
    @Log(title = "背诵打卡点赞", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReciteLike reciteLike)
    {
        return toAjax(reciteLikeService.updateReciteLike(reciteLike));
    }

    /**
     * 删除背诵打卡点赞
     */
    @PreAuthorize("@ss.hasPermi('system:recite_like:remove')")
    @Log(title = "背诵打卡点赞", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(reciteLikeService.deleteReciteLikeByIds(ids));
    }
}
