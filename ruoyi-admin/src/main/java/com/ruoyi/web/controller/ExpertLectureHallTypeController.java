package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ExpertLectureHallType;
import com.ruoyi.system.service.IExpertLectureHallTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 专家讲堂分类Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/hall_type")
public class ExpertLectureHallTypeController extends BaseController
{
    @Autowired
    private IExpertLectureHallTypeService expertLectureHallTypeService;

    /**
     * 查询专家讲堂分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:hall_type:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExpertLectureHallType expertLectureHallType)
    {
        startPage();
        List<ExpertLectureHallType> list = expertLectureHallTypeService.selectExpertLectureHallTypeList(expertLectureHallType);
        return getDataTable(list);
    }

    /**
     * 导出专家讲堂分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:hall_type:export')")
    @Log(title = "专家讲堂分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExpertLectureHallType expertLectureHallType)
    {
        List<ExpertLectureHallType> list = expertLectureHallTypeService.selectExpertLectureHallTypeList(expertLectureHallType);
        ExcelUtil<ExpertLectureHallType> util = new ExcelUtil<ExpertLectureHallType>(ExpertLectureHallType.class);
        util.exportExcel(response, list, "专家讲堂分类数据");
    }

    /**
     * 获取专家讲堂分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:hall_type:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(expertLectureHallTypeService.selectExpertLectureHallTypeById(id));
    }

    /**
     * 新增专家讲堂分类
     */
    @PreAuthorize("@ss.hasPermi('system:hall_type:add')")
    @Log(title = "专家讲堂分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExpertLectureHallType expertLectureHallType)
    {
        return toAjax(expertLectureHallTypeService.insertExpertLectureHallType(expertLectureHallType));
    }

    /**
     * 修改专家讲堂分类
     */
    @PreAuthorize("@ss.hasPermi('system:hall_type:edit')")
    @Log(title = "专家讲堂分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExpertLectureHallType expertLectureHallType)
    {
        return toAjax(expertLectureHallTypeService.updateExpertLectureHallType(expertLectureHallType));
    }

    /**
     * 删除专家讲堂分类
     */
    @PreAuthorize("@ss.hasPermi('system:hall_type:remove')")
    @Log(title = "专家讲堂分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(expertLectureHallTypeService.deleteExpertLectureHallTypeByIds(ids));
    }
}
