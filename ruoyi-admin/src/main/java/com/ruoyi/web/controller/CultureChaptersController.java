package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CultureChapters;
import com.ruoyi.system.service.ICultureChaptersService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文化章节Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/culture_chapters")
public class CultureChaptersController extends BaseController
{
    @Autowired
    private ICultureChaptersService cultureChaptersService;

    /**
     * 查询文化章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_chapters:list')")
    @GetMapping("/list")
    public TableDataInfo list(CultureChapters cultureChapters)
    {
        startPage();
        List<CultureChapters> list = cultureChaptersService.selectCultureChaptersList(cultureChapters);
        return getDataTable(list);
    }

    /**
     * 导出文化章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_chapters:export')")
    @Log(title = "文化章节", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CultureChapters cultureChapters)
    {
        List<CultureChapters> list = cultureChaptersService.selectCultureChaptersList(cultureChapters);
        ExcelUtil<CultureChapters> util = new ExcelUtil<CultureChapters>(CultureChapters.class);
        util.exportExcel(response, list, "文化章节数据");
    }

    /**
     * 获取文化章节详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:culture_chapters:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cultureChaptersService.selectCultureChaptersById(id));
    }

    /**
     * 新增文化章节
     */
    @PreAuthorize("@ss.hasPermi('system:culture_chapters:add')")
    @Log(title = "文化章节", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CultureChapters cultureChapters)
    {
        return toAjax(cultureChaptersService.insertCultureChapters(cultureChapters));
    }

    /**
     * 修改文化章节
     */
    @PreAuthorize("@ss.hasPermi('system:culture_chapters:edit')")
    @Log(title = "文化章节", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CultureChapters cultureChapters)
    {
        return toAjax(cultureChaptersService.updateCultureChapters(cultureChapters));
    }

    /**
     * 删除文化章节
     */
    @PreAuthorize("@ss.hasPermi('system:culture_chapters:remove')")
    @Log(title = "文化章节", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cultureChaptersService.deleteCultureChaptersByIds(ids));
    }
}
