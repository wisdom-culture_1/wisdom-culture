package com.ruoyi.web.controller.business;

import java.util.Date;
import java.util.List;

import com.ruoyi.business.entity.ChapterProcess;
import com.ruoyi.business.entity.MasterpiecesBook;
import com.ruoyi.business.mapper.BizChapterProcessMapper;
import com.ruoyi.business.service.IMasterpiecesBookService;
import com.ruoyi.common.utils.SecurityUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 名著书籍Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/masterpieces")
public class BizMasterpiecesBookController extends BaseController
{
    @Autowired
    private IMasterpiecesBookService masterpiecesBookService;

    @Autowired
    private BizChapterProcessMapper bizChapterProcessMapper;

    /**
     * 查询名著书籍列表
     */
//    @PreAuthorize("@ss.hasPermi('system:book:list')")
    @GetMapping("/book_list")
    public AjaxResult list(MasterpiecesBook masterpiecesBook)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

//        startPage();
        List<MasterpiecesBook> list = masterpiecesBookService.selectMasterpiecesBookList(masterpiecesBook);

        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出名著书籍列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:export')")
//    @Log(title = "名著书籍", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, MasterpiecesBook masterpiecesBook)
//    {
//        List<MasterpiecesBook> list = masterpiecesBookService.selectMasterpiecesBookList(masterpiecesBook);
//        ExcelUtil<MasterpiecesBook> util = new ExcelUtil<MasterpiecesBook>(MasterpiecesBook.class);
//        util.exportExcel(response, list, "名著书籍数据");
//    }

    /**
     * 获取名著书籍详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:book:query')")
    @GetMapping(value = "/chapter_list")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        MasterpiecesBook masterpiecesBook = masterpiecesBookService.selectMasterpiecesBookById(id);
        ajaxResult.put("data",masterpiecesBook);
        return ajaxResult;
    }

    /**
     * 获取名著书籍详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:book:query')")
    @PostMapping(value = "/saveChapterProcess")
    @Transactional
    public AjaxResult saveChapterProcess(@RequestBody ChapterProcess process)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

        process.setUserId(String.valueOf(SecurityUtils.getUserId()));
        ChapterProcess chapterProcess = bizChapterProcessMapper.selectChapterProcess(process);
        if(null != chapterProcess){
            process.setUpdatedAt(new Date());
            bizChapterProcessMapper.updateChapterProcess(process);
        }else{
            process.setCreatedAt(new Date());
            process.setUpdatedAt(new Date());
            bizChapterProcessMapper.insertChapterProcess(process);
        }
        return ajaxResult;
    }

//    /**
//     * 新增名著书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:add')")
//    @Log(title = "名著书籍", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody MasterpiecesBook masterpiecesBook)
//    {
//        return toAjax(masterpiecesBookService.insertMasterpiecesBook(masterpiecesBook));
//    }
//
//    /**
//     * 修改名著书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:edit')")
//    @Log(title = "名著书籍", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody MasterpiecesBook masterpiecesBook)
//    {
//        return toAjax(masterpiecesBookService.updateMasterpiecesBook(masterpiecesBook));
//    }
//
//    /**
//     * 删除名著书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:remove')")
//    @Log(title = "名著书籍", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(masterpiecesBookService.deleteMasterpiecesBookByIds(ids));
//    }
}
