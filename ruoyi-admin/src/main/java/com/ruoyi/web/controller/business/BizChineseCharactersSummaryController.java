package com.ruoyi.web.controller.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.entity.ChineseCharactersSummary;
import com.ruoyi.business.service.IChineseCharactersSummaryService;

/**
 * 文字汇总Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizChineseCharactersSummaryController extends BaseController
{
    @Autowired
    private IChineseCharactersSummaryService chineseCharactersSummaryService;

//    /**
//     * 查询文字汇总列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(ChineseCharactersSummary chineseCharactersSummary)
//    {
//        startPage();
//        List<ChineseCharactersSummary> list = chineseCharactersSummaryService.selectChineseCharactersSummaryList(chineseCharactersSummary);
//        return getDataTable(list);
//    }

//    /**
//     * 导出文字汇总列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:export')")
//    @Log(title = "文字汇总", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ChineseCharactersSummary chineseCharactersSummary)
//    {
//        List<ChineseCharactersSummary> list = chineseCharactersSummaryService.selectChineseCharactersSummaryList(chineseCharactersSummary);
//        ExcelUtil<ChineseCharactersSummary> util = new ExcelUtil<ChineseCharactersSummary>(ChineseCharactersSummary.class);
//        util.exportExcel(response, list, "文字汇总数据");
//    }

//    /**
//     * 获取文字汇总详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(chineseCharactersSummaryService.selectChineseCharactersSummaryById(id));
//    }

    /**
     * 新增文字汇总
     */
//    @PreAuthorize("@ss.hasPermi('system:summary:add')")
    @Log(title = "文字汇总", businessType = BusinessType.INSERT)
    @PostMapping("/words_update_summary")
    public AjaxResult add(@RequestBody ChineseCharactersSummary chineseCharactersSummary)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        chineseCharactersSummaryService.insertChineseCharactersSummary(chineseCharactersSummary);
        return ajaxResult;
    }

//    /**
//     * 修改文字汇总
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:edit')")
//    @Log(title = "文字汇总", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ChineseCharactersSummary chineseCharactersSummary)
//    {
//        return toAjax(chineseCharactersSummaryService.updateChineseCharactersSummary(chineseCharactersSummary));
//    }
//
//    /**
//     * 删除文字汇总
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:remove')")
//    @Log(title = "文字汇总", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(chineseCharactersSummaryService.deleteChineseCharactersSummaryByIds(ids));
//    }
}
