package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.CultureChapters;
import com.ruoyi.business.service.ICultureChaptersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 文化章节Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/culture")
public class BizCultureChaptersController extends BaseController
{
    @Autowired
    private ICultureChaptersService cultureChaptersService;

    /**
     * 查询文化章节列表
     */
//    @PreAuthorize("@ss.hasPermi('system:chapters:list')")
    @GetMapping("/chapter_list")
    public AjaxResult list(CultureChapters cultureChapters)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

        cultureChapters.setPid(Long.valueOf(0));
//        startPage();
        List<CultureChapters> list = cultureChaptersService.selectCultureChaptersList(cultureChapters);

        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出文化章节列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapters:export')")
//    @Log(title = "文化章节", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CultureChapters cultureChapters)
//    {
//        List<CultureChapters> list = cultureChaptersService.selectCultureChaptersList(cultureChapters);
//        ExcelUtil<CultureChapters> util = new ExcelUtil<CultureChapters>(CultureChapters.class);
//        util.exportExcel(response, list, "文化章节数据");
//    }

//    /**
//     * 获取文化章节详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapters:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(cultureChaptersService.selectCultureChaptersById(id));
//    }

//    /**
//     * 新增文化章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapters:add')")
//    @Log(title = "文化章节", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody CultureChapters cultureChapters)
//    {
//        return toAjax(cultureChaptersService.insertCultureChapters(cultureChapters));
//    }

//    /**
//     * 修改文化章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapters:edit')")
//    @Log(title = "文化章节", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody CultureChapters cultureChapters)
//    {
//        return toAjax(cultureChaptersService.updateCultureChapters(cultureChapters));
//    }

//    /**
//     * 删除文化章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapters:remove')")
//    @Log(title = "文化章节", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(cultureChaptersService.deleteCultureChaptersByIds(ids));
//    }
}
