package com.ruoyi.web.controller.business;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.business.entity.Etymology;
import com.ruoyi.business.mapper.BizEtymologyMapper;
import com.ruoyi.business.service.IEtymologyService;
import com.ruoyi.web.controller.util.CommonUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 字源Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/etymology")
public class BizEtymologyController extends BaseController
{
    @Autowired
    private IEtymologyService etymologyService;
    @Autowired
    private BizEtymologyMapper bizEtymologyMapper;

    /**
     * 查询字源列表
     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:list')")
    @GetMapping("/etymology_strokes_number")
    public AjaxResult list(Etymology etymology)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<Etymology> list = etymologyService.selectEtymologyList(etymology);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 查询字源列表
     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:list')")/auth/login
    @GetMapping("/etymology_type")
    public AjaxResult etymologyType(Etymology etymology)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<Etymology> list = etymologyService.selectEtymologyList(etymology);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 查询字源列表
     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:list')")/auth/login
    @GetMapping("/everydayDic")
    public AjaxResult everydayDic()
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        int yearDay = CommonUtil.yearDay();
        Integer count = bizEtymologyMapper.selectCount();

        int sort = yearDay % count;
        Etymology etymology = new Etymology ();
        List<Etymology> list = new ArrayList<Etymology>();
        if(sort == 0){
            etymology.setSort(Long.valueOf(count));
            list = etymologyService.selectEtymologyList(etymology);
        }else{
            etymology.setSort(Long.valueOf(sort));
            list = etymologyService.selectEtymologyList(etymology);
        }

        if(!CollectionUtils.isEmpty(list)){
            ajaxResult.put("data",list.get(0));

        }
        return ajaxResult;
    }

//    /**
//     * 导出字源列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:export')")
//    @Log(title = "字源", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, Etymology etymology)
//    {
//        List<Etymology> list = etymologyService.selectEtymologyList(etymology);
//        ExcelUtil<Etymology> util = new ExcelUtil<Etymology>(Etymology.class);
//        util.exportExcel(response, list, "字源数据");
//    }

    /**
     * 获取字源详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:query')")
    @GetMapping(value = "/etymology_detail")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        Etymology etymology = etymologyService.selectEtymologyById(id);
        ajaxResult.put("data",etymology);
        return ajaxResult;
    }
//
//    /**
//     * 新增字源
//     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:add')")
//    @Log(title = "字源", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody Etymology etymology)
//    {
//        return toAjax(etymologyService.insertEtymology(etymology));
//    }
//
//    /**
//     * 修改字源
//     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:edit')")
//    @Log(title = "字源", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody Etymology etymology)
//    {
//        return toAjax(etymologyService.updateEtymology(etymology));
//    }
//
//    /**
//     * 删除字源
//     */
//    @PreAuthorize("@ss.hasPermi('system:etymology:remove')")
//    @Log(title = "字源", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(etymologyService.deleteEtymologyByIds(ids));
//    }
}
