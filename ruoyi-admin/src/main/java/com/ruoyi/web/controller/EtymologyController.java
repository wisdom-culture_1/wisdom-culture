package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Etymology;
import com.ruoyi.system.service.IEtymologyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 字源Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/etymology")
public class EtymologyController extends BaseController
{
    @Autowired
    private IEtymologyService etymologyService;

    /**
     * 查询字源列表
     */
    @PreAuthorize("@ss.hasPermi('system:etymology:list')")
    @GetMapping("/list")
    public TableDataInfo list(Etymology etymology)
    {
        startPage();
        List<Etymology> list = etymologyService.selectEtymologyList(etymology);
        return getDataTable(list);
    }

    /**
     * 导出字源列表
     */
    @PreAuthorize("@ss.hasPermi('system:etymology:export')")
    @Log(title = "字源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Etymology etymology)
    {
        List<Etymology> list = etymologyService.selectEtymologyList(etymology);
        ExcelUtil<Etymology> util = new ExcelUtil<Etymology>(Etymology.class);
        util.exportExcel(response, list, "字源数据");
    }

    /**
     * 获取字源详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:etymology:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(etymologyService.selectEtymologyById(id));
    }

    /**
     * 新增字源
     */
    @PreAuthorize("@ss.hasPermi('system:etymology:add')")
    @Log(title = "字源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Etymology etymology)
    {
        return toAjax(etymologyService.insertEtymology(etymology));
    }

    /**
     * 修改字源
     */
    @PreAuthorize("@ss.hasPermi('system:etymology:edit')")
    @Log(title = "字源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Etymology etymology)
    {
        return toAjax(etymologyService.updateEtymology(etymology));
    }

    /**
     * 删除字源
     */
    @PreAuthorize("@ss.hasPermi('system:etymology:remove')")
    @Log(title = "字源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(etymologyService.deleteEtymologyByIds(ids));
    }
}
