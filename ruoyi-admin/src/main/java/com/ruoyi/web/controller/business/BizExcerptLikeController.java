package com.ruoyi.web.controller.business;

import java.util.Date;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.ExcerptLike;
import com.ruoyi.business.service.IExcerptLikeService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

/**
 * 金句摘抄点赞Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/excerpt")
public class BizExcerptLikeController extends BaseController
{
    @Autowired
    private IExcerptLikeService excerptLikeService;

//    /**
//     * 查询金句摘抄点赞列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(ExcerptLike excerptLike)
//    {
//        startPage();
//        List<ExcerptLike> list = excerptLikeService.selectExcerptLikeList(excerptLike);
//        return getDataTable(list);
//    }

//    /**
//     * 导出金句摘抄点赞列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:export')")
//    @Log(title = "金句摘抄点赞", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ExcerptLike excerptLike)
//    {
//        List<ExcerptLike> list = excerptLikeService.selectExcerptLikeList(excerptLike);
//        ExcelUtil<ExcerptLike> util = new ExcelUtil<ExcerptLike>(ExcerptLike.class);
//        util.exportExcel(response, list, "金句摘抄点赞数据");
//    }
//
//    /**
//     * 获取金句摘抄点赞详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(excerptLikeService.selectExcerptLikeById(id));
//    }

    /**
     * 新增金句摘抄点赞
     */
//    @PreAuthorize("@ss.hasPermi('system:like:add')")
    @Log(title = "金句摘抄点赞", businessType = BusinessType.INSERT)
    @PostMapping("like")
    public AjaxResult add(@RequestBody ExcerptLike excerptLike)
    {
        logger.info("excerptLike is {}",JSON.toJSONString(excerptLike));
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        excerptLike.setUserId(SecurityUtils.getUserId());
        excerptLike.setExcerptId(excerptLike.getId());
        excerptLike.setCreatedAt(new Date());
        excerptLike.setUpdatedAt(new Date());
        excerptLikeService.insertExcerptLike(excerptLike);
        return ajaxResult;
    }

//    /**
//     * 修改金句摘抄点赞
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:edit')")
//    @Log(title = "金句摘抄点赞", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ExcerptLike excerptLike)
//    {
//        return toAjax(excerptLikeService.updateExcerptLike(excerptLike));
//    }
//
//    /**
//     * 删除金句摘抄点赞
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:remove')")
//    @Log(title = "金句摘抄点赞", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(excerptLikeService.deleteExcerptLikeByIds(ids));
//    }
}
