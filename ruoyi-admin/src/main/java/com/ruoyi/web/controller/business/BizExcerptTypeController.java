package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.ExcerptType;
import com.ruoyi.business.service.IExcerptTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 金句摘抄分类Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/excerpt")
public class BizExcerptTypeController extends BaseController
{
    @Autowired
    private IExcerptTypeService excerptTypeService;

    /**
     * 查询金句摘抄分类列表
     */
//    @PreAuthorize("@ss.hasPermi('system:type:list')")
    @GetMapping("/type")
    public AjaxResult list(ExcerptType excerptType)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ExcerptType> list = excerptTypeService.selectExcerptTypeList(excerptType);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出金句摘抄分类列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:export')")
//    @Log(title = "金句摘抄分类", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ExcerptType excerptType)
//    {
//        List<ExcerptType> list = excerptTypeService.selectExcerptTypeList(excerptType);
//        ExcelUtil<ExcerptType> util = new ExcelUtil<ExcerptType>(ExcerptType.class);
//        util.exportExcel(response, list, "金句摘抄分类数据");
//    }
//
//    /**
//     * 获取金句摘抄分类详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(excerptTypeService.selectExcerptTypeById(id));
//    }
//
//    /**
//     * 新增金句摘抄分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:add')")
//    @Log(title = "金句摘抄分类", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ExcerptType excerptType)
//    {
//        return toAjax(excerptTypeService.insertExcerptType(excerptType));
//    }
//
//    /**
//     * 修改金句摘抄分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:edit')")
//    @Log(title = "金句摘抄分类", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ExcerptType excerptType)
//    {
//        return toAjax(excerptTypeService.updateExcerptType(excerptType));
//    }
//
//    /**
//     * 删除金句摘抄分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:remove')")
//    @Log(title = "金句摘抄分类", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(excerptTypeService.deleteExcerptTypeByIds(ids));
//    }
}
