package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.NewWordsSummaryRsp;
import com.ruoyi.business.entity.ZdPinyin;
import com.ruoyi.business.mapper.BizChineseCharactersMapper;
import com.ruoyi.business.mapper.BizChineseCharactersSummaryMapper;
import com.ruoyi.business.mapper.BizZdPinyinMapper;
import com.ruoyi.common.utils.SecurityUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.ChineseCharacters;
import com.ruoyi.business.service.IChineseCharactersService;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizChineseCharactersController extends BaseController
{
    @Autowired
    private IChineseCharactersService chineseCharactersService;

    @Autowired
    private BizChineseCharactersMapper bizChineseCharactersMapper;

    @Autowired
    private BizZdPinyinMapper bizZdPinyinMapper;
    
    @Autowired
    private BizChineseCharactersSummaryMapper bizChineseCharactersSummaryMapper;

//    /**
//     * 查询【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:characters:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(ChineseCharacters chineseCharacters)
//    {
//        startPage();
//        List<ChineseCharacters> list = chineseCharactersService.selectChineseCharactersList(chineseCharacters);
//        return getDataTable(list);
//    }

//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:characters:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ChineseCharacters chineseCharacters)
//    {
//        List<ChineseCharacters> list = chineseCharactersService.selectChineseCharactersList(chineseCharacters);
//        ExcelUtil<ChineseCharacters> util = new ExcelUtil<ChineseCharacters>(ChineseCharacters.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }

    /**
     * 获取【请填写功能名称】详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:characters:query')")
    @GetMapping("/character_detail")
    public AjaxResult getInfo(@Param("refid") Long refid)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        ajaxResult.put("data",chineseCharactersService.selectChineseCharactersByRefid(refid));
        return ajaxResult;
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:characters:query')")
    @GetMapping("/new_words_summary_list")
    public AjaxResult newWordsSummaryList(@Param("between") String between,@Param("level") String level)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

        NewWordsSummaryRsp newWordsSummaryRsp = chineseCharactersService.selectNewWordListSummary(between, level);

        ajaxResult.put("data",newWordsSummaryRsp);
        return ajaxResult;
    }

    //    @PreAuthorize("@ss.hasPermi('system:characters:query')")
    @GetMapping("/new_words_list")
    public AjaxResult newWordsList(@Param("between") String between,@Param("status") Integer status,@Param("key") String key)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        List<ChineseCharacters> chineseCharacters = chineseCharactersService.selectNewWordsList(between,status,key);
        ajaxResult.put("data",chineseCharacters);
        return ajaxResult;
    }

    @PostMapping("/new_words_del")
    public AjaxResult newWordsDel(@Param("between") String between,@Param("status") Integer status)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

        bizChineseCharactersSummaryMapper.deleteChineseCharactersSummaryByUserId(SecurityUtils.getUserId());

        return ajaxResult;
    }

    @GetMapping("/character_refid")
    public AjaxResult characterRefidByCharacter(@Param("zi") String zi)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        ZdPinyin zdPinyin = new ZdPinyin();
        zdPinyin.setZi(zi);
        List<ZdPinyin> zdPinyins = bizZdPinyinMapper.selectZdPinyinList(zdPinyin);
        ajaxResult.put("data",zdPinyins);
        return ajaxResult;
    }


//
//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:characters:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ChineseCharacters chineseCharacters)
//    {
//        return toAjax(chineseCharactersService.insertChineseCharacters(chineseCharacters));
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:characters:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ChineseCharacters chineseCharacters)
//    {
//        return toAjax(chineseCharactersService.updateChineseCharacters(chineseCharacters));
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:characters:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{refids}")
//    public AjaxResult remove(@PathVariable Long[] refids)
//    {
//        return toAjax(chineseCharactersService.deleteChineseCharactersByRefids(refids));
//    }
}
