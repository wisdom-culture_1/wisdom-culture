package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.ClassicalText;
import com.ruoyi.system.service.IClassicalTextService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文言文正文Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/classical_text")
public class ClassicalTextController extends BaseController
{
    @Autowired
    private IClassicalTextService classicalTextService;

    /**
     * 查询文言文正文列表
     */
    @PreAuthorize("@ss.hasPermi('system:classical_text:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClassicalText classicalText)
    {
        startPage();
        List<ClassicalText> list = classicalTextService.selectClassicalTextList(classicalText);
        return getDataTable(list);
    }

    /**
     * 导出文言文正文列表
     */
    @PreAuthorize("@ss.hasPermi('system:classical_text:export')")
    @Log(title = "文言文正文", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ClassicalText classicalText)
    {
        List<ClassicalText> list = classicalTextService.selectClassicalTextList(classicalText);
        ExcelUtil<ClassicalText> util = new ExcelUtil<ClassicalText>(ClassicalText.class);
        util.exportExcel(response, list, "文言文正文数据");
    }

    /**
     * 获取文言文正文详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:classical_text:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(classicalTextService.selectClassicalTextById(id));
    }

    /**
     * 新增文言文正文
     */
    @PreAuthorize("@ss.hasPermi('system:classical_text:add')")
    @Log(title = "文言文正文", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClassicalText classicalText)
    {
        return toAjax(classicalTextService.insertClassicalText(classicalText));
    }

    /**
     * 修改文言文正文
     */
    @PreAuthorize("@ss.hasPermi('system:classical_text:edit')")
    @Log(title = "文言文正文", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClassicalText classicalText)
    {
        return toAjax(classicalTextService.updateClassicalText(classicalText));
    }

    /**
     * 删除文言文正文
     */
    @PreAuthorize("@ss.hasPermi('system:classical_text:remove')")
    @Log(title = "文言文正文", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(classicalTextService.deleteClassicalTextByIds(ids));
    }
}
