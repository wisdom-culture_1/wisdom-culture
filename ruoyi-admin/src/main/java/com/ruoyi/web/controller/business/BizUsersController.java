package com.ruoyi.web.controller.business;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.service.IUsersService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/account")
public class BizUsersController extends BaseController
{
    @Autowired
    private IUsersService usersService;

    @Autowired
    private ISysUserService iSysUserService;
    private static final Logger log = LoggerFactory.getLogger(BizUsersController.class);


//    /**
//     * 查询【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:users:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(Users users)
//    {
//        startPage();
//        List<Users> list = usersService.selectUsersList(users);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:users:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, Users users)
//    {
//        List<Users> list = usersService.selectUsersList(users);
//        ExcelUtil<Users> util = new ExcelUtil<Users>(Users.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }

    /**
     * 获取【请填写功能名称】详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:users:query')")
    @GetMapping(value = "/info")
    public AjaxResult getInfo(@Param("id") String id)
    {
        log.info("BizUsersController info is :{}",id);
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        log.info("SecurityUtils.getUserId() : {}",SecurityUtils.getUserId());
        ajaxResult.put("data",iSysUserService.selectUserById(SecurityUtils.getUserId()));
        log.info("BizUsersController ajaxResult is :{}",JSON.toJSONString(ajaxResult));

        return ajaxResult;
    }

//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:users:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody Users users)
//    {
//        return toAjax(usersService.insertUsers(users));
//    }

    /**
     * 修改【用户信息】
     */
//    @PreAuthorize("@ss.hasPermi('system:users:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("update_grade")
    public AjaxResult edit(@RequestBody SysUser users)
    {
        log.info("BizUsersController update_grade is :{}",JSON.toJSONString(users));
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        users.setUserId(SecurityUtils.getUserId());
        users.setGradeId(users.getId());
        iSysUserService.updateUser(users);
        return ajaxResult;
    }

    /**
     * 修改【用户信息】
     */
//    @PreAuthorize("@ss.hasPermi('system:users:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("update_info")
    public AjaxResult updateInfo(@RequestBody SysUser users)
    {
        log.info("BizUsersController update_info is :{}",JSON.toJSONString(users));

        AjaxResult ajaxResult = AjaxResult.newSuccess();

        if(users.getUserId() != null){
            users.setUserId(users.getUserId());
        }else{
            users.setUserId(SecurityUtils.getUserId());
            log.info("BizUsersController update_info SecurityUtils.getUserId() is :{}",JSON.toJSONString(SecurityUtils.getUserId()));
        }

        if(users.getExpirationTime() != null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");  //年月日时分秒
            String stringDay = simpleDateFormat.format(users.getExpirationTime());
            String s = stringDay + " 23:59:59";
            users.setExpirationTime(formatDate(s,"yyyy-MM-dd HH:mm:ss"));
        }
        iSysUserService.updateUser(users);
        return ajaxResult;
    }

    public static Date formatDate(String strDate, String format) {
        try {
            return new SimpleDateFormat(format).parse(String.valueOf(strDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date(0);
    }


//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:users:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable String[] ids)
//    {
//        return toAjax(usersService.deleteUsersByIds(ids));
//    }
}
