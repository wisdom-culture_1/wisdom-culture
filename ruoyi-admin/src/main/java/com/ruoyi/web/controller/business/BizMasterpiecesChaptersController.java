package com.ruoyi.web.controller.business;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.entity.MasterpiecesChapters;
import com.ruoyi.business.service.IMasterpiecesChaptersService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 名著章节Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/system/chapters")
public class BizMasterpiecesChaptersController extends BaseController
{
    @Autowired
    private IMasterpiecesChaptersService masterpiecesChaptersService;

    /**
     * 查询名著章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:chapters:list')")
    @GetMapping("/list")
    public TableDataInfo list(MasterpiecesChapters masterpiecesChapters)
    {
        startPage();
        List<MasterpiecesChapters> list = masterpiecesChaptersService.selectMasterpiecesChaptersList(masterpiecesChapters);
        return getDataTable(list);
    }

    /**
     * 导出名著章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:chapters:export')")
    @Log(title = "名著章节", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MasterpiecesChapters masterpiecesChapters)
    {
        List<MasterpiecesChapters> list = masterpiecesChaptersService.selectMasterpiecesChaptersList(masterpiecesChapters);
        ExcelUtil<MasterpiecesChapters> util = new ExcelUtil<MasterpiecesChapters>(MasterpiecesChapters.class);
        util.exportExcel(response, list, "名著章节数据");
    }

    /**
     * 获取名著章节详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:chapters:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(masterpiecesChaptersService.selectMasterpiecesChaptersById(id));
    }

    /**
     * 新增名著章节
     */
    @PreAuthorize("@ss.hasPermi('system:chapters:add')")
    @Log(title = "名著章节", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MasterpiecesChapters masterpiecesChapters)
    {
        return toAjax(masterpiecesChaptersService.insertMasterpiecesChapters(masterpiecesChapters));
    }

    /**
     * 修改名著章节
     */
    @PreAuthorize("@ss.hasPermi('system:chapters:edit')")
    @Log(title = "名著章节", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MasterpiecesChapters masterpiecesChapters)
    {
        return toAjax(masterpiecesChaptersService.updateMasterpiecesChapters(masterpiecesChapters));
    }

    /**
     * 删除名著章节
     */
    @PreAuthorize("@ss.hasPermi('system:chapters:remove')")
    @Log(title = "名著章节", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(masterpiecesChaptersService.deleteMasterpiecesChaptersByIds(ids));
    }
}
