package com.ruoyi.web.controller.tool.system;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.ruoyi.web.controller.constants.RedisKey;
import com.ruoyi.web.controller.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.RegisterBody;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.SysRegisterService;
import com.ruoyi.system.service.ISysConfigService;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

/**
 * 注册验证
 * 
 * @author ruoyi
 */
@RestController
@Api(tags="注册验证")
public class SysRegisterController extends BaseController
{
    @Autowired
    private SysRegisterService registerService;

    @Autowired
    private ISysConfigService configService;

    @Value("${sms.aliyun_sms_ak}")
    private String smsAk;

    @Value("${sms.aliyun_sms_sk}")
    private String smsSk;

    @Value("${sms.aliyun_sms_endpoint}")
    private String smsEndpoint;

    @Value("${sms.aliyun_sms_signname}")
    private String smsSignname;

    @Value("${sms.aliyun_sms_template_code}")
    private String smsTemplateCode;

    private static final long CODE_EXPIRED = 1000 * 60 * 10;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping("/auth/register")
    @ApiOperation("注册验证")
    public AjaxResult register(@RequestBody RegisterBody user)
    {
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
        {
            return error("当前系统没有开启注册功能！");
        }
        String msg = registerService.register(user);
        return StringUtils.isEmpty(msg) ? AjaxResult.newSuccess() : AjaxResult.newError(msg);
    }

    @PostMapping("/auth/sendCode")
    @ApiOperation("发送短信验证码")
    public AjaxResult sendCodeMethod(@NotNull(message = "手机号码是必填项")  String phoneNumber) throws Exception {
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
        {
            return error("当前系统没有开启注册功能！");
        }


        String cacheKey = String.format(RedisKey.CHECK_CODE_KEY,phoneNumber);
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);

        // 如果不为空，再判断是否是60秒以内重复发送，value格式 0122_8735679
        if(StringUtils.isNotBlank(cacheValue)){
            long ttl = Long.parseLong(cacheValue.split("_")[1]);
            // 当前时间戳-验证码发送的时间戳，如果小于60秒，则不给重复发送
            long leftTime = CommonUtil.getCurrentTimestamp() - ttl;
            if(leftTime < (1000 * 60)){
                logger.info("重复发送短信验证码，时间间隔：{}秒",leftTime);
                return AjaxResult.newError("验证码发送过快,60秒内不允许重复发送！");
            }
        }

        sendCode(phoneNumber);
        return AjaxResult.newSuccess();
    }

    public Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new com.aliyun.teaopenapi.models.Config()
                // 必填，您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 必填，您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = smsEndpoint;
        return new Client(config);
    }


    public void sendCode(String phoneNumber) throws Exception {
        String cacheKey = String.format(RedisKey.CHECK_CODE_KEY, phoneNumber);
        String code = CommonUtil.getRandomCode(6);
        //生成拼接好的验证码
        String value = code + "_" + CommonUtil.getCurrentTimestamp();
        redisTemplate.opsForValue().set(cacheKey,value,CODE_EXPIRED,TimeUnit.MILLISECONDS);
        String templateContent = "{\"code\":\"----\"}";
        templateContent = templateContent.replace("----",code);
        // 工程代码泄露可能会导致AccessKey泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378657.html
        Client client = createClient(smsAk,smsSk);
        try {
            // 复制代码运行请自行打印 API 的返回值
            SendSmsRequest sendSmsRequest = new SendSmsRequest()
                    .setSignName(smsSignname)
                    .setTemplateCode(smsTemplateCode)
                    .setPhoneNumbers(phoneNumber)
                    .setTemplateParam(templateContent);

            SendSmsResponse sendSmsResponse = client.sendSms(sendSmsRequest);
            System.out.println(sendSmsResponse.getBody());
        } catch (TeaException error) {
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
    }
}
