package com.ruoyi.web.controller.business;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.entity.ZdZbh;
import com.ruoyi.business.service.IZdZbhService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/system/zbh")
public class BizZdZbhController extends BaseController
{
    @Autowired
    private IZdZbhService zdZbhService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:zbh:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZdZbh zdZbh)
    {
        startPage();
        List<ZdZbh> list = zdZbhService.selectZdZbhList(zdZbh);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:zbh:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ZdZbh zdZbh)
    {
        List<ZdZbh> list = zdZbhService.selectZdZbhList(zdZbh);
        ExcelUtil<ZdZbh> util = new ExcelUtil<ZdZbh>(ZdZbh.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:zbh:query')")
    @GetMapping(value = "/{xuhao}")
    public AjaxResult getInfo(@PathVariable("xuhao") Long xuhao)
    {
        return success(zdZbhService.selectZdZbhByXuhao(xuhao));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:zbh:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ZdZbh zdZbh)
    {
        return toAjax(zdZbhService.insertZdZbh(zdZbh));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:zbh:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ZdZbh zdZbh)
    {
        return toAjax(zdZbhService.updateZdZbh(zdZbh));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:zbh:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{xuhaos}")
    public AjaxResult remove(@PathVariable Long[] xuhaos)
    {
        return toAjax(zdZbhService.deleteZdZbhByXuhaos(xuhaos));
    }
}
