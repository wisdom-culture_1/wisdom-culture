package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.InitialConsonant;
import com.ruoyi.system.service.IInitialConsonantService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 声母 韵母Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/consonant")
public class InitialConsonantController extends BaseController
{
    @Autowired
    private IInitialConsonantService initialConsonantService;

    /**
     * 查询声母 韵母列表
     */
    @PreAuthorize("@ss.hasPermi('system:consonant:list')")
    @GetMapping("/list")
    public TableDataInfo list(InitialConsonant initialConsonant)
    {
        startPage();
        List<InitialConsonant> list = initialConsonantService.selectInitialConsonantList(initialConsonant);
        return getDataTable(list);
    }

    /**
     * 导出声母 韵母列表
     */
    @PreAuthorize("@ss.hasPermi('system:consonant:export')")
    @Log(title = "声母 韵母", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InitialConsonant initialConsonant)
    {
        List<InitialConsonant> list = initialConsonantService.selectInitialConsonantList(initialConsonant);
        ExcelUtil<InitialConsonant> util = new ExcelUtil<InitialConsonant>(InitialConsonant.class);
        util.exportExcel(response, list, "声母 韵母数据");
    }

    /**
     * 获取声母 韵母详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:consonant:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(initialConsonantService.selectInitialConsonantById(id));
    }

    /**
     * 新增声母 韵母
     */
    @PreAuthorize("@ss.hasPermi('system:consonant:add')")
    @Log(title = "声母 韵母", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InitialConsonant initialConsonant)
    {
        return toAjax(initialConsonantService.insertInitialConsonant(initialConsonant));
    }

    /**
     * 修改声母 韵母
     */
    @PreAuthorize("@ss.hasPermi('system:consonant:edit')")
    @Log(title = "声母 韵母", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InitialConsonant initialConsonant)
    {
        return toAjax(initialConsonantService.updateInitialConsonant(initialConsonant));
    }

    /**
     * 删除声母 韵母
     */
    @PreAuthorize("@ss.hasPermi('system:consonant:remove')")
    @Log(title = "声母 韵母", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(initialConsonantService.deleteInitialConsonantByIds(ids));
    }
}
