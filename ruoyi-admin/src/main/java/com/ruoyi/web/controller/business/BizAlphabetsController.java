package com.ruoyi.web.controller.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.Alphabets;
import com.ruoyi.business.service.IAlphabetsService;

/**
 * 字母Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizAlphabetsController extends BaseController
{
    @Autowired
    private IAlphabetsService alphabetsService;

    /**
     * 查询字母列表
     */
//    @PreAuthorize("@ss.hasPermi('system:alphabets:list')")
    @GetMapping("/alphabet")
    public AjaxResult list(Alphabets alphabets)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<Alphabets> list = alphabetsService.selectAlphabetsList(alphabets);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出字母列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:alphabets:export')")
//    @Log(title = "字母", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, Alphabets alphabets)
//    {
//        List<Alphabets> list = alphabetsService.selectAlphabetsList(alphabets);
//        ExcelUtil<Alphabets> util = new ExcelUtil<Alphabets>(Alphabets.class);
//        util.exportExcel(response, list, "字母数据");
//    }
//
//    /**
//     * 获取字母详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:alphabets:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(alphabetsService.selectAlphabetsById(id));
//    }
//
//    /**
//     * 新增字母
//     */
//    @PreAuthorize("@ss.hasPermi('system:alphabets:add')")
//    @Log(title = "字母", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody Alphabets alphabets)
//    {
//        return toAjax(alphabetsService.insertAlphabets(alphabets));
//    }
//
//    /**
//     * 修改字母
//     */
//    @PreAuthorize("@ss.hasPermi('system:alphabets:edit')")
//    @Log(title = "字母", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody Alphabets alphabets)
//    {
//        return toAjax(alphabetsService.updateAlphabets(alphabets));
//    }
//
//    /**
//     * 删除字母
//     */
//    @PreAuthorize("@ss.hasPermi('system:alphabets:remove')")
//    @Log(title = "字母", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(alphabetsService.deleteAlphabetsByIds(ids));
//    }
}
