package com.ruoyi.web.controller.tool.system;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.ruoyi.business.entity.Users;
import com.ruoyi.business.service.IUsersService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.user.CaptchaException;
import com.ruoyi.common.exception.user.CaptchaExpireException;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.framework.manager.factory.AsyncFactory;
import com.ruoyi.framework.web.service.SysRegisterService;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.web.controller.constants.ErrorCode;
import com.ruoyi.web.controller.constants.RedisKey;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.framework.web.service.SysPermissionService;
import com.ruoyi.system.service.ISysMenuService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录验证
 * 
 * @author ruoyi
 */
@RestController
@Api(tags="登录验证")

public class SysLoginController extends BaseController
{
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private LogoutSuccessHandler handler;


    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private IUsersService usersService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    private static final Logger log = LoggerFactory.getLogger(SysLoginController.class);

    @Autowired
    private SysUserMapper userMapper;

    /**
     * 登录方法
     * 
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/auth/login")
    @ApiOperation("登陆")
    public AjaxResult login(@RequestBody LoginBody loginBody)
    {
        AjaxResult ajax = AjaxResult.newSuccess();
//        // 校验短信验证码
//        String cacheKey = String.format(RedisKey.CHECK_CODE_KEY,loginBody.getCode());
//        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
//
//        if(StringUtils.isBlank(cacheValue)){
//            ajax.put("message","短信验证码已失效，请重新获取！");
//            ajax.put("status",ErrorCode.MESSAGE_CODE_ERROR);
//            return ajax;
//        }
//
//        if(StringUtils.isNotEmpty(cacheValue)){
//            String messageCodeCache = cacheValue.split("_")[0];
//            if(!loginBody.getMessageCode().equals(messageCodeCache)){
//                ajax.put("message","短信验证码已失效，请重新获取！");
//                ajax.put("status",ErrorCode.MESSAGE_CODE_ERROR);
//                return ajax;
//            }
//        }

        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword());
        ajax.put(Constants.TOKEN, token);
        ajax.put(Constants.USERNAME, loginBody.getUsername());

        SysUser sysUser = userMapper.selectUserByUserNameOrPhone(loginBody.getUsername());
        Date date = new Date();
        if(date.compareTo(sysUser.getExpirationTime()) >0){
            ajax.put("message","会员权益已到期！");
            ajax.put("status",ErrorCode.CODE_EXPIRE_TIME);
            return ajax;
        }

        if(StringUtils.isEmpty(sysUser.getPhonenumber())){
            ajax.put("message","请绑定手机号");
            ajax.put("status",ErrorCode.NOT_BIND_PHONE_NUMBER);
            return ajax;
        }

        return ajax;
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code 验证码
     * @return 结果
     */
    public void validateCaptcha(String username, String code)
    {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.nvl(username, "");
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
    }

    /**
     * 修改用户
     */
//    @Log(title = "忘记密码", businessType = BusinessType.UPDATE)
    @PostMapping("/auth/retrieve_password")
    @ApiOperation("忘记密码")
    public AjaxResult updateProfile(@RequestBody LoginBody loginBody)
    {

        // 1、校验手机号是否存在
        SysUser sysUser = userMapper.selectUserByUserNameOrPhone(loginBody.getUsername());
        if(sysUser == null){
            return error("用户不存在！");
        }

        // 2、校验用户手机号和验证码是否正确
        String cacheKey = String.format(com.ruoyi.framework.constants.RedisKey.CHECK_CODE_KEY,loginBody.getUsername());
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
        log.info("update password---------"+cacheValue);

        // 如果不为空，再判断是否是60秒以内重复发送，value格式 0122_8735679
        if(StringUtils.isNotBlank(cacheValue)){
            if(!loginBody.getMessageCode().equals(cacheValue.split("_")[0])){
                log.info("短信验证码错误！");
                return error("短信验证码错误");
            }else{
                redisTemplate.delete(cacheKey);
            }
        }else{
            return error("请输入正确的验证码!");
        }
        sysUser.setUserName(sysUser.getUserName());
        String encryptPassword = SecurityUtils.encryptPassword(loginBody.getPassword());
        sysUser.setUserId(sysUser.getUserId());
        sysUser.setPassword(encryptPassword);
        log.info("update password encryptPassword is :{}",encryptPassword);
        sysUserService.updateUserProfile(sysUser);

        Users users = new Users();
        users.setUsername(sysUser.getUserName());
        users.setUpdateBy(sysUser.getUserName());
        users.setUpdatedAt(new Date());
        users.setPassword(encryptPassword);
        usersService.updateUsersByUserName(users);
        log.info("update password success ---------");

        return AjaxResult.newSuccess();
    }

    /**
     * 退出登录
     *
     * @param
     * @return 结果
     */
    @ApiOperation("退出登录")
    @PostMapping("/account/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        handler.onLogoutSuccess(request,response,null);
    }


    /**
     * 获取用户信息
     * 
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo()
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    /**
     * 获取路由信息
     * 
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public AjaxResult getRouters()
    {
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(userId);
        return AjaxResult.success(menuService.buildMenus(menus));
    }
}
