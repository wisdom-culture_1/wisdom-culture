package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MasterpiecesBook;
import com.ruoyi.system.service.IMasterpiecesBookService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 名著书籍Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/masterpieces_book")
public class MasterpiecesBookController extends BaseController
{
    @Autowired
    private IMasterpiecesBookService masterpiecesBookService;

    /**
     * 查询名著书籍列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_book:list')")
    @GetMapping("/list")
    public TableDataInfo list(MasterpiecesBook masterpiecesBook)
    {
        startPage();
        List<MasterpiecesBook> list = masterpiecesBookService.selectMasterpiecesBookList(masterpiecesBook);
        return getDataTable(list);
    }

    /**
     * 导出名著书籍列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_book:export')")
    @Log(title = "名著书籍", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MasterpiecesBook masterpiecesBook)
    {
        List<MasterpiecesBook> list = masterpiecesBookService.selectMasterpiecesBookList(masterpiecesBook);
        ExcelUtil<MasterpiecesBook> util = new ExcelUtil<MasterpiecesBook>(MasterpiecesBook.class);
        util.exportExcel(response, list, "名著书籍数据");
    }

    /**
     * 获取名著书籍详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_book:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(masterpiecesBookService.selectMasterpiecesBookById(id));
    }

    /**
     * 新增名著书籍
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_book:add')")
    @Log(title = "名著书籍", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MasterpiecesBook masterpiecesBook)
    {
        return toAjax(masterpiecesBookService.insertMasterpiecesBook(masterpiecesBook));
    }

    /**
     * 修改名著书籍
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_book:edit')")
    @Log(title = "名著书籍", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MasterpiecesBook masterpiecesBook)
    {
        return toAjax(masterpiecesBookService.updateMasterpiecesBook(masterpiecesBook));
    }

    /**
     * 删除名著书籍
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_book:remove')")
    @Log(title = "名著书籍", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(masterpiecesBookService.deleteMasterpiecesBookByIds(ids));
    }
}
