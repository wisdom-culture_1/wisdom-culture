package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ChallengeGroup;
import com.ruoyi.system.service.IChallengeGroupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 挑战组别Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/challenge_group")
public class ChallengeGroupController extends BaseController
{
    @Autowired
    private IChallengeGroupService challengeGroupService;

    /**
     * 查询挑战组别列表
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_group:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChallengeGroup challengeGroup)
    {
        startPage();
        List<ChallengeGroup> list = challengeGroupService.selectChallengeGroupList(challengeGroup);
        return getDataTable(list);
    }

    /**
     * 导出挑战组别列表
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_group:export')")
    @Log(title = "挑战组别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChallengeGroup challengeGroup)
    {
        List<ChallengeGroup> list = challengeGroupService.selectChallengeGroupList(challengeGroup);
        ExcelUtil<ChallengeGroup> util = new ExcelUtil<ChallengeGroup>(ChallengeGroup.class);
        util.exportExcel(response, list, "挑战组别数据");
    }

    /**
     * 获取挑战组别详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_group:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(challengeGroupService.selectChallengeGroupById(id));
    }

    /**
     * 新增挑战组别
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_group:add')")
    @Log(title = "挑战组别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChallengeGroup challengeGroup)
    {
        return toAjax(challengeGroupService.insertChallengeGroup(challengeGroup));
    }

    /**
     * 修改挑战组别
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_group:edit')")
    @Log(title = "挑战组别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChallengeGroup challengeGroup)
    {
        return toAjax(challengeGroupService.updateChallengeGroup(challengeGroup));
    }

    /**
     * 删除挑战组别
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_group:remove')")
    @Log(title = "挑战组别", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(challengeGroupService.deleteChallengeGroupByIds(ids));
    }
}
