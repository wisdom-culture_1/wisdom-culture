package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Alphabets;
import com.ruoyi.system.service.IAlphabetsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 字母Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/alphabets")
public class AlphabetsController extends BaseController
{
    @Autowired
    private IAlphabetsService alphabetsService;

    /**
     * 查询字母列表
     */
    @PreAuthorize("@ss.hasPermi('system:alphabets:list')")
    @GetMapping("/list")
    public TableDataInfo list(Alphabets alphabets)
    {
        startPage();
        List<Alphabets> list = alphabetsService.selectAlphabetsList(alphabets);
        return getDataTable(list);
    }

    /**
     * 导出字母列表
     */
    @PreAuthorize("@ss.hasPermi('system:alphabets:export')")
    @Log(title = "字母", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Alphabets alphabets)
    {
        List<Alphabets> list = alphabetsService.selectAlphabetsList(alphabets);
        ExcelUtil<Alphabets> util = new ExcelUtil<Alphabets>(Alphabets.class);
        util.exportExcel(response, list, "字母数据");
    }

    /**
     * 获取字母详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:alphabets:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(alphabetsService.selectAlphabetsById(id));
    }

    /**
     * 新增字母
     */
    @PreAuthorize("@ss.hasPermi('system:alphabets:add')")
    @Log(title = "字母", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Alphabets alphabets)
    {
        return toAjax(alphabetsService.insertAlphabets(alphabets));
    }

    /**
     * 修改字母
     */
    @PreAuthorize("@ss.hasPermi('system:alphabets:edit')")
    @Log(title = "字母", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Alphabets alphabets)
    {
        return toAjax(alphabetsService.updateAlphabets(alphabets));
    }

    /**
     * 删除字母
     */
    @PreAuthorize("@ss.hasPermi('system:alphabets:remove')")
    @Log(title = "字母", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(alphabetsService.deleteAlphabetsByIds(ids));
    }
}
