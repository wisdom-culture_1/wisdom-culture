package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MasterpiecesType;
import com.ruoyi.system.service.IMasterpiecesTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 名著阅读分类Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/masterpieces_type")
public class MasterpiecesTypeController extends BaseController
{
    @Autowired
    private IMasterpiecesTypeService masterpiecesTypeService;

    /**
     * 查询名著阅读分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_type:list')")
    @GetMapping("/list")
    public TableDataInfo list(MasterpiecesType masterpiecesType)
    {
        startPage();
        List<MasterpiecesType> list = masterpiecesTypeService.selectMasterpiecesTypeList(masterpiecesType);
        return getDataTable(list);
    }

    /**
     * 导出名著阅读分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_type:export')")
    @Log(title = "名著阅读分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MasterpiecesType masterpiecesType)
    {
        List<MasterpiecesType> list = masterpiecesTypeService.selectMasterpiecesTypeList(masterpiecesType);
        ExcelUtil<MasterpiecesType> util = new ExcelUtil<MasterpiecesType>(MasterpiecesType.class);
        util.exportExcel(response, list, "名著阅读分类数据");
    }

    /**
     * 获取名著阅读分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_type:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(masterpiecesTypeService.selectMasterpiecesTypeById(id));
    }

    /**
     * 新增名著阅读分类
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_type:add')")
    @Log(title = "名著阅读分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MasterpiecesType masterpiecesType)
    {
        return toAjax(masterpiecesTypeService.insertMasterpiecesType(masterpiecesType));
    }

    /**
     * 修改名著阅读分类
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_type:edit')")
    @Log(title = "名著阅读分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MasterpiecesType masterpiecesType)
    {
        return toAjax(masterpiecesTypeService.updateMasterpiecesType(masterpiecesType));
    }

    /**
     * 删除名著阅读分类
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_type:remove')")
    @Log(title = "名著阅读分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(masterpiecesTypeService.deleteMasterpiecesTypeByIds(ids));
    }
}
