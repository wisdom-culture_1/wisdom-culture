package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.ExpertLectureHallType;
import com.ruoyi.business.service.IExpertLectureHallTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 专家讲堂分类Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/lecture")
public class BizExpertLectureHallTypeController extends BaseController
{
    @Autowired
    private IExpertLectureHallTypeService expertLectureHallTypeService;

    /**
     * 查询专家讲堂分类列表
     */
//    @PreAuthorize("@ss.hasPermi('system:type:list')")
    @GetMapping("/type")
    public AjaxResult list(ExpertLectureHallType expertLectureHallType)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ExpertLectureHallType> list = expertLectureHallTypeService.selectExpertLectureHallTypeList(expertLectureHallType);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出专家讲堂分类列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:export')")
//    @Log(title = "专家讲堂分类", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ExpertLectureHallType expertLectureHallType)
//    {
//        List<ExpertLectureHallType> list = expertLectureHallTypeService.selectExpertLectureHallTypeList(expertLectureHallType);
//        ExcelUtil<ExpertLectureHallType> util = new ExcelUtil<ExpertLectureHallType>(ExpertLectureHallType.class);
//        util.exportExcel(response, list, "专家讲堂分类数据");
//    }
//
//    /**
//     * 获取专家讲堂分类详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(expertLectureHallTypeService.selectExpertLectureHallTypeById(id));
//    }
//
//    /**
//     * 新增专家讲堂分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:add')")
//    @Log(title = "专家讲堂分类", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ExpertLectureHallType expertLectureHallType)
//    {
//        return toAjax(expertLectureHallTypeService.insertExpertLectureHallType(expertLectureHallType));
//    }
//
//    /**
//     * 修改专家讲堂分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:edit')")
//    @Log(title = "专家讲堂分类", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ExpertLectureHallType expertLectureHallType)
//    {
//        return toAjax(expertLectureHallTypeService.updateExpertLectureHallType(expertLectureHallType));
//    }
//
//    /**
//     * 删除专家讲堂分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:remove')")
//    @Log(title = "专家讲堂分类", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(expertLectureHallTypeService.deleteExpertLectureHallTypeByIds(ids));
//    }
}
