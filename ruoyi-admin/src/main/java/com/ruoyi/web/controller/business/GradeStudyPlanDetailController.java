package com.ruoyi.web.controller.business;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.entity.GradePlan;
import com.ruoyi.business.entity.GradeStudyPlan;
import com.ruoyi.business.entity.GradeStudyPlanDetailReq;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.entity.GradeStudyPlanDetail;
import com.ruoyi.business.service.IGradeStudyPlanDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学习计划（细化到文章）Controller
 * 
 * @author ruoyi
 * @date 2024-06-01
 */
@RestController
@RequestMapping("/system/detail")
public class GradeStudyPlanDetailController extends BaseController
{
    @Autowired
    private IGradeStudyPlanDetailService gradeStudyPlanDetailService;

    /**
     * 查询学习计划（细化到文章）列表
     */
    @PreAuthorize("@ss.hasPermi('system:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(GradeStudyPlanDetail gradeStudyPlanDetail)
    {
        startPage();
        List<GradeStudyPlanDetail> list = gradeStudyPlanDetailService.selectGradeStudyPlanDetailList(gradeStudyPlanDetail);
        return getDataTable(list);
    }

    /**
     * 导出学习计划（细化到文章）列表
     */
    @PreAuthorize("@ss.hasPermi('system:detail:export')")
    @PostMapping("/export")
    public void export(HttpServletResponse response, GradeStudyPlanDetail gradeStudyPlanDetail)
    {
        List<GradeStudyPlanDetail> list = gradeStudyPlanDetailService.selectGradeStudyPlanDetailList(gradeStudyPlanDetail);
        ExcelUtil<GradeStudyPlanDetail> util = new ExcelUtil<GradeStudyPlanDetail>(GradeStudyPlanDetail.class);
        util.exportExcel(response, list, "学习计划（细化到文章）数据");
    }

    /**
     * 获取学习计划（细化到文章）详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(gradeStudyPlanDetailService.selectGradeStudyPlanDetailById(id));
    }

    /**
     * 新增学习计划（细化到文章）
     */
    @PreAuthorize("@ss.hasPermi('system:detail:add')")
    @PostMapping
    public AjaxResult add(@RequestBody GradeStudyPlanDetail gradeStudyPlanDetail)
    {
        return toAjax(gradeStudyPlanDetailService.insertGradeStudyPlanDetail(gradeStudyPlanDetail));
    }

    /**
     * 修改学习计划（细化到文章）
     */
    @PreAuthorize("@ss.hasPermi('system:detail:edit')")
    @PutMapping
    public AjaxResult edit(@RequestBody GradeStudyPlanDetail gradeStudyPlanDetail)
    {
        return toAjax(gradeStudyPlanDetailService.updateGradeStudyPlanDetail(gradeStudyPlanDetail));
    }

    /**
     * 删除学习计划（细化到文章）
     */
    @PreAuthorize("@ss.hasPermi('system:detail:remove')")
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(gradeStudyPlanDetailService.deleteGradeStudyPlanDetailByIds(ids));
    }

    /**
     * 修改学习计划（细化到文章）
     */
    @Transactional
    @PostMapping("/update")
    public AjaxResult add(@RequestBody GradeStudyPlanDetailReq req)
    {
        GradeStudyPlanDetail gradeStudyPlanDetailDel = new GradeStudyPlanDetail();
        BeanUtils.copyProperties(req, gradeStudyPlanDetailDel);
        gradeStudyPlanDetailService.deleteGradeStudyPlanDetailByGrade(gradeStudyPlanDetailDel);
        List<String> detailIds = req.getDetailIds();
        if(CollectionUtils.isNotEmpty(req.getDetailIds())){
            for (String item : detailIds) {
                GradeStudyPlanDetail detail = new GradeStudyPlanDetail();
                detail.setGradeId(req.getGradeId());
                detail.setModule(req.getModule());
                detail.setChildModule(req.getChildModule());
                detail.setDetailId(item);
                // 如果数据库中已经存在相同数据则跳过此轮
                if(!gradeStudyPlanDetailService.selectGradeStudyPlanDetailList(detail).isEmpty()){
                    continue;
                }
                gradeStudyPlanDetailService.insertGradeStudyPlanDetail(detail);
            }
        }
        return success();
    }

}
