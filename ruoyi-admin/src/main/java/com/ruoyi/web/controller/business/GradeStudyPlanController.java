package com.ruoyi.web.controller.business;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.entity.GradePlan;
import com.ruoyi.business.entity.GradeStudyPlan;
import com.ruoyi.business.entity.ModuleDict;
import com.ruoyi.business.mapper.ModuleDictMapper;
import com.ruoyi.business.service.IGradeStudyPlanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

/**
 * 学习计划Controller
 * 
 * @author ruoyi
 * @date 2024-04-14
 */
@RestController
@RequestMapping("/system/plan")
public class GradeStudyPlanController extends BaseController
{
    @Autowired
    private IGradeStudyPlanService gradeStudyPlanService;
    @Autowired
    private ModuleDictMapper moduleDictMapper;


    //    判断模块是否可以手动调整显示的内容
    private static Map<String, String> DYNAMIC_DETAILS  = new HashMap<>();
    static{
//        判断模块是否可以手动调整显示的内容 y:1 n:0
        DYNAMIC_DETAILS.put("1_1","0");
        DYNAMIC_DETAILS.put("1_2","0");
        DYNAMIC_DETAILS.put("1_3","0");

        DYNAMIC_DETAILS.put("2_1","0");
        DYNAMIC_DETAILS.put("2_2","0");
//        LIGHT_RECORDS.put("2_2","汉字起源");
//        LIGHT_RECORDS.put("2_3","成语典故");

//        LIGHT_RECORDS.put("3_1","文言名篇");
//        LIGHT_RECORDS.put("3_2","中华文化");
//        LIGHT_RECORDS.put("3_3","诗词歌赋");
        DYNAMIC_DETAILS.put("3_1","1");
        DYNAMIC_DETAILS.put("3_2","1");
        DYNAMIC_DETAILS.put("3_3","1");
        DYNAMIC_DETAILS.put("3_4","1");
        DYNAMIC_DETAILS.put("3_5", "1");
        DYNAMIC_DETAILS.put("3_6", "1");
        DYNAMIC_DETAILS.put("3_7", "1");
        DYNAMIC_DETAILS.put("3_8", "1");
        DYNAMIC_DETAILS.put("3_9", "1");
        DYNAMIC_DETAILS.put("3_10","1");
        DYNAMIC_DETAILS.put("3_11","1");
        DYNAMIC_DETAILS.put("3_12","1");
        DYNAMIC_DETAILS.put("3_13","1");
        DYNAMIC_DETAILS.put("3_14","1");
        DYNAMIC_DETAILS.put("3_15","1");
        DYNAMIC_DETAILS.put("3_16","1");
        DYNAMIC_DETAILS.put("3_17","1");
        DYNAMIC_DETAILS.put("3_18","1");
        DYNAMIC_DETAILS.put("3_19","1");
        DYNAMIC_DETAILS.put("3_20","1");
        DYNAMIC_DETAILS.put("3_21","1");
        DYNAMIC_DETAILS.put("3_22","1");
        DYNAMIC_DETAILS.put("3_23","1");
        DYNAMIC_DETAILS.put("3_24","1");
        DYNAMIC_DETAILS.put("3_25","1");
        DYNAMIC_DETAILS.put("3_26","1");
        DYNAMIC_DETAILS.put("3_27","1");
        DYNAMIC_DETAILS.put("3_28","1");

//        LIGHT_RECORDS.put("4_1", "专家讲堂");
//        LIGHT_RECORDS.put("4_2", "经典赏析");
//        LIGHT_RECORDS.put("4_3", "北大直播");
        DYNAMIC_DETAILS.put("4_1", "0");
        DYNAMIC_DETAILS.put("4_2", "0");
        DYNAMIC_DETAILS.put("4_3", "0");
        DYNAMIC_DETAILS.put("4_4", "0");
        DYNAMIC_DETAILS.put("4_5", "0");
        DYNAMIC_DETAILS.put("4_6", "0");
        DYNAMIC_DETAILS.put("4_7", "0");
        DYNAMIC_DETAILS.put("4_8", "0");
        DYNAMIC_DETAILS.put("4_9", "0");
        DYNAMIC_DETAILS.put("4_10","0");
    }

    /**
     * 查询学习计划列表
     */
//    @PreAuthorize("@ss.hasPermi('system:plan:list')")
    @PostMapping("/list")
    public AjaxResult list(@RequestBody GradeStudyPlan gradeStudyPlan)
    {
        List<GradeStudyPlan> list = gradeStudyPlanService.selectGradeStudyPlanList(gradeStudyPlan);
        List<GradePlan> plans = Lists.newArrayList();


        Map<String,GradePlan> gradePlanMap = new HashMap<>();
        list.forEach(item ->{
            if(gradePlanMap.containsKey(item.getGradeId()+"_"+item.getModule())){
                GradePlan gradePlan = gradePlanMap.get(item.getGradeId()+"_"+item.getModule());
                gradePlan.getChildModule().add(item.getChildModule());
//                String isDynamic = DYNAMIC_DETAILS.get(item.getModule() + "_" + item.getChildModule());

                ModuleDict selModuleDict = new ModuleDict();
                Long isDynamic = null;
                selModuleDict.setModule(item.getModule());
                selModuleDict.setChildModule(item.getChildModule());
                List<ModuleDict> moduleDictList = moduleDictMapper.selectModuleDictList(selModuleDict);
                if (!org.springframework.util.CollectionUtils.isEmpty(moduleDictList)){
                    isDynamic = moduleDictList.get(0).getDynamic();
                }

                if (isDynamic != null && isDynamic == 1L){gradePlan.getIsDynamic().add(item.getChildModule());}
            }else{
                GradePlan plan = new GradePlan();
                plan.setGradeId(item.getGradeId());
                plan.setGradeName(item.getGradeName());
                plan.setModule(item.getModule());
                plan.getChildModule().add(item.getChildModule());
//                String isDynamic = DYNAMIC_DETAILS.get(item.getModule() + "_" + item.getChildModule());

                ModuleDict selModuleDict = new ModuleDict();
                Long isDynamic = null;
                selModuleDict.setModule(item.getModule());
                selModuleDict.setChildModule(item.getChildModule());
                List<ModuleDict> moduleDictList = moduleDictMapper.selectModuleDictList(selModuleDict);
                if (!org.springframework.util.CollectionUtils.isEmpty(moduleDictList)){
                    isDynamic = moduleDictList.get(0).getDynamic();
                }

                if (isDynamic != null && isDynamic == 1L){plan.getIsDynamic().add(item.getChildModule());}
                plan.setModuleNickname(item.getModuleNickname());
                plan.setPic(item.getPic());
                gradePlanMap.put(item.getGradeId()+"_"+item.getModule(),plan);
                plans.add(plan);
            }
        });

        return success(plans);
    }

    /**
     * 导出学习计划列表
     */
//    @PreAuthorize("@ss.hasPermi('system:plan:export')")
    @Log(title = "学习计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GradeStudyPlan gradeStudyPlan)
    {
        List<GradeStudyPlan> list = gradeStudyPlanService.selectGradeStudyPlanList(gradeStudyPlan);
        ExcelUtil<GradeStudyPlan> util = new ExcelUtil<GradeStudyPlan>(GradeStudyPlan.class);
        util.exportExcel(response, list, "学习计划数据");
    }

    /**
     * 获取学习计划详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:plan:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(gradeStudyPlanService.selectGradeStudyPlanById(id));
    }

    /**
     * 新增学习计划
     */
//    @PreAuthorize("@ss.hasPermi('system:plan:add')")
    @Log(title = "学习计划", businessType = BusinessType.INSERT)
    @PostMapping("/insert")
    public AjaxResult add(@RequestBody GradePlan gradePlan)
    {
        gradeStudyPlanService.deleteGradeStudyPlanByGrade(gradePlan);
        if(gradePlan != null && CollectionUtils.isNotEmpty(gradePlan.getChildModule())){
            gradePlan.getChildModule().forEach(item ->{
                GradeStudyPlan plan = new GradeStudyPlan();
                plan.setGradeId(gradePlan.getGradeId());
                plan.setGradeName(gradePlan.getGradeName());
                plan.setModule(gradePlan.getModule());
                plan.setChildModule(item);
                plan.setModuleNickname(gradePlan.getModuleNickname());
                plan.setPic(gradePlan.getPic());
                plan.setCreateTime(new Date());
                plan.setUpdateTime(new Date());
                gradeStudyPlanService.insertGradeStudyPlan(plan);
            });
        }
        return success();
    }

    /**
     * 修改学习计划
     */
//    @PreAuthorize("@ss.hasPermi('system:plan:edit')")
    @Log(title = "学习计划", businessType = BusinessType.UPDATE)
    @PostMapping("update")
    public AjaxResult edit(@RequestBody GradePlan gradePlan)
    {
        gradeStudyPlanService.deleteGradeStudyPlanByGrade(gradePlan);
        if(gradePlan != null && CollectionUtils.isNotEmpty(gradePlan.getChildModule())){
            gradePlan.getChildModule().forEach(item ->{
                GradeStudyPlan plan = new GradeStudyPlan();
                plan.setGradeId(gradePlan.getGradeId());
                plan.setGradeName(gradePlan.getGradeName());
                plan.setModule(gradePlan.getModule());
                plan.setChildModule(item);
                plan.setPic(gradePlan.getPic());
                plan.setCreateTime(new Date());
                plan.setUpdateTime(new Date());
                plan.setModuleNickname(gradePlan.getModuleNickname());
                gradeStudyPlanService.insertGradeStudyPlan(plan);
            });
        }
        return success();
    }

    /**
     * 删除学习计划
     */
//    @PreAuthorize("@ss.hasPermi('system:plan:remove')")
    @Log(title = "学习计划", businessType = BusinessType.DELETE)
    @PostMapping("delete")
    public AjaxResult remove(@RequestBody GradePlan gradePlan)
    {
        return toAjax(gradeStudyPlanService.deleteGradeStudyPlanByGrade(gradePlan));
    }
}
