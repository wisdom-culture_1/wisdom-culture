package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.ChallengeComplete;
import com.ruoyi.business.entity.ChallengeGroup;
import com.ruoyi.business.entity.ChallengeQuestions;
import com.ruoyi.business.mapper.BizChallengeCompleteMapper;
import com.ruoyi.business.mapper.BizChallengeGroupMapper;
import com.ruoyi.business.service.IChallengeQuestionsService;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.impl.ChallengeGroupServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 其他挑战（成语诗词....）Controller
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@RestController
@RequestMapping("/challenge")
public class BizChallengeQuestionsController extends BaseController
{
    @Autowired
    private IChallengeQuestionsService challengeQuestionsService;

    @Autowired
    private BizChallengeCompleteMapper challengeCompleteMapper;

    @Autowired
    private BizChallengeGroupMapper bizChallengeGroupMapper;

    /**
     * 查询其他挑战（成语诗词....）列表
     */
//    @PreAuthorize("@ss.hasPermi('system:questions:list')")
    @GetMapping("/questions")
    public AjaxResult list(ChallengeQuestions challengeQuestions)
    {
        if(challengeQuestions.getIndex() != null && (challengeQuestions.getIndex().intValue() == 0 || challengeQuestions.getIndex().intValue() == 1)){
            AjaxResult ajaxResult = AjaxResult.newSuccess();

            List<ChallengeQuestions> list = challengeQuestionsService.selectChallengeQuestionsList(challengeQuestions);
            ajaxResult.put("data",getDataTable(list));
            return ajaxResult;
        }

//        如果index=2（第三个），根据排序查第二个的完成情况，因为前两个是特殊排序，也因为（主要是因为）原本的代码太垃圾，没法用灵活的方式查，只能暂时这样写
        // 这里为啥这么改呢，都是大于2，屎山代码就是这么来的
        if (challengeQuestions.getIndex().intValue() == 2){
            ChallengeGroup challengeGroup = new ChallengeGroup();
            challengeGroup.setTypeId(challengeQuestions.getTypeId());
            List<ChallengeGroup> challengeGroups = bizChallengeGroupMapper.selectChallengeGroupList(challengeGroup);
            if(challengeGroups.size() > 2){
                ChallengeComplete challengeComplete = new ChallengeComplete();
                challengeComplete.setuId(SecurityUtils.getUserId());
                challengeComplete.setGroupId(challengeGroups.get(1).getId());
                challengeComplete.setTypeId(challengeQuestions.getTypeId());
                ChallengeComplete CheckChallengeComplete = challengeCompleteMapper.selectLatestChallengeCompliete(challengeComplete);
                if(CheckChallengeComplete != null && CheckChallengeComplete.getAchievement().intValue() >= 60){
                    AjaxResult ajaxResult = AjaxResult.newSuccess();
                    List<ChallengeQuestions> list = challengeQuestionsService.selectChallengeQuestionsList(challengeQuestions);
                    ajaxResult.put("data",getDataTable(list));
                    return ajaxResult;
                }
                else{
                    AjaxResult ajaxResult = AjaxResult.newError("请完成上一关！");
                    return ajaxResult;
                }
            }
        }


        ChallengeComplete challengeComplete = new ChallengeComplete();
        challengeComplete.setuId(SecurityUtils.getUserId());
        challengeComplete.setGroupId(challengeQuestions.getGroupId()-1);
        challengeComplete.setTypeId(challengeQuestions.getTypeId());
        ChallengeComplete challengeComplete1 = challengeCompleteMapper.selectLatestChallengeCompliete(challengeComplete);
        if(challengeComplete1 != null && challengeComplete1.getAchievement().intValue() >= 60){
            AjaxResult ajaxResult = AjaxResult.newSuccess();

            List<ChallengeQuestions> list = challengeQuestionsService.selectChallengeQuestionsList(challengeQuestions);
            ajaxResult.put("data",getDataTable(list));
            return ajaxResult;
        }
        else{
            AjaxResult ajaxResult = AjaxResult.newError("请完成上一关！");
            return ajaxResult;
        }
    }
//
//    /**
//     * 导出其他挑战（成语诗词....）列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:questions:export')")
//    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ChallengeQuestions challengeQuestions)
//    {
//        List<ChallengeQuestions> list = challengeQuestionsService.selectChallengeQuestionsList(challengeQuestions);
//        ExcelUtil<ChallengeQuestions> util = new ExcelUtil<ChallengeQuestions>(ChallengeQuestions.class);
//        util.exportExcel(response, list, "其他挑战（成语诗词....）数据");
//    }
//
//    /**
//     * 获取其他挑战（成语诗词....）详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:questions:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(challengeQuestionsService.selectChallengeQuestionsById(id));
//    }
//
//    /**
//     * 新增其他挑战（成语诗词....）
//     */
//    @PreAuthorize("@ss.hasPermi('system:questions:add')")
//    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ChallengeQuestions challengeQuestions)
//    {
//        return toAjax(challengeQuestionsService.insertChallengeQuestions(challengeQuestions));
//    }
//
//    /**
//     * 修改其他挑战（成语诗词....）
//     */
//    @PreAuthorize("@ss.hasPermi('system:questions:edit')")
//    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ChallengeQuestions challengeQuestions)
//    {
//        return toAjax(challengeQuestionsService.updateChallengeQuestions(challengeQuestions));
//    }
//
//    /**
//     * 删除其他挑战（成语诗词....）
//     */
//    @PreAuthorize("@ss.hasPermi('system:questions:remove')")
//    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(challengeQuestionsService.deleteChallengeQuestionsByIds(ids));
//    }
}
