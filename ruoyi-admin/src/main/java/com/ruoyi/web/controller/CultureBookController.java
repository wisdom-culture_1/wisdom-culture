package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CultureBook;
import com.ruoyi.system.service.ICultureBookService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 中华文化书籍Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/culture_book")
public class CultureBookController extends BaseController
{
    @Autowired
    private ICultureBookService cultureBookService;

    /**
     * 查询中华文化书籍列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_book:list')")
    @GetMapping("/list")
    public TableDataInfo list(CultureBook cultureBook)
    {
        startPage();
        List<CultureBook> list = cultureBookService.selectCultureBookList(cultureBook);
        return getDataTable(list);
    }

    /**
     * 导出中华文化书籍列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_book:export')")
    @Log(title = "中华文化书籍", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CultureBook cultureBook)
    {
        List<CultureBook> list = cultureBookService.selectCultureBookList(cultureBook);
        ExcelUtil<CultureBook> util = new ExcelUtil<CultureBook>(CultureBook.class);
        util.exportExcel(response, list, "中华文化书籍数据");
    }

    /**
     * 获取中华文化书籍详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:culture_book:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cultureBookService.selectCultureBookById(id));
    }

    /**
     * 新增中华文化书籍
     */
    @PreAuthorize("@ss.hasPermi('system:culture_book:add')")
    @Log(title = "中华文化书籍", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CultureBook cultureBook)
    {
        return toAjax(cultureBookService.insertCultureBook(cultureBook));
    }

    /**
     * 修改中华文化书籍
     */
    @PreAuthorize("@ss.hasPermi('system:culture_book:edit')")
    @Log(title = "中华文化书籍", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CultureBook cultureBook)
    {
        return toAjax(cultureBookService.updateCultureBook(cultureBook));
    }

    /**
     * 删除中华文化书籍
     */
    @PreAuthorize("@ss.hasPermi('system:culture_book:remove')")
    @Log(title = "中华文化书籍", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cultureBookService.deleteCultureBookByIds(ids));
    }
}
