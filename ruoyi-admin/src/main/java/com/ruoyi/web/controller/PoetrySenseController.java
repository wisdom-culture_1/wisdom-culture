package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PoetrySense;
import com.ruoyi.system.service.IPoetrySenseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 诗词常识Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/sense")
public class PoetrySenseController extends BaseController
{
    @Autowired
    private IPoetrySenseService poetrySenseService;

    /**
     * 查询诗词常识列表
     */
    @PreAuthorize("@ss.hasPermi('system:sense:list')")
    @GetMapping("/list")
    public TableDataInfo list(PoetrySense poetrySense)
    {
        startPage();
        List<PoetrySense> list = poetrySenseService.selectPoetrySenseList(poetrySense);
        return getDataTable(list);
    }

    /**
     * 导出诗词常识列表
     */
    @PreAuthorize("@ss.hasPermi('system:sense:export')")
    @Log(title = "诗词常识", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PoetrySense poetrySense)
    {
        List<PoetrySense> list = poetrySenseService.selectPoetrySenseList(poetrySense);
        ExcelUtil<PoetrySense> util = new ExcelUtil<PoetrySense>(PoetrySense.class);
        util.exportExcel(response, list, "诗词常识数据");
    }

    /**
     * 获取诗词常识详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:sense:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(poetrySenseService.selectPoetrySenseById(id));
    }

    /**
     * 新增诗词常识
     */
    @PreAuthorize("@ss.hasPermi('system:sense:add')")
    @Log(title = "诗词常识", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PoetrySense poetrySense)
    {
        return toAjax(poetrySenseService.insertPoetrySense(poetrySense));
    }

    /**
     * 修改诗词常识
     */
    @PreAuthorize("@ss.hasPermi('system:sense:edit')")
    @Log(title = "诗词常识", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PoetrySense poetrySense)
    {
        return toAjax(poetrySenseService.updatePoetrySense(poetrySense));
    }

    /**
     * 删除诗词常识
     */
    @PreAuthorize("@ss.hasPermi('system:sense:remove')")
    @Log(title = "诗词常识", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(poetrySenseService.deletePoetrySenseByIds(ids));
    }
}
