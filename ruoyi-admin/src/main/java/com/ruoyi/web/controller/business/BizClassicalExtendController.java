package com.ruoyi.web.controller.business;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.entity.ClassicalExtend;
import com.ruoyi.business.service.IClassicalExtendService;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 文言名篇扩展Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/classical")
public class BizClassicalExtendController extends BaseController
{
    @Autowired
    private IClassicalExtendService classicalExtendService;

    /**
     * 查询文言名篇扩展列表
     */
//    @PreAuthorize("@ss.hasPermi('system:extend:list')")
    @GetMapping("/classical_extend_list")
    public AjaxResult list(ClassicalExtend classicalExtend)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ClassicalExtend> list = classicalExtendService.selectClassicalExtendList(classicalExtend);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 导出文言名篇扩展列表
     */
    @PreAuthorize("@ss.hasPermi('system:extend:export')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ClassicalExtend classicalExtend)
    {
        List<ClassicalExtend> list = classicalExtendService.selectClassicalExtendList(classicalExtend);
        ExcelUtil<ClassicalExtend> util = new ExcelUtil<ClassicalExtend>(ClassicalExtend.class);
        util.exportExcel(response, list, "文言名篇扩展数据");
    }

    /**
     * 获取文言名篇扩展详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:extend:query')")
    @GetMapping("/classical_extend_detail")
    public AjaxResult getInfo(@RequestParam("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        ClassicalExtend classicalExtend = classicalExtendService.selectClassicalExtendById(id);
        ajaxResult.put("data",classicalExtend);
        return ajaxResult;
    }

    /**
     * 新增文言名篇扩展
     */
    @PreAuthorize("@ss.hasPermi('system:extend:add')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClassicalExtend classicalExtend)
    {
        return toAjax(classicalExtendService.insertClassicalExtend(classicalExtend));
    }

    /**
     * 修改文言名篇扩展
     */
    @PreAuthorize("@ss.hasPermi('system:extend:edit')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClassicalExtend classicalExtend)
    {
        return toAjax(classicalExtendService.updateClassicalExtend(classicalExtend));
    }

    /**
     * 删除文言名篇扩展
     */
    @PreAuthorize("@ss.hasPermi('system:extend:remove')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(classicalExtendService.deleteClassicalExtendByIds(ids));
    }
}
