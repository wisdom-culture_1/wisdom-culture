package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ExpertLectureHallChapter;
import com.ruoyi.system.service.IExpertLectureHallChapterService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 专家讲堂章节Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/hall_chapter")
public class ExpertLectureHallChapterController extends BaseController
{
    @Autowired
    private IExpertLectureHallChapterService expertLectureHallChapterService;

    /**
     * 查询专家讲堂章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:hall_chapter:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExpertLectureHallChapter expertLectureHallChapter)
    {
        startPage();
        List<ExpertLectureHallChapter> list = expertLectureHallChapterService.selectExpertLectureHallChapterList(expertLectureHallChapter);
        return getDataTable(list);
    }

    /**
     * 导出专家讲堂章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:hall_chapter:export')")
    @Log(title = "专家讲堂章节", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExpertLectureHallChapter expertLectureHallChapter)
    {
        List<ExpertLectureHallChapter> list = expertLectureHallChapterService.selectExpertLectureHallChapterList(expertLectureHallChapter);
        ExcelUtil<ExpertLectureHallChapter> util = new ExcelUtil<ExpertLectureHallChapter>(ExpertLectureHallChapter.class);
        util.exportExcel(response, list, "专家讲堂章节数据");
    }

    /**
     * 获取专家讲堂章节详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:hall_chapter:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(expertLectureHallChapterService.selectExpertLectureHallChapterById(id));
    }

    /**
     * 新增专家讲堂章节
     */
    @PreAuthorize("@ss.hasPermi('system:hall_chapter:add')")
    @Log(title = "专家讲堂章节", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExpertLectureHallChapter expertLectureHallChapter)
    {
        return toAjax(expertLectureHallChapterService.insertExpertLectureHallChapter(expertLectureHallChapter));
    }

    /**
     * 修改专家讲堂章节
     */
    @PreAuthorize("@ss.hasPermi('system:hall_chapter:edit')")
    @Log(title = "专家讲堂章节", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExpertLectureHallChapter expertLectureHallChapter)
    {
        return toAjax(expertLectureHallChapterService.updateExpertLectureHallChapter(expertLectureHallChapter));
    }

    /**
     * 删除专家讲堂章节
     */
    @PreAuthorize("@ss.hasPermi('system:hall_chapter:remove')")
    @Log(title = "专家讲堂章节", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(expertLectureHallChapterService.deleteExpertLectureHallChapterByIds(ids));
    }
}
