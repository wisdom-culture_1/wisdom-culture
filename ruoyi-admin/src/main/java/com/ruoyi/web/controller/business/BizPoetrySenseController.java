package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.PoetrySense;
import com.ruoyi.business.service.IPoetrySenseService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 诗词常识Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/poetry")
public class BizPoetrySenseController extends BaseController
{
    @Autowired
    private IPoetrySenseService poetrySenseService;

    /**
     * 查询诗词常识列表
     */
//    @PreAuthorize("@ss.hasPermi('system:sense:list')")
    @GetMapping("/sense_list")
    public AjaxResult list(PoetrySense poetrySense)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        startPage();
        List<PoetrySense> list = poetrySenseService.selectPoetrySenseList(poetrySense);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出诗词常识列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:sense:export')")
//    @Log(title = "诗词常识", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, PoetrySense poetrySense)
//    {
//        List<PoetrySense> list = poetrySenseService.selectPoetrySenseList(poetrySense);
//        ExcelUtil<PoetrySense> util = new ExcelUtil<PoetrySense>(PoetrySense.class);
//        util.exportExcel(response, list, "诗词常识数据");
//    }

    /**
     * 获取诗词常识详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:sense:query')")
    @GetMapping(value = "/sense_detail")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        ajaxResult.put("data",poetrySenseService.selectPoetrySenseById(id));
        return ajaxResult;
    }

//    /**
//     * 新增诗词常识
//     */
//    @PreAuthorize("@ss.hasPermi('system:sense:add')")
//    @Log(title = "诗词常识", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody PoetrySense poetrySense)
//    {
//        return toAjax(poetrySenseService.insertPoetrySense(poetrySense));
//    }
//
//    /**
//     * 修改诗词常识
//     */
//    @PreAuthorize("@ss.hasPermi('system:sense:edit')")
//    @Log(title = "诗词常识", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody PoetrySense poetrySense)
//    {
//        return toAjax(poetrySenseService.updatePoetrySense(poetrySense));
//    }
//
//    /**
//     * 删除诗词常识
//     */
//    @PreAuthorize("@ss.hasPermi('system:sense:remove')")
//    @Log(title = "诗词常识", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(poetrySenseService.deletePoetrySenseByIds(ids));
//    }
}
