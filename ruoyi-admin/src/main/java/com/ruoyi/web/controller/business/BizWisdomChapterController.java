package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.WisdomChapter;
import com.ruoyi.business.service.IWisdomChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 智慧元典章节Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/wisdom")
public class BizWisdomChapterController extends BaseController
{
    @Autowired
    private IWisdomChapterService wisdomChapterService;

    /**
     * 查询智慧元典章节列表
     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:list')")
    @PostMapping("/chapter_list")
    public AjaxResult list(@RequestBody WisdomChapter wisdomChapter)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<WisdomChapter> list = wisdomChapterService.selectWisdomChapterList(wisdomChapter);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 查询智慧元典章节列表(仅列表)
     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:list')")
    @PostMapping("/chapter_list_sample")
    public AjaxResult listSample(@RequestBody WisdomChapter wisdomChapter)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
//        List<WisdomChapter> list = wisdomChapterService.selectWisdomChapterList(wisdomChapter);
        List<WisdomChapter> list = wisdomChapterService.selectWisdomChapterListSample(wisdomChapter);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    //    @PreAuthorize("@ss.hasPermi('system:chapter:list')")
    @GetMapping("/detail")
    public AjaxResult detail(WisdomChapter wisdomChapter)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        List<WisdomChapter> wisdomChapters = wisdomChapterService.selectWisdomByXuhao(wisdomChapter);
        ajaxResult.put("data",wisdomChapters);
        return ajaxResult;
    }

//    /**
//     * 导出智慧元典章节列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:export')")
//    @Log(title = "智慧元典章节", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, WisdomChapter wisdomChapter)
//    {
//        List<WisdomChapter> list = wisdomChapterService.selectWisdomChapterList(wisdomChapter);
//        ExcelUtil<WisdomChapter> util = new ExcelUtil<WisdomChapter>(WisdomChapter.class);
//        util.exportExcel(response, list, "智慧元典章节数据");
//    }
//
//    /**
//     * 获取智慧元典章节详细信息
//     */
////    @PreAuthorize("@ss.hasPermi('system:chapter:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(wisdomChapterService.selectWisdomChapterById(id));
//    }

//    /**
//     * 新增智慧元典章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:add')")
//    @Log(title = "智慧元典章节", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody WisdomChapter wisdomChapter)
//    {
//        return toAjax(wisdomChapterService.insertWisdomChapter(wisdomChapter));
//    }
//
//    /**
//     * 修改智慧元典章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:edit')")
//    @Log(title = "智慧元典章节", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody WisdomChapter wisdomChapter)
//    {
//        return toAjax(wisdomChapterService.updateWisdomChapter(wisdomChapter));
//    }
//
//    /**
//     * 删除智慧元典章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:remove')")
//    @Log(title = "智慧元典章节", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(wisdomChapterService.deleteWisdomChapterByIds(ids));
//    }
}
