package com.ruoyi.web.controller.business;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.entity.ModuleDict;
import com.ruoyi.business.service.IModuleDictService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 模块对应关系字典Controller
 *
 * @author ruoyi
 * @date 2024-06-22
 */
@RestController
@RequestMapping("/system/dict")
public class ModuleDictController extends BaseController
{
    @Autowired
    private IModuleDictService moduleDictService;

    /**
     * 查询模块对应关系字典列表
     */
//    @PreAuthorize("@ss.hasPermi('system:dict:list')")
    @PostMapping("/list")
    public AjaxResult list(@RequestBody ModuleDict moduleDict)
    {
        List<ModuleDict> list = moduleDictService.selectModuleDictList(moduleDict);
        return AjaxResult.success(list);
    }

    /**
     * 导出模块对应关系字典列表
     */
    @PreAuthorize("@ss.hasPermi('system:dict:export')")
    @Log(title = "模块对应关系字典", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ModuleDict moduleDict)
    {
        List<ModuleDict> list = moduleDictService.selectModuleDictList(moduleDict);
        ExcelUtil<ModuleDict> util = new ExcelUtil<ModuleDict>(ModuleDict.class);
        util.exportExcel(response, list, "模块对应关系字典数据");
    }

    /**
     * 获取模块对应关系字典详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:dict:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(moduleDictService.selectModuleDictById(id));
    }

    /**
     * 新增模块对应关系字典
     */
    @PreAuthorize("@ss.hasPermi('system:dict:add')")
    @Log(title = "模块对应关系字典", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ModuleDict moduleDict)
    {
        return toAjax(moduleDictService.insertModuleDict(moduleDict));
    }

    /**
     * 修改模块对应关系字典
     */
    @PreAuthorize("@ss.hasPermi('system:dict:edit')")
    @Log(title = "模块对应关系字典", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ModuleDict moduleDict)
    {
        return toAjax(moduleDictService.updateModuleDict(moduleDict));
    }

    /**
     * 删除模块对应关系字典
     */
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @Log(title = "模块对应关系字典", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(moduleDictService.deleteModuleDictByIds(ids));
    }
}
