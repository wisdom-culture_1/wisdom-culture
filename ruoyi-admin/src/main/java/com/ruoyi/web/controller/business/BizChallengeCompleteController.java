package com.ruoyi.web.controller.business;

import com.ruoyi.business.entity.ChallengeComplete;
import com.ruoyi.business.service.IChallengeCompleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

/**
 * 挑战结果Controller
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@RestController
@RequestMapping("/challenge")
public class BizChallengeCompleteController extends BaseController
{
    @Autowired
    private IChallengeCompleteService challengeCompleteService;

//    /**
//     * 查询挑战结果列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:complete:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(ChallengeComplete challengeComplete)
//    {
//        startPage();
//        List<ChallengeComplete> list = challengeCompleteService.selectChallengeCompleteList(challengeComplete);
//        return getDataTable(list);
//    }


    /**
     * 新增挑战结果
     */
//    @PreAuthorize("@ss.hasPermi('system:complete:add')")
    @Log(title = "挑战结果", businessType = BusinessType.INSERT)
    @PostMapping("/complete")
    public AjaxResult add(@RequestBody ChallengeComplete challengeComplete)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        challengeCompleteService.insertChallengeComplete(challengeComplete);
        return ajaxResult;
    }
}
