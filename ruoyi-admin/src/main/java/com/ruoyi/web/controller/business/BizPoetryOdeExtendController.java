package com.ruoyi.web.controller.business;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.business.entity.PoetryOde;
import com.ruoyi.business.entity.PoetryOdeExtend;
import com.ruoyi.business.service.IPoetryOdeExtendService;
import com.ruoyi.business.service.IPoetryOdeService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 诗词歌赋扩展Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/poetry")
public class BizPoetryOdeExtendController extends BaseController
{
    @Autowired
    private IPoetryOdeExtendService poetryOdeExtendService;

    @Autowired
    private IPoetryOdeService iPoetryOdeService;

    /**
     * 查询诗词歌赋扩展列表
     */
//    @PreAuthorize("@ss.hasPermi('system:extend:list')")
    @GetMapping("/ode_extend_list")
    public AjaxResult list(PoetryOdeExtend poetryOdeExtend)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        List<PoetryOdeExtend> list = new ArrayList<>();
        list = poetryOdeExtendService.selectPoetryOdeExtendList(poetryOdeExtend);
        if(CollectionUtils.isEmpty(list)){
            PoetryOdeExtend poetryOdeExtend1 = new PoetryOdeExtend();
            PoetryOde poetryOde = iPoetryOdeService.selectPoetryOdeById(poetryOdeExtend.getPoetryOdeId());
            poetryOdeExtend1.setTitle(poetryOde.getExtendTitle());
            poetryOdeExtend1.setContent(poetryOde != null ? poetryOde.getExtendContent() : null);
            list.add(poetryOdeExtend1);
        }
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出诗词歌赋扩展列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:export')")
//    @Log(title = "诗词歌赋扩展", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, PoetryOdeExtend poetryOdeExtend)
//    {
//        List<PoetryOdeExtend> list = poetryOdeExtendService.selectPoetryOdeExtendList(poetryOdeExtend);
//        ExcelUtil<PoetryOdeExtend> util = new ExcelUtil<PoetryOdeExtend>(PoetryOdeExtend.class);
//        util.exportExcel(response, list, "诗词歌赋扩展数据");
//    }
//
//    /**
//     * 获取诗词歌赋扩展详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(poetryOdeExtendService.selectPoetryOdeExtendById(id));
//    }
//
//    /**
//     * 新增诗词歌赋扩展
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:add')")
//    @Log(title = "诗词歌赋扩展", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody PoetryOdeExtend poetryOdeExtend)
//    {
//        return toAjax(poetryOdeExtendService.insertPoetryOdeExtend(poetryOdeExtend));
//    }
//
//    /**
//     * 修改诗词歌赋扩展
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:edit')")
//    @Log(title = "诗词歌赋扩展", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody PoetryOdeExtend poetryOdeExtend)
//    {
//        return toAjax(poetryOdeExtendService.updatePoetryOdeExtend(poetryOdeExtend));
//    }
//
//    /**
//     * 删除诗词歌赋扩展
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:remove')")
//    @Log(title = "诗词歌赋扩展", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(poetryOdeExtendService.deletePoetryOdeExtendByIds(ids));
//    }
}
