package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.WisdomBookExtend;
import com.ruoyi.business.service.IWisdomBookExtendService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 智慧元典书籍扩展Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/wisdom")
public class BizWisdomBookExtendController extends BaseController
{
    @Autowired
    private IWisdomBookExtendService wisdomBookExtendService;

    /**
     * 查询智慧元典书籍扩展列表
     */
//    @PreAuthorize("@ss.hasPermi('system:extend:list')")
    @GetMapping("/book_extend_list")
    public AjaxResult list(WisdomBookExtend wisdomBookExtend)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

//        startPage();
        List<WisdomBookExtend> list = wisdomBookExtendService.selectWisdomBookExtendList(wisdomBookExtend);

        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出智慧元典书籍扩展列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:export')")
//    @Log(title = "智慧元典书籍扩展", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, WisdomBookExtend wisdomBookExtend)
//    {
//        List<WisdomBookExtend> list = wisdomBookExtendService.selectWisdomBookExtendList(wisdomBookExtend);
//        ExcelUtil<WisdomBookExtend> util = new ExcelUtil<WisdomBookExtend>(WisdomBookExtend.class);
//        util.exportExcel(response, list, "智慧元典书籍扩展数据");
//    }

    /**
     * 获取智慧元典书籍扩展详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:extend:query')")
    @GetMapping(value = "/book_extend_detail")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        WisdomBookExtend wisdomBookExtend = wisdomBookExtendService.selectWisdomBookExtendById(id);
        ajaxResult.put("data",wisdomBookExtend);
        return ajaxResult;
    }

//    /**
//     * 新增智慧元典书籍扩展
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:add')")
//    @Log(title = "智慧元典书籍扩展", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody WisdomBookExtend wisdomBookExtend)
//    {
//        return toAjax(wisdomBookExtendService.insertWisdomBookExtend(wisdomBookExtend));
//    }
//
//    /**
//     * 修改智慧元典书籍扩展
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:edit')")
//    @Log(title = "智慧元典书籍扩展", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody WisdomBookExtend wisdomBookExtend)
//    {
//        return toAjax(wisdomBookExtendService.updateWisdomBookExtend(wisdomBookExtend));
//    }
//
//    /**
//     * 删除智慧元典书籍扩展
//     */
//    @PreAuthorize("@ss.hasPermi('system:extend:remove')")
//    @Log(title = "智慧元典书籍扩展", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(wisdomBookExtendService.deleteWisdomBookExtendByIds(ids));
//    }
}
