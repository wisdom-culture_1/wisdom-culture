package com.ruoyi.web.controller.business;

import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.MasterpiecesText;
import com.ruoyi.business.service.IMasterpiecesTextService;
import com.ruoyi.common.utils.Threads;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 名著章节Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/masterpieces")
public class BizMasterpiecesTextController extends BaseController
{
    @Autowired
    private IMasterpiecesTextService masterpiecesTextService;
    private static final Logger logger = LoggerFactory.getLogger(BizMasterpiecesTextController.class);

    /**
     * 查询名著章节列表
     */
//    @PreAuthorize("@ss.hasPermi('system:text:list')")
    @GetMapping("/detail")
    public AjaxResult list(MasterpiecesText masterpiecesText)
    {
        logger.info("BizMasterpiecesTextController list masterpiecesText is :{}",JSON.toJSONString(masterpiecesText));
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<MasterpiecesText> list = masterpiecesTextService.selectMasterpiecesTextList(masterpiecesText);
        ajaxResult.put("data",list);
        return ajaxResult;    }

//    /**
//     * 导出名著章节列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:export')")
//    @Log(title = "名著章节", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, MasterpiecesText masterpiecesText)
//    {
//        List<MasterpiecesText> list = masterpiecesTextService.selectMasterpiecesTextList(masterpiecesText);
//        ExcelUtil<MasterpiecesText> util = new ExcelUtil<MasterpiecesText>(MasterpiecesText.class);
//        util.exportExcel(response, list, "名著章节数据");
//    }
//
//    /**
//     * 获取名著章节详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(masterpiecesTextService.selectMasterpiecesTextById(id));
//    }
//
//    /**
//     * 新增名著章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:add')")
//    @Log(title = "名著章节", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody MasterpiecesText masterpiecesText)
//    {
//        return toAjax(masterpiecesTextService.insertMasterpiecesText(masterpiecesText));
//    }
//
//    /**
//     * 修改名著章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:edit')")
//    @Log(title = "名著章节", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody MasterpiecesText masterpiecesText)
//    {
//        return toAjax(masterpiecesTextService.updateMasterpiecesText(masterpiecesText));
//    }
//
//    /**
//     * 删除名著章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:text:remove')")
//    @Log(title = "名著章节", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(masterpiecesTextService.deleteMasterpiecesTextByIds(ids));
//    }
}
