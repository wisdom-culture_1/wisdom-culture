package com.ruoyi.web.controller.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.github.pagehelper.PageHelper;
import com.ruoyi.business.entity.*;
//import com.ruoyi.business.mapper.GradeMapper;
import com.ruoyi.business.entity.CultureChapters;
import com.ruoyi.business.entity.CultureText;
import com.ruoyi.business.entity.Excerpt;
import com.ruoyi.business.entity.ExcerptComment;
import com.ruoyi.business.entity.ExcerptLike;
import com.ruoyi.business.entity.WisdomChapter;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.*;
import com.ruoyi.business.service.impl.BizExcerptServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.Classical;
import com.ruoyi.system.domain.CultureBook;
import com.ruoyi.system.domain.DictionaryChildren;
import com.ruoyi.system.domain.Etymology;
import com.ruoyi.system.domain.IdiomStory;
import com.ruoyi.system.domain.MasterpiecesBook;
import com.ruoyi.system.domain.OriginOfChineseCharacters;
import com.ruoyi.system.domain.PoetryOde;
import com.ruoyi.system.domain.PoetrySense;
import com.ruoyi.system.mapper.*;
import com.ruoyi.web.controller.tool.system.SysLoginController;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/beyondcampus")

public class BeyondcampusController extends BaseController
{
    @Autowired
    private IBeyondcampusService beyondcampusService;
    @Autowired
    private StudyPlanModuleStayLightMapper studyPlanModuleStayLightMapper;
    @Autowired
    private StudyPlanModuleStayTimeMapper studyPlanModuleStayTimeMapper;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private GradeStudyPlanMapper gradeStudyPlanMapper;
    @Autowired
    private DictionaryChildrenMapper dictionaryChildrenMapper;
    @Autowired
    private IdiomStoryMapper idiomStoryMapper;
    @Autowired
    private GradeStudyPlanDetailMapper gradeStudyPlanDetailMapper;
    @Autowired
    private ModuleDictMapper moduleDictMapper;
    @Autowired
    private EtymologyMapper etymologyMapper;
    @Autowired
    private OriginOfChineseCharactersMapper originOfChineseCharactersMapper;
    @Autowired
    private BizWisdomChapterMapper bizWisdomChapterMapper;
    @Autowired
    private PoetryOdeMapper PoetryOdeMapper;
    @Autowired
    private PoetrySenseMapper poetrySenseMapper;
    @Autowired
    private ClassicalMapper classicalMapper;
    @Autowired
    private MasterpiecesBookMapper MasterpiecesBookMapper;
    @Autowired
    private CultureBookMapper CultureBookMapper;
    @Autowired
    private ICultureChaptersService cultureChaptersService;
    @Autowired
    private ICultureTextService cultureTextService;
    @Autowired
    private ExcerptMapper excerptMapper;
    @Autowired
    private BizExcerptCommentMapper excerptCommentMapper;
    @Autowired
    private BizExcerptLikeMapper excerptLikeMapper;
    @Autowired
    private IChineseCharactersService chineseCharactersService;

//    模块对应字典
    private static Map<String, String> LIGHT_RECORDS = new HashMap<>();

//    判断模块是否可以手动调整显示的内容
    private static Map<String, Long> DYNAMIC_DETAILS  = new HashMap<>();

    private static final Logger log = LoggerFactory.getLogger(BeyondcampusController.class);

    static{
        LIGHT_RECORDS.put("1_1","字母表");
        LIGHT_RECORDS.put("1_2","声母表");
        LIGHT_RECORDS.put("1_3","韵母");

        LIGHT_RECORDS.put("2_1","速斩生字");
        LIGHT_RECORDS.put("2_2","生词闯大关");
//        LIGHT_RECORDS.put("2_2","汉字起源");
//        LIGHT_RECORDS.put("2_3","成语典故");

//        LIGHT_RECORDS.put("3_1","文言名篇");
//        LIGHT_RECORDS.put("3_2","中华文化");
//        LIGHT_RECORDS.put("3_3","诗词歌赋");
        LIGHT_RECORDS.put("3_1","成语典故");
        LIGHT_RECORDS.put("3_2","人物外貌");
        LIGHT_RECORDS.put("3_3","人物类型");
        LIGHT_RECORDS.put("3_4","人物心理");
        LIGHT_RECORDS.put("3_5", "人物心情");
        LIGHT_RECORDS.put("3_6", "人物品质");
        LIGHT_RECORDS.put("3_7", "言辞表达");
        LIGHT_RECORDS.put("3_8", "为人处世");
        LIGHT_RECORDS.put("3_9", "描写景物");
        LIGHT_RECORDS.put("3_10","描写四季");
        LIGHT_RECORDS.put("3_11","描写气候");
        LIGHT_RECORDS.put("3_12","十二生肖");
        LIGHT_RECORDS.put("3_13","其他动物");
        LIGHT_RECORDS.put("3_14","其他");
        LIGHT_RECORDS.put("3_15","对比、比较");
        LIGHT_RECORDS.put("3_16","社会生活");
        LIGHT_RECORDS.put("3_17","社会斗争");
        LIGHT_RECORDS.put("3_18","教育学习");
        LIGHT_RECORDS.put("3_19","政治法律");
        LIGHT_RECORDS.put("3_20","军事经济");
        LIGHT_RECORDS.put("3_21","成语结构");
        LIGHT_RECORDS.put("3_22","近反义词");
        LIGHT_RECORDS.put("3_23","成语故事");
        LIGHT_RECORDS.put("3_24","语文大纲");
        LIGHT_RECORDS.put("3_25","四大名著");
        LIGHT_RECORDS.put("3_26","其他名著");
        LIGHT_RECORDS.put("3_27","出自名人");
        LIGHT_RECORDS.put("3_28","其他和人有关");

//        LIGHT_RECORDS.put("4_1", "专家讲堂");
//        LIGHT_RECORDS.put("4_2", "经典赏析");
//        LIGHT_RECORDS.put("4_3", "北大直播");
        LIGHT_RECORDS.put("4_1", "国学启蒙");
        LIGHT_RECORDS.put("4_2", "一年级背诵篇目");
        LIGHT_RECORDS.put("4_3", "二年级背诵篇目");
        LIGHT_RECORDS.put("4_4", "三年级背诵篇目");
        LIGHT_RECORDS.put("4_5", "四年级背诵篇目");
        LIGHT_RECORDS.put("4_6", "五年级背诵篇目");
        LIGHT_RECORDS.put("4_7", "六年级背诵篇目");
        LIGHT_RECORDS.put("4_8", "初一背诵篇目");
        LIGHT_RECORDS.put("4_9", "初二背诵篇目");
        LIGHT_RECORDS.put("4_10","初三背诵篇目");


//        判断模块是否可以手动调整显示的内容 y:1 n:0
        DYNAMIC_DETAILS.put("1_1",0L);
        DYNAMIC_DETAILS.put("1_2",0L);
        DYNAMIC_DETAILS.put("1_3",0L);

        DYNAMIC_DETAILS.put("2_1",0L);
        DYNAMIC_DETAILS.put("2_2",0L);
//        LIGHT_RECORDS.put("2_2","汉字起源");
//        LIGHT_RECORDS.put("2_3","成语典故");

//        LIGHT_RECORDS.put("3_1","文言名篇");
//        LIGHT_RECORDS.put("3_2","中华文化");
//        LIGHT_RECORDS.put("3_3","诗词歌赋");
        DYNAMIC_DETAILS.put("3_1",1L);
        DYNAMIC_DETAILS.put("3_2",1L);
        DYNAMIC_DETAILS.put("3_3",1L);
        DYNAMIC_DETAILS.put("3_4",1L);
        DYNAMIC_DETAILS.put("3_5", 1L);
        DYNAMIC_DETAILS.put("3_6", 1L);
        DYNAMIC_DETAILS.put("3_7", 1L);
        DYNAMIC_DETAILS.put("3_8", 1L);
        DYNAMIC_DETAILS.put("3_9", 1L);
        DYNAMIC_DETAILS.put("3_10",1L);
        DYNAMIC_DETAILS.put("3_11",1L);
        DYNAMIC_DETAILS.put("3_12",1L);
        DYNAMIC_DETAILS.put("3_13",1L);
        DYNAMIC_DETAILS.put("3_14",1L);
        DYNAMIC_DETAILS.put("3_15",1L);
        DYNAMIC_DETAILS.put("3_16",1L);
        DYNAMIC_DETAILS.put("3_17",1L);
        DYNAMIC_DETAILS.put("3_18",1L);
        DYNAMIC_DETAILS.put("3_19",1L);
        DYNAMIC_DETAILS.put("3_20",1L);
        DYNAMIC_DETAILS.put("3_21",1L);
        DYNAMIC_DETAILS.put("3_22",1L);
        DYNAMIC_DETAILS.put("3_23",1L);
        DYNAMIC_DETAILS.put("3_24",1L);
        DYNAMIC_DETAILS.put("3_25",1L);
        DYNAMIC_DETAILS.put("3_26",1L);
        DYNAMIC_DETAILS.put("3_27",1L);
        DYNAMIC_DETAILS.put("3_28",1L);

//        LIGHT_RECORDS.put("4_1", "专家讲堂");
//        LIGHT_RECORDS.put("4_2", "经典赏析");
//        LIGHT_RECORDS.put("4_3", "北大直播");
        DYNAMIC_DETAILS.put("4_1", 0L);
        DYNAMIC_DETAILS.put("4_2", 0L);
        DYNAMIC_DETAILS.put("4_3", 0L);
        DYNAMIC_DETAILS.put("4_4", 0L);
        DYNAMIC_DETAILS.put("4_5", 0L);
        DYNAMIC_DETAILS.put("4_6", 0L);
        DYNAMIC_DETAILS.put("4_7", 0L);
        DYNAMIC_DETAILS.put("4_8", 0L);
        DYNAMIC_DETAILS.put("4_9", 0L);
        DYNAMIC_DETAILS.put("4_10",0L);
    }


    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:beyondcampus:list')")
    @GetMapping("/list")

    public AjaxResult list(Beyondcampus beyondcampus)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<Beyondcampus> list = beyondcampusService.selectBeyondcampusList(beyondcampus);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    @GetMapping("/select_list")
    public AjaxResult selectList(Beyondcampus beyondcampus)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<Beyondcampus> list = beyondcampusService.selectBeyondcampusList(beyondcampus);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 当前模块停留时长
     * @param studyPlanModuleStayTime
     * @return
     */
    @PostMapping("/recordStayTime")
    public AjaxResult recordStayTime(@RequestBody StudyPlanModuleStayTime studyPlanModuleStayTime)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        studyPlanModuleStayTime.setUserId(SecurityUtils.getUserId());
        studyPlanModuleStayTimeMapper.insertStudyPlanModuleStayTime(studyPlanModuleStayTime);
        return ajaxResult;
    }

    /**
     * 当前模块停留时长
     * @param studyPlanModuleStayTime
     * @return
     */
    @GetMapping("/wetherLight")
    public AjaxResult wetherLight(StudyPlanModuleStayTime studyPlanModuleStayTime)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        studyPlanModuleStayTime.setUserId(SecurityUtils.getUserId());
        StudyPlanModuleStayTime studyPlanModuleStayTimes = studyPlanModuleStayTimeMapper.selectStudyPlanModuleStayTimeVo(studyPlanModuleStayTime);
        if(studyPlanModuleStayTime != null && studyPlanModuleStayTimes.getStayTime() >= 3000){
            ajaxResult.put("data",1);//可以点亮
        }else{
            ajaxResult.put("data",0);//不能点亮
        }
        return ajaxResult;
    }

    /**
     * 记录点亮模块
     * @param
     * @return
     */
    @PostMapping("/recordLightInfo")
    public AjaxResult recordLightInfo(@RequestBody StudyPlanModuleLight studyPlanModuleLight)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        studyPlanModuleLight.setUserId(SecurityUtils.getUserId());
        SysUser sysUser = sysUserMapper.selectUserById(SecurityUtils.getUserId());
        studyPlanModuleLight.setGradeId(sysUser.getGradeId());

        List<StudyPlanModuleLight> studyPlanModuleLights = studyPlanModuleStayLightMapper.selectStudyPlanModuleLight(studyPlanModuleLight);
        if(!CollectionUtils.isEmpty(studyPlanModuleLights)){
            StudyPlanModuleLight studyPlanModuleLight1 = studyPlanModuleLights.get(0);
            studyPlanModuleLight1.setIsLight(studyPlanModuleLight.getIsLight());
            studyPlanModuleStayLightMapper.updateStudyPlanModuleLight(studyPlanModuleLight1);
        }else{
            studyPlanModuleStayLightMapper.insertStudyPlanModuleLight(studyPlanModuleLight);
        }
        return ajaxResult;
    }

    /**
     * 获取点亮记录 （获取学习计划目录）
     * @param
     * @return
     */
    @GetMapping("/getRecordLightInfo")
    public AjaxResult getRecordLightInfo(StudyPlanModuleLight studyPlanModuleLight)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        log.info("getRecordLightInfo SecurityUtils.getUserId() is :{}",JSON.toJSONString(SecurityUtils.getUserId()));

        studyPlanModuleLight.setUserId(SecurityUtils.getUserId());
        SysUser sysUser = sysUserMapper.selectUserById(SecurityUtils.getUserId());
        studyPlanModuleLight.setGradeId(sysUser.getGradeId());
        log.info("getRecordLightInfo studyPlanModuleLight is :{}",JSON.toJSONString(studyPlanModuleLight));

        List<StudyPlanModuleLight> studyPlanModuleLights = studyPlanModuleStayLightMapper.selectStudyPlanModuleLightByUserId(studyPlanModuleLight);
        studyPlanModuleLights.forEach(item->{
            item.setName(LIGHT_RECORDS.get(item.getModule()+"_"+item.getChildModule()));
            item.setIsDynamic(DYNAMIC_DETAILS.get(item.getModule()+"_"+item.getChildModule()));
        });
        Map<Integer, List<StudyPlanModuleLight>> collect = studyPlanModuleLights.stream().collect(Collectors.groupingBy(StudyPlanModuleLight::getModule));
        return ajaxResult.put("data",collect);
    }

    /**
     * 获取点亮记录 （获取学习计划目录）
     * @param
     * @return
     */
    @GetMapping("/getRecordLightInfoNew")
    public AjaxResult getRecordLightInfoNew(@RequestParam("gradeId") Long gradeId)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        log.info("getRecordLightInfoNew SecurityUtils.getUserId() is :{}",JSON.toJSONString(SecurityUtils.getUserId()));
        StudyPlanModuleLight studyPlanModuleLight = new StudyPlanModuleLight();
        studyPlanModuleLight.setUserId(SecurityUtils.getUserId());
//        SysUser sysUser = sysUserMapper.selectUserById(SecurityUtils.getUserId());
        studyPlanModuleLight.setGradeId(gradeId);

        log.info("getRecordLightInfoNew is :{}",JSON.toJSONString(studyPlanModuleLight));
        List<StudyPlanModuleLight> studyPlanModuleLights = studyPlanModuleStayLightMapper.selectStudyPlanModuleLightByUserId(studyPlanModuleLight);
        studyPlanModuleLights.forEach(item->{
            ModuleDict selModuleDict = new ModuleDict();
            selModuleDict.setModule(item.getModule().longValue());
            selModuleDict.setChildModule(item.getChildModule().longValue());
            List<ModuleDict> moduleDictList = moduleDictMapper.selectModuleDictList(selModuleDict);
            if (!CollectionUtils.isEmpty(moduleDictList)){
                item.setName(moduleDictList.get(0).getChildModuleName());
                item.setIsDynamic(moduleDictList.get(0).getDynamic());
            }
        });
        Map<Integer, List<StudyPlanModuleLight>> collect = studyPlanModuleLights.stream().collect(Collectors.groupingBy(StudyPlanModuleLight::getModule));
        return ajaxResult.put("data",collect);
    }

//    private void buildLightReturn(List<StudyPlanModuleLight> dbLights, List<StudyPlanModuleLight> returnLights) {
//        if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(dbLights)){
//            dbLights.forEach(item -> {
//                returnLights.forEach(x ->{
//                    if(item.getUserId() == x.getUserId() && item.getModule() == x.getModule() && item.getChildModule() == x.getChildModule()){
//                        x.setIsLight(item.getIsLight());
//                    }
//                });
//            });
//        }
//    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:beyondcampus:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Beyondcampus beyondcampus)
    {
        List<Beyondcampus> list = beyondcampusService.selectBeyondcampusList(beyondcampus);
        ExcelUtil<Beyondcampus> util = new ExcelUtil<Beyondcampus>(Beyondcampus.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:beyondcampus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(beyondcampusService.selectBeyondcampusById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:beyondcampus:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Beyondcampus beyondcampus)
    {
        return toAjax(beyondcampusService.insertBeyondcampus(beyondcampus));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:beyondcampus:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Beyondcampus beyondcampus)
    {
        return toAjax(beyondcampusService.updateBeyondcampus(beyondcampus));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:beyondcampus:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(beyondcampusService.deleteBeyondcampusByIds(ids));
    }


    /**
     * 按照年级 模块 子模块 查询细节文章详情
     * @param req
     * @return
     */
    @PostMapping("/module_detail")
    public AjaxResult moduleDetail(@RequestBody GradeStudyPlanDetail req){
        log.info("module_detail is: {}", JSON.toJSONString(req));
        Long module = req.getModule();
        Long childModule = req.getChildModule();
        Integer pageNum = req.getPageNum();
        Integer pageSize = req.getPageSize();
        String detailName = req.getDetailName();

//        moule为1：child_module 1：字母表，2：声母表，3：韵母
//        moule为2：child_module 1：速斩生字，2：生词闯大关
//        moule为3：child_module 1：成语典故，2：人物外貌，3：人物类型，4：人物心理，5：人物心情
//        6：人物品质，7：言辞表达，8：为人处世，9：描写景物，10：描写四季
//        11：描写气候，12：十二生肖，13：其他动物 14：其他 15： 对比、比较
//        16：社会生活 17、社会斗争 18教育学习 19 政治法律 20 军事经济 21 成语结构
//        22： 近反义词 23：成语故事 24：语文大纲 25：四大名著 26：其他名著 27：出自名人
//        28：其他和人有关
//        Long isDynamic = DYNAMIC_DETAILS.get(module + "_" + childModule);
//        String moduleName = LIGHT_RECORDS.get(module + "_" + childModule);

        String moduleName = null;
        Long isDynamic = null;
        ModuleDict selModuleDict = new ModuleDict();
        selModuleDict.setModule(module);
        selModuleDict.setChildModule(childModule);
        List<ModuleDict> moduleDictList = moduleDictMapper.selectModuleDictList(selModuleDict);
        if (!CollectionUtils.isEmpty(moduleDictList)){
            moduleName = moduleDictList.get(0).getModuleName();
            isDynamic = moduleDictList.get(0).getDynamic();
        }
        if (isDynamic == null){
            return AjaxResult.error("查询不到模块内容1");
        }else if (isDynamic != 1L){
            return AjaxResult.error("此模块不允许自定义显示内容");
        }

        // 根据模块和子模块，自定义查询内容
        if (module == 1){
            //...
            // 1-2 速战生词
            if (childModule == 2){
                NewWordsSummaryRsp newWordsSummaryRsp = chineseCharactersService.selectNewWordListSummaryAll(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(newWordsSummaryRsp);
            }
            // 1-3 字源学字
            if (childModule == 3){
                List<EtymologyResp> etymologyRespList;
                PageHelper.startPage(pageNum,pageSize);
                etymologyRespList = etymologyMapper.selectEtymologyListAll(req.getGradeId(), req.getModule(), req.getChildModule(),detailName);
                return AjaxResult.success(getDataTable(etymologyRespList));
            }
            // 1-5 成语典故（新）
            if (childModule == 5){
                List<IdiomStoryResp> idiomStoryRespList;
                PageHelper.startPage(pageNum,pageSize);
                idiomStoryRespList = idiomStoryMapper.selectIdiomStoryListAll(req.getGradeId(), req.getModule(), req.getChildModule(),detailName);
                return AjaxResult.success(getDataTable(idiomStoryRespList));
            }
            // 1-6 汉字起源
            if (childModule == 6){
                List<OriginOfChineseCharactersResp> originOfChineseCharactersRespList;
                PageHelper.startPage(pageNum,pageSize);
                originOfChineseCharactersRespList = originOfChineseCharactersMapper.selectOriginOfChineseCharactersListAll(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(originOfChineseCharactersRespList));
            }
        }
        if (module == 2){
            // 2-1 国学启蒙
            if (childModule == 1){
                List<WisdomChapterResp> wisdomChapterRespList;
                PageHelper.startPage(pageNum,pageSize);
                wisdomChapterRespList = bizWisdomChapterMapper.selectWisdomChapterListAll(req.getGradeId(), req.getModule(), req.getChildModule(),1L,detailName);
                return AjaxResult.success(getDataTable(wisdomChapterRespList));
            }
            // 2-2 思想元典
            if (childModule == 2){
                List<WisdomChapterResp> wisdomChapterRespList;
                PageHelper.startPage(pageNum,pageSize);
                wisdomChapterRespList = bizWisdomChapterMapper.selectWisdomChapterListAll(req.getGradeId(), req.getModule(), req.getChildModule(),2L,detailName);
                return AjaxResult.success(getDataTable(wisdomChapterRespList));
            }
            // 2-3 唐诗宋词
            if (childModule == 3){
                List<PoetryOdeResp> poetryOdeRespList;
                PageHelper.startPage(pageNum,pageSize);
                poetryOdeRespList = PoetryOdeMapper.selectPoetryOdeListAll(req.getGradeId(), req.getModule(), req.getChildModule(),detailName);
                return AjaxResult.success(getDataTable(poetryOdeRespList));
            }
            // 2-4 诗词常识
            if (childModule == 4){
                List<PoetrySenseResp> poetrySenseRespList;
                PageHelper.startPage(pageNum,pageSize);
                poetrySenseRespList = poetrySenseMapper.selectPoetrySenseListAll(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(poetrySenseRespList));
            }
        }
        if (module == 3){
            // 3-1 古文初识
            if (childModule == 1){
                List<ClassicalResp> classicalRespList;
                PageHelper.startPage(pageNum,pageSize);
                classicalRespList = classicalMapper.selectClassicaListAll(req.getGradeId(), req.getModule(), req.getChildModule(),1L,detailName);
                return AjaxResult.success(getDataTable(classicalRespList));
            }
            // 3-2 必备名篇
            if (childModule == 2){
                List<ClassicalResp> classicalRespList;
                PageHelper.startPage(pageNum,pageSize);
                classicalRespList = classicalMapper.selectClassicaListAll(req.getGradeId(), req.getModule(), req.getChildModule(),2L,detailName);
                return AjaxResult.success(getDataTable(classicalRespList));
            }
            // 3-3 课外拓展
            if (childModule == 3){
                List<ClassicalResp> classicalRespList;
                PageHelper.startPage(pageNum,pageSize);
                classicalRespList = classicalMapper.selectClassicaListAll(req.getGradeId(), req.getModule(), req.getChildModule(),3L,detailName);
                return AjaxResult.success(getDataTable(classicalRespList));
            }
        }
        if (module == 4){
            // 4-1 名著阅读(经典赏析)
            if (childModule == 1){
                List<MasterpiecesBookResp> masterpiecesBookRespList;
                PageHelper.startPage(pageNum,pageSize);
                masterpiecesBookRespList = MasterpiecesBookMapper.selectMasterpiecesBookListAll(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(masterpiecesBookRespList));
            }
            // 4-2 中华文化
            if (childModule == 2){
                List<CultureBookResp> cultureBookRespList;
                PageHelper.startPage(pageNum,pageSize);
                cultureBookRespList = CultureBookMapper.selectcultureBookListAll(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(cultureBookRespList));
            }
            // 4-6 金句摘抄
            if (childModule == 6){
                List<ExcerptResp> ExcerptRespList;
                PageHelper.startPage(pageNum,pageSize);
                ExcerptRespList = excerptMapper.selectExcerptListAll(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(ExcerptRespList));
            }
        }

//        return gradeStudyPlanDetailService.moduleDetail();

        return  AjaxResult.error("查询不到模块内容2");
    }


    /**
     * 按照年级 模块 子模块 查询细节文章详情 小程序端，只显示能显示的
     * @param req
     * @return
     */
    @PostMapping("/module_detail_mobile")
    public AjaxResult moduleDetailMobile(@RequestBody GradeStudyPlanDetail req){
        log.info("module_detail_mobile is: {}", JSON.toJSONString(req));
        Long module = req.getModule();
        Long childModule = req.getChildModule();
        Integer pageNum = req.getPageNum();
        Integer pageSize = req.getPageSize();
        String detailName = req.getDetailName();
//        moule为1：child_module 1：字母表，2：声母表，3：韵母
//        moule为2：child_module 1：速斩生字，2：生词闯大关
//        moule为3：child_module 1：成语典故，2：人物外貌，3：人物类型，4：人物心理，5：人物心情
//        6：人物品质，7：言辞表达，8：为人处世，9：描写景物，10：描写四季
//        11：描写气候，12：十二生肖，13：其他动物 14：其他 15： 对比、比较
//        16：社会生活 17、社会斗争 18教育学习 19 政治法律 20 军事经济 21 成语结构
//        22： 近反义词 23：成语故事 24：语文大纲 25：四大名著 26：其他名著 27：出自名人
//        28：其他和人有关
//        Long isDynamic = DYNAMIC_DETAILS.get(module + "_" + childModule);
//        String moduleName = LIGHT_RECORDS.get(module + "_" + childModule);

        Long isDynamic = null;
        ModuleDict selModuleDict = new ModuleDict();
        selModuleDict.setModule(module);
        selModuleDict.setChildModule(childModule);
        List<ModuleDict> moduleDictList = moduleDictMapper.selectModuleDictList(selModuleDict);
        if (!CollectionUtils.isEmpty(moduleDictList)){
            isDynamic = moduleDictList.get(0).getDynamic();
        }
        if (isDynamic == null){
            return AjaxResult.error("查询不到模块内容1");
        }else if (isDynamic != 1L){
            return AjaxResult.error("此模块不允许自定义显示内容");
        }

        // 根据模块和子模块，自定义查询内容
        if (module == 1){
            //...
            // 1-2 速战生词
            if (childModule == 2){
                NewWordsSummaryRsp newWordsSummaryRsp = chineseCharactersService.selectNewWordListSummaryIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(newWordsSummaryRsp);
            }
            // 1-3 字源学字
            if (childModule == 3){
                // 查询显示文章的列表
                PageHelper.startPage(pageNum,pageSize);
                List<Etymology> etymologyRespList = etymologyMapper.selectEtymologyListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(), detailName);
                return AjaxResult.success(getDataTable(etymologyRespList));
            }
            // 1-5 成语典故
            if (childModule == 5){
                List<IdiomStory> idiomStoryRespList;
                // 查询显示文章的列表
                PageHelper.startPage(pageNum,pageSize);
                idiomStoryRespList = idiomStoryMapper.selectIdiomStoryListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(), detailName);
                return AjaxResult.success(getDataTable(idiomStoryRespList));
            }
            // 1-6 汉字起源
            if (childModule == 6){
                List<OriginOfChineseCharacters> originOfChineseCharactersList;
                PageHelper.startPage(pageNum,pageSize);
                originOfChineseCharactersList = originOfChineseCharactersMapper.selectOriginOfChineseCharactersListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(originOfChineseCharactersList));
            }
        }
        if (module == 2){
            //...
            // 2-1 国学启蒙
            if (childModule == 1){
                List<WisdomChapter> wisdomChapterList;
                PageHelper.startPage(pageNum,pageSize);
                wisdomChapterList = bizWisdomChapterMapper.selectWisdomChapterListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(),1L,detailName);
                return AjaxResult.success(getDataTable(wisdomChapterList));
            }
            // 2-2 思想元典
            if (childModule == 2){
                List<WisdomChapter> wisdomChapterList;
                PageHelper.startPage(pageNum,pageSize);
                wisdomChapterList = bizWisdomChapterMapper.selectWisdomChapterListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(),2L,detailName);
                return AjaxResult.success(getDataTable(wisdomChapterList));
            }
            // 2-3 唐诗宋词
            if (childModule == 3){
                List<PoetryOdeResp> poetryOdeRespList;
                PageHelper.startPage(pageNum,pageSize);
                poetryOdeRespList = PoetryOdeMapper.selectPoetryOdeListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(),detailName);
                return AjaxResult.success(getDataTable(poetryOdeRespList));
            }
            // 2-4 诗词常识
            if (childModule == 4){
                List<PoetrySenseResp> poetrySenseRespList;
                PageHelper.startPage(pageNum,pageSize);
                poetrySenseRespList = poetrySenseMapper.selectPoetrySenseListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(poetrySenseRespList));
            }
        }
        if (module == 3){
            // 3-1 古文初识
            if (childModule == 1){
                List<ClassicalResp> classicalRespList;
                PageHelper.startPage(pageNum,pageSize);
                classicalRespList = classicalMapper.selectClassicaListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(),1L,detailName);
                return AjaxResult.success(getDataTable(classicalRespList));
            }
            // 3-2 必备名篇
            if (childModule == 2){
                List<ClassicalResp> classicalRespList;
                PageHelper.startPage(pageNum,pageSize);
                classicalRespList = classicalMapper.selectClassicaListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(),2L,detailName);
                return AjaxResult.success(getDataTable(classicalRespList));
            }
            // 3-3 课外拓展
            if (childModule == 3){
                List<ClassicalResp> classicalRespList;
                PageHelper.startPage(pageNum,pageSize);
                classicalRespList = classicalMapper.selectClassicaListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule(),3L,detailName);
                return AjaxResult.success(getDataTable(classicalRespList));
            }
        }
        if (module == 4){
            // 4-1 名著阅读(经典赏析)
            if (childModule == 1){
                List<MasterpiecesBookResp> masterpiecesBookRespList;
                PageHelper.startPage(pageNum,pageSize);
                masterpiecesBookRespList = MasterpiecesBookMapper.selectMasterpiecesBookListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule());
                return AjaxResult.success(getDataTable(masterpiecesBookRespList));
            }
            // 4-2 中华文化
            if (childModule == 2){
                List<CultureBookResp> cultureBookRespRespList;
                PageHelper.startPage(pageNum,pageSize);
                cultureBookRespRespList = CultureBookMapper.selectcultureBookListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule());
                cultureBookRespRespList.stream().forEach(item -> {
                    CultureChapters cultureChapters = new CultureChapters();
                    cultureChapters.setBookId(item.getId());
                    cultureChapters.setPid(Long.valueOf(0));
                    List<CultureChapters> cultureChapters1 = cultureChaptersService.selectCultureChaptersList(cultureChapters);
                    if(!CollectionUtils.isEmpty(cultureChapters1)){
                        CultureText cultureText = new CultureText();
                        cultureText.setBookId(item.getId());
                        cultureText.setChapterId(cultureChapters1.get(0).getId());
                        List<CultureText> cultureTexts = cultureTextService.selectCultureTextList(cultureText);
                        item.setList(cultureTexts);
                    }
                });
                return AjaxResult.success(getDataTable(cultureBookRespRespList));
            }
            // 4-6 金句摘抄
            if (childModule == 6){
                List<ExcerptResp> ExcerptRespList;
                PageHelper.startPage(pageNum,pageSize);
                ExcerptRespList = excerptMapper.selectExcerptListIsDynamic(req.getGradeId(), req.getModule(), req.getChildModule());
                ExcerptRespList.forEach(item ->{
                    getExcerptLikeAndCommentNum(item);
                });
                return AjaxResult.success(getDataTable(ExcerptRespList));
            }
        }
//        return gradeStudyPlanDetailService.moduleDetail();
        return  AjaxResult.error("查询不到模块内容2");
    }

    private void getExcerptLikeAndCommentNum(ExcerptResp item) {
        ExcerptComment comment = new ExcerptComment();
        comment.setExcerptId(item.getId());
        List<ExcerptComment> excerptComments = excerptCommentMapper.selectExcerptCommentList(comment);
        if(!CollectionUtils.isEmpty(excerptComments)){
            item.setCommentNumber(Integer.toUnsignedLong(excerptComments.size()));
        }

        ExcerptLike excerptLike = new ExcerptLike();
        excerptLike.setExcerptId(item.getId());
        List<ExcerptLike> excerptLikes = excerptLikeMapper.selectExcerptLikeList(excerptLike);
        if(!CollectionUtils.isEmpty(excerptLikes)){
            item.setLikeNumber(Integer.toUnsignedLong(excerptLikes.size()));
        }


        ExcerptLike excerptLike1 = new ExcerptLike();
        excerptLike1.setExcerptId(item.getId());
        excerptLike1.setUserId(SecurityUtils.getUserId());
        List<ExcerptLike> excerptLikes1 = excerptLikeMapper.selectExcerptLikeList(excerptLike1);
        if(!CollectionUtils.isEmpty(excerptLikes1)){
            item.setIs_like(String.valueOf(excerptLikes1.get(0).getIsLike()));
        }
    }


}
