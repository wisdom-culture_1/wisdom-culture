package com.ruoyi.web.controller.business;

import com.alibaba.fastjson2.JSON;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.web.controller.util.ConstantPropertiesUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 诗词磨耳朵Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
public class BizUploadOSSFileController extends BaseController
{





    /**
     * 查询诗词磨耳朵列表
     */
    @PostMapping("/public_upload_oss")
    public AjaxResult list(@RequestParam("file")MultipartFile file)
    {
//        logger.info("public_upload_oss uploadFileAvatar file is {}",JSON.toJSONString(file));
        logger.info("uploadFileAvatar url inputStream originalFilename is {}",file.getOriginalFilename());

        AjaxResult ajaxResult = AjaxResult.newSuccess();
        String url = uploadFileAvatar(file);
        logger.info("public_upload_oss uploadFileAvatar url is {}",url);
        ajaxResult.put("data",url);
        return ajaxResult;
    }


    public String uploadFileAvatar(MultipartFile file) {

        //通过工具类来获取相应的值
        String endpoint = ConstantPropertiesUtils.END_POINT;
        String accessKeyId = ConstantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.ACCESS_KEY_SECRET;
        String bucketName = ConstantPropertiesUtils.BUCKET_NAME;

        try{

            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 获取上传文件的输入流
            InputStream inputStream = file.getInputStream();
//            logger.info("uploadFileAvatar url inputStream is {}",inputStream);

            String originalFilename = file.getOriginalFilename();


            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            String filePath = "uploads/"+ format.format(new Date())+"-"+ gen()+originalFilename.substring(originalFilename.lastIndexOf("."));
            //调用OSS方法实现上传
            //第一个参数 Bucket名称
            //第二个参数  上传到OSS文件路径和文件名称
            //第三个参数  上传文件输入流
            ossClient.putObject(bucketName, filePath, inputStream);

            // 关闭OSSClient。
            ossClient.shutdown();

            //把上传之后文件路径返回
            //需要把上传到阿里云oss路径手动拼接出来
            String url = filePath;
            logger.info("uploadFileAvatar url is {}",url);
            return url;
        }catch (Exception e){
            logger.error("public_upload_oss error",e);
            return null;
        }
    }

    public int gen() {
        Random r = new Random( System.currentTimeMillis() );
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }


}
