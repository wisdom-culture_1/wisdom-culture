package com.ruoyi.web.controller.business;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.business.entity.*;
import com.ruoyi.business.mapper.*;
import com.ruoyi.business.service.IListenBookService;
import com.ruoyi.common.utils.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 诗词磨耳朵Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/listen")
public class BizListenBookController extends BaseController
{
    @Autowired
    private IListenBookService listenBookService;

    @Autowired
    private BizWisdomBookMapper bizWisdomBookMapper;

    @Autowired
    private BizWisdomChapterMapper bizWisdomChapterMapper;

    @Autowired
    private BizPoetryOdeMapper bizPoetryOdeMapper;

    @Autowired
    private BizClassicalMapper bizClassicalMapper;

    @Autowired
    private BizMasterpiecesBookMapper bizMasterpiecesBookMapper;

    @Autowired
    private BizMasterpiecesTextMapper bizMasterpiecesTextMapper;

    /**
     * 查询诗词磨耳朵列表
     */
//    @PreAuthorize("@ss.hasPermi('system:book:list')")
    @PostMapping("/list")
    public AjaxResult list(@RequestBody ListenBook listenBook)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        if(StringUtils.isNotBlank(listenBook.getTitle())) listenBook.setName(listenBook.getTitle());
        List<ListenBook> list = listenBookService.selectListenBookList(listenBook);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出诗词磨耳朵列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:export')")
//    @Log(title = "诗词磨耳朵", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ListenBook listenBook)
//    {
//        List<ListenBook> list = listenBookService.selectListenBookList(listenBook);
//        ExcelUtil<ListenBook> util = new ExcelUtil<ListenBook>(ListenBook.class);
//        util.exportExcel(response, list, "诗词磨耳朵数据");
//    }

    /**
     * 查询逻辑：
     * 1.根据传递的id值查询表listen_book的数据。
     *
     * 2根据步骤1得到的数据字段-column判断查询哪张表及哪些字段，
     * column为1时，先根据步骤1得到的name字段（对应wisdom_book中的title）查询表wisdom_book中的id字段，
     * 取第一条数据；接着根据这个id（对应wisdom_chapter表中的book_id）的值查询表wisdom_chapter中的title、audio_src字段。
     *
     * column为2时，根据步骤1得到的name字段（对应poetry_ode中的title）查询表poetry_ode中的title、audio_src字段。
     *
     * column为3时，根据步骤1得到的name字段（对应classical中的title）查询表classical中的title、animation_audio_src字段，
     * 并将animation_audio_src替换为audio_src。
     *
     * column为4时，先根据步骤1得到的name字段（对应masterpieces_book中的title）查询表masterpieces_book中的id字段，
     * 取第一条数据；接着根据这个id（对应masterpieces_text表中的book_id）的值查询表masterpieces_text中的title、audio_src字段
     */
//    @PreAuthorize("@ss.hasPermi('system:book:query')")
    @GetMapping(value = "/getAudio")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

        List<ListenAudio> listenAudioList = new ArrayList<ListenAudio>();
        ListenBook listenBook = listenBookService.selectListenBookById(id);
        if(null != listenBook){
            if("1".equals(listenBook.getColumn())){
                WisdomBook wisdomBook = new WisdomBook();
                wisdomBook.setTitle(listenBook.getName());
                List<WisdomBook> wisdomBooks = bizWisdomBookMapper.selectWisdomBookList(wisdomBook);
                if(CollectionUtils.isNotEmpty(wisdomBooks)){
                    WisdomChapter wisdomChapter = new WisdomChapter();
                    wisdomChapter.setBookId(wisdomBooks.get(0).getId());
                    List<WisdomChapter> wisdomChapters = bizWisdomChapterMapper.selectWisdomChapterListWithoutBigText(wisdomChapter);
                    wisdomChapters.forEach(item ->{
                        ListenAudio listenAudio = new ListenAudio();
                        listenAudio.setAudio_src(item.getAudioSrc());
                        listenAudio.setTitle(item.getTitle());
                        listenAudioList.add(listenAudio);
                    });
                }
            }else if("2".equals(listenBook.getColumn())){
                PoetryOde poetryOde = new PoetryOde();
                poetryOde.setTitle(listenBook.getName());
                List<PoetryOde> poetryOdes = bizPoetryOdeMapper.selectPoetryOdeList(poetryOde);
                poetryOdes.forEach(item ->{
                    ListenAudio listenAudio = new ListenAudio();
                    listenAudio.setTitle(item.getTitle());
                    listenAudio.setAudio_src(item.getAudioSrc());
                    listenAudioList.add(listenAudio);
                });
            }else if("3".equals(listenBook.getColumn())){
                Classical classical = new Classical();
                classical.setTitle(listenBook.getName());
                List<Classical> classicals = bizClassicalMapper.selectClassicalListWithoutBigText(classical);
                classicals.forEach(item -> {
                    ListenAudio listenAudio = new ListenAudio();
                    listenAudio.setTitle(item.getTitle());
                    listenAudio.setAudio_src(item.getAnimationAudioSrc());
                    listenAudioList.add(listenAudio);
                });

            }else if("4".equals(listenBook.getColumn())){
                MasterpiecesBook masterpiecesBook = new MasterpiecesBook();
                masterpiecesBook.setTitle(listenBook.getName());
                List<MasterpiecesBook> masterpiecesBooks = bizMasterpiecesBookMapper.selectMasterpiecesBookList(masterpiecesBook);
                if(CollectionUtils.isNotEmpty(masterpiecesBooks)){
                    MasterpiecesText masterpiecesText = new MasterpiecesText();
                    masterpiecesText.setBookId(masterpiecesBooks.get(0).getId());
                    List<MasterpiecesText> masterpiecesTexts = bizMasterpiecesTextMapper.selectMasterpiecesTextListWithoutBigText(masterpiecesText);
                    masterpiecesTexts.forEach(item -> {
                        ListenAudio listenAudio = new ListenAudio();
                        listenAudio.setAudio_src(item.getAudioSrc());
                        listenAudio.setTitle(item.getTitle());
                        listenAudioList.add(listenAudio);
                    });
                }
            }
        }
        ajaxResult.put("data",listenAudioList);
        return ajaxResult;
    }

//    /**
//     * 新增诗词磨耳朵
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:add')")
//    @Log(title = "诗词磨耳朵", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ListenBook listenBook)
//    {
//        return toAjax(listenBookService.insertListenBook(listenBook));
//    }
//
//    /**
//     * 修改诗词磨耳朵
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:edit')")
//    @Log(title = "诗词磨耳朵", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ListenBook listenBook)
//    {
//        return toAjax(listenBookService.updateListenBook(listenBook));
//    }
//
//    /**
//     * 删除诗词磨耳朵
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:remove')")
//    @Log(title = "诗词磨耳朵", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(listenBookService.deleteListenBookByIds(ids));
//    }
}
