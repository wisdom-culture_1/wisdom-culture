package com.ruoyi.web.controller.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.ZdBushou;
import com.ruoyi.business.service.IZdBushouService;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizZdBushouController extends BaseController
{
    @Autowired
    private IZdBushouService zdBushouService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:bushou:list')")
    @GetMapping("/radical_character")
    public AjaxResult list(ZdBushou zdBushou)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ZdBushou> list = zdBushouService.selectZdBushouList(zdBushou);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:bushou:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ZdBushou zdBushou)
//    {
//        List<ZdBushou> list = zdBushouService.selectZdBushouList(zdBushou);
//        ExcelUtil<ZdBushou> util = new ExcelUtil<ZdBushou>(ZdBushou.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }
//
//    /**
//     * 获取【请填写功能名称】详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:bushou:query')")
//    @GetMapping(value = "/{xuhao}")
//    public AjaxResult getInfo(@PathVariable("xuhao") Long xuhao)
//    {
//        return success(zdBushouService.selectZdBushouByXuhao(xuhao));
//    }
//
//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:bushou:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ZdBushou zdBushou)
//    {
//        return toAjax(zdBushouService.insertZdBushou(zdBushou));
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:bushou:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ZdBushou zdBushou)
//    {
//        return toAjax(zdBushouService.updateZdBushou(zdBushou));
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:bushou:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{xuhaos}")
//    public AjaxResult remove(@PathVariable Long[] xuhaos)
//    {
//        return toAjax(zdBushouService.deleteZdBushouByXuhaos(xuhaos));
//    }
}
