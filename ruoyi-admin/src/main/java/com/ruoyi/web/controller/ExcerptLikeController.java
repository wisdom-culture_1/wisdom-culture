package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ExcerptLike;
import com.ruoyi.system.service.IExcerptLikeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金句摘抄点赞Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/excerpt_like")
public class ExcerptLikeController extends BaseController
{
    @Autowired
    private IExcerptLikeService excerptLikeService;

    /**
     * 查询金句摘抄点赞列表
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_like:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExcerptLike excerptLike)
    {
        startPage();
        List<ExcerptLike> list = excerptLikeService.selectExcerptLikeList(excerptLike);
        return getDataTable(list);
    }

    /**
     * 导出金句摘抄点赞列表
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_like:export')")
    @Log(title = "金句摘抄点赞", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExcerptLike excerptLike)
    {
        List<ExcerptLike> list = excerptLikeService.selectExcerptLikeList(excerptLike);
        ExcelUtil<ExcerptLike> util = new ExcelUtil<ExcerptLike>(ExcerptLike.class);
        util.exportExcel(response, list, "金句摘抄点赞数据");
    }

    /**
     * 获取金句摘抄点赞详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_like:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(excerptLikeService.selectExcerptLikeById(id));
    }

    /**
     * 新增金句摘抄点赞
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_like:add')")
    @Log(title = "金句摘抄点赞", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExcerptLike excerptLike)
    {
        return toAjax(excerptLikeService.insertExcerptLike(excerptLike));
    }

    /**
     * 修改金句摘抄点赞
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_like:edit')")
    @Log(title = "金句摘抄点赞", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExcerptLike excerptLike)
    {
        return toAjax(excerptLikeService.updateExcerptLike(excerptLike));
    }

    /**
     * 删除金句摘抄点赞
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_like:remove')")
    @Log(title = "金句摘抄点赞", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(excerptLikeService.deleteExcerptLikeByIds(ids));
    }
}
