package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MasterpiecesChaptersProcess;
import com.ruoyi.system.service.IMasterpiecesChaptersProcessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 章节试听进度Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/masterpieces_chapters_process")
public class MasterpiecesChaptersProcessController extends BaseController
{
    @Autowired
    private IMasterpiecesChaptersProcessService masterpiecesChaptersProcessService;

    /**
     * 查询章节试听进度列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_chapters_process:list')")
    @GetMapping("/list")
    public TableDataInfo list(MasterpiecesChaptersProcess masterpiecesChaptersProcess)
    {
        startPage();
        List<MasterpiecesChaptersProcess> list = masterpiecesChaptersProcessService.selectMasterpiecesChaptersProcessList(masterpiecesChaptersProcess);
        return getDataTable(list);
    }

    /**
     * 导出章节试听进度列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_chapters_process:export')")
    @Log(title = "章节试听进度", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MasterpiecesChaptersProcess masterpiecesChaptersProcess)
    {
        List<MasterpiecesChaptersProcess> list = masterpiecesChaptersProcessService.selectMasterpiecesChaptersProcessList(masterpiecesChaptersProcess);
        ExcelUtil<MasterpiecesChaptersProcess> util = new ExcelUtil<MasterpiecesChaptersProcess>(MasterpiecesChaptersProcess.class);
        util.exportExcel(response, list, "章节试听进度数据");
    }

    /**
     * 获取章节试听进度详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_chapters_process:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(masterpiecesChaptersProcessService.selectMasterpiecesChaptersProcessById(id));
    }

    /**
     * 新增章节试听进度
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_chapters_process:add')")
    @Log(title = "章节试听进度", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MasterpiecesChaptersProcess masterpiecesChaptersProcess)
    {
        return toAjax(masterpiecesChaptersProcessService.insertMasterpiecesChaptersProcess(masterpiecesChaptersProcess));
    }

    /**
     * 修改章节试听进度
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_chapters_process:edit')")
    @Log(title = "章节试听进度", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MasterpiecesChaptersProcess masterpiecesChaptersProcess)
    {
        return toAjax(masterpiecesChaptersProcessService.updateMasterpiecesChaptersProcess(masterpiecesChaptersProcess));
    }

    /**
     * 删除章节试听进度
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_chapters_process:remove')")
    @Log(title = "章节试听进度", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(masterpiecesChaptersProcessService.deleteMasterpiecesChaptersProcessByIds(ids));
    }
}
