package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.ExpertLectureHall;
import com.ruoyi.business.service.IExpertLectureHallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 专家讲堂视频Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/lecture")
public class BizExpertLectureHallController extends BaseController
{
    @Autowired
    private IExpertLectureHallService expertLectureHallService;

    /**
     * 查询专家讲堂视频列表
     */
//    @PreAuthorize("@ss.hasPermi('system:hall:list')")
    @GetMapping("/hall_type")
    public AjaxResult list(ExpertLectureHall expertLectureHall)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ExpertLectureHall> list = expertLectureHallService.selectExpertLectureHallList(expertLectureHall);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;

    }

//    /**
//     * 导出专家讲堂视频列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:hall:export')")
//    @Log(title = "专家讲堂视频", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ExpertLectureHall expertLectureHall)
//    {
//        List<ExpertLectureHall> list = expertLectureHallService.selectExpertLectureHallList(expertLectureHall);
//        ExcelUtil<ExpertLectureHall> util = new ExcelUtil<ExpertLectureHall>(ExpertLectureHall.class);
//        util.exportExcel(response, list, "专家讲堂视频数据");
//    }

    /**
     * 获取专家讲堂视频详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:hall:query')")
    @GetMapping(value = "/type_id")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(expertLectureHallService.selectExpertLectureHallById(id));
    }

//    /**
//     * 新增专家讲堂视频
//     */
//    @PreAuthorize("@ss.hasPermi('system:hall:add')")
//    @Log(title = "专家讲堂视频", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ExpertLectureHall expertLectureHall)
//    {
//        return toAjax(expertLectureHallService.insertExpertLectureHall(expertLectureHall));
//    }
//
//    /**
//     * 修改专家讲堂视频
//     */
//    @PreAuthorize("@ss.hasPermi('system:hall:edit')")
//    @Log(title = "专家讲堂视频", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ExpertLectureHall expertLectureHall)
//    {
//        return toAjax(expertLectureHallService.updateExpertLectureHall(expertLectureHall));
//    }
//
//    /**
//     * 删除专家讲堂视频
//     */
//    @PreAuthorize("@ss.hasPermi('system:hall:remove')")
//    @Log(title = "专家讲堂视频", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(expertLectureHallService.deleteExpertLectureHallByIds(ids));
//    }
}
