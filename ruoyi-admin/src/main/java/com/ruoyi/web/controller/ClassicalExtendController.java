package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.ClassicalExtend;
import com.ruoyi.system.service.IClassicalExtendService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文言名篇扩展Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/classical_extend")
public class ClassicalExtendController extends BaseController
{
    @Autowired
    private IClassicalExtendService classicalExtendService;

    /**
     * 查询文言名篇扩展列表
     */
    @PreAuthorize("@ss.hasPermi('system:classical_extend:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClassicalExtend classicalExtend)
    {
        startPage();
        List<ClassicalExtend> list = classicalExtendService.selectClassicalExtendList(classicalExtend);
        return getDataTable(list);
    }

    /**
     * 导出文言名篇扩展列表
     */
    @PreAuthorize("@ss.hasPermi('system:classical_extend:export')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ClassicalExtend classicalExtend)
    {
        List<ClassicalExtend> list = classicalExtendService.selectClassicalExtendList(classicalExtend);
        ExcelUtil<ClassicalExtend> util = new ExcelUtil<ClassicalExtend>(ClassicalExtend.class);
        util.exportExcel(response, list, "文言名篇扩展数据");
    }

    /**
     * 获取文言名篇扩展详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:classical_extend:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(classicalExtendService.selectClassicalExtendById(id));
    }

    /**
     * 新增文言名篇扩展
     */
    @PreAuthorize("@ss.hasPermi('system:classical_extend:add')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClassicalExtend classicalExtend)
    {
        return toAjax(classicalExtendService.insertClassicalExtend(classicalExtend));
    }

    /**
     * 修改文言名篇扩展
     */
    @PreAuthorize("@ss.hasPermi('system:classical_extend:edit')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClassicalExtend classicalExtend)
    {
        return toAjax(classicalExtendService.updateClassicalExtend(classicalExtend));
    }

    /**
     * 删除文言名篇扩展
     */
    @PreAuthorize("@ss.hasPermi('system:classical_extend:remove')")
    @Log(title = "文言名篇扩展", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(classicalExtendService.deleteClassicalExtendByIds(ids));
    }
}
