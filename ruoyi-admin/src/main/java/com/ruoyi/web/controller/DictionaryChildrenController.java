package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DictionaryChildren;
import com.ruoyi.system.service.IDictionaryChildrenService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 子字典表Controller
 * 
 * @author ruoyi
 * @date 2023-08-25
 */
@RestController
@RequestMapping("/system/dictionaryChildren")
public class DictionaryChildrenController extends BaseController
{
    @Autowired
    private IDictionaryChildrenService dictionaryChildrenService;

    /**
     * 查询子字典表列表
     */
    @PreAuthorize("@ss.hasPermi('system:dictionaryChildren:list')")
    @GetMapping("/list")
    public TableDataInfo list(DictionaryChildren dictionaryChildren)
    {
        startPage();
        List<DictionaryChildren> list = dictionaryChildrenService.selectDictionaryChildrenList(dictionaryChildren);
        return getDataTable(list);
    }

    /**
     * 导出子字典表列表
     */
    @PreAuthorize("@ss.hasPermi('system:dictionaryChildren:export')")
    @Log(title = "子字典表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DictionaryChildren dictionaryChildren)
    {
        List<DictionaryChildren> list = dictionaryChildrenService.selectDictionaryChildrenList(dictionaryChildren);
        ExcelUtil<DictionaryChildren> util = new ExcelUtil<DictionaryChildren>(DictionaryChildren.class);
        util.exportExcel(response, list, "子字典表数据");
    }

    /**
     * 获取子字典表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:dictionaryChildren:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(dictionaryChildrenService.selectDictionaryChildrenById(id));
    }

    /**
     * 新增子字典表
     */
    @PreAuthorize("@ss.hasPermi('system:dictionaryChildren:add')")
    @Log(title = "子字典表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DictionaryChildren dictionaryChildren)
    {
        return toAjax(dictionaryChildrenService.insertDictionaryChildren(dictionaryChildren));
    }

    /**
     * 修改子字典表
     */
    @PreAuthorize("@ss.hasPermi('system:dictionaryChildren:edit')")
    @Log(title = "子字典表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DictionaryChildren dictionaryChildren)
    {
        return toAjax(dictionaryChildrenService.updateDictionaryChildren(dictionaryChildren));
    }

    /**
     * 删除子字典表
     */
    @PreAuthorize("@ss.hasPermi('system:dictionaryChildren:remove')")
    @Log(title = "子字典表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(dictionaryChildrenService.deleteDictionaryChildrenByIds(ids));
    }
}
