package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WisdomBook;
import com.ruoyi.system.service.IWisdomBookService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 智慧元典书籍Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/book")
public class WisdomBookController extends BaseController
{
    @Autowired
    private IWisdomBookService wisdomBookService;

    /**
     * 查询智慧元典书籍列表
     */
    @PreAuthorize("@ss.hasPermi('system:book:list')")
    @GetMapping("/list")
    public TableDataInfo list(WisdomBook wisdomBook)
    {
        startPage();
        List<WisdomBook> list = wisdomBookService.selectWisdomBookList(wisdomBook);
        return getDataTable(list);
    }

    /**
     * 导出智慧元典书籍列表
     */
    @PreAuthorize("@ss.hasPermi('system:book:export')")
    @Log(title = "智慧元典书籍", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WisdomBook wisdomBook)
    {
        List<WisdomBook> list = wisdomBookService.selectWisdomBookList(wisdomBook);
        ExcelUtil<WisdomBook> util = new ExcelUtil<WisdomBook>(WisdomBook.class);
        util.exportExcel(response, list, "智慧元典书籍数据");
    }

    /**
     * 获取智慧元典书籍详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:book:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wisdomBookService.selectWisdomBookById(id));
    }

    /**
     * 新增智慧元典书籍
     */
    @PreAuthorize("@ss.hasPermi('system:book:add')")
    @Log(title = "智慧元典书籍", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WisdomBook wisdomBook)
    {
        return toAjax(wisdomBookService.insertWisdomBook(wisdomBook));
    }

    /**
     * 修改智慧元典书籍
     */
    @PreAuthorize("@ss.hasPermi('system:book:edit')")
    @Log(title = "智慧元典书籍", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WisdomBook wisdomBook)
    {
        return toAjax(wisdomBookService.updateWisdomBook(wisdomBook));
    }

    /**
     * 删除智慧元典书籍
     */
    @PreAuthorize("@ss.hasPermi('system:book:remove')")
    @Log(title = "智慧元典书籍", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wisdomBookService.deleteWisdomBookByIds(ids));
    }
}
