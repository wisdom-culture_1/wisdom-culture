package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PoetryOdeExtend;
import com.ruoyi.system.service.IPoetryOdeExtendService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 诗词歌赋扩展Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/extend")
public class PoetryOdeExtendController extends BaseController
{
    @Autowired
    private IPoetryOdeExtendService poetryOdeExtendService;

    /**
     * 查询诗词歌赋扩展列表
     */
    @PreAuthorize("@ss.hasPermi('system:extend:list')")
    @GetMapping("/list")
    public TableDataInfo list(PoetryOdeExtend poetryOdeExtend)
    {
        startPage();
        List<PoetryOdeExtend> list = poetryOdeExtendService.selectPoetryOdeExtendList(poetryOdeExtend);
        return getDataTable(list);
    }

    /**
     * 导出诗词歌赋扩展列表
     */
    @PreAuthorize("@ss.hasPermi('system:extend:export')")
    @Log(title = "诗词歌赋扩展", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PoetryOdeExtend poetryOdeExtend)
    {
        List<PoetryOdeExtend> list = poetryOdeExtendService.selectPoetryOdeExtendList(poetryOdeExtend);
        ExcelUtil<PoetryOdeExtend> util = new ExcelUtil<PoetryOdeExtend>(PoetryOdeExtend.class);
        util.exportExcel(response, list, "诗词歌赋扩展数据");
    }

    /**
     * 获取诗词歌赋扩展详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:extend:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(poetryOdeExtendService.selectPoetryOdeExtendById(id));
    }

    /**
     * 新增诗词歌赋扩展
     */
    @PreAuthorize("@ss.hasPermi('system:extend:add')")
    @Log(title = "诗词歌赋扩展", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PoetryOdeExtend poetryOdeExtend)
    {
        return toAjax(poetryOdeExtendService.insertPoetryOdeExtend(poetryOdeExtend));
    }

    /**
     * 修改诗词歌赋扩展
     */
    @PreAuthorize("@ss.hasPermi('system:extend:edit')")
    @Log(title = "诗词歌赋扩展", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PoetryOdeExtend poetryOdeExtend)
    {
        return toAjax(poetryOdeExtendService.updatePoetryOdeExtend(poetryOdeExtend));
    }

    /**
     * 删除诗词歌赋扩展
     */
    @PreAuthorize("@ss.hasPermi('system:extend:remove')")
    @Log(title = "诗词歌赋扩展", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(poetryOdeExtendService.deletePoetryOdeExtendByIds(ids));
    }
}
