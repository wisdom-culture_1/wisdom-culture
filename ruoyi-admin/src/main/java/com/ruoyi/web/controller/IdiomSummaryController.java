package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.IdiomSummary;
import com.ruoyi.system.service.IIdiomSummaryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 成语汇总Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/summary")
public class IdiomSummaryController extends BaseController
{
    @Autowired
    private IIdiomSummaryService idiomSummaryService;

    /**
     * 查询成语汇总列表
     */
    @PreAuthorize("@ss.hasPermi('system:summary:list')")
    @GetMapping("/list")
    public TableDataInfo list(IdiomSummary idiomSummary)
    {
        startPage();
        List<IdiomSummary> list = idiomSummaryService.selectIdiomSummaryList(idiomSummary);
        return getDataTable(list);
    }

    /**
     * 导出成语汇总列表
     */
    @PreAuthorize("@ss.hasPermi('system:summary:export')")
    @Log(title = "成语汇总", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IdiomSummary idiomSummary)
    {
        List<IdiomSummary> list = idiomSummaryService.selectIdiomSummaryList(idiomSummary);
        ExcelUtil<IdiomSummary> util = new ExcelUtil<IdiomSummary>(IdiomSummary.class);
        util.exportExcel(response, list, "成语汇总数据");
    }

    /**
     * 获取成语汇总详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:summary:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(idiomSummaryService.selectIdiomSummaryById(id));
    }

    /**
     * 新增成语汇总
     */
    @PreAuthorize("@ss.hasPermi('system:summary:add')")
    @Log(title = "成语汇总", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IdiomSummary idiomSummary)
    {
        return toAjax(idiomSummaryService.insertIdiomSummary(idiomSummary));
    }

    /**
     * 修改成语汇总
     */
    @PreAuthorize("@ss.hasPermi('system:summary:edit')")
    @Log(title = "成语汇总", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IdiomSummary idiomSummary)
    {
        return toAjax(idiomSummaryService.updateIdiomSummary(idiomSummary));
    }

    /**
     * 删除成语汇总
     */
    @PreAuthorize("@ss.hasPermi('system:summary:remove')")
    @Log(title = "成语汇总", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(idiomSummaryService.deleteIdiomSummaryByIds(ids));
    }
}
