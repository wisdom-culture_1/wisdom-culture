package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WisdomText;
import com.ruoyi.system.service.IWisdomTextService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 智慧元典正文Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/text")
public class WisdomTextController extends BaseController
{
    @Autowired
    private IWisdomTextService wisdomTextService;

    /**
     * 查询智慧元典正文列表
     */
    @PreAuthorize("@ss.hasPermi('system:text:list')")
    @GetMapping("/list")
    public TableDataInfo list(WisdomText wisdomText)
    {
        startPage();
        List<WisdomText> list = wisdomTextService.selectWisdomTextList(wisdomText);
        return getDataTable(list);
    }

    /**
     * 导出智慧元典正文列表
     */
    @PreAuthorize("@ss.hasPermi('system:text:export')")
    @Log(title = "智慧元典正文", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WisdomText wisdomText)
    {
        List<WisdomText> list = wisdomTextService.selectWisdomTextList(wisdomText);
        ExcelUtil<WisdomText> util = new ExcelUtil<WisdomText>(WisdomText.class);
        util.exportExcel(response, list, "智慧元典正文数据");
    }

    /**
     * 获取智慧元典正文详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:text:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wisdomTextService.selectWisdomTextById(id));
    }

    /**
     * 新增智慧元典正文
     */
    @PreAuthorize("@ss.hasPermi('system:text:add')")
    @Log(title = "智慧元典正文", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WisdomText wisdomText)
    {
        return toAjax(wisdomTextService.insertWisdomText(wisdomText));
    }

    /**
     * 修改智慧元典正文
     */
    @PreAuthorize("@ss.hasPermi('system:text:edit')")
    @Log(title = "智慧元典正文", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WisdomText wisdomText)
    {
        return toAjax(wisdomTextService.updateWisdomText(wisdomText));
    }

    /**
     * 删除智慧元典正文
     */
    @PreAuthorize("@ss.hasPermi('system:text:remove')")
    @Log(title = "智慧元典正文", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wisdomTextService.deleteWisdomTextByIds(ids));
    }
}
