package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.WisdomBook;
import com.ruoyi.business.service.IWisdomBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 智慧元典书籍Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/wisdom")
public class BizWisdomBookController extends BaseController
{
    @Autowired
    private IWisdomBookService wisdomBookService;

    /**
     * 查询智慧元典书籍列表
     */
//    @PreAuthorize("@ss.hasPermi('system:book:list')")
    @GetMapping("/book_list")
    public AjaxResult list(WisdomBook wisdomBook)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<WisdomBook> list = wisdomBookService.selectWisdomBookList(wisdomBook);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出智慧元典书籍列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:export')")
//    @Log(title = "智慧元典书籍", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, WisdomBook wisdomBook)
//    {
//        List<WisdomBook> list = wisdomBookService.selectWisdomBookList(wisdomBook);
//        ExcelUtil<WisdomBook> util = new ExcelUtil<WisdomBook>(WisdomBook.class);
//        util.exportExcel(response, list, "智慧元典书籍数据");
//    }
//
//    /**
//     * 获取智慧元典书籍详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(wisdomBookService.selectWisdomBookById(id));
//    }
//
//    /**
//     * 新增智慧元典书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:add')")
//    @Log(title = "智慧元典书籍", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody WisdomBook wisdomBook)
//    {
//        return toAjax(wisdomBookService.insertWisdomBook(wisdomBook));
//    }
//
//    /**
//     * 修改智慧元典书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:edit')")
//    @Log(title = "智慧元典书籍", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody WisdomBook wisdomBook)
//    {
//        return toAjax(wisdomBookService.updateWisdomBook(wisdomBook));
//    }
//
//    /**
//     * 删除智慧元典书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:remove')")
//    @Log(title = "智慧元典书籍", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(wisdomBookService.deleteWisdomBookByIds(ids));
//    }
}
