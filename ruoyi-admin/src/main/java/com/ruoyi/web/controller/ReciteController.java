package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Recite;
import com.ruoyi.system.service.IReciteService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 背诵打卡Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/recite")
public class ReciteController extends BaseController
{
    @Autowired
    private IReciteService reciteService;

    /**
     * 查询背诵打卡列表
     */
    @PreAuthorize("@ss.hasPermi('system:recite:list')")
    @GetMapping("/list")
    public TableDataInfo list(Recite recite)
    {
        startPage();
        List<Recite> list = reciteService.selectReciteList(recite);
        return getDataTable(list);
    }

    /**
     * 导出背诵打卡列表
     */
    @PreAuthorize("@ss.hasPermi('system:recite:export')")
    @Log(title = "背诵打卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Recite recite)
    {
        List<Recite> list = reciteService.selectReciteList(recite);
        ExcelUtil<Recite> util = new ExcelUtil<Recite>(Recite.class);
        util.exportExcel(response, list, "背诵打卡数据");
    }

    /**
     * 获取背诵打卡详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:recite:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(reciteService.selectReciteById(id));
    }

    /**
     * 新增背诵打卡
     */
    @PreAuthorize("@ss.hasPermi('system:recite:add')")
    @Log(title = "背诵打卡", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Recite recite)
    {
        return toAjax(reciteService.insertRecite(recite));
    }

    /**
     * 修改背诵打卡
     */
    @PreAuthorize("@ss.hasPermi('system:recite:edit')")
    @Log(title = "背诵打卡", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Recite recite)
    {
        return toAjax(reciteService.updateRecite(recite));
    }

    /**
     * 删除背诵打卡
     */
    @PreAuthorize("@ss.hasPermi('system:recite:remove')")
    @Log(title = "背诵打卡", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(reciteService.deleteReciteByIds(ids));
    }
}
