package com.ruoyi.web.controller.business;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.InitialConsonant;
import com.ruoyi.business.service.IInitialConsonantService;

/**
 * 声母 韵母Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizInitialConsonantController extends BaseController
{
    @Autowired
    private IInitialConsonantService initialConsonantService;

    /**
     * 查询声母 韵母列表
     */
//    @PreAuthorize("@ss.hasPermi('system:consonant:list')")
    @GetMapping("/consonant_letter")
    public AjaxResult list(InitialConsonant initialConsonant)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<InitialConsonant> list = initialConsonantService.selectInitialConsonantList(initialConsonant);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出声母 韵母列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:consonant:export')")
//    @Log(title = "声母 韵母", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, InitialConsonant initialConsonant)
//    {
//        List<InitialConsonant> list = initialConsonantService.selectInitialConsonantList(initialConsonant);
//        ExcelUtil<InitialConsonant> util = new ExcelUtil<InitialConsonant>(InitialConsonant.class);
//        util.exportExcel(response, list, "声母 韵母数据");
//    }

    /**
     * 获取声母 韵母详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:consonant:query')")
    @GetMapping(value = "/consonant_detail")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        InitialConsonant initialConsonant = initialConsonantService.selectInitialConsonantById(id);
        ajaxResult.put("data",initialConsonant);
        return ajaxResult;
    }
//
//    /**
//     * 新增声母 韵母
//     */
//    @PreAuthorize("@ss.hasPermi('system:consonant:add')")
//    @Log(title = "声母 韵母", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody InitialConsonant initialConsonant)
//    {
//        return toAjax(initialConsonantService.insertInitialConsonant(initialConsonant));
//    }
//
//    /**
//     * 修改声母 韵母
//     */
//    @PreAuthorize("@ss.hasPermi('system:consonant:edit')")
//    @Log(title = "声母 韵母", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody InitialConsonant initialConsonant)
//    {
//        return toAjax(initialConsonantService.updateInitialConsonant(initialConsonant));
//    }
//
//    /**
//     * 删除声母 韵母
//     */
//    @PreAuthorize("@ss.hasPermi('system:consonant:remove')")
//    @Log(title = "声母 韵母", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(initialConsonantService.deleteInitialConsonantByIds(ids));
//    }
}
