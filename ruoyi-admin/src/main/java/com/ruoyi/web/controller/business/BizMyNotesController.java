package com.ruoyi.web.controller.business;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.entity.AdminMenu;
import com.ruoyi.business.entity.MyNotes;
import com.ruoyi.business.service.IAdminMenuService;
import com.ruoyi.business.service.IMyNotesService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/mynotes")
public class BizMyNotesController extends BaseController
{
    @Autowired
    private IMyNotesService myNotesService;
    @Autowired
    private IAdminMenuService adminMenuService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:notes:list')")
    @GetMapping("/list")
    public AjaxResult list(MyNotes myNotes)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<MyNotes> list = myNotesService.selectMyNotesList(myNotes);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:notes:list')")
    @GetMapping("/module_list")
    public AjaxResult list(AdminMenu adminMenu)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        adminMenu.setParentId(Long.valueOf(0));
        List<AdminMenu> adminMenus = adminMenuService.selectAdminMenuList(adminMenu);

        ajaxResult.put("data",adminMenus);
        return ajaxResult;
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:notes:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MyNotes myNotes)
    {
        List<MyNotes> list = myNotesService.selectMyNotesList(myNotes);
        ExcelUtil<MyNotes> util = new ExcelUtil<MyNotes>(MyNotes.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:notes:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(myNotesService.selectMyNotesById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
//    @PreAuthorize("@ss.hasPermi('system:notes:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody MyNotes myNotes)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        myNotes.setCreatedAt(new Date());
        myNotes.setUpdatedAt(new Date());
        myNotesService.insertMyNotes(myNotes);
        ajaxResult.put("updatedAt",myNotes.getUpdatedAt());
        return ajaxResult;
    }

    /**
     * 修改【请填写功能名称】
     */
//    @PreAuthorize("@ss.hasPermi('system:notes:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    public AjaxResult edit(@RequestBody MyNotes myNotes)
    {
        myNotes.setId(myNotes.getNotes_id());
        return toAjax(myNotesService.updateMyNotes(myNotes));
    }

    /**
     * 删除【请填写功能名称】
     */
//    @PreAuthorize("@ss.hasPermi('system:notes:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@GetMapping("/del")
    public AjaxResult remove(@RequestParam("notes_id")String id)
    {
        return toAjax(myNotesService.deleteMyNotesById(id));
    }
}
