package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MasterpiecesText;
import com.ruoyi.system.service.IMasterpiecesTextService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 名著章节内容Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/masterpieces_text")
public class MasterpiecesTextController extends BaseController
{
    @Autowired
    private IMasterpiecesTextService masterpiecesTextService;

    /**
     * 查询名著章节内容列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_text:list')")
    @GetMapping("/list")
    public TableDataInfo list(MasterpiecesText masterpiecesText)
    {
        startPage();
        List<MasterpiecesText> list = masterpiecesTextService.selectMasterpiecesTextList(masterpiecesText);
        return getDataTable(list);
    }

    /**
     * 导出名著章节内容列表
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_text:export')")
    @Log(title = "名著章节内容", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MasterpiecesText masterpiecesText)
    {
        List<MasterpiecesText> list = masterpiecesTextService.selectMasterpiecesTextList(masterpiecesText);
        ExcelUtil<MasterpiecesText> util = new ExcelUtil<MasterpiecesText>(MasterpiecesText.class);
        util.exportExcel(response, list, "名著章节内容数据");
    }

    /**
     * 获取名著章节内容详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_text:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(masterpiecesTextService.selectMasterpiecesTextById(id));
    }

    /**
     * 新增名著章节内容
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_text:add')")
    @Log(title = "名著章节内容", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MasterpiecesText masterpiecesText)
    {
        return toAjax(masterpiecesTextService.insertMasterpiecesText(masterpiecesText));
    }

    /**
     * 修改名著章节内容
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_text:edit')")
    @Log(title = "名著章节内容", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MasterpiecesText masterpiecesText)
    {
        return toAjax(masterpiecesTextService.updateMasterpiecesText(masterpiecesText));
    }

    /**
     * 删除名著章节内容
     */
    @PreAuthorize("@ss.hasPermi('system:masterpieces_text:remove')")
    @Log(title = "名著章节内容", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(masterpiecesTextService.deleteMasterpiecesTextByIds(ids));
    }
}
