package com.ruoyi.web.controller.business;

import com.ruoyi.business.entity.BeyondcampusUnion;
import com.ruoyi.business.entity.BeyondcampusUser;
import com.ruoyi.business.service.IBeyondcampusUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/beyond")
public class BizBeyondcampusUserController extends BaseController
{
    @Autowired
    private IBeyondcampusUserService beyondcampusUserService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/list")
    public AjaxResult list(BeyondcampusUser beyondcampusUser)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        BeyondcampusUnion union = beyondcampusUserService.selectBeyondcampusAndMyList(beyondcampusUser);
        ajaxResult.put("data",union);
        return ajaxResult;
    }

//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:user:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, BeyondcampusUser beyondcampusUser)
//    {
//        List<BeyondcampusUser> list = beyondcampusUserService.selectBeyondcampusUserList(beyondcampusUser);
//        ExcelUtil<BeyondcampusUser> util = new ExcelUtil<BeyondcampusUser>(BeyondcampusUser.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }
//
//    /**
//     * 获取【请填写功能名称】详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:user:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(beyondcampusUserService.selectBeyondcampusUserById(id));
//    }
//
//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:user:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody BeyondcampusUser beyondcampusUser)
//    {
//        return toAjax(beyondcampusUserService.insertBeyondcampusUser(beyondcampusUser));
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:user:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody BeyondcampusUser beyondcampusUser)
//    {
//        return toAjax(beyondcampusUserService.updateBeyondcampusUser(beyondcampusUser));
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:user:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(beyondcampusUserService.deleteBeyondcampusUserByIds(ids));
//    }
}
