package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.PoetryOde;
import com.ruoyi.business.service.IPoetryOdeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 诗词歌赋Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/poetry")
public class BizPoetryOdeController extends BaseController
{
    @Autowired
    private IPoetryOdeService poetryOdeService;

    /**
     * 查询诗词歌赋列表
     */
//    @PreAuthorize("@ss.hasPermi('system:ode:list')")
    @GetMapping("/ode_list")
    public AjaxResult list(PoetryOde poetryOde)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        startPage();
        List<PoetryOde> list = poetryOdeService.selectPoetryOdeList(poetryOde);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出诗词歌赋列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:ode:export')")
//    @Log(title = "诗词歌赋", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, PoetryOde poetryOde)
//    {
//        List<PoetryOde> list = poetryOdeService.selectPoetryOdeList(poetryOde);
//        ExcelUtil<PoetryOde> util = new ExcelUtil<PoetryOde>(PoetryOde.class);
//        util.exportExcel(response, list, "诗词歌赋数据");
//    }

    /**
     * 获取诗词歌赋详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:ode:query')")
    @GetMapping(value = "/ode_detail")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        ajaxResult.put("data",poetryOdeService.selectPoetryOdeById(id));
        return ajaxResult;
    }

//    /**
//     * 新增诗词歌赋
//     */
//    @PreAuthorize("@ss.hasPermi('system:ode:add')")
//    @Log(title = "诗词歌赋", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody PoetryOde poetryOde)
//    {
//        return toAjax(poetryOdeService.insertPoetryOde(poetryOde));
//    }
//
//    /**
//     * 修改诗词歌赋
//     */
//    @PreAuthorize("@ss.hasPermi('system:ode:edit')")
//    @Log(title = "诗词歌赋", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody PoetryOde poetryOde)
//    {
//        return toAjax(poetryOdeService.updatePoetryOde(poetryOde));
//    }
//
//    /**
//     * 删除诗词歌赋
//     */
//    @PreAuthorize("@ss.hasPermi('system:ode:remove')")
//    @Log(title = "诗词歌赋", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(poetryOdeService.deletePoetryOdeByIds(ids));
//    }
}
