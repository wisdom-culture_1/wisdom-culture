package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.ExpertLectureHallChapter;
import com.ruoyi.business.service.IExpertLectureHallChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 专家讲堂章节Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/lecture")
public class BizExpertLectureHallChapterController extends BaseController
{
    @Autowired
    private IExpertLectureHallChapterService expertLectureHallChapterService;

    /**
     * 查询专家讲堂章节列表
     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:list')")
    @GetMapping("/hall_chapter")
    public AjaxResult list(ExpertLectureHallChapter expertLectureHallChapter)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ExpertLectureHallChapter> list = expertLectureHallChapterService.selectExpertLectureHallChapterList(expertLectureHallChapter);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出专家讲堂章节列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:export')")
//    @Log(title = "专家讲堂章节", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ExpertLectureHallChapter expertLectureHallChapter)
//    {
//        List<ExpertLectureHallChapter> list = expertLectureHallChapterService.selectExpertLectureHallChapterList(expertLectureHallChapter);
//        ExcelUtil<ExpertLectureHallChapter> util = new ExcelUtil<ExpertLectureHallChapter>(ExpertLectureHallChapter.class);
//        util.exportExcel(response, list, "专家讲堂章节数据");
//    }
//
//    /**
//     * 获取专家讲堂章节详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(expertLectureHallChapterService.selectExpertLectureHallChapterById(id));
//    }
//
//    /**
//     * 新增专家讲堂章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:add')")
//    @Log(title = "专家讲堂章节", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ExpertLectureHallChapter expertLectureHallChapter)
//    {
//        return toAjax(expertLectureHallChapterService.insertExpertLectureHallChapter(expertLectureHallChapter));
//    }
//
//    /**
//     * 修改专家讲堂章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:edit')")
//    @Log(title = "专家讲堂章节", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ExpertLectureHallChapter expertLectureHallChapter)
//    {
//        return toAjax(expertLectureHallChapterService.updateExpertLectureHallChapter(expertLectureHallChapter));
//    }
//
//    /**
//     * 删除专家讲堂章节
//     */
//    @PreAuthorize("@ss.hasPermi('system:chapter:remove')")
//    @Log(title = "专家讲堂章节", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(expertLectureHallChapterService.deleteExpertLectureHallChapterByIds(ids));
//    }
}
