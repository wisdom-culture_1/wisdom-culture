package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.IdiomStory;
import com.ruoyi.business.service.IIdiomStoryService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 成语典故Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/etymology")
public class BizIdiomStoryController extends BaseController
{
    @Autowired
    private IIdiomStoryService idiomStoryService;

    /**
     * 查询成语典故列表
     */
//    @PreAuthorize("@ss.hasPermi('system:story:list')")
    @PostMapping("/idiom_list")
    public AjaxResult list(@RequestBody  IdiomStory idiomStory)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        startPage();
        List<IdiomStory> list = idiomStoryService.selectIdiomStoryList(idiomStory);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出成语典故列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:story:export')")
//    @Log(title = "成语典故", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, IdiomStory idiomStory)
//    {
//        List<IdiomStory> list = idiomStoryService.selectIdiomStoryList(idiomStory);
//        ExcelUtil<IdiomStory> util = new ExcelUtil<IdiomStory>(IdiomStory.class);
//        util.exportExcel(response, list, "成语典故数据");
//    }

    /**
     * 获取成语典故详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:story:query')")
    @GetMapping(value = "/idiom_detail")
    public AjaxResult getInfo(@Param("id") Long id)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

        IdiomStory idiomStory = idiomStoryService.selectIdiomStoryById(id);
        ajaxResult.put("data",idiomStory);
        return ajaxResult;
    }

//    /**
//     * 新增成语典故
//     */
//    @PreAuthorize("@ss.hasPermi('system:story:add')")
//    @Log(title = "成语典故", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody IdiomStory idiomStory)
//    {
//        return toAjax(idiomStoryService.insertIdiomStory(idiomStory));
//    }
//
//    /**
//     * 修改成语典故
//     */
//    @PreAuthorize("@ss.hasPermi('system:story:edit')")
//    @Log(title = "成语典故", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody IdiomStory idiomStory)
//    {
//        return toAjax(idiomStoryService.updateIdiomStory(idiomStory));
//    }
//
//    /**
//     * 删除成语典故
//     */
//    @PreAuthorize("@ss.hasPermi('system:story:remove')")
//    @Log(title = "成语典故", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(idiomStoryService.deleteIdiomStoryByIds(ids));
//    }
}
