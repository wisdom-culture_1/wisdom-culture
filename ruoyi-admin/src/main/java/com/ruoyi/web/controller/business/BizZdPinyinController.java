package com.ruoyi.web.controller.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.ZdPinyin;
import com.ruoyi.business.service.IZdPinyinService;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizZdPinyinController extends BaseController
{
    @Autowired
    private IZdPinyinService zdPinyinService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:pinyin:list')")
    @GetMapping("/pinyin_character")
    public AjaxResult list(ZdPinyin zdPinyin)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ZdPinyin> list = zdPinyinService.selectZdPinyinList(zdPinyin);

        // 去重 多音字只出现一个
        List<Integer> toRemoveIndex = new ArrayList<>();
        if (list.size() >= 2) {
            ZdPinyin zdPinyinIndex = list.get(0);
            for (int i = 1; i < list.size(); i++) {
                ZdPinyin zdPinyinPointer = list.get(i);
                if (zdPinyinIndex.getZi().equals(zdPinyinPointer.getZi())) {
                    toRemoveIndex.add(i);
                } else {
                    zdPinyinIndex = zdPinyinPointer;
                }
            }
        }

        // Reverse the indexes before removing to avoid index shifting issues.
        toRemoveIndex.sort(Collections.reverseOrder());
        for (Integer index : toRemoveIndex) {
            list.remove((int)index);
        }

        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }
//
//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:pinyin:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ZdPinyin zdPinyin)
//    {
//        List<ZdPinyin> list = zdPinyinService.selectZdPinyinList(zdPinyin);
//        ExcelUtil<ZdPinyin> util = new ExcelUtil<ZdPinyin>(ZdPinyin.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }
//
//    /**
//     * 获取【请填写功能名称】详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:pinyin:query')")
//    @GetMapping(value = "/{xuhao}")
//    public AjaxResult getInfo(@PathVariable("xuhao") String xuhao)
//    {
//        return success(zdPinyinService.selectZdPinyinByXuhao(xuhao));
//    }
//
//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:pinyin:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ZdPinyin zdPinyin)
//    {
//        return toAjax(zdPinyinService.insertZdPinyin(zdPinyin));
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:pinyin:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ZdPinyin zdPinyin)
//    {
//        return toAjax(zdPinyinService.updateZdPinyin(zdPinyin));
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:pinyin:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{xuhaos}")
//    public AjaxResult remove(@PathVariable String[] xuhaos)
//    {
//        return toAjax(zdPinyinService.deleteZdPinyinByXuhaos(xuhaos));
//    }
}
