package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.IdiomSummary;
import com.ruoyi.business.entity.IdiomSummaryRsp;
import com.ruoyi.business.service.IIdiomSummaryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 成语汇总Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/etymology")
public class BizIdiomSummaryController extends BaseController
{
    @Autowired
    private IIdiomSummaryService idiomSummaryService;

    /**
     * 查询成语汇总列表
     */
    @PreAuthorize("@ss.hasPermi('system:summary:list')")
    @GetMapping("/list")
    public TableDataInfo list(IdiomSummary idiomSummary)
    {
//        startPage();
        List<IdiomSummary> list = idiomSummaryService.selectIdiomSummaryList(idiomSummary);
        return getDataTable(list);
    }

//    /**
//     * 导出成语汇总列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:export')")
//    @Log(title = "成语汇总", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, IdiomSummary idiomSummary)
//    {
//        List<IdiomSummary> list = idiomSummaryService.selectIdiomSummaryList(idiomSummary);
//        ExcelUtil<IdiomSummary> util = new ExcelUtil<IdiomSummary>(IdiomSummary.class);
//        util.exportExcel(response, list, "成语汇总数据");
//    }

    /**
     * 获取成语汇总详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:summary:query')")
    @GetMapping(value = "/idiom_summary")
    public AjaxResult getInfo()
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        IdiomSummaryRsp idiomSummaryRsp = idiomSummaryService.selectIdiomSummaryUserIdAndStatus();
        ajaxResult.put("data",idiomSummaryRsp);
        return ajaxResult;
    }

    /**
     * 新增成语汇总
     */
//    @PreAuthorize("@ss.hasPermi('system:summary:add')")
    @Log(title = "成语汇总", businessType = BusinessType.INSERT)
    @PostMapping("/idiom_update_summary")
    public AjaxResult add(@RequestBody IdiomSummary idiomSummary)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        idiomSummaryService.insertIdiomSummary(idiomSummary);
        return ajaxResult;
    }

//    /**
//     * 修改成语汇总
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:edit')")
//    @Log(title = "成语汇总", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody IdiomSummary idiomSummary)
//    {
//        return toAjax(idiomSummaryService.updateIdiomSummary(idiomSummary));
//    }
//
//    /**
//     * 删除成语汇总
//     */
//    @PreAuthorize("@ss.hasPermi('system:summary:remove')")
//    @Log(title = "成语汇总", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(idiomSummaryService.deleteIdiomSummaryByIds(ids));
//    }
}
