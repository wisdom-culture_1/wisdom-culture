package com.ruoyi.web.controller.business;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.business.entity.ChallengeComplete;
import com.ruoyi.business.entity.ChallengeEtymological;
import com.ruoyi.business.mapper.BizChallengeCompleteMapper;
import com.ruoyi.business.service.IChallengeEtymologicalService;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@RestController
@RequestMapping("/challenge")
public class BizChallengeEtymologicalController extends BaseController
{
    @Autowired
    private IChallengeEtymologicalService challengeEtymologicalService;

    @Resource
    private BizChallengeCompleteMapper bizChallengeCompleteMapper;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:etymological:list')")
    @GetMapping("/etymological")
    public AjaxResult list(ChallengeEtymological challengeEtymological)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        List<ChallengeEtymological> list = new ArrayList<ChallengeEtymological>();

        // 字源学字：字源大挑战
        if(challengeEtymological.getGroupId() == 318){
            list = challengeEtymologicalService.selectChallengeEtymologicalList(challengeEtymological);
            ajaxResult.put("data",getDataTable(list));
            return ajaxResult;
        }

        ChallengeComplete challengeComplete = new ChallengeComplete();
        challengeComplete.setuId(SecurityUtils.getUserId());
        challengeComplete.setGroupId(challengeEtymological.getGroupId()-1);
        challengeComplete.setTypeId(Long.valueOf(1));
        ChallengeComplete challengeComplete1 = bizChallengeCompleteMapper.selectLatestChallengeCompliete(challengeComplete);
        if(challengeComplete1 != null && challengeComplete1.getAchievement().intValue() >= 60){
            list = challengeEtymologicalService.selectChallengeEtymologicalList(challengeEtymological);
            ajaxResult.put("data",getDataTable(list));
            return ajaxResult;
        }
        else{
            ajaxResult = AjaxResult.newError("请完成上一关！");
            return ajaxResult;
        }
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:etymological:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChallengeEtymological challengeEtymological)
    {
        List<ChallengeEtymological> list = challengeEtymologicalService.selectChallengeEtymologicalList(challengeEtymological);
        ExcelUtil<ChallengeEtymological> util = new ExcelUtil<ChallengeEtymological>(ChallengeEtymological.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:etymological:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(challengeEtymologicalService.selectChallengeEtymologicalById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:etymological:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChallengeEtymological challengeEtymological)
    {
        return toAjax(challengeEtymologicalService.insertChallengeEtymological(challengeEtymological));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:etymological:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChallengeEtymological challengeEtymological)
    {
        return toAjax(challengeEtymologicalService.updateChallengeEtymological(challengeEtymological));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:etymological:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(challengeEtymologicalService.deleteChallengeEtymologicalByIds(ids));
    }
}
