package com.ruoyi.web.controller.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.ZdBsbh;
import com.ruoyi.business.service.IZdBsbhService;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizZdBsbhController extends BaseController
{
    @Autowired
    private IZdBsbhService zdBsbhService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:bsbh:list')")
    @GetMapping("/radical")
    public AjaxResult list(ZdBsbh zdBsbh)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ZdBsbh> list = zdBsbhService.selectZdBsbhList(zdBsbh);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:bsbh:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ZdBsbh zdBsbh)
//    {
//        List<ZdBsbh> list = zdBsbhService.selectZdBsbhList(zdBsbh);
//        ExcelUtil<ZdBsbh> util = new ExcelUtil<ZdBsbh>(ZdBsbh.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }
//
//    /**
//     * 获取【请填写功能名称】详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:bsbh:query')")
//    @GetMapping(value = "/{bu}")
//    public AjaxResult getInfo(@PathVariable("bu") String bu)
//    {
//        return success(zdBsbhService.selectZdBsbhByBu(bu));
//    }
//
//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:bsbh:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ZdBsbh zdBsbh)
//    {
//        return toAjax(zdBsbhService.insertZdBsbh(zdBsbh));
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:bsbh:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ZdBsbh zdBsbh)
//    {
//        return toAjax(zdBsbhService.updateZdBsbh(zdBsbh));
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:bsbh:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{bus}")
//    public AjaxResult remove(@PathVariable String[] bus)
//    {
//        return toAjax(zdBsbhService.deleteZdBsbhByBus(bus));
//    }
}
