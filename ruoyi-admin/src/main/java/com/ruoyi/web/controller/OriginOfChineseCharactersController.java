package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.OriginOfChineseCharacters;
import com.ruoyi.system.service.IOriginOfChineseCharactersService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 汉字起源Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/characters")
public class OriginOfChineseCharactersController extends BaseController
{
    @Autowired
    private IOriginOfChineseCharactersService originOfChineseCharactersService;

    /**
     * 查询汉字起源列表
     */
    @PreAuthorize("@ss.hasPermi('system:characters:list')")
    @GetMapping("/list")
    public TableDataInfo list(OriginOfChineseCharacters originOfChineseCharacters)
    {
        startPage();
        List<OriginOfChineseCharacters> list = originOfChineseCharactersService.selectOriginOfChineseCharactersList(originOfChineseCharacters);
        return getDataTable(list);
    }

    /**
     * 导出汉字起源列表
     */
    @PreAuthorize("@ss.hasPermi('system:characters:export')")
    @Log(title = "汉字起源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OriginOfChineseCharacters originOfChineseCharacters)
    {
        List<OriginOfChineseCharacters> list = originOfChineseCharactersService.selectOriginOfChineseCharactersList(originOfChineseCharacters);
        ExcelUtil<OriginOfChineseCharacters> util = new ExcelUtil<OriginOfChineseCharacters>(OriginOfChineseCharacters.class);
        util.exportExcel(response, list, "汉字起源数据");
    }

    /**
     * 获取汉字起源详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:characters:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(originOfChineseCharactersService.selectOriginOfChineseCharactersById(id));
    }

    /**
     * 新增汉字起源
     */
    @PreAuthorize("@ss.hasPermi('system:characters:add')")
    @Log(title = "汉字起源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OriginOfChineseCharacters originOfChineseCharacters)
    {
        return toAjax(originOfChineseCharactersService.insertOriginOfChineseCharacters(originOfChineseCharacters));
    }

    /**
     * 修改汉字起源
     */
    @PreAuthorize("@ss.hasPermi('system:characters:edit')")
    @Log(title = "汉字起源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OriginOfChineseCharacters originOfChineseCharacters)
    {
        return toAjax(originOfChineseCharactersService.updateOriginOfChineseCharacters(originOfChineseCharacters));
    }

    /**
     * 删除汉字起源
     */
    @PreAuthorize("@ss.hasPermi('system:characters:remove')")
    @Log(title = "汉字起源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(originOfChineseCharactersService.deleteOriginOfChineseCharactersByIds(ids));
    }
}
