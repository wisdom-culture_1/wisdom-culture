package com.ruoyi.web.controller.business;

import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.ReciteTypeContent;
import com.ruoyi.business.service.IReciteTypeContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 背诵打卡类别内容Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/recite")
public class BizReciteTypeContentController extends BaseController
{
    @Autowired
    private IReciteTypeContentService reciteTypeContentService;

    /**
     * 查询背诵打卡类别内容列表
     */
//    @PreAuthorize("@ss.hasPermi('system:content:list')")
    @GetMapping("/type_content")
    public AjaxResult list(ReciteTypeContent reciteTypeContent)
    {

        logger.info("reciteTypeContent is{}",JSON.toJSONString(reciteTypeContent));
//        startPage();
        AjaxResult ajaxResult = AjaxResult.newSuccess();

        List<ReciteTypeContent> list = reciteTypeContentService.selectReciteTypeContentList(reciteTypeContent);
        if(!CollectionUtils.isEmpty(list)){
            ajaxResult.put("data",list.get(0));
        }
        return ajaxResult;
    }

//    /**
//     * 导出背诵打卡类别内容列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:content:export')")
//    @Log(title = "背诵打卡类别内容", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ReciteTypeContent reciteTypeContent)
//    {
//        List<ReciteTypeContent> list = reciteTypeContentService.selectReciteTypeContentList(reciteTypeContent);
//        ExcelUtil<ReciteTypeContent> util = new ExcelUtil<ReciteTypeContent>(ReciteTypeContent.class);
//        util.exportExcel(response, list, "背诵打卡类别内容数据");
//    }
//
//    /**
//     * 获取背诵打卡类别内容详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:content:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(reciteTypeContentService.selectReciteTypeContentById(id));
//    }
//
//    /**
//     * 新增背诵打卡类别内容
//     */
//    @PreAuthorize("@ss.hasPermi('system:content:add')")
//    @Log(title = "背诵打卡类别内容", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ReciteTypeContent reciteTypeContent)
//    {
//        return toAjax(reciteTypeContentService.insertReciteTypeContent(reciteTypeContent));
//    }
//
//    /**
//     * 修改背诵打卡类别内容
//     */
//    @PreAuthorize("@ss.hasPermi('system:content:edit')")
//    @Log(title = "背诵打卡类别内容", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ReciteTypeContent reciteTypeContent)
//    {
//        return toAjax(reciteTypeContentService.updateReciteTypeContent(reciteTypeContent));
//    }
//
//    /**
//     * 删除背诵打卡类别内容
//     */
//    @PreAuthorize("@ss.hasPermi('system:content:remove')")
//    @Log(title = "背诵打卡类别内容", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(reciteTypeContentService.deleteReciteTypeContentByIds(ids));
//    }
}
