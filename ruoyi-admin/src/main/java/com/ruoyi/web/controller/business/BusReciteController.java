package com.ruoyi.web.controller.business;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.Recite;
import com.ruoyi.business.entity.ReciteLike;
import com.ruoyi.business.entity.WxInfo;
import com.ruoyi.business.mapper.BizReciteLikeMapper;
import com.ruoyi.business.mapper.BizWxInfoMapper;
import com.ruoyi.business.service.IReciteService;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysUserMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

/**
 * 背诵打卡Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/recite")
public class BusReciteController extends BaseController
{
    @Autowired
    private IReciteService reciteService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BizReciteLikeMapper reciteLikeMapper;

    @Autowired
    BizWxInfoMapper wxInfoMapper;

    /**
     * 查询背诵打卡列表
     */
//    @PreAuthorize("@ss.hasPermi('system:recite:list')")
    @GetMapping("/recite_list")
    public AjaxResult list(@RequestParam("type_id") Long typeId)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        startPage();

        Recite recite = new Recite();
//        Long userId = SecurityUtils.getUserId();
        recite.setTypeId(typeId);
//        recite.setUserId(userId);
        logger.info("recite/recite_list recite is {}",JSON.toJSONString(recite));

        List<Recite> list = reciteService.selectReciteList(recite);

        logger.info("recite/recite_list recite list is {}",JSON.toJSONString(list));
        list.forEach(item -> {
            // 用户昵称
            SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
            WxInfo wxInfo = wxInfoMapper.getWxInfo(item.getUserId());
            WxInfo userwx = new WxInfo();
            if (null != wxInfo){
                userwx.setWx_name(wxInfo.getWx_name());
                userwx.setWx_img(wxInfo.getWx_img());
                userwx.setWx_openid(wxInfo.getWx_openid());
            }else if (null != sysUser){
                userwx.setWx_name(sysUser.getNickname());
                userwx.setWx_img(sysUser.getWxImg());
                userwx.setWx_openid(sysUser.getWxOpenid());
            }
            item.setUserwx(userwx);

            // 录音总点赞数量
            ReciteLike reciteLike = new ReciteLike();
            reciteLike.setReciteId(item.getId());
            Integer integer = reciteLikeMapper.selectReciteLikeCount(reciteLike);
            item.setLike_count(Long.valueOf(integer));

            // 当前用户对录音是否点赞
            reciteLike.setUserId(SecurityUtils.getUserId());
            ReciteLike reciteLike1 = reciteLikeMapper.selectReciteLikeByUserIdAndReciteId(reciteLike);

            logger.info("recite/recite_list reciteLike1 is {} ",JSON.toJSONString(reciteLike1));

            if(null == reciteLike1){
                item.setLog_user("0");
            }else {
                if(reciteLike1.getIs_like().equals(Long.valueOf(1))){
                    item.setLog_user("1");
                }else {
                    item.setLog_user("0");
                }
            }
            logger.info("recite/recite_list item is {} ",JSON.toJSONString(item));

        });
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 查询背诵打卡列表
     */
//    @PreAuthorize("@ss.hasPermi('system:recite:list')")
    @GetMapping("/reciteMyList")
    public AjaxResult reciteMyList(@RequestParam("type_id") Long typeId)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        startPage();

        Recite recite = new Recite();
        Long userId = SecurityUtils.getUserId();
        recite.setTypeId(typeId);
        recite.setUserId(userId);
        logger.info("recite/reciteMyList recite is {}",JSON.toJSONString(recite));

        List<Recite> list = reciteService.selectReciteList(recite);

        logger.info("recite/reciteMyList recite list is {}",JSON.toJSONString(list));
        list.forEach(item -> {
            // 用户昵称
            SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
            WxInfo wxInfo = wxInfoMapper.getWxInfo(item.getUserId());
            WxInfo userwx = new WxInfo();
            if (null != wxInfo){
                userwx.setWx_name(wxInfo.getWx_name());
                userwx.setWx_img(wxInfo.getWx_img());
                userwx.setWx_openid(wxInfo.getWx_openid());
            }else if (null != sysUser){
                userwx.setWx_name(sysUser.getNickname());
                userwx.setWx_img(sysUser.getWxImg());
                userwx.setWx_openid(sysUser.getWxOpenid());
            }
            item.setUserwx(userwx);

            // 录音总点赞数量
            ReciteLike reciteLike = new ReciteLike();
            reciteLike.setReciteId(item.getId());
            Integer integer = reciteLikeMapper.selectReciteLikeCount(reciteLike);
            item.setLike_count(Long.valueOf(integer));

            // 当前用户对录音是否点赞
            reciteLike.setUserId(SecurityUtils.getUserId());
            ReciteLike reciteLike1 = reciteLikeMapper.selectReciteLikeByUserIdAndReciteId(reciteLike);

            logger.info("recite/reciteMyList reciteLike1 is {} ",JSON.toJSONString(reciteLike1));

            if(null == reciteLike1){
                item.setLog_user("0");
            }else {
                if(reciteLike1.getIs_like().equals(Long.valueOf(1))){
                    item.setLog_user("1");
                }else {
                    item.setLog_user("0");
                }
            }
            logger.info("recite/reciteMyList item is {} ",JSON.toJSONString(item));

        });
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }


//    /**
//     * 导出背诵打卡列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:recite:export')")
//    @Log(title = "背诵打卡", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, Recite recite)
//    {
//        List<Recite> list = reciteService.selectReciteList(recite);
//        ExcelUtil<Recite> util = new ExcelUtil<Recite>(Recite.class);
//        util.exportExcel(response, list, "背诵打卡数据");
//    }
//
//    /**
//     * 获取背诵打卡详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:recite:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(reciteService.selectReciteById(id));
//    }

    /**
     * 新增背诵打卡
     */
//    @PreAuthorize("@ss.hasPermi('system:recite:add')")
    @Log(title = "背诵打卡", businessType = BusinessType.INSERT)
    @PostMapping("set_recite")
    public AjaxResult add(@RequestBody Recite recite)
    {
        logger.info("recite is {}", JSON.toJSONString(recite));
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        recite.setUserId(SecurityUtils.getUserId());
        recite.setCreatedAt(new Date());
        recite.setUpdatedAt(new Date());
        reciteService.insertRecite(recite);
        return ajaxResult;

    }

//    /**
//     * 修改背诵打卡
//     */
//    @PreAuthorize("@ss.hasPermi('system:recite:edit')")
//    @Log(title = "背诵打卡", businessTychapter_listpe = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody Recite recite)
//    {
//        return toAjax(reciteService.updateRecite(recite));
//    }
//
//    /**
//     * 删除背诵打卡
//     */
//    @PreAuthorize("@ss.hasPermi('system:recite:remove')")
//    @Log(title = "背诵打卡", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(reciteService.deleteReciteByIds(ids));
//    }
}
