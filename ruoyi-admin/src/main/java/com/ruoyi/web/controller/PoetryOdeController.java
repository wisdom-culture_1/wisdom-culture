package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PoetryOde;
import com.ruoyi.system.service.IPoetryOdeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 诗词歌赋Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/ode")
public class PoetryOdeController extends BaseController
{
    @Autowired
    private IPoetryOdeService poetryOdeService;

    /**
     * 查询诗词歌赋列表
     */
    @PreAuthorize("@ss.hasPermi('system:ode:list')")
    @GetMapping("/list")
    public TableDataInfo list(PoetryOde poetryOde)
    {
        startPage();
        List<PoetryOde> list = poetryOdeService.selectPoetryOdeList(poetryOde);
        return getDataTable(list);
    }

    /**
     * 导出诗词歌赋列表
     */
    @PreAuthorize("@ss.hasPermi('system:ode:export')")
    @Log(title = "诗词歌赋", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PoetryOde poetryOde)
    {
        List<PoetryOde> list = poetryOdeService.selectPoetryOdeList(poetryOde);
        ExcelUtil<PoetryOde> util = new ExcelUtil<PoetryOde>(PoetryOde.class);
        util.exportExcel(response, list, "诗词歌赋数据");
    }

    /**
     * 获取诗词歌赋详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:ode:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(poetryOdeService.selectPoetryOdeById(id));
    }

    /**
     * 新增诗词歌赋
     */
    @PreAuthorize("@ss.hasPermi('system:ode:add')")
    @Log(title = "诗词歌赋", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PoetryOde poetryOde)
    {
        return toAjax(poetryOdeService.insertPoetryOde(poetryOde));
    }

    /**
     * 修改诗词歌赋
     */
    @PreAuthorize("@ss.hasPermi('system:ode:edit')")
    @Log(title = "诗词歌赋", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PoetryOde poetryOde)
    {
        return toAjax(poetryOdeService.updatePoetryOde(poetryOde));
    }

    /**
     * 删除诗词歌赋
     */
    @PreAuthorize("@ss.hasPermi('system:ode:remove')")
    @Log(title = "诗词歌赋", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(poetryOdeService.deletePoetryOdeByIds(ids));
    }
}
