package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Excerpt;
import com.ruoyi.system.service.IExcerptService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金句摘抄Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/excerpt")
public class ExcerptController extends BaseController
{
    @Autowired
    private IExcerptService excerptService;

    /**
     * 查询金句摘抄列表
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt:list')")
    @GetMapping("/list")
    public TableDataInfo list(Excerpt excerpt)
    {
        startPage();
        List<Excerpt> list = excerptService.selectExcerptList(excerpt);
        return getDataTable(list);
    }

    /**
     * 导出金句摘抄列表
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt:export')")
    @Log(title = "金句摘抄", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Excerpt excerpt)
    {
        List<Excerpt> list = excerptService.selectExcerptList(excerpt);
        ExcelUtil<Excerpt> util = new ExcelUtil<Excerpt>(Excerpt.class);
        util.exportExcel(response, list, "金句摘抄数据");
    }

    /**
     * 获取金句摘抄详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(excerptService.selectExcerptById(id));
    }

    /**
     * 新增金句摘抄
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt:add')")
    @Log(title = "金句摘抄", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Excerpt excerpt)
    {
        return toAjax(excerptService.insertExcerpt(excerpt));
    }

    /**
     * 修改金句摘抄
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt:edit')")
    @Log(title = "金句摘抄", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Excerpt excerpt)
    {
        return toAjax(excerptService.updateExcerpt(excerpt));
    }

    /**
     * 删除金句摘抄
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt:remove')")
    @Log(title = "金句摘抄", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(excerptService.deleteExcerptByIds(ids));
    }
}
