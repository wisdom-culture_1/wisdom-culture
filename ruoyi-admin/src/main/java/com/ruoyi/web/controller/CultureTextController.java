package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CultureText;
import com.ruoyi.system.service.ICultureTextService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 中华文化内容Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/culture_text")
public class CultureTextController extends BaseController
{
    @Autowired
    private ICultureTextService cultureTextService;

    /**
     * 查询中华文化内容列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_text:list')")
    @GetMapping("/list")
    public TableDataInfo list(CultureText cultureText)
    {
        startPage();
        List<CultureText> list = cultureTextService.selectCultureTextList(cultureText);
        return getDataTable(list);
    }

    /**
     * 导出中华文化内容列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_text:export')")
    @Log(title = "中华文化内容", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CultureText cultureText)
    {
        List<CultureText> list = cultureTextService.selectCultureTextList(cultureText);
        ExcelUtil<CultureText> util = new ExcelUtil<CultureText>(CultureText.class);
        util.exportExcel(response, list, "中华文化内容数据");
    }

    /**
     * 获取中华文化内容详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:culture_text:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cultureTextService.selectCultureTextById(id));
    }

    /**
     * 新增中华文化内容
     */
    @PreAuthorize("@ss.hasPermi('system:culture_text:add')")
    @Log(title = "中华文化内容", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CultureText cultureText)
    {
        return toAjax(cultureTextService.insertCultureText(cultureText));
    }

    /**
     * 修改中华文化内容
     */
    @PreAuthorize("@ss.hasPermi('system:culture_text:edit')")
    @Log(title = "中华文化内容", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CultureText cultureText)
    {
        return toAjax(cultureTextService.updateCultureText(cultureText));
    }

    /**
     * 删除中华文化内容
     */
    @PreAuthorize("@ss.hasPermi('system:culture_text:remove')")
    @Log(title = "中华文化内容", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cultureTextService.deleteCultureTextByIds(ids));
    }
}
