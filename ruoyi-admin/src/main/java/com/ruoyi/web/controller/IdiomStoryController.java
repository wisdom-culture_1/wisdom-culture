package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.IdiomStory;
import com.ruoyi.system.service.IIdiomStoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 成语典故Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/story")
public class IdiomStoryController extends BaseController
{
    @Autowired
    private IIdiomStoryService idiomStoryService;

    /**
     * 查询成语典故列表
     */
    @PreAuthorize("@ss.hasPermi('system:story:list')")
    @GetMapping("/list")
    public TableDataInfo list(IdiomStory idiomStory)
    {
        startPage();
        List<IdiomStory> list = idiomStoryService.selectIdiomStoryList(idiomStory);
        return getDataTable(list);
    }

    /**
     * 导出成语典故列表
     */
    @PreAuthorize("@ss.hasPermi('system:story:export')")
    @Log(title = "成语典故", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IdiomStory idiomStory)
    {
        List<IdiomStory> list = idiomStoryService.selectIdiomStoryList(idiomStory);
        ExcelUtil<IdiomStory> util = new ExcelUtil<IdiomStory>(IdiomStory.class);
        util.exportExcel(response, list, "成语典故数据");
    }

    /**
     * 获取成语典故详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:story:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(idiomStoryService.selectIdiomStoryById(id));
    }

    /**
     * 新增成语典故
     */
    @PreAuthorize("@ss.hasPermi('system:story:add')")
    @Log(title = "成语典故", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IdiomStory idiomStory)
    {
        return toAjax(idiomStoryService.insertIdiomStory(idiomStory));
    }

    /**
     * 修改成语典故
     */
    @PreAuthorize("@ss.hasPermi('system:story:edit')")
    @Log(title = "成语典故", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IdiomStory idiomStory)
    {
        return toAjax(idiomStoryService.updateIdiomStory(idiomStory));
    }

    /**
     * 删除成语典故
     */
    @PreAuthorize("@ss.hasPermi('system:story:remove')")
    @Log(title = "成语典故", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(idiomStoryService.deleteIdiomStoryByIds(ids));
    }
}
