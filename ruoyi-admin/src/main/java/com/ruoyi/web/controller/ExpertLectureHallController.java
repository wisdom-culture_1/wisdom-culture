package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ExpertLectureHall;
import com.ruoyi.system.service.IExpertLectureHallService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 专家讲堂视频Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/expert_lecture_hall")
public class ExpertLectureHallController extends BaseController
{
    @Autowired
    private IExpertLectureHallService expertLectureHallService;

    /**
     * 查询专家讲堂视频列表
     */
    @PreAuthorize("@ss.hasPermi('system:expert_lecture_hall:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExpertLectureHall expertLectureHall)
    {
        startPage();
        List<ExpertLectureHall> list = expertLectureHallService.selectExpertLectureHallList(expertLectureHall);
        return getDataTable(list);
    }

    /**
     * 导出专家讲堂视频列表
     */
    @PreAuthorize("@ss.hasPermi('system:expert_lecture_hall:export')")
    @Log(title = "专家讲堂视频", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExpertLectureHall expertLectureHall)
    {
        List<ExpertLectureHall> list = expertLectureHallService.selectExpertLectureHallList(expertLectureHall);
        ExcelUtil<ExpertLectureHall> util = new ExcelUtil<ExpertLectureHall>(ExpertLectureHall.class);
        util.exportExcel(response, list, "专家讲堂视频数据");
    }

    /**
     * 获取专家讲堂视频详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:expert_lecture_hall:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(expertLectureHallService.selectExpertLectureHallById(id));
    }

    /**
     * 新增专家讲堂视频
     */
    @PreAuthorize("@ss.hasPermi('system:expert_lecture_hall:add')")
    @Log(title = "专家讲堂视频", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExpertLectureHall expertLectureHall)
    {
        return toAjax(expertLectureHallService.insertExpertLectureHall(expertLectureHall));
    }

    /**
     * 修改专家讲堂视频
     */
    @PreAuthorize("@ss.hasPermi('system:expert_lecture_hall:edit')")
    @Log(title = "专家讲堂视频", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExpertLectureHall expertLectureHall)
    {
        return toAjax(expertLectureHallService.updateExpertLectureHall(expertLectureHall));
    }

    /**
     * 删除专家讲堂视频
     */
    @PreAuthorize("@ss.hasPermi('system:expert_lecture_hall:remove')")
    @Log(title = "专家讲堂视频", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(expertLectureHallService.deleteExpertLectureHallByIds(ids));
    }
}
