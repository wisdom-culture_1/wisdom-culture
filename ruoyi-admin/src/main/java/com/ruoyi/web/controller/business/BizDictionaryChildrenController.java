package com.ruoyi.web.controller.business;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.entity.DictionaryChildren;
import com.ruoyi.business.service.IDictionaryChildrenService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/dictionary")
public class BizDictionaryChildrenController extends BaseController
{
    @Autowired
    private IDictionaryChildrenService dictionaryChildrenService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:children:list')")
    @GetMapping("/type")
    public AjaxResult list(DictionaryChildren dictionaryChildren)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();

//        startPage();
        List<DictionaryChildren> list = dictionaryChildrenService.selectDictionaryChildrenList(dictionaryChildren);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:children:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DictionaryChildren dictionaryChildren)
    {
        List<DictionaryChildren> list = dictionaryChildrenService.selectDictionaryChildrenList(dictionaryChildren);
        ExcelUtil<DictionaryChildren> util = new ExcelUtil<DictionaryChildren>(DictionaryChildren.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:children:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(dictionaryChildrenService.selectDictionaryChildrenById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:children:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DictionaryChildren dictionaryChildren)
    {
        return toAjax(dictionaryChildrenService.insertDictionaryChildren(dictionaryChildren));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:children:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DictionaryChildren dictionaryChildren)
    {
        return toAjax(dictionaryChildrenService.updateDictionaryChildren(dictionaryChildren));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:children:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(dictionaryChildrenService.deleteDictionaryChildrenByIds(ids));
    }
}
