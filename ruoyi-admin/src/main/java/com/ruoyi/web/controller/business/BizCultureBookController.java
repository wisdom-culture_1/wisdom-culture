package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.CultureBook;
import com.ruoyi.business.entity.CultureChapters;
import com.ruoyi.business.entity.CultureText;
import com.ruoyi.business.service.ICultureBookService;
import com.ruoyi.business.service.ICultureChaptersService;
import com.ruoyi.business.service.ICultureTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 中华文化书籍Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/culture")
public class BizCultureBookController extends BaseController
{
    @Autowired
    private ICultureBookService cultureBookService;
    @Autowired
    private ICultureChaptersService cultureChaptersService;
    @Autowired
    private ICultureTextService cultureTextService;
    /**
     * 查询中华文化书籍列表
     */
//    @PreAuthorize("@ss.hasPermi('system:book:list')")
    @GetMapping("/book_list")
    public AjaxResult list(CultureBook cultureBook)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<CultureBook> list = cultureBookService.selectCultureBookList(cultureBook);

        list.stream().forEach(item -> {
            CultureChapters cultureChapters = new CultureChapters();
            cultureChapters.setBookId(item.getId());
            cultureChapters.setPid(Long.valueOf(0));
            List<CultureChapters> cultureChapters1 = cultureChaptersService.selectCultureChaptersList(cultureChapters);

            if(!CollectionUtils.isEmpty(cultureChapters1)){
                CultureText cultureText = new CultureText();
                cultureText.setBookId(item.getId());
                cultureText.setChapterId(cultureChapters1.get(0).getId());
                List<CultureText> cultureTexts = cultureTextService.selectCultureTextList(cultureText);
                item.setList(cultureTexts);
            }
        });

        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出中华文化书籍列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:export')")
//    @Log(title = "中华文化书籍", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CultureBook cultureBook)
//    {
//        List<CultureBook> list = cultureBookService.selectCultureBookList(cultureBook);
//        ExcelUtil<CultureBook> util = new ExcelUtil<CultureBook>(CultureBook.class);
//        util.exportExcel(response, list, "中华文化书籍数据");
//    }

//    /**
//     * 获取中华文化书籍详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(cultureBookService.selectCultureBookById(id));
//    }

//    /**
//     * 新增中华文化书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:add')")
//    @Log(title = "中华文化书籍", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody CultureBook cultureBook)
//    {
//        return toAjax(cultureBookService.insertCultureBook(cultureBook));
//    }

//    /**
//     * 修改中华文化书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:edit')")
//    @Log(title = "中华文化书籍", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody CultureBook cultureBook)
//    {
//        return toAjax(cultureBookService.updateCultureBook(cultureBook));
//    }

//    /**
//     * 删除中华文化书籍
//     */
//    @PreAuthorize("@ss.hasPermi('system:book:remove')")
//    @Log(title = "中华文化书籍", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(cultureBookService.deleteCultureBookByIds(ids));
//    }
}
