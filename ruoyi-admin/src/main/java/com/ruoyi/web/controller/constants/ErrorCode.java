package com.ruoyi.web.controller.constants;

public class ErrorCode {

    public static final String NOT_REGISTER = "100";
    public static final String NOT_BIND_PHONE_NUMBER = "101";
    public static final String MESSAGE_CODE_ERROR = "102";
    public static final String CODE_EXPIRE_TIME = "103";

}
