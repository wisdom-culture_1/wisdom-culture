package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ExcerptType;
import com.ruoyi.system.service.IExcerptTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金句摘抄分类Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/excerpt_type")
public class ExcerptTypeController extends BaseController
{
    @Autowired
    private IExcerptTypeService excerptTypeService;

    /**
     * 查询金句摘抄分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_type:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExcerptType excerptType)
    {
        startPage();
        List<ExcerptType> list = excerptTypeService.selectExcerptTypeList(excerptType);
        return getDataTable(list);
    }

    /**
     * 导出金句摘抄分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_type:export')")
    @Log(title = "金句摘抄分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExcerptType excerptType)
    {
        List<ExcerptType> list = excerptTypeService.selectExcerptTypeList(excerptType);
        ExcelUtil<ExcerptType> util = new ExcelUtil<ExcerptType>(ExcerptType.class);
        util.exportExcel(response, list, "金句摘抄分类数据");
    }

    /**
     * 获取金句摘抄分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_type:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(excerptTypeService.selectExcerptTypeById(id));
    }

    /**
     * 新增金句摘抄分类
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_type:add')")
    @Log(title = "金句摘抄分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExcerptType excerptType)
    {
        return toAjax(excerptTypeService.insertExcerptType(excerptType));
    }

    /**
     * 修改金句摘抄分类
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_type:edit')")
    @Log(title = "金句摘抄分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExcerptType excerptType)
    {
        return toAjax(excerptTypeService.updateExcerptType(excerptType));
    }

    /**
     * 删除金句摘抄分类
     */
    @PreAuthorize("@ss.hasPermi('system:excerpt_type:remove')")
    @Log(title = "金句摘抄分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(excerptTypeService.deleteExcerptTypeByIds(ids));
    }
}
