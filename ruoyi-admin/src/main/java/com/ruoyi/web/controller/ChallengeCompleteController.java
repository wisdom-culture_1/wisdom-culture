package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ChallengeComplete;
import com.ruoyi.system.service.IChallengeCompleteService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 挑战结果Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/challenge_complete")
public class ChallengeCompleteController extends BaseController
{
    @Autowired
    private IChallengeCompleteService challengeCompleteService;

    /**
     * 查询挑战结果列表
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_complete:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChallengeComplete challengeComplete)
    {
        startPage();
        List<ChallengeComplete> list = challengeCompleteService.selectChallengeCompleteList(challengeComplete);
        return getDataTable(list);
    }

    /**
     * 导出挑战结果列表
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_complete:export')")
    @Log(title = "挑战结果", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChallengeComplete challengeComplete)
    {
        List<ChallengeComplete> list = challengeCompleteService.selectChallengeCompleteList(challengeComplete);
        ExcelUtil<ChallengeComplete> util = new ExcelUtil<ChallengeComplete>(ChallengeComplete.class);
        util.exportExcel(response, list, "挑战结果数据");
    }

    /**
     * 获取挑战结果详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_complete:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(challengeCompleteService.selectChallengeCompleteById(id));
    }

    /**
     * 新增挑战结果
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_complete:add')")
    @Log(title = "挑战结果", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChallengeComplete challengeComplete)
    {
        return toAjax(challengeCompleteService.insertChallengeComplete(challengeComplete));
    }

    /**
     * 修改挑战结果
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_complete:edit')")
    @Log(title = "挑战结果", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChallengeComplete challengeComplete)
    {
        return toAjax(challengeCompleteService.updateChallengeComplete(challengeComplete));
    }

    /**
     * 删除挑战结果
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_complete:remove')")
    @Log(title = "挑战结果", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(challengeCompleteService.deleteChallengeCompleteByIds(ids));
    }
}
