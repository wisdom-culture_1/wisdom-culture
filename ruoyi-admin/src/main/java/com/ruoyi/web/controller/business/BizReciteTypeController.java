package com.ruoyi.web.controller.business;

import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.ReciteType;
import com.ruoyi.business.service.IReciteTypeService;
import com.ruoyi.web.controller.tool.system.SysLoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 背诵打卡类别Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/recite")
public class BizReciteTypeController extends BaseController
{
    @Autowired
    private IReciteTypeService reciteTypeService;

    private static final Logger log = LoggerFactory.getLogger(BizReciteTypeController.class);

    /**
     * 查询背诵打卡类别列表
     */
//    @PreAuthorize("@ss.hasPermi('system:type:list')")
    @GetMapping("/type_list")
    public AjaxResult list(ReciteType reciteType)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        reciteType.setPid(Long.valueOf(0));
        log.info("BizReciteTypeController type_list is :{}",JSON.toJSONString(reciteType));
        List<ReciteType> list = reciteTypeService.selectReciteTypeList(reciteType);
        ajaxResult.put("data",getDataTable(list));

        return ajaxResult;
    }

//    /**
//     * 导出背诵打卡类别列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:export')")
//    @Log(title = "背诵打卡类别", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ReciteType reciteType)
//    {
//        List<ReciteType> list = reciteTypeService.selectReciteTypeList(reciteType);
//        ExcelUtil<ReciteType> util = new ExcelUtil<ReciteType>(ReciteType.class);
//        util.exportExcel(response, list, "背诵打卡类别数据");
//    }
//
//    /**
//     * 获取背诵打卡类别详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(reciteTypeService.selectReciteTypeById(id));
//    }
//
//    /**
//     * 新增背诵打卡类别
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:add')")
//    @Log(title = "背诵打卡类别", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ReciteType reciteType)
//    {
//        return toAjax(reciteTypeService.insertReciteType(reciteType));
//    }
//
//    /**
//     * 修改背诵打卡类别
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:edit')")
//    @Log(title = "背诵打卡类别", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ReciteType reciteType)
//    {
//        return toAjax(reciteTypeService.updateReciteType(reciteType));
//    }

    /**
     * 删除背诵打卡类别
     */
//    @PreAuthorize("@ss.hasPermi('system:type:remove')")
//    @Log(title = "背诵打卡类别", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(reciteTypeService.deleteReciteTypeByIds(ids));
//    }
}
