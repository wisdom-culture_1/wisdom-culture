package com.ruoyi.web.controller.business;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.Users;
import com.ruoyi.business.mapper.BizAuthorizationCodeMapper;
import com.ruoyi.business.service.IUsersService;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.entity.AuthorizationCode;
import com.ruoyi.business.service.IAuthorizationCodeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 授权码Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/system/code")
public class BizAuthorizationCodeController extends BaseController
{
    @Autowired
    private IAuthorizationCodeService authorizationCodeService;
    @Autowired
    private BizAuthorizationCodeMapper authorizationCodeMapper;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private IUsersService usersService;

    /**
     * 查询授权码列表
     */
//    @PreAuthorize("@ss.hasPermi('system:code:list')")
    @GetMapping("/list")
    public TableDataInfo list(AuthorizationCode authorizationCode)
    {
        startPage();
        List<AuthorizationCode> list = authorizationCodeService.selectAuthorizationCodeList(authorizationCode);
        return getDataTable(list);
    }

    /**
     * 导出授权码列表
     */
    @PreAuthorize("@ss.hasPermi('system:code:export')")
    @Log(title = "授权码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AuthorizationCode authorizationCode)
    {
        List<AuthorizationCode> list = authorizationCodeService.selectAuthorizationCodeList(authorizationCode);
        ExcelUtil<AuthorizationCode> util = new ExcelUtil<AuthorizationCode>(AuthorizationCode.class);
        util.exportExcel(response, list, "授权码数据");
    }

    /**
     * 获取授权码详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:code:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(authorizationCodeService.selectAuthorizationCodeById(id));
    }

    /**
     * 新增授权码
     */
    @PreAuthorize("@ss.hasPermi('system:code:add')")
    @Log(title = "授权码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AuthorizationCode authorizationCode)
    {

        logger.info("authorizationCode add :{}",JSON.toJSONString(authorizationCode));
        AuthorizationCode code = new AuthorizationCode();
        code.setCode(authorizationCode.getCode());
        AuthorizationCode dataBaseCode = authorizationCodeMapper.selectAuthorizationAuthCode(code);
        logger.info("add dataBaseCode---------"+JSON.toJSONString(dataBaseCode));

        if(null != dataBaseCode){
            return AjaxResult.newError("授权码已经存在！");
        }

        if(StringUtils.isNotBlank(authorizationCode.getIsUse())){
            String isUse = authorizationCode.getIsUse().trim();
            authorizationCode.setIsUse(isUse);
            if(isUse.equals("是")){
                authorizationCode.setIsUse("1");
            }

            if(isUse.equals("否")){
                authorizationCode.setIsUse("0");
            }

        }
        authorizationCode.setCreatedAt(new Date());
        authorizationCode.setUpdatedAt(new Date());
        int rows = authorizationCodeMapper.insertAuthorizationCode(authorizationCode);
        return rows > 0 ? AjaxResult.newSuccess() : AjaxResult.newError("生成授权码失败！");

    }

    /**
     * 新增授权码
     */
//    @PreAuthorize("@ss.hasPermi('system:code:add')")
    @Log(title = "授权码", businessType = BusinessType.INSERT)
    @PostMapping("addBatch")
    public AjaxResult addBatch(@RequestBody AuthorizationCode authorizationCode)
    {

        logger.info("authorizationCode addBatch :{}",JSON.toJSONString(authorizationCode));
        if(authorizationCode.getCount() > 0){
            for(int i = 0; i < authorizationCode.getCount(); i++){
                AuthorizationCode code = new AuthorizationCode();
                UUID uuid = UUID.randomUUID();
                String hexString = uuid.toString().replace("-", "").substring(0,16).toUpperCase();
                code.setTitle(authorizationCode.getTitle());
                code.setCode(hexString);
                code.setIsUse("0");
                code.setStartDate(authorizationCode.getStartDate());
                code.setEndDate(authorizationCode.getEndDate());
                code.setCreatedAt(new Date());
                code.setUpdatedAt(new Date());
                authorizationCodeMapper.insertAuthorizationCode(code);
            }
        }
        return  AjaxResult.newSuccess();
    }

    /**
     * 新增授权码
     */
//    @PreAuthorize("@ss.hasPermi('system:code:add')")
//    @Log(title = "获取授权码", businessType = BusinessType.INSERT)
    @GetMapping("getOneCode")
    public AjaxResult getOneCode()
    {
        UUID uuid = UUID.randomUUID();
        String hexString = uuid.toString().replace("-", "").substring(0,16).toUpperCase();
        return  AjaxResult.success("code",hexString);
    }

    /**
     * 修改授权码
     */
    @PreAuthorize("@ss.hasPermi('system:code:edit')")
    @Log(title = "授权码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AuthorizationCode authorizationCode)
    {
        logger.info("authorizationCode edit :{}",JSON.toJSONString(authorizationCode));
        AuthorizationCode code = new AuthorizationCode();
        code.setCode(authorizationCode.getCode());
        AuthorizationCode dataBaseCode = authorizationCodeMapper.selectAuthorizationAuthCode(code);
        logger.info("edit dataBaseCode---------"+JSON.toJSONString(dataBaseCode));

        if(null == dataBaseCode){
            return AjaxResult.newError("授权码不存在！");
        }

        if(StringUtils.isNotBlank(authorizationCode.getIsUse())){
            String isUse = authorizationCode.getIsUse().trim();
            authorizationCode.setIsUse(isUse);
            if(isUse.equals("是")){
                authorizationCode.setIsUse("1");
            }

            if(isUse.equals("否")){
                authorizationCode.setIsUse("0");
            }

        }
        authorizationCode.setCreatedAt(new Date());
        authorizationCode.setUpdatedAt(new Date());
        int rows = authorizationCodeMapper.updateAuthorizationCode(authorizationCode);
        return rows > 0 ? AjaxResult.newSuccess() : AjaxResult.newError("修改失败");
    }

    /**
     * 删除授权码
     */
    @PreAuthorize("@ss.hasPermi('system:code:remove')")
    @Log(title = "授权码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(authorizationCodeService.deleteAuthorizationCodeByIds(ids));
    }

    /**
     * 停用授权码
     */
//    @Log(title = "授权码", businessType = BusinessType.DELETE)
    @PostMapping("/stop")
    @Transactional
    public AjaxResult stop(@RequestBody AuthorizationCode authorizationCode) throws ParseException {
        try{
            logger.info("authorizationCode stop :{}",JSON.toJSONString(authorizationCode));
            AuthorizationCode code = new AuthorizationCode();
            code.setCode(authorizationCode.getCode());
            code.setIsUse("1");
            AuthorizationCode dataBaseCode = authorizationCodeMapper.selectAuthorizationAuthCode(code);
            logger.info("edit dataBaseCode---------"+JSON.toJSONString(dataBaseCode));
            if(null == dataBaseCode){
                return AjaxResult.newError("授权码不存在！");
            }

            // 更新新平台用户到期时间
            SysUser sysUser = new SysUser();
            sysUser.setCode(authorizationCode.getCode());
            List<SysUser> sysUsers = sysUserService.selectUserList(sysUser);
            if(CollectionUtils.isNotEmpty(sysUsers)){
                SysUser sysUserDb = sysUsers.get(0);
                sysUserDb.setExpirationTime(new Date());
                sysUserDb.setStatus("1");
                sysUserService.updateUser(sysUserDb);
            }

            // 更新新平台用户到期时间
            Users users = new Users();
            users.setCode(authorizationCode.getCode());
            List<Users> usersListDb = usersService.selectUsersList(users);
            if(CollectionUtils.isNotEmpty(usersListDb)){
                Users usersDb = usersListDb.get(0);
                usersDb.setExpirationTime(new Date());
                usersService.updateUsers(usersDb);
            }

            // 更新授权码到期时间
            dataBaseCode.setUpdatedAt(new Date());
            Date endDate = DateUtils.parseDate(DateUtils.getDate(), "yyyy-MM-dd");
            dataBaseCode.setEndDate(endDate);
            dataBaseCode.setIsUse("-1");
            int rows = authorizationCodeMapper.updateAuthorizationCode(dataBaseCode);
            return rows > 0 ? AjaxResult.newSuccess() : AjaxResult.newError("修改失败");
        }catch (Exception e){
            throw new GlobalException();
        }
    }

//    /**
//     * 返回随机未使用授权码
//     */
//    @GetMapping("/get_random_code")
//    public AjaxResult getRandomCode()
//    {
//        return success(authorizationCodeService.getRandomCode());
//    }
//
//    /**
//     * 返回随机未使用授权码
//     */
    @GetMapping("/getCodeForIOS")
    public AjaxResult getNewRandomCode()
    {
        UUID uuid = UUID.randomUUID();
        String hexString = uuid.toString().replace("-", "").substring(0,16).toUpperCase();

        return success(hexString);
    }
}
