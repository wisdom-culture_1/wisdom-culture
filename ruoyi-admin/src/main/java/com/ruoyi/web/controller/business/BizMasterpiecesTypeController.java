package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.MasterpiecesType;
import com.ruoyi.business.service.IMasterpiecesTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 名著阅读分类Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/masterpieces")
public class BizMasterpiecesTypeController extends BaseController
{
    @Autowired
    private IMasterpiecesTypeService masterpiecesTypeService;

    /**
     * 查询名著阅读分类列表
     */
//    @PreAuthorize("@ss.hasPermi('system:type:list')")
    @GetMapping("/type")
    public AjaxResult list(MasterpiecesType masterpiecesType)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<MasterpiecesType> list = masterpiecesTypeService.selectMasterpiecesTypeList(masterpiecesType);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出名著阅读分类列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:export')")
//    @Log(title = "名著阅读分类", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, MasterpiecesType masterpiecesType)
//    {
//        List<MasterpiecesType> list = masterpiecesTypeService.selectMasterpiecesTypeList(masterpiecesType);
//        ExcelUtil<MasterpiecesType> util = new ExcelUtil<MasterpiecesType>(MasterpiecesType.class);
//        util.exportExcel(response, list, "名著阅读分类数据");
//    }
//
//    /**
//     * 获取名著阅读分类详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(masterpiecesTypeService.selectMasterpiecesTypeById(id));
//    }
//
//    /**
//     * 新增名著阅读分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:add')")
//    @Log(title = "名著阅读分类", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody MasterpiecesType masterpiecesType)
//    {
//        return toAjax(masterpiecesTypeService.insertMasterpiecesType(masterpiecesType));
//    }
//
//    /**
//     * 修改名著阅读分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:edit')")
//    @Log(title = "名著阅读分类", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody MasterpiecesType masterpiecesType)
//    {
//        return toAjax(masterpiecesTypeService.updateMasterpiecesType(masterpiecesType));
//    }
//
//    /**
//     * 删除名著阅读分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:remove')")
//    @Log(title = "名著阅读分类", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(masterpiecesTypeService.deleteMasterpiecesTypeByIds(ids));
//    }
}
