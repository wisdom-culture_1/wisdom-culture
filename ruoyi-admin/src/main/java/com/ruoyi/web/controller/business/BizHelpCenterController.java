package com.ruoyi.web.controller.business;

import com.ruoyi.business.entity.AccountCenter;
import com.ruoyi.business.entity.HelpCenter;
import com.ruoyi.business.service.IHelpCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 帮助中心Controller
 * 
 * @author ruoyi
 * @date 2023-04-12
 */
@RestController
@RequestMapping("/account")
public class BizHelpCenterController extends BaseController
{
    @Autowired
    private IHelpCenterService helpCenterService;

    /**
     * 查询帮助中心列表
     */
//    @PreAuthorize("@ss.hasPermi('system:center:list')")
    @GetMapping("/help")
    public AjaxResult  list(HelpCenter helpCenter)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<HelpCenter> list = helpCenterService.selectHelpCenterList(helpCenter);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

    /**
     * 查询帮助中心列表
     */
//    @PreAuthorize("@ss.hasPermi('system:center:list')")
    @GetMapping("/center")
    public AjaxResult  center()
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        AccountCenter center = helpCenterService.center();
        ajaxResult.put("data",center);
        return ajaxResult;
    }

//    /**
//     * 导出帮助中心列表
//     */
////    @PreAuthorize("@ss.hasPermi('system:center:export')")
//    @Log(title = "帮助中心", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, HelpCenter helpCenter)
//    {
//        List<HelpCenter> list = helpCenterService.selectHelpCenterList(helpCenter);
//        ExcelUtil<HelpCenter> util = new ExcelUtil<HelpCenter>(HelpCenter.class);
//        util.exportExcel(response, list, "帮助中心数据");
//    }
//
//    /**
//     * 获取帮助中心详细信息
//     */
////    @PreAuthorize("@ss.hasPermi('system:center:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(helpCenterService.selectHelpCenterById(id));
//    }
//
//    /**
//     * 新增帮助中心
//     */
////    @PreAuthorize("@ss.hasPermi('system:center:add')")
//    @Log(title = "帮助中心", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody HelpCenter helpCenter)
//    {
//        return toAjax(helpCenterService.insertHelpCenter(helpCenter));
//    }
//
//    /**
//     * 修改帮助中心
//     */
////    @PreAuthorize("@ss.hasPermi('system:center:edit')")
//    @Log(title = "帮助中心", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody HelpCenter helpCenter)
//    {
//        return toAjax(helpCenterService.updateHelpCenter(helpCenter));
//    }
//
//    /**
//     * 删除帮助中心
//     */
////    @PreAuthorize("@ss.hasPermi('system:center:remove')")
//    @Log(title = "帮助中心", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(helpCenterService.deleteHelpCenterByIds(ids));
//    }
}
