package com.ruoyi.web.controller.business;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.ExcerptComment;
import com.ruoyi.business.service.IExcerptCommentService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

/**
 * 金句摘抄评论Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/excerpt")
public class BizExcerptCommentController extends BaseController
{
    @Autowired
    private IExcerptCommentService excerptCommentService;

    /**
     * 查询金句摘抄评论列表
     */
//    @PreAuthorize("@ss.hasPermi('system:comment:list')")
    @GetMapping("/comment_list")
    public AjaxResult list(ExcerptComment excerptComment)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        excerptComment.setExcerptId(excerptComment.getId());
//        startPage();
        List<ExcerptComment> list = excerptCommentService.selectExcerptCommentList(excerptComment);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出金句摘抄评论列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:comment:export')")
//    @Log(title = "金句摘抄评论", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ExcerptComment excerptComment)
//    {
//        List<ExcerptComment> list = excerptCommentService.selectExcerptCommentList(excerptComment);
//        ExcelUtil<ExcerptComment> util = new ExcelUtil<ExcerptComment>(ExcerptComment.class);
//        util.exportExcel(response, list, "金句摘抄评论数据");
//    }
//
//    /**
//     * 获取金句摘抄评论详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:comment:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(excerptCommentService.selectExcerptCommentById(id));
//    }

    /**
     * 新增金句摘抄评论
     */
//    @PreAuthorize("@ss.hasPermi('system:comment:add')")
    @Log(title = "金句摘抄评论", businessType = BusinessType.INSERT)
    @PostMapping("comment")
    public AjaxResult add(@RequestBody ExcerptComment excerptComment)
    {
        logger.info("excerpt is {}",JSON.toJSONString(excerptComment));
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        excerptComment.setExcerptId(excerptComment.getId());
        excerptComment.setUserId(SecurityUtils.getUserId());
        excerptComment.setCreatedAt(new Date());
        excerptComment.setUpdatedAt(new Date());
        excerptCommentService.insertExcerptComment(excerptComment);
        return ajaxResult;
    }

//    /**
//     * 修改金句摘抄评论
//     */
//    @PreAuthorize("@ss.hasPermi('system:comment:edit')")
//    @Log(title = "金句摘抄评论", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ExcerptComment excerptComment)
//    {
//        return toAjax(excerptCommentService.updateExcerptComment(excerptComment));
//    }
//
//    /**
//     * 删除金句摘抄评论
//     */
//    @PreAuthorize("@ss.hasPermi('system:comment:remove')")
    @Log(title = "金句摘抄评论", businessType = BusinessType.DELETE)
	@PostMapping("/comment_delete")
    public AjaxResult commentDelete(@RequestBody ExcerptComment excerptComment)
    {
        excerptComment.setUserId(SecurityUtils.getUserId());
        return toAjax(excerptCommentService.deleteExcerptCommentByIds(excerptComment));
    }
}
