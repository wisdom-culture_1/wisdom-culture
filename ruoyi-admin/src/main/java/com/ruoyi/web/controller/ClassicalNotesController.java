package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ClassicalNotes;
import com.ruoyi.system.service.IClassicalNotesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文言文注释Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/notes")
public class ClassicalNotesController extends BaseController
{
    @Autowired
    private IClassicalNotesService classicalNotesService;

    /**
     * 查询文言文注释列表
     */
    @PreAuthorize("@ss.hasPermi('system:notes:list')")
    @GetMapping("/list")
    public TableDataInfo list(ClassicalNotes classicalNotes)
    {
        startPage();
        List<ClassicalNotes> list = classicalNotesService.selectClassicalNotesList(classicalNotes);
        return getDataTable(list);
    }

    /**
     * 导出文言文注释列表
     */
    @PreAuthorize("@ss.hasPermi('system:notes:export')")
    @Log(title = "文言文注释", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ClassicalNotes classicalNotes)
    {
        List<ClassicalNotes> list = classicalNotesService.selectClassicalNotesList(classicalNotes);
        ExcelUtil<ClassicalNotes> util = new ExcelUtil<ClassicalNotes>(ClassicalNotes.class);
        util.exportExcel(response, list, "文言文注释数据");
    }

    /**
     * 获取文言文注释详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:notes:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(classicalNotesService.selectClassicalNotesById(id));
    }

    /**
     * 新增文言文注释
     */
    @PreAuthorize("@ss.hasPermi('system:notes:add')")
    @Log(title = "文言文注释", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ClassicalNotes classicalNotes)
    {
        return toAjax(classicalNotesService.insertClassicalNotes(classicalNotes));
    }

    /**
     * 修改文言文注释
     */
    @PreAuthorize("@ss.hasPermi('system:notes:edit')")
    @Log(title = "文言文注释", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ClassicalNotes classicalNotes)
    {
        return toAjax(classicalNotesService.updateClassicalNotes(classicalNotes));
    }

    /**
     * 删除文言文注释
     */
    @PreAuthorize("@ss.hasPermi('system:notes:remove')")
    @Log(title = "文言文注释", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(classicalNotesService.deleteClassicalNotesByIds(ids));
    }
}
