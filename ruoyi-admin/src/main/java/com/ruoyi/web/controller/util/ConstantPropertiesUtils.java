package com.ruoyi.web.controller.util;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//当项目已启动，Spring接口，spring加载之后，执行接口中的一个方法
@Component
public class ConstantPropertiesUtils implements InitializingBean {

    //读取配置文件内容

    @Value("${oss.oss_endpoint}")
    private String endpoint;

    @Value("${oss.oss_access_id}")
    private String keyId;

    @Value("${oss.oss_access_key}")
    private String keySecret;

    @Value("${oss.oss_bucket}")
    private String bucketName;


    //定义公开的静态的常量
    public static String END_POINT;
    public static String ACCESS_KEY_ID;
    public static String ACCESS_KEY_SECRET;
    public static String BUCKET_NAME;


    @Override
    public void afterPropertiesSet() throws Exception {
        END_POINT = endpoint;
        ACCESS_KEY_ID = keyId;
        ACCESS_KEY_SECRET = keySecret;
        BUCKET_NAME = bucketName;
    }
}