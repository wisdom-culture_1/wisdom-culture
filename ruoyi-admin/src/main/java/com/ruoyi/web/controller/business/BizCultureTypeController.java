package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.CultureType;
import com.ruoyi.business.service.ICultureTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 中华文化分类Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/culture")
public class BizCultureTypeController extends BaseController
{
    @Autowired
    private ICultureTypeService cultureTypeService;

    /**
     * 查询中华文化分类列表
     */
//    @PreAuthorize("@ss.hasPermi('system:type:list')")
    @GetMapping("/type")
    public AjaxResult list(CultureType cultureType)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<CultureType> list = cultureTypeService.selectCultureTypeList(cultureType);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出中华文化分类列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:export')")
//    @Log(title = "中华文化分类", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CultureType cultureType)
//    {
//        List<CultureType> list = cultureTypeService.selectCultureTypeList(cultureType);
//        ExcelUtil<CultureType> util = new ExcelUtil<CultureType>(CultureType.class);
//        util.exportExcel(response, list, "中华文化分类数据");
//    }
//
//    /**
//     * 获取中华文化分类详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(cultureTypeService.selectCultureTypeById(id));
//    }
//
//    /**
//     * 新增中华文化分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:add')")
//    @Log(title = "中华文化分类", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody CultureType cultureType)
//    {
//        return toAjax(cultureTypeService.insertCultureType(cultureType));
//    }
//
//    /**
//     * 修改中华文化分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:edit')")
//    @Log(title = "中华文化分类", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody CultureType cultureType)
//    {
//        return toAjax(cultureTypeService.updateCultureType(cultureType));
//    }
//
//    /**
//     * 删除中华文化分类
//     */
//    @PreAuthorize("@ss.hasPermi('system:type:remove')")
//    @Log(title = "中华文化分类", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(cultureTypeService.deleteCultureTypeByIds(ids));
//    }
}
