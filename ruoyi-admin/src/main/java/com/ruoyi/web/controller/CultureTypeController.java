package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CultureType;
import com.ruoyi.system.service.ICultureTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 中华文化分类Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/culture_type")
public class CultureTypeController extends BaseController
{
    @Autowired
    private ICultureTypeService cultureTypeService;

    /**
     * 查询中华文化分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_type:list')")
    @GetMapping("/list")
    public TableDataInfo list(CultureType cultureType)
    {
        startPage();
        List<CultureType> list = cultureTypeService.selectCultureTypeList(cultureType);
        return getDataTable(list);
    }

    /**
     * 导出中华文化分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:culture_type:export')")
    @Log(title = "中华文化分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CultureType cultureType)
    {
        List<CultureType> list = cultureTypeService.selectCultureTypeList(cultureType);
        ExcelUtil<CultureType> util = new ExcelUtil<CultureType>(CultureType.class);
        util.exportExcel(response, list, "中华文化分类数据");
    }

    /**
     * 获取中华文化分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:culture_type:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cultureTypeService.selectCultureTypeById(id));
    }

    /**
     * 新增中华文化分类
     */
    @PreAuthorize("@ss.hasPermi('system:culture_type:add')")
    @Log(title = "中华文化分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CultureType cultureType)
    {
        return toAjax(cultureTypeService.insertCultureType(cultureType));
    }

    /**
     * 修改中华文化分类
     */
    @PreAuthorize("@ss.hasPermi('system:culture_type:edit')")
    @Log(title = "中华文化分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CultureType cultureType)
    {
        return toAjax(cultureTypeService.updateCultureType(cultureType));
    }

    /**
     * 删除中华文化分类
     */
    @PreAuthorize("@ss.hasPermi('system:culture_type:remove')")
    @Log(title = "中华文化分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cultureTypeService.deleteCultureTypeByIds(ids));
    }
}
