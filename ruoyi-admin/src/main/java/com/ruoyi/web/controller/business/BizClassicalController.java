package com.ruoyi.web.controller.business;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.ruoyi.business.entity.Classical;
import com.ruoyi.business.service.IClassicalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 文言文Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/classical")
public class BizClassicalController extends BaseController
{
    @Autowired
    private IClassicalService classicalService;
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 查询文言文列表
     */
//    @PreAuthorize("@ss.hasPermi('system:classical:list')")
    @GetMapping("/classical_type")
    public AjaxResult list(Classical classical, HttpServletRequest request)
    {
        Date startDate = new Date();

        AjaxResult ajaxResult = AjaxResult.newSuccess();

        startPage();
        List<Classical> list = classicalService.selectClassicalList(classical);
        ajaxResult.put("data",getDataTable(list));

        logger.info("BizClassicalController classical_type cost :{} ms",(new Date().getTime()-startDate.getTime()));

        return ajaxResult;
    }

    @GetMapping("/classical_detail")
    public AjaxResult classicalDetail(Classical classical)
    {
        Date startDate = new Date();
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        Classical result = classicalService.classicalDetail(classical);
        ajaxResult.put("data",result);
        logger.info("BizClassicalController classical_detail cost :{} ms",(new Date().getTime()-startDate.getTime()));
        return ajaxResult;
    }

//
//    /**
//     * 导出文言文列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:classical:export')")
//    @Log(title = "文言文", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, Classical classical)
//    {
//        List<Classical> list = classicalService.selectClassicalList(classical);
//        ExcelUtil<Classical> util = new ExcelUtil<Classical>(Classical.class);
//        util.exportExcel(response, list, "文言文数据");
//    }

//    /**
//     * 获取文言文详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:classical:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(classicalService.selectClassicalById(id));
//    }

//    /**
//     * 新增文言文
//     */
//    @PreAuthorize("@ss.hasPermi('system:classical:add')")
//    @Log(title = "文言文", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody Classical classical)
//    {
//        return toAjax(classicalService.insertClassical(classical));
//    }

//    /**
//     * 修改文言文
//     */
//    @PreAuthorize("@ss.hasPermi('system:classical:edit')")
//    @Log(title = "文言文", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody Classical classical)
//    {
//        return toAjax(classicalService.updateClassical(classical));
//    }

//    /**
//     * 删除文言文
//     */
//    @PreAuthorize("@ss.hasPermi('system:classical:remove')")
//    @Log(title = "文言文", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(classicalService.deleteClassicalByIds(ids));
//    }
}
