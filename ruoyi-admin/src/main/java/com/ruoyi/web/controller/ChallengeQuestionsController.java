package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ChallengeQuestions;
import com.ruoyi.system.service.IChallengeQuestionsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 其他挑战（成语诗词....）Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/challenge_questions")
public class ChallengeQuestionsController extends BaseController
{
    @Autowired
    private IChallengeQuestionsService challengeQuestionsService;

    /**
     * 查询其他挑战（成语诗词....）列表
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_questions:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChallengeQuestions challengeQuestions)
    {
        startPage();
        List<ChallengeQuestions> list = challengeQuestionsService.selectChallengeQuestionsList(challengeQuestions);
        return getDataTable(list);
    }

    /**
     * 导出其他挑战（成语诗词....）列表
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_questions:export')")
    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChallengeQuestions challengeQuestions)
    {
        List<ChallengeQuestions> list = challengeQuestionsService.selectChallengeQuestionsList(challengeQuestions);
        ExcelUtil<ChallengeQuestions> util = new ExcelUtil<ChallengeQuestions>(ChallengeQuestions.class);
        util.exportExcel(response, list, "其他挑战（成语诗词....）数据");
    }

    /**
     * 获取其他挑战（成语诗词....）详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_questions:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(challengeQuestionsService.selectChallengeQuestionsById(id));
    }

    /**
     * 新增其他挑战（成语诗词....）
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_questions:add')")
    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChallengeQuestions challengeQuestions)
    {
        return toAjax(challengeQuestionsService.insertChallengeQuestions(challengeQuestions));
    }

    /**
     * 修改其他挑战（成语诗词....）
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_questions:edit')")
    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChallengeQuestions challengeQuestions)
    {
        return toAjax(challengeQuestionsService.updateChallengeQuestions(challengeQuestions));
    }

    /**
     * 删除其他挑战（成语诗词....）
     */
    @PreAuthorize("@ss.hasPermi('system:challenge_questions:remove')")
    @Log(title = "其他挑战（成语诗词....）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(challengeQuestionsService.deleteChallengeQuestionsByIds(ids));
    }
}
