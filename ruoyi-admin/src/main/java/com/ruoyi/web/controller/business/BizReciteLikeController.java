package com.ruoyi.web.controller.business;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.ReciteDeleteVo;
import com.ruoyi.business.entity.ReciteLike;
import com.ruoyi.business.service.IReciteLikeService;
import com.ruoyi.system.service.impl.ReciteServiceImpl;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 背诵打卡点赞Controller
 * 
 * @author ruoyi
 * @date 2023-04-15
 */
@RestController
@RequestMapping("/recite")
public class BizReciteLikeController extends BaseController
{
    @Autowired
    private IReciteLikeService reciteLikeService;
    @Autowired
    private ReciteServiceImpl reciteService;

    /**
     * 查询背诵打卡点赞列表
     */
    @PreAuthorize("@ss.hasPermi('system:like:list')")
    @GetMapping("/list")
    public TableDataInfo list(ReciteLike reciteLike)
    {
        startPage();
        List<ReciteLike> list = reciteLikeService.selectReciteLikeList(reciteLike);
        return getDataTable(list);
    }

//    /**
//     * 导出背诵打卡点赞列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:export')")
//    @Log(title = "背诵打卡点赞", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ReciteLike reciteLike)
//    {
//        List<ReciteLike> list = reciteLikeService.selectReciteLikeList(reciteLike);
//        ExcelUtil<ReciteLike> util = new ExcelUtil<ReciteLike>(ReciteLike.class);
//        util.exportExcel(response, list, "背诵打卡点赞数据");
//    }
//
//    /**
//     * 获取背诵打卡点赞详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(reciteLikeService.selectReciteLikeById(id));
//    }

    /**
     * 新增背诵打卡点赞
     */
//    @PreAuthorize("@ss.hasPermi('system:like:add')")
    @Log(title = "背诵打卡点赞", businessType = BusinessType.INSERT)
    @PostMapping("like")
    public AjaxResult add(@RequestBody ReciteLike reciteLike)
    {
        logger.info("recite/like reciteLike is {}", JSON.toJSONString(reciteLike));
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        reciteLike.setReciteId(reciteLike.getId());
        reciteLike.setCreatedAt(new Date());
        reciteLike.setUpdatedAt(new Date());
        reciteLikeService.insertReciteLike(reciteLike);
        return ajaxResult;
    }

//    /**
//     * 修改背诵打卡点赞
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:edit')")
//    @Log(title = "背诵打卡点赞", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ReciteLike reciteLike)
//    {
//        return toAjax(reciteLikeService.updateReciteLike(reciteLike));
//    }
//
//    /**
//     * 删除背诵打卡点赞
//     */
//    @PreAuthorize("@ss.hasPermi('system:like:remove')")
//    @Log(title = "背诵打卡点赞", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(reciteLikeService.deleteReciteLikeByIds(ids));
//    }

    @GetMapping("recite_delete/{id}")
    public AjaxResult reciteDelete(@PathVariable Long id){
        return toAjax(reciteService.deleteReciteById(id));
    }
}
