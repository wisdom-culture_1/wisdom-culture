package com.ruoyi.web.controller.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.ZdOrderpy;
import com.ruoyi.business.service.IZdOrderpyService;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizZdOrderpyController extends BaseController
{
    @Autowired
    private IZdOrderpyService zdOrderpyService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:orderpy:list')")
        @GetMapping("/pinyin")
    public AjaxResult list(ZdOrderpy zdOrderpy)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ZdOrderpy> list = zdOrderpyService.selectZdOrderpyList(zdOrderpy);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:orderpy:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ZdOrderpy zdOrderpy)
//    {
//        List<ZdOrderpy> list = zdOrderpyService.selectZdOrderpyList(zdOrderpy);
//        ExcelUtil<ZdOrderpy> util = new ExcelUtil<ZdOrderpy>(ZdOrderpy.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }
//
//    /**
//     * 获取【请填写功能名称】详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:orderpy:query')")
//    @GetMapping(value = "/{head}")
//    public AjaxResult getInfo(@PathVariable("head") String head)
//    {
//        return success(zdOrderpyService.selectZdOrderpyByHead(head));
//    }
//
//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:orderpy:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ZdOrderpy zdOrderpy)
//    {
//        return toAjax(zdOrderpyService.insertZdOrderpy(zdOrderpy));
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:orderpy:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ZdOrderpy zdOrderpy)
//    {
//        return toAjax(zdOrderpyService.updateZdOrderpy(zdOrderpy));
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:orderpy:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{heads}")
//    public AjaxResult remove(@PathVariable String[] heads)
//    {
//        return toAjax(zdOrderpyService.deleteZdOrderpyByHeads(heads));
//    }
}
