package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.ChallengeGroup;
import com.ruoyi.business.service.IChallengeGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 挑战组别Controller
 * 
 * @author ruoyi
 * @date 2023-04-23
 */
@RestController
@RequestMapping("/challenge")
public class BizChallengeGroupController extends BaseController
{
    @Autowired
    private IChallengeGroupService challengeGroupService;

    /**
     * 查询挑战组别列表
     */
//    @PreAuthorize("@ss.hasPermi('system:group:list')")
    @GetMapping("/groups")
    public AjaxResult list(ChallengeGroup challengeGroup)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<ChallengeGroup> list = challengeGroupService.selectChallengeGroupList(challengeGroup);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出挑战组别列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:group:export')")
//    @Log(title = "挑战组别", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ChallengeGroup challengeGroup)
//    {
//        List<ChallengeGroup> list = challengeGroupService.selectChallengeGroupList(challengeGroup);
//        ExcelUtil<ChallengeGroup> util = new ExcelUtil<ChallengeGroup>(ChallengeGroup.class);
//        util.exportExcel(response, list, "挑战组别数据");
//    }
//
//    /**
//     * 获取挑战组别详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:group:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(challengeGroupService.selectChallengeGroupById(id));
//    }
//
//    /**
//     * 新增挑战组别
//     */
//    @PreAuthorize("@ss.hasPermi('system:group:add')")
//    @Log(title = "挑战组别", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ChallengeGroup challengeGroup)
//    {
//        return toAjax(challengeGroupService.insertChallengeGroup(challengeGroup));
//    }
//
//    /**
//     * 修改挑战组别
//     */
//    @PreAuthorize("@ss.hasPermi('system:group:edit')")
//    @Log(title = "挑战组别", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ChallengeGroup challengeGroup)
//    {
//        return toAjax(challengeGroupService.updateChallengeGroup(challengeGroup));
//    }
//
//    /**
//     * 删除挑战组别
//     */
//    @PreAuthorize("@ss.hasPermi('system:group:remove')")
//    @Log(title = "挑战组别", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(challengeGroupService.deleteChallengeGroupByIds(ids));
//    }
}
