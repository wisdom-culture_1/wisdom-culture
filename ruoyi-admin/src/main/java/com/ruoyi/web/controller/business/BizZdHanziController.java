package com.ruoyi.web.controller.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ruoyi.business.entity.ZdPinyin;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.business.entity.ZdHanzi;
import com.ruoyi.business.service.IZdHanziService;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/etymology")
public class BizZdHanziController extends BaseController
{
    @Autowired
    private IZdHanziService zdHanziService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:hanzi:list')")
    @GetMapping("/stroke_character")
    public AjaxResult list(@Param("begin")String begin,@Param("bihua") String bihua)
    {
//        startPage();
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        List<ZdHanzi> list = zdHanziService.selectStrokeCharacter(begin,bihua);

//        // 去重 多音字只出现一个
//        List<Integer> toRemoveIndex = new ArrayList<>();
//        if (list.size() >= 2) {
//            ZdHanzi zdPinyinIndex = list.get(0);
//            for (int i = 1; i < list.size(); i++) {
//                ZdHanzi zdPinyinPointer = list.get(i);
//                if (zdPinyinIndex.getZi().equals(zdPinyinPointer.getZi())) {
//                    toRemoveIndex.add(i);
//                } else {
//                    zdPinyinIndex = zdPinyinPointer;
//                }
//            }
//        }
//
//        // Reverse the indexes before removing to avoid index shifting issues.
//        toRemoveIndex.sort(Collections.reverseOrder());
//        for (Integer index : toRemoveIndex) {
//            list.remove((int)index);
//        }

        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;

    }

//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:hanzi:export')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, ZdHanzi zdHanzi)
//    {
//        List<ZdHanzi> list = zdHanziService.selectZdHanziList(zdHanzi);
//        ExcelUtil<ZdHanzi> util = new ExcelUtil<ZdHanzi>(ZdHanzi.class);
//        util.exportExcel(response, list, "【请填写功能名称】数据");
//    }
//
//    /**
//     * 获取【请填写功能名称】详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:hanzi:query')")
//    @GetMapping(value = "/{refid}")
//    public AjaxResult getInfo(@PathVariable("refid") Long refid)
//    {
//        return success(zdHanziService.selectZdHanziByRefid(refid));
//    }
//
//    /**
//     * 新增【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:hanzi:add')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody ZdHanzi zdHanzi)
//    {
//        return toAjax(zdHanziService.insertZdHanzi(zdHanzi));
//    }
//
//    /**
//     * 修改【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:hanzi:edit')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody ZdHanzi zdHanzi)
//    {
//        return toAjax(zdHanziService.updateZdHanzi(zdHanzi));
//    }
//
//    /**
//     * 删除【请填写功能名称】
//     */
//    @PreAuthorize("@ss.hasPermi('system:hanzi:remove')")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{refids}")
//    public AjaxResult remove(@PathVariable Long[] refids)
//    {
//        return toAjax(zdHanziService.deleteZdHanziByRefids(refids));
//    }
}
