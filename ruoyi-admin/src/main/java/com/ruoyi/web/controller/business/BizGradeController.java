package com.ruoyi.web.controller.business;

import java.util.List;

import com.ruoyi.business.entity.Grade;
import com.ruoyi.business.entity.UserGradeResp;
import com.ruoyi.business.service.IGradeService;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 年级Controller
 * 
 * @author ruoyi
 * @date 2023-04-12
 */
@RestController
@RequestMapping("")
public class BizGradeController extends BaseController
{
    @Autowired
    private IGradeService gradeService;
    @Autowired
    private  SysUserMapper sysUserMapper;

    /**
     * 查询年级列表
     */
//    @PreAuthorize("@ss.hasPermi('system:grade:list')")
    @GetMapping("/account/grade")
    public AjaxResult  list(Grade grade)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
        UserGradeResp resp = new UserGradeResp();
//        startPage();
        List<Grade> list = gradeService.selectGradeList(grade);
        resp.setGrade(list);

        SysUser sysUser = sysUserMapper.selectUserById(SecurityUtils.getUserId());
        resp.setUser_grade(null != sysUser ? sysUser.getGradeId() : null);

        ajaxResult.put("data",resp);
        return ajaxResult;
    }

    /**
     * 查询年级列表
     */
//    @PreAuthorize("@ss.hasPermi('system:grade:list')")
    @GetMapping("/beyond/grade")
    public AjaxResult  grade(Grade grade)
    {
        AjaxResult ajaxResult = AjaxResult.newSuccess();
//        startPage();
        List<Grade> list = gradeService.selectGradeList(grade);
        ajaxResult.put("data",getDataTable(list));
        return ajaxResult;
    }

//    /**
//     * 导出年级列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:grade:export')")
//    @Log(title = "年级", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, Grade grade)
//    {
//        List<Grade> list = gradeService.selectGradeList(grade);
//        ExcelUtil<Grade> util = new ExcelUtil<Grade>(Grade.class);
//        util.exportExcel(response, list, "年级数据");
//    }
//
//    /**
//     * 获取年级详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:grade:query')")
//    @GetMapping(value = "/{id}")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(gradeService.selectGradeById(id));
//    }
//
//    /**
//     * 新增年级
//     */
//    @PreAuthorize("@ss.hasPermi('system:grade:add')")
//    @Log(title = "年级", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody Grade grade)
//    {
//        return toAjax(gradeService.insertGrade(grade));
//    }
//
//    /**
//     * 修改年级
//     */
//    @PreAuthorize("@ss.hasPermi('system:grade:edit')")
//    @Log(title = "年级", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody Grade grade)
//    {
//        return toAjax(gradeService.updateGrade(grade));
//    }
//
//    /**
//     * 删除年级
//     */
//    @PreAuthorize("@ss.hasPermi('system:grade:remove')")
//    @Log(title = "年级", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(gradeService.deleteGradeByIds(ids));
//    }
}
