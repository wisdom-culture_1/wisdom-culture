package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WisdomChapter;
import com.ruoyi.system.service.IWisdomChapterService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 智慧元典章节Controller
 * 
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/chapter")
public class WisdomChapterController extends BaseController
{
    @Autowired
    private IWisdomChapterService wisdomChapterService;

    /**
     * 查询智慧元典章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:list')")
    @GetMapping("/list")
    public TableDataInfo list(WisdomChapter wisdomChapter)
    {
        startPage();
        List<WisdomChapter> list = wisdomChapterService.selectWisdomChapterList(wisdomChapter);
        return getDataTable(list);
    }

    /**
     * 导出智慧元典章节列表
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:export')")
    @Log(title = "智慧元典章节", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WisdomChapter wisdomChapter)
    {
        List<WisdomChapter> list = wisdomChapterService.selectWisdomChapterList(wisdomChapter);
        ExcelUtil<WisdomChapter> util = new ExcelUtil<WisdomChapter>(WisdomChapter.class);
        util.exportExcel(response, list, "智慧元典章节数据");
    }

    /**
     * 获取智慧元典章节详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wisdomChapterService.selectWisdomChapterById(id));
    }

    /**
     * 新增智慧元典章节
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:add')")
    @Log(title = "智慧元典章节", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WisdomChapter wisdomChapter)
    {
        return toAjax(wisdomChapterService.insertWisdomChapter(wisdomChapter));
    }

    /**
     * 修改智慧元典章节
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:edit')")
    @Log(title = "智慧元典章节", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WisdomChapter wisdomChapter)
    {
        return toAjax(wisdomChapterService.updateWisdomChapter(wisdomChapter));
    }

    /**
     * 删除智慧元典章节
     */
    @PreAuthorize("@ss.hasPermi('system:chapter:remove')")
    @Log(title = "智慧元典章节", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wisdomChapterService.deleteWisdomChapterByIds(ids));
    }
}
