package com.ruoyi.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.EtymologyChildren;
import com.ruoyi.system.service.IEtymologyChildrenService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 字源示例Controller
 * 
 * @author ruoyi
 * @date 2023-08-24
 */
@RestController
@RequestMapping("/system/children")
public class EtymologyChildrenController extends BaseController
{
    @Autowired
    private IEtymologyChildrenService etymologyChildrenService;

    /**
     * 查询字源示例列表
     */
    @PreAuthorize("@ss.hasPermi('system:children:list')")
    @GetMapping("/list")
    public TableDataInfo list(EtymologyChildren etymologyChildren)
    {
        startPage();
        List<EtymologyChildren> list = etymologyChildrenService.selectEtymologyChildrenList(etymologyChildren);
        return getDataTable(list);
    }

    /**
     * 导出字源示例列表
     */
    @PreAuthorize("@ss.hasPermi('system:children:export')")
    @Log(title = "字源示例", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EtymologyChildren etymologyChildren)
    {
        List<EtymologyChildren> list = etymologyChildrenService.selectEtymologyChildrenList(etymologyChildren);
        ExcelUtil<EtymologyChildren> util = new ExcelUtil<EtymologyChildren>(EtymologyChildren.class);
        util.exportExcel(response, list, "字源示例数据");
    }

    /**
     * 获取字源示例详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:children:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(etymologyChildrenService.selectEtymologyChildrenById(id));
    }

    /**
     * 新增字源示例
     */
    @PreAuthorize("@ss.hasPermi('system:children:add')")
    @Log(title = "字源示例", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EtymologyChildren etymologyChildren)
    {
        return toAjax(etymologyChildrenService.insertEtymologyChildren(etymologyChildren));
    }

    /**
     * 修改字源示例
     */
    @PreAuthorize("@ss.hasPermi('system:children:edit')")
    @Log(title = "字源示例", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EtymologyChildren etymologyChildren)
    {
        return toAjax(etymologyChildrenService.updateEtymologyChildren(etymologyChildren));
    }

    /**
     * 删除字源示例
     */
    @PreAuthorize("@ss.hasPermi('system:children:remove')")
    @Log(title = "字源示例", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(etymologyChildrenService.deleteEtymologyChildrenByIds(ids));
    }
}
