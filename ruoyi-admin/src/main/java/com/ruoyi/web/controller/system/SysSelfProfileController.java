package com.ruoyi.web.controller.system;

import com.ruoyi.business.entity.Users;
import com.ruoyi.business.service.IUsersService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.ChangePasswdReq;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.web.controller.constants.ErrorCode;
import com.ruoyi.web.controller.constants.RedisKey;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * 个人信息 业务处理
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/account")
@Api(tags="个人信息 业务处理")
public class SysSelfProfileController extends BaseController
{
    @Autowired
    private ISysUserService userService;

    @Autowired
    private IUsersService usersService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 个人信息
     */
    @GetMapping
    public AjaxResult profile()
    {
        LoginUser loginUser = getLoginUser();
        SysUser user = loginUser.getUser();
        AjaxResult ajax = AjaxResult.success(user);
        ajax.put("roleGroup", userService.selectUserRoleGroup(loginUser.getUsername()));
        ajax.put("postGroup", userService.selectUserPostGroup(loginUser.getUsername()));
        return ajax;
    }

    /**
     * 绑定手机号
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PostMapping("/change_username")
    @ApiOperation("绑定手机号")
    public AjaxResult updateProfile(@RequestBody SysUser user)
    {

        LoginUser loginUser = getLoginUser();
        SysUser sysUser = loginUser.getUser();
        user.setUserName(sysUser.getUserName());
        if (StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
        {
            return AjaxResult.newError("绑定用户'" + user.getUserName() + "手机号失败，手机号码已存在");
        }

        // 校验短信验证码
        String cacheKey = String.format(RedisKey.CHECK_CODE_KEY,user.getPhonenumber());
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);

        if(StringUtils.isBlank(cacheValue)){
            AjaxResult ajaxResult = AjaxResult.error("短信验证码已失效，请重新获取！");
            return ajaxResult;
        }

        if(StringUtils.isNotEmpty(cacheValue)){
            String messageCodeCache = cacheValue.split("_")[0];
            if(!user.getMessageCode().equals(messageCodeCache)){
                AjaxResult ajaxResult = AjaxResult.error("短信验证码已失效，请重新获取！");
                return ajaxResult;
            }else{
                redisTemplate.delete(cacheKey);
            }
        }

        user.setUserId(sysUser.getUserId());
        user.setPhonenumber(user.getPhonenumber());
        if (userService.updateUserProfile(user) > 0)
        {
            return AjaxResult.newSuccess();
        }
        return AjaxResult.newError("修改个人信息异常，请联系管理员");
    }

    /**
     * 重置密码
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update_password")
    public AjaxResult updatePwd(@RequestBody ChangePasswdReq req)
    {
        LoginUser loginUser = getLoginUser();
        String userName = loginUser.getUsername();
        String password = loginUser.getPassword();
        if (!SecurityUtils.matchesPassword(req.getOld_password(), password))
        {
            return error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(req.getPassword(), password))
        {
            return error("新密码不能与旧密码相同");
        }
        if(!req.getPassword().equals(req.getPassword_confirmation())){
            return error("两次输入密码不一致");
        }
        String encryptPassword = SecurityUtils.encryptPassword(req.getPassword());
        if (userService.resetUserPwd(userName, encryptPassword) > 0)
        {
            Users users = new Users();
            users.setUsername(userName);
            users.setUpdateBy(getUsername());
            users.setUpdatedAt(new Date());
            users.setPassword(encryptPassword);
            usersService.updateUsersByUserName(users);

            // 更新缓存用户密码
            loginUser.getUser().setPassword(encryptPassword);
            tokenService.setLoginUser(loginUser);
            return success();
        }
        return AjaxResult.newError("修改密码异常，请联系管理员");
    }

    /**
     * 头像上传
     */
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @PostMapping("/avatar")
    public AjaxResult avatar(@RequestParam("avatarfile") MultipartFile file) throws Exception
    {
        if (!file.isEmpty())
        {
            LoginUser loginUser = getLoginUser();
            String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            if (userService.updateUserAvatar(loginUser.getUsername(), avatar))
            {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("imgUrl", avatar);
                // 更新缓存用户头像
                loginUser.getUser().setAvatar(avatar);
                tokenService.setLoginUser(loginUser);
                return ajax;
            }
        }
        return error("上传图片异常，请联系管理员");
    }
}
