package com.ruoyi.web.task;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.business.entity.Users;
import com.ruoyi.business.mapper.BizUsersMapper;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.web.controller.tool.system.SysLoginController;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NewSysUserSyncToOldSysTask {

    private static final Logger log = LoggerFactory.getLogger(NewSysUserSyncToOldSysTask.class);
    @Autowired
    private ISysUserService userService;
    @Autowired
    private BizUsersMapper bizUsersMapper;

    @Autowired
    private SysUserMapper userMapper;

    @Scheduled(fixedRate = 4000) // 每隔5秒执行一次
    public void printCurrentTime() {
        log.info("NewSysUserSyncToOldSysTask start");

        List<Users> idsNotInNewSystem = bizUsersMapper.getIdsNotInNewSystem();
        log.info("NewSysUserSyncToOldSysTask idsNotInNewSystem :{}",JSON.toJSONString(idsNotInNewSystem));
        if(CollectionUtils.isNotEmpty(idsNotInNewSystem)){
            idsNotInNewSystem.forEach(item -> {
                log.info("NewSysUserSyncToOldSysTask insert old id is :{}",item.getId());
                SysUser sysUser = new SysUser();
                sysUser.setUserName(item.getUsername());
                sysUser.setPassword(item.getPassword());
                sysUser.setCode(item.getCode());
                sysUser.setExpirationTime(item.getExpirationTime());
                sysUser.setNickname(item.getUsername());
                if(StringUtils.isNotBlank(item.getNickname())){
                    sysUser.setNickname(item.getNickname());
                }
                sysUser.setChildrenName(item.getChildrenName());
                sysUser.setChildrenNickname(item.getChildrenNickname());
                sysUser.setAddress(item.getAddress());
                sysUser.setPhonenumber(item.getPhone());
                sysUser.setEmail(item.getEmail());
                sysUser.setGradeId(item.getGradeId());
                sysUser.setGreadUad(item.getGreadUad());
                sysUser.setWxImg(item.getWxImg());
                sysUser.setWxName(item.getWxName());
                sysUser.setWxOpenid(item.getWxOpenid());
                sysUser.setCreateTime(item.getCreatedAt());
                sysUser.setUpdateTime(item.getUpdatedAt());
                boolean regFlag = userService.registerUser(sysUser);
                if (!regFlag)
                {
                    log.info("NewSysUserSyncToOldSysTask insert old id is :{},fail",item.getId());
                }else{
                    log.info("NewSysUserSyncToOldSysTask insert old id is :{},success",item.getId());
                }
            });
        }
        log.info("NewSysUserSyncToOldSysTask end");
    }
}
